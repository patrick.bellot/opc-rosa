NOTE. This is the opc-rosa implementation submitted to the following license :

- If distributed, the license of mbedtls library is "Apache 2.0 license"
  as can be found in mbedtls/apache-2.0.txt. mbedtls library can be 
  downloaded from https://tls.mbed.org

- roxml library is under licence LGPL v2.1 slightly modified as can
  be found in opcua/lib/libroxml/License.txt. It is provided by
  Tristan Lelong (tristan.lelong@libroxml.net).  

- the code of the stack under opc-rosa/s2opc is under licence 
  Apache 2.0 which is under opc-rosa/s2opc/apache-2.0.txt. It is due to
  the french company Systerel.
  
- The rest of the code is distributed with the following licence :
 
----------------------------------------------------------------------

This file is part of OPC-ROSA.

OPC-ROSA is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OPC-ROSA is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.

/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

<!--
	This file is part of OPC-ROSA.

	OPC-ROSA is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	OPC-ROSA is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
-->

