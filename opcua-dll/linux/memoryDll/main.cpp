/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib/OpcUa.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/AddressSpace/All.h"
#include "lib/CommonParametersTypes/OpcUa_IPCS_NumericRange.h"
#include "lib/Utils/OpcUa_Clock.h"

extern "C" {



static opcua::Byte * memoryByte ;

int32_t start()
{
	memoryByte = new opcua::Byte((uint8_t)0) ;
	memoryByte->take() ;
	return (int32_t)0 ;
}

int32_t stop()
{
	return (int32_t)0 ;
}

int32_t get(opcua::ExternalVariable  * node,
			opcua::NumericRange      * indexRange,
			opcua::BaseDataType     ** value,
			int64_t                  * microseconds,
			int32_t                  * rc)
{
	node = NULL ;

	if (! indexRange->isNullOrEmpty()) {
		* rc = _Bad_IndexRangeInvalid ;
		return (int32_t)1 ;
	}

	if (memoryByte == NULL) {
		* rc = _Bad_NotReadable ;
		return (int32_t)1 ;
	}

	* value        = memoryByte ;
	* microseconds = opcua::Clock::get_microsecondssince1601() ;
	* rc           = _Good_LocalOverride ;

	return (int32_t)0 ;
}

int32_t put(opcua::ExternalVariable * node,
			opcua::NumericRange     * indexRange,
			opcua::BaseDataType     * value,
			int32_t                 * rc)
{
	node = NULL ;

	if (! indexRange->isNullOrEmpty()) {
		* rc = _Bad_IndexRangeInvalid ;
		return (int32_t)1 ;
	}

	if (value == NULL) {
		* rc = _Bad_NotWritable ;
		return (int32_t)1 ;
	}

	if (value->getTypeId() != OpcUaId_Byte) {
		* rc = _Bad_TypeMismatch ;
		return (int32_t)2 ;
	}

	opcua::Byte * tmpByte = memoryByte ;

	if ((memoryByte = dynamic_cast<opcua::Byte *>(value)))
		value->take() ;

	if (tmpByte != NULL)
		tmpByte->release() ;

	* rc = _Good ;
	return (int32_t)0 ;
}

}
