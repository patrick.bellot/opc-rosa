#include <sopc_endpoint.h>

#if (WITH_DISCOVERY == 1)
extern struct SOPC_ServiceType OpcUa_GetEndpoints_ServiceType ;
#endif
extern struct SOPC_ServiceType OpcUa_ActivateSession_ServiceType ;
#if (WITH_NODESMNGT == 1)
extern struct SOPC_ServiceType OpcUa_AddNodes_ServiceType ;
#endif
#if (WITH_NODESMNGT == 1)
extern struct SOPC_ServiceType OpcUa_AddReferences_ServiceType ;
#endif
#if (WITH_BROWSE == 1)
extern struct SOPC_ServiceType OpcUa_Browse_ServiceType ;
#endif
#if (WITH_CALL == 1)
extern struct SOPC_ServiceType OpcUa_Call_ServiceType ;
#endif
extern struct SOPC_ServiceType OpcUa_CloseSession_ServiceType ;
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_CreateMonitoredItems_ServiceType ;
#endif
extern struct SOPC_ServiceType OpcUa_CreateSession_ServiceType ;
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_CreateSubscription_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_DeleteMonitoredItems_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_DeleteSubscriptions_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_ModifyMonitoredItems_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_ModifySubscription_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_Publish_ServiceType ;
#endif
#if (WITH_READ == 1)
extern struct SOPC_ServiceType OpcUa_Read_ServiceType ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
extern struct SOPC_ServiceType OpcUa_RegisterNodes_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_Republish_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_SetMonitoringMode_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_SetPublishingMode_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern struct SOPC_ServiceType OpcUa_TransferSubscriptions_ServiceType ;
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
extern struct SOPC_ServiceType OpcUa_TranslateBrowsePathsToNodeIds_ServiceType ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
extern struct SOPC_ServiceType OpcUa_UnregisterNodes_ServiceType ;
#endif
#if (WITH_WRITE == 1)
extern struct SOPC_ServiceType OpcUa_Write_ServiceType ;
#endif


#if (WITH_DISCOVERY == 1)
void setOpcUa_GetEndpoints_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_GetEndpoints_ServiceType.InvokeService = f ;
}
#endif

void setOpcUa_ActivateSession_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_ActivateSession_ServiceType.InvokeService = f ;
}

#if (WITH_NODESMNGT == 1)
void setOpcUa_AddNodes_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_AddNodes_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_NODESMNGT == 1)
void setOpcUa_AddReferences_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_AddReferences_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_BROWSE == 1)
void setOpcUa_Browse_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Browse_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_CALL == 1)
void setOpcUa_Call_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Call_ServiceType.InvokeService = f ;
}
#endif

void setOpcUa_CloseSession_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_CloseSession_ServiceType.InvokeService = f ;
}

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_CreateMonitoredItems_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_CreateMonitoredItems_ServiceType.InvokeService = f ;
}
#endif

void setOpcUa_CreateSession_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_CreateSession_ServiceType.InvokeService = f ;
}

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_CreateSubscription_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_CreateSubscription_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_DeleteMonitoredItems_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_DeleteMonitoredItems_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_DeleteSubscriptions_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_DeleteSubscriptions_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_ModifyMonitoredItems_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_ModifyMonitoredItems_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_ModifySubscription_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_ModifySubscription_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_Publish_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Publish_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_READ == 1)
void setOpcUa_Read_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Read_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
void setOpcUa_RegisterNodes_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_RegisterNodes_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_Republish_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Republish_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_SetMonitoringMode_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_SetMonitoringMode_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_SetPublishingMode_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_SetPublishingMode_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_SUBSCRIPTION == 1)
void setOpcUa_TransferSubscriptions_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_TransferSubscriptions_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
void setOpcUa_TranslateBrowsePathsToNodeIds_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_TranslateBrowsePathsToNodeIds_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
void setOpcUa_UnregisterNodes_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_UnregisterNodes_ServiceType.InvokeService = f ;
}
#endif

#if (WITH_WRITE == 1)
void setOpcUa_Write_ServiceType_InvokeService(SOPC_InvokeService * f)
{
   OpcUa_Write_ServiceType.InvokeService = f ;
}
#endif
