public class ImplantedServices
{
    static final public String[] services = {
	"GetEndpoints","WITH_DISCOVERY",
	"ActivateSession",null,
	"AddNodes","WITH_NODESMNGT",
	"AddReferences","WITH_NODESMNGT",
	"Browse","WITH_BROWSE",
	"Call","WITH_CALL",
	"CloseSession",null,
	"CreateMonitoredItems","WITH_SUBSCRIPTION",
	"CreateSession",null,
	"CreateSubscription","WITH_SUBSCRIPTION",
	"DeleteMonitoredItems","WITH_SUBSCRIPTION",
	"DeleteSubscriptions","WITH_SUBSCRIPTION",
	"ModifyMonitoredItems","WITH_SUBSCRIPTION",
	"ModifySubscription","WITH_SUBSCRIPTION",
	"Publish","WITH_SUBSCRIPTION",
	"Read","WITH_READ",
	"RegisterNodes","WITH_REGISTER_UNREGISTER_NODES",
	"Republish","WITH_SUBSCRIPTION",
	"SetMonitoringMode","WITH_SUBSCRIPTION",
	"SetPublishingMode","WITH_SUBSCRIPTION",
	"TransferSubscriptions","WITH_SUBSCRIPTION",
	"TranslateBrowsePathsToNodeIds","WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS",
	"UnregisterNodes","WITH_REGISTER_UNREGISTER_NODES",
	"Write","WITH_WRITE"
    } ;
}
