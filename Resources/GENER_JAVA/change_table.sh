#!/bin/zsh

if (( $# != 1 )) ; then
   echo 'What is $1 ?' ;
   exit 1 ;
fi

echo "Running for $1"
 
perl -pi'.bak' -e"s/Table<$1>/Table$1  /g"                   ~/Desktop/opc-rosa/**/*.{h,cpp,c}  
perl -pi'.bak' -e"s/Table$1FromCtoCpp/Table$1::fromCtoCpp/g" ~/Desktop/opc-rosa/**/*.{h,cpp,c}
perl -pi'.bak' -e"s/fromCpptoC$1/fromCpptoC/g"               ~/Desktop/opc-rosa/**/*.{h,cpp,c}
                    
rm ~/Desktop/opc-rosa/**/*.bak 

echo 'Done'
exit 0 ;


