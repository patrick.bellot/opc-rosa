#ifndef _OPCUA_IPCS_TABLES_H
#define _OPCUA_IPCS_TABLES_H

#include "OpcUa_IPCS_Table.h"

namespace opcua {


class String ;
class MYDLL TableString : public Table
{
public:

    TableString(int32_t _len)
        : Table(_len)
    {}

    virtual TableString * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_String ; }
    String * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfStrings,SOPC_String *& pStrings) const ;
    static TableString * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfStrings,SOPC_String const * pStrings) ;
} ; // class TableString


class NodeId ;
class MYDLL TableNodeId : public Table
{
public:

    TableNodeId(int32_t _len)
        : Table(_len)
    {}

    virtual TableNodeId * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_NodeId ; }
    NodeId * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfNodeIds,SOPC_NodeId *& pNodeIds) const ;
    static TableNodeId * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfNodeIds,SOPC_NodeId const * pNodeIds) ;
} ; // class TableNodeId


class SByte ;
class MYDLL TableSByte : public Table
{
public:

    TableSByte(int32_t _len)
        : Table(_len)
    {}

    virtual TableSByte * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_SByte ; }
    SByte * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSBytes,SOPC_SByte *& pSBytes) const ;
    static TableSByte * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSBytes,SOPC_SByte const * pSBytes) ;
} ; // class TableSByte


class Byte ;
class MYDLL TableByte : public Table
{
public:

    TableByte(int32_t _len)
        : Table(_len)
    {}

    virtual TableByte * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Byte ; }
    Byte * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBytes,SOPC_Byte *& pBytes) const ;
    static TableByte * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBytes,SOPC_Byte const * pBytes) ;
} ; // class TableByte


class DateTime ;
class MYDLL TableDateTime : public Table
{
public:

    TableDateTime(int32_t _len)
        : Table(_len)
    {}

    virtual TableDateTime * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_DateTime ; }
    DateTime * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDateTimes,SOPC_DateTime *& pDateTimes) const ;
    static TableDateTime * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDateTimes,SOPC_DateTime const * pDateTimes) ;
} ; // class TableDateTime


class Guid ;
class MYDLL TableGuid : public Table
{
public:

    TableGuid(int32_t _len)
        : Table(_len)
    {}

    virtual TableGuid * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Guid ; }
    Guid * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfGuids,SOPC_Guid *& pGuids) const ;
    static TableGuid * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfGuids,SOPC_Guid const * pGuids) ;
} ; // class TableGuid


class ByteString ;
class MYDLL TableByteString : public Table
{
public:

    TableByteString(int32_t _len)
        : Table(_len)
    {}

    virtual TableByteString * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ByteString ; }
    ByteString * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfByteStrings,SOPC_ByteString *& pByteStrings) const ;
    static TableByteString * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfByteStrings,SOPC_ByteString const * pByteStrings) ;
} ; // class TableByteString


class ExpandedNodeId ;
class MYDLL TableExpandedNodeId : public Table
{
public:

    TableExpandedNodeId(int32_t _len)
        : Table(_len)
    {}

    virtual TableExpandedNodeId * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ExpandedNodeId ; }
    ExpandedNodeId * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfExpandedNodeIds,SOPC_ExpandedNodeId *& pExpandedNodeIds) const ;
    static TableExpandedNodeId * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfExpandedNodeIds,SOPC_ExpandedNodeId const * pExpandedNodeIds) ;
} ; // class TableExpandedNodeId


class QualifiedName ;
class MYDLL TableQualifiedName : public Table
{
public:

    TableQualifiedName(int32_t _len)
        : Table(_len)
    {}

    virtual TableQualifiedName * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_QualifiedName ; }
    QualifiedName * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfQualifiedNames,SOPC_QualifiedName *& pQualifiedNames) const ;
    static TableQualifiedName * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfQualifiedNames,SOPC_QualifiedName const * pQualifiedNames) ;
} ; // class TableQualifiedName


class LocalizedText ;
class MYDLL TableLocalizedText : public Table
{
public:

    TableLocalizedText(int32_t _len)
        : Table(_len)
    {}

    virtual TableLocalizedText * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_LocalizedText ; }
    LocalizedText * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfLocalizedTexts,SOPC_LocalizedText *& pLocalizedTexts) const ;
    static TableLocalizedText * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfLocalizedTexts,SOPC_LocalizedText const * pLocalizedTexts) ;
} ; // class TableLocalizedText


class ExtensionObject ;
class MYDLL TableExtensionObject : public Table
{
public:

    TableExtensionObject(int32_t _len)
        : Table(_len)
    {}

    virtual TableExtensionObject * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return 0 ; }
    ExtensionObject * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfExtensionObjects,SOPC_ExtensionObject *& pExtensionObjects) const ;
    static TableExtensionObject * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfExtensionObjects,SOPC_ExtensionObject const * pExtensionObjects) ;
} ; // class TableExtensionObject


class Variant ;
class MYDLL TableVariant : public Table
{
public:

    TableVariant(int32_t _len)
        : Table(_len)
    {}

    virtual TableVariant * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return 0 ; }
    Variant * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfVariants,SOPC_Variant *& pVariants) const ;
    static TableVariant * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfVariants,SOPC_Variant const * pVariants) ;
} ; // class TableVariant


class Argument ;
class MYDLL TableArgument : public Table
{
public:

    TableArgument(int32_t _len)
        : Table(_len)
    {}

    virtual TableArgument * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Argument ; }
    Argument * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfArguments,OpcUa_Argument *& pArguments) const ;
    static TableArgument * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfArguments,OpcUa_Argument const * pArguments) ;
} ; // class TableArgument


class DataValue ;
class MYDLL TableDataValue : public Table
{
public:

    TableDataValue(int32_t _len)
        : Table(_len)
    {}

    virtual TableDataValue * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_DataValue ; }
    DataValue * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDataValues,SOPC_DataValue *& pDataValues) const ;
    static TableDataValue * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDataValues,SOPC_DataValue const * pDataValues) ;
} ; // class TableDataValue


class DiagnosticInfo ;
class MYDLL TableDiagnosticInfo : public Table
{
public:

    TableDiagnosticInfo(int32_t _len)
        : Table(_len)
    {}

    virtual TableDiagnosticInfo * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_DiagnosticInfo ; }
    DiagnosticInfo * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDiagnosticInfos,SOPC_DiagnosticInfo *& pDiagnosticInfos) const ;
    static TableDiagnosticInfo * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDiagnosticInfos,SOPC_DiagnosticInfo const * pDiagnosticInfos) ;
} ; // class TableDiagnosticInfo


class StatusCode ;
class MYDLL TableStatusCode : public Table
{
public:

    TableStatusCode(int32_t _len)
        : Table(_len)
    {}

    virtual TableStatusCode * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_StatusCode ; }
    StatusCode * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfStatusCodes,SOPC_StatusCode *& pStatusCodes) const ;
    static TableStatusCode * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfStatusCodes,SOPC_StatusCode const * pStatusCodes) ;
} ; // class TableStatusCode

#if (WITH_NODEMNGT == 1)

class AddNodesItem ;
class MYDLL TableAddNodesItem : public Table
{
public:

    TableAddNodesItem(int32_t _len)
        : Table(_len)
    {}

    virtual TableAddNodesItem * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_AddNodesItem ; }
    AddNodesItem * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddNodesItems,OpcUa_AddNodesItem *& pAddNodesItems) const ;
    static TableAddNodesItem * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddNodesItems,OpcUa_AddNodesItem const * pAddNodesItems) ;
} ; // class TableAddNodesItem

#endif
#if (WITH_NODEMNGT == 1)

class AddNodesResult ;
class MYDLL TableAddNodesResult : public Table
{
public:

    TableAddNodesResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableAddNodesResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_AddNodesResult ; }
    AddNodesResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddNodesResults,OpcUa_AddNodesResult *& pAddNodesResults) const ;
    static TableAddNodesResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddNodesResults,OpcUa_AddNodesResult const * pAddNodesResults) ;
} ; // class TableAddNodesResult

#endif
#if (WITH_NODEMNGT == 1)

class AddReferencesItem ;
class MYDLL TableAddReferencesItem : public Table
{
public:

    TableAddReferencesItem(int32_t _len)
        : Table(_len)
    {}

    virtual TableAddReferencesItem * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_AddReferencesItem ; }
    AddReferencesItem * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddReferencesItems,OpcUa_AddReferencesItem *& pAddReferencesItems) const ;
    static TableAddReferencesItem * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddReferencesItems,OpcUa_AddReferencesItem const * pAddReferencesItems) ;
} ; // class TableAddReferencesItem

#endif
#if (WITH_SUBSCRIPTION == 1)

class MonitoredItemCreateRequest ;
class MYDLL TableMonitoredItemCreateRequest : public Table
{
public:

    TableMonitoredItemCreateRequest(int32_t _len)
        : Table(_len)
    {}

    virtual TableMonitoredItemCreateRequest * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemCreateRequest ; }
    MonitoredItemCreateRequest * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemCreateRequests,OpcUa_MonitoredItemCreateRequest *& pMonitoredItemCreateRequests) const ;
    static TableMonitoredItemCreateRequest * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemCreateRequests,OpcUa_MonitoredItemCreateRequest const * pMonitoredItemCreateRequests) ;
} ; // class TableMonitoredItemCreateRequest

#endif
#if (WITH_SUBSCRIPTION == 1)

class MonitoredItemCreateResult ;
class MYDLL TableMonitoredItemCreateResult : public Table
{
public:

    TableMonitoredItemCreateResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableMonitoredItemCreateResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemCreateResult ; }
    MonitoredItemCreateResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemCreateResults,OpcUa_MonitoredItemCreateResult *& pMonitoredItemCreateResults) const ;
    static TableMonitoredItemCreateResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemCreateResults,OpcUa_MonitoredItemCreateResult const * pMonitoredItemCreateResults) ;
} ; // class TableMonitoredItemCreateResult

#endif
#if (WITH_SUBSCRIPTION == 1)

class MonitoredItemModifyRequest ;
class MYDLL TableMonitoredItemModifyRequest : public Table
{
public:

    TableMonitoredItemModifyRequest(int32_t _len)
        : Table(_len)
    {}

    virtual TableMonitoredItemModifyRequest * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemModifyRequest ; }
    MonitoredItemModifyRequest * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemModifyRequests,OpcUa_MonitoredItemModifyRequest *& pMonitoredItemModifyRequests) const ;
    static TableMonitoredItemModifyRequest * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemModifyRequests,OpcUa_MonitoredItemModifyRequest const * pMonitoredItemModifyRequests) ;
} ; // class TableMonitoredItemModifyRequest

#endif
#if (WITH_SUBSCRIPTION == 1)

class MonitoredItemModifyResult ;
class MYDLL TableMonitoredItemModifyResult : public Table
{
public:

    TableMonitoredItemModifyResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableMonitoredItemModifyResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemModifyResult ; }
    MonitoredItemModifyResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemModifyResults,OpcUa_MonitoredItemModifyResult *& pMonitoredItemModifyResults) const ;
    static TableMonitoredItemModifyResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemModifyResults,OpcUa_MonitoredItemModifyResult const * pMonitoredItemModifyResults) ;
} ; // class TableMonitoredItemModifyResult

#endif
#if (WITH_SUBSCRIPTION == 1)

class TransferResult ;
class MYDLL TableTransferResult : public Table
{
public:

    TableTransferResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableTransferResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_TransferResult ; }
    TransferResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfTransferResults,OpcUa_TransferResult *& pTransferResults) const ;
    static TableTransferResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfTransferResults,OpcUa_TransferResult const * pTransferResults) ;
} ; // class TableTransferResult

#endif
#if (WITH_SUBSCRIPTION == 1)

class SubscriptionAcknowledgement ;
class MYDLL TableSubscriptionAcknowledgement : public Table
{
public:

    TableSubscriptionAcknowledgement(int32_t _len)
        : Table(_len)
    {}

    virtual TableSubscriptionAcknowledgement * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_SubscriptionAcknowledgement ; }
    SubscriptionAcknowledgement * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSubscriptionAcknowledgements,OpcUa_SubscriptionAcknowledgement *& pSubscriptionAcknowledgements) const ;
    static TableSubscriptionAcknowledgement * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSubscriptionAcknowledgements,OpcUa_SubscriptionAcknowledgement const * pSubscriptionAcknowledgements) ;
} ; // class TableSubscriptionAcknowledgement

#endif
#if (WITH_SUBSCRIPTION == 1)

class MonitoredItemNotification ;
class MYDLL TableMonitoredItemNotification : public Table
{
public:

    TableMonitoredItemNotification(int32_t _len)
        : Table(_len)
    {}

    virtual TableMonitoredItemNotification * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemNotification ; }
    MonitoredItemNotification * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemNotifications,OpcUa_MonitoredItemNotification *& pMonitoredItemNotifications) const ;
    static TableMonitoredItemNotification * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemNotifications,OpcUa_MonitoredItemNotification const * pMonitoredItemNotifications) ;
} ; // class TableMonitoredItemNotification

#endif
#if (WITH_SUBSCRIPTION == 1)

class EventFieldList ;
class MYDLL TableEventFieldList : public Table
{
public:

    TableEventFieldList(int32_t _len)
        : Table(_len)
    {}

    virtual TableEventFieldList * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_EventFieldList ; }
    EventFieldList * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEventFieldLists,OpcUa_EventFieldList *& pEventFieldLists) const ;
    static TableEventFieldList * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEventFieldLists,OpcUa_EventFieldList const * pEventFieldLists) ;
} ; // class TableEventFieldList

#endif

class EndpointDescription ;
class MYDLL TableEndpointDescription : public Table
{
public:

    TableEndpointDescription(int32_t _len)
        : Table(_len)
    {}

    virtual TableEndpointDescription * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_EndpointDescription ; }
    EndpointDescription * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEndpointDescriptions,OpcUa_EndpointDescription *& pEndpointDescriptions) const ;
    static TableEndpointDescription * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEndpointDescriptions,OpcUa_EndpointDescription const * pEndpointDescriptions) ;
} ; // class TableEndpointDescription


class SignedSoftwareCertificate ;
class MYDLL TableSignedSoftwareCertificate : public Table
{
public:

    TableSignedSoftwareCertificate(int32_t _len)
        : Table(_len)
    {}

    virtual TableSignedSoftwareCertificate * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_SignedSoftwareCertificate ; }
    SignedSoftwareCertificate * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSignedSoftwareCertificates,OpcUa_SignedSoftwareCertificate *& pSignedSoftwareCertificates) const ;
    static TableSignedSoftwareCertificate * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSignedSoftwareCertificates,OpcUa_SignedSoftwareCertificate const * pSignedSoftwareCertificates) ;
} ; // class TableSignedSoftwareCertificate


class UserTokenPolicy ;
class MYDLL TableUserTokenPolicy : public Table
{
public:

    TableUserTokenPolicy(int32_t _len)
        : Table(_len)
    {}

    virtual TableUserTokenPolicy * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_UserTokenPolicy ; }
    UserTokenPolicy * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUserTokenPolicys,OpcUa_UserTokenPolicy *& pUserTokenPolicys) const ;
    static TableUserTokenPolicy * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUserTokenPolicys,OpcUa_UserTokenPolicy const * pUserTokenPolicys) ;
} ; // class TableUserTokenPolicy

#if (WITH_READ == 1)

class ReadValueId ;
class MYDLL TableReadValueId : public Table
{
public:

    TableReadValueId(int32_t _len)
        : Table(_len)
    {}

    virtual TableReadValueId * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ReadValueId ; }
    ReadValueId * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfReadValueIds,OpcUa_ReadValueId *& pReadValueIds) const ;
    static TableReadValueId * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfReadValueIds,OpcUa_ReadValueId const * pReadValueIds) ;
} ; // class TableReadValueId

#endif
#if (WITH_WRITE == 1)

class WriteValue ;
class MYDLL TableWriteValue : public Table
{
public:

    TableWriteValue(int32_t _len)
        : Table(_len)
    {}

    virtual TableWriteValue * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_WriteValue ; }
    WriteValue * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfWriteValues,OpcUa_WriteValue *& pWriteValues) const ;
    static TableWriteValue * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfWriteValues,OpcUa_WriteValue const * pWriteValues) ;
} ; // class TableWriteValue

#endif
#if (WITH_CALL == 1)

class CallMethodRequest ;
class MYDLL TableCallMethodRequest : public Table
{
public:

    TableCallMethodRequest(int32_t _len)
        : Table(_len)
    {}

    virtual TableCallMethodRequest * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_CallMethodRequest ; }
    CallMethodRequest * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCallMethodRequests,OpcUa_CallMethodRequest *& pCallMethodRequests) const ;
    static TableCallMethodRequest * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCallMethodRequests,OpcUa_CallMethodRequest const * pCallMethodRequests) ;
} ; // class TableCallMethodRequest

#endif
#if (WITH_CALL == 1)

class CallMethodResult ;
class MYDLL TableCallMethodResult : public Table
{
public:

    TableCallMethodResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableCallMethodResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_CallMethodResult ; }
    CallMethodResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCallMethodResults,OpcUa_CallMethodResult *& pCallMethodResults) const ;
    static TableCallMethodResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCallMethodResults,OpcUa_CallMethodResult const * pCallMethodResults) ;
} ; // class TableCallMethodResult

#endif
#if (WITH_BROWSE == 1)

class BrowseDescription ;
class MYDLL TableBrowseDescription : public Table
{
public:

    TableBrowseDescription(int32_t _len)
        : Table(_len)
    {}

    virtual TableBrowseDescription * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_BrowseDescription ; }
    BrowseDescription * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowseDescriptions,OpcUa_BrowseDescription *& pBrowseDescriptions) const ;
    static TableBrowseDescription * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowseDescriptions,OpcUa_BrowseDescription const * pBrowseDescriptions) ;
} ; // class TableBrowseDescription

#endif
#if (WITH_BROWSE == 1)

class BrowseResult ;
class MYDLL TableBrowseResult : public Table
{
public:

    TableBrowseResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableBrowseResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_BrowseResult ; }
    BrowseResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowseResults,OpcUa_BrowseResult *& pBrowseResults) const ;
    static TableBrowseResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowseResults,OpcUa_BrowseResult const * pBrowseResults) ;
} ; // class TableBrowseResult

#endif
#if (WITH_BROWSE == 1)

class ReferenceDescription ;
class MYDLL TableReferenceDescription : public Table
{
public:

    TableReferenceDescription(int32_t _len)
        : Table(_len)
    {}

    virtual TableReferenceDescription * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ReferenceDescription ; }
    ReferenceDescription * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfReferenceDescriptions,OpcUa_ReferenceDescription *& pReferenceDescriptions) const ;
    static TableReferenceDescription * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfReferenceDescriptions,OpcUa_ReferenceDescription const * pReferenceDescriptions) ;
} ; // class TableReferenceDescription

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

class BrowsePath ;
class MYDLL TableBrowsePath : public Table
{
public:

    TableBrowsePath(int32_t _len)
        : Table(_len)
    {}

    virtual TableBrowsePath * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_BrowsePath ; }
    BrowsePath * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePaths,OpcUa_BrowsePath *& pBrowsePaths) const ;
    static TableBrowsePath * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePaths,OpcUa_BrowsePath const * pBrowsePaths) ;
} ; // class TableBrowsePath

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

class RelativePathElement ;
class MYDLL TableRelativePathElement : public Table
{
public:

    TableRelativePathElement(int32_t _len)
        : Table(_len)
    {}

    virtual TableRelativePathElement * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_RelativePathElement ; }
    RelativePathElement * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfRelativePathElements,OpcUa_RelativePathElement *& pRelativePathElements) const ;
    static TableRelativePathElement * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfRelativePathElements,OpcUa_RelativePathElement const * pRelativePathElements) ;
} ; // class TableRelativePathElement

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

class BrowsePathResult ;
class MYDLL TableBrowsePathResult : public Table
{
public:

    TableBrowsePathResult(int32_t _len)
        : Table(_len)
    {}

    virtual TableBrowsePathResult * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_BrowsePathResult ; }
    BrowsePathResult * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePathResults,OpcUa_BrowsePathResult *& pBrowsePathResults) const ;
    static TableBrowsePathResult * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePathResults,OpcUa_BrowsePathResult const * pBrowsePathResults) ;
} ; // class TableBrowsePathResult

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

class BrowsePathTarget ;
class MYDLL TableBrowsePathTarget : public Table
{
public:

    TableBrowsePathTarget(int32_t _len)
        : Table(_len)
    {}

    virtual TableBrowsePathTarget * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_BrowsePathTarget ; }
    BrowsePathTarget * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePathTargets,OpcUa_BrowsePathTarget *& pBrowsePathTargets) const ;
    static TableBrowsePathTarget * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePathTargets,OpcUa_BrowsePathTarget const * pBrowsePathTargets) ;
} ; // class TableBrowsePathTarget

#endif
#if (WITH_QUERY == 1)

class ContentFilterElement ;
class MYDLL TableContentFilterElement : public Table
{
public:

    TableContentFilterElement(int32_t _len)
        : Table(_len)
    {}

    virtual TableContentFilterElement * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ContentFilterElement ; }
    ContentFilterElement * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfContentFilterElements,OpcUa_ContentFilterElement *& pContentFilterElements) const ;
    static TableContentFilterElement * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfContentFilterElements,OpcUa_ContentFilterElement const * pContentFilterElements) ;
} ; // class TableContentFilterElement

#endif
#if (WITH_QUERY == 1)

class QueryDataDescription ;
class MYDLL TableQueryDataDescription : public Table
{
public:

    TableQueryDataDescription(int32_t _len)
        : Table(_len)
    {}

    virtual TableQueryDataDescription * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_QueryDataDescription ; }
    QueryDataDescription * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfQueryDataDescriptions,OpcUa_QueryDataDescription *& pQueryDataDescriptions) const ;
    static TableQueryDataDescription * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfQueryDataDescriptions,OpcUa_QueryDataDescription const * pQueryDataDescriptions) ;
} ; // class TableQueryDataDescription

#endif
#if (WITH_DISCOVERY == 1)

class ApplicationDescription ;
class MYDLL TableApplicationDescription : public Table
{
public:

    TableApplicationDescription(int32_t _len)
        : Table(_len)
    {}

    virtual TableApplicationDescription * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_ApplicationDescription ; }
    ApplicationDescription * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfApplicationDescriptions,OpcUa_ApplicationDescription *& pApplicationDescriptions) const ;
    static TableApplicationDescription * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfApplicationDescriptions,OpcUa_ApplicationDescription const * pApplicationDescriptions) ;
} ; // class TableApplicationDescription

#endif

class Range ;
class MYDLL TableRange : public Table
{
public:

    TableRange(int32_t _len)
        : Table(_len)
    {}

    virtual TableRange * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Range ; }
    Range * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfRanges,OpcUa_Range *& pRanges) const ;
    static TableRange * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfRanges,OpcUa_Range const * pRanges) ;
} ; // class TableRange


class EUInformation ;
class MYDLL TableEUInformation : public Table
{
public:

    TableEUInformation(int32_t _len)
        : Table(_len)
    {}

    virtual TableEUInformation * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_EUInformation ; }
    EUInformation * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEUInformations,OpcUa_EUInformation *& pEUInformations) const ;
    static TableEUInformation * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEUInformations,OpcUa_EUInformation const * pEUInformations) ;
} ; // class TableEUInformation

#if (WITH_QUERY == 1)

class FilterOperand ;
class MYDLL TableFilterOperand : public Table
{
public:

    TableFilterOperand(int32_t _len)
        : Table(_len)
    {}

    virtual TableFilterOperand * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_FilterOperand ; }
    FilterOperand * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfFilterOperands,SOPC_ExtensionObject *& pFilterOperands) const ;
    static TableFilterOperand * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfFilterOperands,SOPC_ExtensionObject const * pFilterOperands) ;
} ; // class TableFilterOperand

#endif

class IntegerId ;
class MYDLL TableIntegerId : public Table
{
public:

    TableIntegerId(int32_t _len)
        : Table(_len)
    {}

    virtual TableIntegerId * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_IntegerId ; }
    IntegerId * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfIntegerIds,uint32_t *& pIntegerIds) const ;
    static TableIntegerId * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfIntegerIds,uint32_t const * pIntegerIds) ;
} ; // class TableIntegerId


class Counter ;
class MYDLL TableCounter : public Table
{
public:

    TableCounter(int32_t _len)
        : Table(_len)
    {}

    virtual TableCounter * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Counter ; }
    Counter * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCounters,uint32_t *& pCounters) const ;
    static TableCounter * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCounters,uint32_t const * pCounters) ;
} ; // class TableCounter

#if (WITH_SUBSCRIPTION == 1)

class NotificationData ;
class MYDLL TableNotificationData : public Table
{
public:

    TableNotificationData(int32_t _len)
        : Table(_len)
    {}

    virtual TableNotificationData * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_NotificationData ; }
    NotificationData * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfNotificationDatas,SOPC_ExtensionObject *& pNotificationDatas) const ;
    static TableNotificationData * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfNotificationDatas,SOPC_ExtensionObject const * pNotificationDatas) ;
} ; // class TableNotificationData

#endif

class LocaleId ;
class MYDLL TableLocaleId : public Table
{
public:

    TableLocaleId(int32_t _len)
        : Table(_len)
    {}

    virtual TableLocaleId * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_LocaleId ; }
    LocaleId * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfLocaleIds,SOPC_String *& pLocaleIds) const ;
    static TableLocaleId * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfLocaleIds,SOPC_String const * pLocaleIds) ;
} ; // class TableLocaleId


class Boolean ;
class MYDLL TableBoolean : public Table
{
public:

    TableBoolean(int32_t _len)
        : Table(_len)
    {}

    virtual TableBoolean * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Boolean ; }
    Boolean * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBooleans,uint8_t *& pBooleans) const ;
    static TableBoolean * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBooleans,uint8_t const * pBooleans) ;
} ; // class TableBoolean


class Int16 ;
class MYDLL TableInt16 : public Table
{
public:

    TableInt16(int32_t _len)
        : Table(_len)
    {}

    virtual TableInt16 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Int16 ; }
    Int16 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt16s,int16_t *& pInt16s) const ;
    static TableInt16 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt16s,int16_t const * pInt16s) ;
} ; // class TableInt16


class UInt16 ;
class MYDLL TableUInt16 : public Table
{
public:

    TableUInt16(int32_t _len)
        : Table(_len)
    {}

    virtual TableUInt16 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_UInt16 ; }
    UInt16 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt16s,uint16_t *& pUInt16s) const ;
    static TableUInt16 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt16s,uint16_t const * pUInt16s) ;
} ; // class TableUInt16


class Int32 ;
class MYDLL TableInt32 : public Table
{
public:

    TableInt32(int32_t _len)
        : Table(_len)
    {}

    virtual TableInt32 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Int32 ; }
    Int32 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt32s,int32_t *& pInt32s) const ;
    static TableInt32 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt32s,int32_t const * pInt32s) ;
} ; // class TableInt32


class UInt32 ;
class MYDLL TableUInt32 : public Table
{
public:

    TableUInt32(int32_t _len)
        : Table(_len)
    {}

    virtual TableUInt32 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_UInt32 ; }
    UInt32 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt32s,uint32_t *& pUInt32s) const ;
    static TableUInt32 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt32s,uint32_t const * pUInt32s) ;
} ; // class TableUInt32


class Int64 ;
class MYDLL TableInt64 : public Table
{
public:

    TableInt64(int32_t _len)
        : Table(_len)
    {}

    virtual TableInt64 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Int64 ; }
    Int64 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt64s,int64_t *& pInt64s) const ;
    static TableInt64 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt64s,int64_t const * pInt64s) ;
} ; // class TableInt64


class UInt64 ;
class MYDLL TableUInt64 : public Table
{
public:

    TableUInt64(int32_t _len)
        : Table(_len)
    {}

    virtual TableUInt64 * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_UInt64 ; }
    UInt64 * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt64s,uint64_t *& pUInt64s) const ;
    static TableUInt64 * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt64s,uint64_t const * pUInt64s) ;
} ; // class TableUInt64


class Float ;
class MYDLL TableFloat : public Table
{
public:

    TableFloat(int32_t _len)
        : Table(_len)
    {}

    virtual TableFloat * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Float ; }
    Float * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfFloats,float *& pFloats) const ;
    static TableFloat * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfFloats,float const * pFloats) ;
} ; // class TableFloat


class Double ;
class MYDLL TableDouble : public Table
{
public:

    TableDouble(int32_t _len)
        : Table(_len)
    {}

    virtual TableDouble * getTable(int n) const ;

    virtual uint32_t getTypeId() const { return OpcUaId_Double ; }
    Double * get(int32_t i) const ;
    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDoubles,double *& pDoubles) const ;
    static TableDouble * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDoubles,double const * pDoubles) ;
} ; // class TableDouble


} //namespace opcua

#endif // _OPCUA_IPCS_TABLES_H
