#include "OpcUa_IPCS_Table.h"

#include "../StandardDataTypes/OpcUa_IPCS_String.h"
#include "../StandardDataTypes/OpcUa_IPCS_NodeId.h"
#include "../StandardDataTypes/OpcUa_IPCS_SByte.h"
#include "../StandardDataTypes/OpcUa_IPCS_Byte.h"
#include "../StandardDataTypes/OpcUa_IPCS_DateTime.h"
#include "../StandardDataTypes/OpcUa_IPCS_Guid.h"
#include "../StandardDataTypes/OpcUa_IPCS_ByteString.h"
#include "../StandardDataTypes/OpcUa_IPCS_ExpandedNodeId.h"
#include "../StandardDataTypes/OpcUa_IPCS_QualifiedName.h"
#include "../StandardDataTypes/OpcUa_IPCS_LocalizedText.h"
#include "../StandardDataTypes/OpcUa_IPCS_ExtensionObject.h"
#include "../StandardDataTypes/OpcUa_IPCS_Variant.h"
#include "../StandardDataTypes/OpcUa_IPCS_Argument.h"
#include "../StandardDataTypes/OpcUa_IPCS_LocaleId.h"
#include "../StandardDataTypes/OpcUa_IPCS_Boolean.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int16.h"
#include "../StandardDataTypes/OpcUa_IPCS_UInt16.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int32.h"
#include "../StandardDataTypes/OpcUa_IPCS_UInt32.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int64.h"
#include "../StandardDataTypes/OpcUa_IPCS_UInt64.h"
#include "../StandardDataTypes/OpcUa_IPCS_Float.h"
#include "../StandardDataTypes/OpcUa_IPCS_Double.h"
#include "../CommonParametersTypes/OpcUa_IPCS_DataValue.h"
#include "../CommonParametersTypes/OpcUa_IPCS_DiagnosticInfo.h"
#include "../CommonParametersTypes/OpcUa_IPCS_StatusCode.h"
#include "../CommonParametersTypes/OpcUa_IPCS_EndpointDescription.h"
#include "../CommonParametersTypes/OpcUa_IPCS_SignedSoftwareCertificate.h"
#include "../CommonParametersTypes/OpcUa_IPCS_UserTokenPolicy.h"
#if (WITH_READ == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_ReadValueId.h"
#endif
#if (WITH_WRITE == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_WriteValue.h"
#endif
#if (WITH_CALL == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_CallMethodRequest.h"
#endif
#if (WITH_CALL == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_CallMethodResult.h"
#endif
#if (WITH_BROWSE == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_BrowseDescription.h"
#endif
#if (WITH_BROWSE == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_BrowseResult.h"
#endif
#if (WITH_BROWSE == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_ReferenceDescription.h"
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_BrowsePath.h"
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_RelativePathElement.h"
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_BrowsePathResult.h"
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_BrowsePathTarget.h"
#endif
#if (WITH_QUERY == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_ContentFilterElement.h"
#endif
#if (WITH_QUERY == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_QueryDataDescription.h"
#endif
#if (WITH_DISCOVERY == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_ApplicationDescription.h"
#endif
#if (WITH_QUERY == 1)
#include "../CommonParametersTypes/OpcUa_IPCS_FilterOperand.h"
#endif
#include "../CommonParametersTypes/OpcUa_IPCS_IntegerId.h"
#include "../CommonParametersTypes/OpcUa_IPCS_Counter.h"
#if (WITH_NODEMNGT == 1)
#include "../NodeManagement/OpcUa_IPCS_AddNodesItem.h"
#endif
#if (WITH_NODEMNGT == 1)
#include "../NodeManagement/OpcUa_IPCS_AddNodesResult.h"
#endif
#if (WITH_NODEMNGT == 1)
#include "../NodeManagement/OpcUa_IPCS_AddReferencesItem.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemCreateRequest.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemCreateResult.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemModifyRequest.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemModifyResult.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_TransferResult.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_SubscriptionAcknowledgement.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemNotification.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_EventFieldList.h"
#endif
#if (WITH_SUBSCRIPTION == 1)
#include "../NotificationsAndEvents/OpcUa_IPCS_NotificationData.h"
#endif
#include "../DataAccess/OpcUa_IPCS_Range.h"
#include "../DataAccess/OpcUa_IPCS_EUInformation.h"

namespace opcua {


MYDLL TableString * TableString::getTable(int n) const { return new TableString(n) ; }

String * TableString::get(int32_t i) const { return dynamic_cast<String *>(tab[i]) ; }

MYDLL void TableString::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfStrings,SOPC_String *& pStrings) const
{
    pNoOfStrings = len ;
    if (len > 0) {
        pStrings = reinterpret_cast<SOPC_String *>(calloc(len, sizeof(SOPC_String))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pStrings[i]) ;
    } else {
        pStrings = NULL ;
    }
}

MYDLL TableString * TableString::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfStrings,SOPC_String const * pStrings)
{
    TableString * res = new TableString(pNoOfStrings) ;
    for (int i = 0 ; i < pNoOfStrings ; i++)
        res->set(i,String::fromCtoCpp(pStatus, pStrings[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableNodeId * TableNodeId::getTable(int n) const { return new TableNodeId(n) ; }

NodeId * TableNodeId::get(int32_t i) const { return dynamic_cast<NodeId *>(tab[i]) ; }

MYDLL void TableNodeId::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfNodeIds,SOPC_NodeId *& pNodeIds) const
{
    pNoOfNodeIds = len ;
    if (len > 0) {
        pNodeIds = reinterpret_cast<SOPC_NodeId *>(calloc(len, sizeof(SOPC_NodeId))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pNodeIds[i]) ;
    } else {
        pNodeIds = NULL ;
    }
}

MYDLL TableNodeId * TableNodeId::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfNodeIds,SOPC_NodeId const * pNodeIds)
{
    TableNodeId * res = new TableNodeId(pNoOfNodeIds) ;
    for (int i = 0 ; i < pNoOfNodeIds ; i++)
        res->set(i,NodeId::fromCtoCpp(pStatus, pNodeIds[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableSByte * TableSByte::getTable(int n) const { return new TableSByte(n) ; }

SByte * TableSByte::get(int32_t i) const { return dynamic_cast<SByte *>(tab[i]) ; }

MYDLL void TableSByte::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSBytes,SOPC_SByte *& pSBytes) const
{
    pNoOfSBytes = len ;
    if (len > 0) {
        pSBytes = reinterpret_cast<SOPC_SByte *>(calloc(len, sizeof(SOPC_SByte))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pSBytes[i]) ;
    } else {
        pSBytes = NULL ;
    }
}

MYDLL TableSByte * TableSByte::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSBytes,SOPC_SByte const * pSBytes)
{
    TableSByte * res = new TableSByte(pNoOfSBytes) ;
    for (int i = 0 ; i < pNoOfSBytes ; i++)
        res->set(i,SByte::fromCtoCpp(pStatus, pSBytes[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableByte * TableByte::getTable(int n) const { return new TableByte(n) ; }

Byte * TableByte::get(int32_t i) const { return dynamic_cast<Byte *>(tab[i]) ; }

MYDLL void TableByte::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBytes,SOPC_Byte *& pBytes) const
{
    pNoOfBytes = len ;
    if (len > 0) {
        pBytes = reinterpret_cast<SOPC_Byte *>(calloc(len, sizeof(SOPC_Byte))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBytes[i]) ;
    } else {
        pBytes = NULL ;
    }
}

MYDLL TableByte * TableByte::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBytes,SOPC_Byte const * pBytes)
{
    TableByte * res = new TableByte(pNoOfBytes) ;
    for (int i = 0 ; i < pNoOfBytes ; i++)
        res->set(i,Byte::fromCtoCpp(pStatus, pBytes[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableDateTime * TableDateTime::getTable(int n) const { return new TableDateTime(n) ; }

DateTime * TableDateTime::get(int32_t i) const { return dynamic_cast<DateTime *>(tab[i]) ; }

MYDLL void TableDateTime::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDateTimes,SOPC_DateTime *& pDateTimes) const
{
    pNoOfDateTimes = len ;
    if (len > 0) {
        pDateTimes = reinterpret_cast<SOPC_DateTime *>(calloc(len, sizeof(SOPC_DateTime))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pDateTimes[i]) ;
    } else {
        pDateTimes = NULL ;
    }
}

MYDLL TableDateTime * TableDateTime::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDateTimes,SOPC_DateTime const * pDateTimes)
{
    TableDateTime * res = new TableDateTime(pNoOfDateTimes) ;
    for (int i = 0 ; i < pNoOfDateTimes ; i++)
        res->set(i,DateTime::fromCtoCpp(pStatus, pDateTimes[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableGuid * TableGuid::getTable(int n) const { return new TableGuid(n) ; }

Guid * TableGuid::get(int32_t i) const { return dynamic_cast<Guid *>(tab[i]) ; }

MYDLL void TableGuid::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfGuids,SOPC_Guid *& pGuids) const
{
    pNoOfGuids = len ;
    if (len > 0) {
        pGuids = reinterpret_cast<SOPC_Guid *>(calloc(len, sizeof(SOPC_Guid))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pGuids[i]) ;
    } else {
        pGuids = NULL ;
    }
}

MYDLL TableGuid * TableGuid::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfGuids,SOPC_Guid const * pGuids)
{
    TableGuid * res = new TableGuid(pNoOfGuids) ;
    for (int i = 0 ; i < pNoOfGuids ; i++)
        res->set(i,Guid::fromCtoCpp(pStatus, pGuids[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableByteString * TableByteString::getTable(int n) const { return new TableByteString(n) ; }

ByteString * TableByteString::get(int32_t i) const { return dynamic_cast<ByteString *>(tab[i]) ; }

MYDLL void TableByteString::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfByteStrings,SOPC_ByteString *& pByteStrings) const
{
    pNoOfByteStrings = len ;
    if (len > 0) {
        pByteStrings = reinterpret_cast<SOPC_ByteString *>(calloc(len, sizeof(SOPC_ByteString))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pByteStrings[i]) ;
    } else {
        pByteStrings = NULL ;
    }
}

MYDLL TableByteString * TableByteString::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfByteStrings,SOPC_ByteString const * pByteStrings)
{
    TableByteString * res = new TableByteString(pNoOfByteStrings) ;
    for (int i = 0 ; i < pNoOfByteStrings ; i++)
        res->set(i,ByteString::fromCtoCpp(pStatus, pByteStrings[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableExpandedNodeId * TableExpandedNodeId::getTable(int n) const { return new TableExpandedNodeId(n) ; }

ExpandedNodeId * TableExpandedNodeId::get(int32_t i) const { return dynamic_cast<ExpandedNodeId *>(tab[i]) ; }

MYDLL void TableExpandedNodeId::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfExpandedNodeIds,SOPC_ExpandedNodeId *& pExpandedNodeIds) const
{
    pNoOfExpandedNodeIds = len ;
    if (len > 0) {
        pExpandedNodeIds = reinterpret_cast<SOPC_ExpandedNodeId *>(calloc(len, sizeof(SOPC_ExpandedNodeId))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pExpandedNodeIds[i]) ;
    } else {
        pExpandedNodeIds = NULL ;
    }
}

MYDLL TableExpandedNodeId * TableExpandedNodeId::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfExpandedNodeIds,SOPC_ExpandedNodeId const * pExpandedNodeIds)
{
    TableExpandedNodeId * res = new TableExpandedNodeId(pNoOfExpandedNodeIds) ;
    for (int i = 0 ; i < pNoOfExpandedNodeIds ; i++)
        res->set(i,ExpandedNodeId::fromCtoCpp(pStatus, pExpandedNodeIds[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableQualifiedName * TableQualifiedName::getTable(int n) const { return new TableQualifiedName(n) ; }

QualifiedName * TableQualifiedName::get(int32_t i) const { return dynamic_cast<QualifiedName *>(tab[i]) ; }

MYDLL void TableQualifiedName::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfQualifiedNames,SOPC_QualifiedName *& pQualifiedNames) const
{
    pNoOfQualifiedNames = len ;
    if (len > 0) {
        pQualifiedNames = reinterpret_cast<SOPC_QualifiedName *>(calloc(len, sizeof(SOPC_QualifiedName))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pQualifiedNames[i]) ;
    } else {
        pQualifiedNames = NULL ;
    }
}

MYDLL TableQualifiedName * TableQualifiedName::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfQualifiedNames,SOPC_QualifiedName const * pQualifiedNames)
{
    TableQualifiedName * res = new TableQualifiedName(pNoOfQualifiedNames) ;
    for (int i = 0 ; i < pNoOfQualifiedNames ; i++)
        res->set(i,QualifiedName::fromCtoCpp(pStatus, pQualifiedNames[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableLocalizedText * TableLocalizedText::getTable(int n) const { return new TableLocalizedText(n) ; }

LocalizedText * TableLocalizedText::get(int32_t i) const { return dynamic_cast<LocalizedText *>(tab[i]) ; }

MYDLL void TableLocalizedText::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfLocalizedTexts,SOPC_LocalizedText *& pLocalizedTexts) const
{
    pNoOfLocalizedTexts = len ;
    if (len > 0) {
        pLocalizedTexts = reinterpret_cast<SOPC_LocalizedText *>(calloc(len, sizeof(SOPC_LocalizedText))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pLocalizedTexts[i]) ;
    } else {
        pLocalizedTexts = NULL ;
    }
}

MYDLL TableLocalizedText * TableLocalizedText::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfLocalizedTexts,SOPC_LocalizedText const * pLocalizedTexts)
{
    TableLocalizedText * res = new TableLocalizedText(pNoOfLocalizedTexts) ;
    for (int i = 0 ; i < pNoOfLocalizedTexts ; i++)
        res->set(i,LocalizedText::fromCtoCpp(pStatus, pLocalizedTexts[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableExtensionObject * TableExtensionObject::getTable(int n) const { return new TableExtensionObject(n) ; }

ExtensionObject * TableExtensionObject::get(int32_t i) const { return dynamic_cast<ExtensionObject *>(tab[i]) ; }

MYDLL void TableExtensionObject::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfExtensionObjects,SOPC_ExtensionObject *& pExtensionObjects) const
{
    pNoOfExtensionObjects = len ;
    if (len > 0) {
        pExtensionObjects = reinterpret_cast<SOPC_ExtensionObject *>(calloc(len, sizeof(SOPC_ExtensionObject))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pExtensionObjects[i]) ;
    } else {
        pExtensionObjects = NULL ;
    }
}

MYDLL TableExtensionObject * TableExtensionObject::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfExtensionObjects,SOPC_ExtensionObject const * pExtensionObjects)
{
    TableExtensionObject * res = new TableExtensionObject(pNoOfExtensionObjects) ;
    for (int i = 0 ; i < pNoOfExtensionObjects ; i++)
        res->set(i,ExtensionObject::fromCtoCpp(pStatus, pExtensionObjects[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableVariant * TableVariant::getTable(int n) const { return new TableVariant(n) ; }

Variant * TableVariant::get(int32_t i) const { return dynamic_cast<Variant *>(tab[i]) ; }

MYDLL void TableVariant::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfVariants,SOPC_Variant *& pVariants) const
{
    pNoOfVariants = len ;
    if (len > 0) {
        pVariants = reinterpret_cast<SOPC_Variant *>(calloc(len, sizeof(SOPC_Variant))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pVariants[i]) ;
    } else {
        pVariants = NULL ;
    }
}

MYDLL TableVariant * TableVariant::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfVariants,SOPC_Variant const * pVariants)
{
    TableVariant * res = new TableVariant(pNoOfVariants) ;
    for (int i = 0 ; i < pNoOfVariants ; i++)
        res->set(i,Variant::fromCtoCpp(pStatus, pVariants[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableArgument * TableArgument::getTable(int n) const { return new TableArgument(n) ; }

Argument * TableArgument::get(int32_t i) const { return dynamic_cast<Argument *>(tab[i]) ; }

MYDLL void TableArgument::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfArguments,OpcUa_Argument *& pArguments) const
{
    pNoOfArguments = len ;
    if (len > 0) {
        pArguments = reinterpret_cast<OpcUa_Argument *>(calloc(len, sizeof(OpcUa_Argument))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pArguments[i]) ;
    } else {
        pArguments = NULL ;
    }
}

MYDLL TableArgument * TableArgument::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfArguments,OpcUa_Argument const * pArguments)
{
    TableArgument * res = new TableArgument(pNoOfArguments) ;
    for (int i = 0 ; i < pNoOfArguments ; i++)
        res->set(i,Argument::fromCtoCpp(pStatus, pArguments[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableDataValue * TableDataValue::getTable(int n) const { return new TableDataValue(n) ; }

DataValue * TableDataValue::get(int32_t i) const { return dynamic_cast<DataValue *>(tab[i]) ; }

MYDLL void TableDataValue::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDataValues,SOPC_DataValue *& pDataValues) const
{
    pNoOfDataValues = len ;
    if (len > 0) {
        pDataValues = reinterpret_cast<SOPC_DataValue *>(calloc(len, sizeof(SOPC_DataValue))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pDataValues[i]) ;
    } else {
        pDataValues = NULL ;
    }
}

MYDLL TableDataValue * TableDataValue::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDataValues,SOPC_DataValue const * pDataValues)
{
    TableDataValue * res = new TableDataValue(pNoOfDataValues) ;
    for (int i = 0 ; i < pNoOfDataValues ; i++)
        res->set(i,DataValue::fromCtoCpp(pStatus, pDataValues[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableDiagnosticInfo * TableDiagnosticInfo::getTable(int n) const { return new TableDiagnosticInfo(n) ; }

DiagnosticInfo * TableDiagnosticInfo::get(int32_t i) const { return dynamic_cast<DiagnosticInfo *>(tab[i]) ; }

MYDLL void TableDiagnosticInfo::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDiagnosticInfos,SOPC_DiagnosticInfo *& pDiagnosticInfos) const
{
    pNoOfDiagnosticInfos = len ;
    if (len > 0) {
        pDiagnosticInfos = reinterpret_cast<SOPC_DiagnosticInfo *>(calloc(len, sizeof(SOPC_DiagnosticInfo))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pDiagnosticInfos[i]) ;
    } else {
        pDiagnosticInfos = NULL ;
    }
}

MYDLL TableDiagnosticInfo * TableDiagnosticInfo::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDiagnosticInfos,SOPC_DiagnosticInfo const * pDiagnosticInfos)
{
    TableDiagnosticInfo * res = new TableDiagnosticInfo(pNoOfDiagnosticInfos) ;
    for (int i = 0 ; i < pNoOfDiagnosticInfos ; i++)
        res->set(i,DiagnosticInfo::fromCtoCpp(pStatus, pDiagnosticInfos[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableStatusCode * TableStatusCode::getTable(int n) const { return new TableStatusCode(n) ; }

StatusCode * TableStatusCode::get(int32_t i) const { return dynamic_cast<StatusCode *>(tab[i]) ; }

MYDLL void TableStatusCode::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfStatusCodes,SOPC_StatusCode *& pStatusCodes) const
{
    pNoOfStatusCodes = len ;
    if (len > 0) {
        pStatusCodes = reinterpret_cast<SOPC_StatusCode *>(calloc(len, sizeof(SOPC_StatusCode))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pStatusCodes[i]) ;
    } else {
        pStatusCodes = NULL ;
    }
}

MYDLL TableStatusCode * TableStatusCode::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfStatusCodes,SOPC_StatusCode const * pStatusCodes)
{
    TableStatusCode * res = new TableStatusCode(pNoOfStatusCodes) ;
    for (int i = 0 ; i < pNoOfStatusCodes ; i++)
        res->set(i,StatusCode::fromCtoCpp(pStatus, pStatusCodes[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#if (WITH_NODEMNGT == 1)

MYDLL TableAddNodesItem * TableAddNodesItem::getTable(int n) const { return new TableAddNodesItem(n) ; }

AddNodesItem * TableAddNodesItem::get(int32_t i) const { return dynamic_cast<AddNodesItem *>(tab[i]) ; }

MYDLL void TableAddNodesItem::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddNodesItems,OpcUa_AddNodesItem *& pAddNodesItems) const
{
    pNoOfAddNodesItems = len ;
    if (len > 0) {
        pAddNodesItems = reinterpret_cast<OpcUa_AddNodesItem *>(calloc(len, sizeof(OpcUa_AddNodesItem))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pAddNodesItems[i]) ;
    } else {
        pAddNodesItems = NULL ;
    }
}

MYDLL TableAddNodesItem * TableAddNodesItem::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddNodesItems,OpcUa_AddNodesItem const * pAddNodesItems)
{
    TableAddNodesItem * res = new TableAddNodesItem(pNoOfAddNodesItems) ;
    for (int i = 0 ; i < pNoOfAddNodesItems ; i++)
        res->set(i,AddNodesItem::fromCtoCpp(pStatus, pAddNodesItems[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_NODEMNGT == 1)

MYDLL TableAddNodesResult * TableAddNodesResult::getTable(int n) const { return new TableAddNodesResult(n) ; }

AddNodesResult * TableAddNodesResult::get(int32_t i) const { return dynamic_cast<AddNodesResult *>(tab[i]) ; }

MYDLL void TableAddNodesResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddNodesResults,OpcUa_AddNodesResult *& pAddNodesResults) const
{
    pNoOfAddNodesResults = len ;
    if (len > 0) {
        pAddNodesResults = reinterpret_cast<OpcUa_AddNodesResult *>(calloc(len, sizeof(OpcUa_AddNodesResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pAddNodesResults[i]) ;
    } else {
        pAddNodesResults = NULL ;
    }
}

MYDLL TableAddNodesResult * TableAddNodesResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddNodesResults,OpcUa_AddNodesResult const * pAddNodesResults)
{
    TableAddNodesResult * res = new TableAddNodesResult(pNoOfAddNodesResults) ;
    for (int i = 0 ; i < pNoOfAddNodesResults ; i++)
        res->set(i,AddNodesResult::fromCtoCpp(pStatus, pAddNodesResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_NODEMNGT == 1)

MYDLL TableAddReferencesItem * TableAddReferencesItem::getTable(int n) const { return new TableAddReferencesItem(n) ; }

AddReferencesItem * TableAddReferencesItem::get(int32_t i) const { return dynamic_cast<AddReferencesItem *>(tab[i]) ; }

MYDLL void TableAddReferencesItem::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfAddReferencesItems,OpcUa_AddReferencesItem *& pAddReferencesItems) const
{
    pNoOfAddReferencesItems = len ;
    if (len > 0) {
        pAddReferencesItems = reinterpret_cast<OpcUa_AddReferencesItem *>(calloc(len, sizeof(OpcUa_AddReferencesItem))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pAddReferencesItems[i]) ;
    } else {
        pAddReferencesItems = NULL ;
    }
}

MYDLL TableAddReferencesItem * TableAddReferencesItem::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfAddReferencesItems,OpcUa_AddReferencesItem const * pAddReferencesItems)
{
    TableAddReferencesItem * res = new TableAddReferencesItem(pNoOfAddReferencesItems) ;
    for (int i = 0 ; i < pNoOfAddReferencesItems ; i++)
        res->set(i,AddReferencesItem::fromCtoCpp(pStatus, pAddReferencesItems[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableMonitoredItemCreateRequest * TableMonitoredItemCreateRequest::getTable(int n) const { return new TableMonitoredItemCreateRequest(n) ; }

MonitoredItemCreateRequest * TableMonitoredItemCreateRequest::get(int32_t i) const { return dynamic_cast<MonitoredItemCreateRequest *>(tab[i]) ; }

MYDLL void TableMonitoredItemCreateRequest::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemCreateRequests,OpcUa_MonitoredItemCreateRequest *& pMonitoredItemCreateRequests) const
{
    pNoOfMonitoredItemCreateRequests = len ;
    if (len > 0) {
        pMonitoredItemCreateRequests = reinterpret_cast<OpcUa_MonitoredItemCreateRequest *>(calloc(len, sizeof(OpcUa_MonitoredItemCreateRequest))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pMonitoredItemCreateRequests[i]) ;
    } else {
        pMonitoredItemCreateRequests = NULL ;
    }
}

MYDLL TableMonitoredItemCreateRequest * TableMonitoredItemCreateRequest::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemCreateRequests,OpcUa_MonitoredItemCreateRequest const * pMonitoredItemCreateRequests)
{
    TableMonitoredItemCreateRequest * res = new TableMonitoredItemCreateRequest(pNoOfMonitoredItemCreateRequests) ;
    for (int i = 0 ; i < pNoOfMonitoredItemCreateRequests ; i++)
        res->set(i,MonitoredItemCreateRequest::fromCtoCpp(pStatus, pMonitoredItemCreateRequests[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableMonitoredItemCreateResult * TableMonitoredItemCreateResult::getTable(int n) const { return new TableMonitoredItemCreateResult(n) ; }

MonitoredItemCreateResult * TableMonitoredItemCreateResult::get(int32_t i) const { return dynamic_cast<MonitoredItemCreateResult *>(tab[i]) ; }

MYDLL void TableMonitoredItemCreateResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemCreateResults,OpcUa_MonitoredItemCreateResult *& pMonitoredItemCreateResults) const
{
    pNoOfMonitoredItemCreateResults = len ;
    if (len > 0) {
        pMonitoredItemCreateResults = reinterpret_cast<OpcUa_MonitoredItemCreateResult *>(calloc(len, sizeof(OpcUa_MonitoredItemCreateResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pMonitoredItemCreateResults[i]) ;
    } else {
        pMonitoredItemCreateResults = NULL ;
    }
}

MYDLL TableMonitoredItemCreateResult * TableMonitoredItemCreateResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemCreateResults,OpcUa_MonitoredItemCreateResult const * pMonitoredItemCreateResults)
{
    TableMonitoredItemCreateResult * res = new TableMonitoredItemCreateResult(pNoOfMonitoredItemCreateResults) ;
    for (int i = 0 ; i < pNoOfMonitoredItemCreateResults ; i++)
        res->set(i,MonitoredItemCreateResult::fromCtoCpp(pStatus, pMonitoredItemCreateResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableMonitoredItemModifyRequest * TableMonitoredItemModifyRequest::getTable(int n) const { return new TableMonitoredItemModifyRequest(n) ; }

MonitoredItemModifyRequest * TableMonitoredItemModifyRequest::get(int32_t i) const { return dynamic_cast<MonitoredItemModifyRequest *>(tab[i]) ; }

MYDLL void TableMonitoredItemModifyRequest::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemModifyRequests,OpcUa_MonitoredItemModifyRequest *& pMonitoredItemModifyRequests) const
{
    pNoOfMonitoredItemModifyRequests = len ;
    if (len > 0) {
        pMonitoredItemModifyRequests = reinterpret_cast<OpcUa_MonitoredItemModifyRequest *>(calloc(len, sizeof(OpcUa_MonitoredItemModifyRequest))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pMonitoredItemModifyRequests[i]) ;
    } else {
        pMonitoredItemModifyRequests = NULL ;
    }
}

MYDLL TableMonitoredItemModifyRequest * TableMonitoredItemModifyRequest::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemModifyRequests,OpcUa_MonitoredItemModifyRequest const * pMonitoredItemModifyRequests)
{
    TableMonitoredItemModifyRequest * res = new TableMonitoredItemModifyRequest(pNoOfMonitoredItemModifyRequests) ;
    for (int i = 0 ; i < pNoOfMonitoredItemModifyRequests ; i++)
        res->set(i,MonitoredItemModifyRequest::fromCtoCpp(pStatus, pMonitoredItemModifyRequests[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableMonitoredItemModifyResult * TableMonitoredItemModifyResult::getTable(int n) const { return new TableMonitoredItemModifyResult(n) ; }

MonitoredItemModifyResult * TableMonitoredItemModifyResult::get(int32_t i) const { return dynamic_cast<MonitoredItemModifyResult *>(tab[i]) ; }

MYDLL void TableMonitoredItemModifyResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemModifyResults,OpcUa_MonitoredItemModifyResult *& pMonitoredItemModifyResults) const
{
    pNoOfMonitoredItemModifyResults = len ;
    if (len > 0) {
        pMonitoredItemModifyResults = reinterpret_cast<OpcUa_MonitoredItemModifyResult *>(calloc(len, sizeof(OpcUa_MonitoredItemModifyResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pMonitoredItemModifyResults[i]) ;
    } else {
        pMonitoredItemModifyResults = NULL ;
    }
}

MYDLL TableMonitoredItemModifyResult * TableMonitoredItemModifyResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemModifyResults,OpcUa_MonitoredItemModifyResult const * pMonitoredItemModifyResults)
{
    TableMonitoredItemModifyResult * res = new TableMonitoredItemModifyResult(pNoOfMonitoredItemModifyResults) ;
    for (int i = 0 ; i < pNoOfMonitoredItemModifyResults ; i++)
        res->set(i,MonitoredItemModifyResult::fromCtoCpp(pStatus, pMonitoredItemModifyResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableTransferResult * TableTransferResult::getTable(int n) const { return new TableTransferResult(n) ; }

TransferResult * TableTransferResult::get(int32_t i) const { return dynamic_cast<TransferResult *>(tab[i]) ; }

MYDLL void TableTransferResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfTransferResults,OpcUa_TransferResult *& pTransferResults) const
{
    pNoOfTransferResults = len ;
    if (len > 0) {
        pTransferResults = reinterpret_cast<OpcUa_TransferResult *>(calloc(len, sizeof(OpcUa_TransferResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pTransferResults[i]) ;
    } else {
        pTransferResults = NULL ;
    }
}

MYDLL TableTransferResult * TableTransferResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfTransferResults,OpcUa_TransferResult const * pTransferResults)
{
    TableTransferResult * res = new TableTransferResult(pNoOfTransferResults) ;
    for (int i = 0 ; i < pNoOfTransferResults ; i++)
        res->set(i,TransferResult::fromCtoCpp(pStatus, pTransferResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableSubscriptionAcknowledgement * TableSubscriptionAcknowledgement::getTable(int n) const { return new TableSubscriptionAcknowledgement(n) ; }

SubscriptionAcknowledgement * TableSubscriptionAcknowledgement::get(int32_t i) const { return dynamic_cast<SubscriptionAcknowledgement *>(tab[i]) ; }

MYDLL void TableSubscriptionAcknowledgement::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSubscriptionAcknowledgements,OpcUa_SubscriptionAcknowledgement *& pSubscriptionAcknowledgements) const
{
    pNoOfSubscriptionAcknowledgements = len ;
    if (len > 0) {
        pSubscriptionAcknowledgements = reinterpret_cast<OpcUa_SubscriptionAcknowledgement *>(calloc(len, sizeof(OpcUa_SubscriptionAcknowledgement))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pSubscriptionAcknowledgements[i]) ;
    } else {
        pSubscriptionAcknowledgements = NULL ;
    }
}

MYDLL TableSubscriptionAcknowledgement * TableSubscriptionAcknowledgement::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSubscriptionAcknowledgements,OpcUa_SubscriptionAcknowledgement const * pSubscriptionAcknowledgements)
{
    TableSubscriptionAcknowledgement * res = new TableSubscriptionAcknowledgement(pNoOfSubscriptionAcknowledgements) ;
    for (int i = 0 ; i < pNoOfSubscriptionAcknowledgements ; i++)
        res->set(i,SubscriptionAcknowledgement::fromCtoCpp(pStatus, pSubscriptionAcknowledgements[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableMonitoredItemNotification * TableMonitoredItemNotification::getTable(int n) const { return new TableMonitoredItemNotification(n) ; }

MonitoredItemNotification * TableMonitoredItemNotification::get(int32_t i) const { return dynamic_cast<MonitoredItemNotification *>(tab[i]) ; }

MYDLL void TableMonitoredItemNotification::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfMonitoredItemNotifications,OpcUa_MonitoredItemNotification *& pMonitoredItemNotifications) const
{
    pNoOfMonitoredItemNotifications = len ;
    if (len > 0) {
        pMonitoredItemNotifications = reinterpret_cast<OpcUa_MonitoredItemNotification *>(calloc(len, sizeof(OpcUa_MonitoredItemNotification))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pMonitoredItemNotifications[i]) ;
    } else {
        pMonitoredItemNotifications = NULL ;
    }
}

MYDLL TableMonitoredItemNotification * TableMonitoredItemNotification::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfMonitoredItemNotifications,OpcUa_MonitoredItemNotification const * pMonitoredItemNotifications)
{
    TableMonitoredItemNotification * res = new TableMonitoredItemNotification(pNoOfMonitoredItemNotifications) ;
    for (int i = 0 ; i < pNoOfMonitoredItemNotifications ; i++)
        res->set(i,MonitoredItemNotification::fromCtoCpp(pStatus, pMonitoredItemNotifications[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_SUBSCRIPTION == 1)

MYDLL TableEventFieldList * TableEventFieldList::getTable(int n) const { return new TableEventFieldList(n) ; }

EventFieldList * TableEventFieldList::get(int32_t i) const { return dynamic_cast<EventFieldList *>(tab[i]) ; }

MYDLL void TableEventFieldList::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEventFieldLists,OpcUa_EventFieldList *& pEventFieldLists) const
{
    pNoOfEventFieldLists = len ;
    if (len > 0) {
        pEventFieldLists = reinterpret_cast<OpcUa_EventFieldList *>(calloc(len, sizeof(OpcUa_EventFieldList))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pEventFieldLists[i]) ;
    } else {
        pEventFieldLists = NULL ;
    }
}

MYDLL TableEventFieldList * TableEventFieldList::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEventFieldLists,OpcUa_EventFieldList const * pEventFieldLists)
{
    TableEventFieldList * res = new TableEventFieldList(pNoOfEventFieldLists) ;
    for (int i = 0 ; i < pNoOfEventFieldLists ; i++)
        res->set(i,EventFieldList::fromCtoCpp(pStatus, pEventFieldLists[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif

MYDLL TableEndpointDescription * TableEndpointDescription::getTable(int n) const { return new TableEndpointDescription(n) ; }

EndpointDescription * TableEndpointDescription::get(int32_t i) const { return dynamic_cast<EndpointDescription *>(tab[i]) ; }

MYDLL void TableEndpointDescription::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEndpointDescriptions,OpcUa_EndpointDescription *& pEndpointDescriptions) const
{
    pNoOfEndpointDescriptions = len ;
    if (len > 0) {
        pEndpointDescriptions = reinterpret_cast<OpcUa_EndpointDescription *>(calloc(len, sizeof(OpcUa_EndpointDescription))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pEndpointDescriptions[i]) ;
    } else {
        pEndpointDescriptions = NULL ;
    }
}

MYDLL TableEndpointDescription * TableEndpointDescription::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEndpointDescriptions,OpcUa_EndpointDescription const * pEndpointDescriptions)
{
    TableEndpointDescription * res = new TableEndpointDescription(pNoOfEndpointDescriptions) ;
    for (int i = 0 ; i < pNoOfEndpointDescriptions ; i++)
        res->set(i,EndpointDescription::fromCtoCpp(pStatus, pEndpointDescriptions[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableSignedSoftwareCertificate * TableSignedSoftwareCertificate::getTable(int n) const { return new TableSignedSoftwareCertificate(n) ; }

SignedSoftwareCertificate * TableSignedSoftwareCertificate::get(int32_t i) const { return dynamic_cast<SignedSoftwareCertificate *>(tab[i]) ; }

MYDLL void TableSignedSoftwareCertificate::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfSignedSoftwareCertificates,OpcUa_SignedSoftwareCertificate *& pSignedSoftwareCertificates) const
{
    pNoOfSignedSoftwareCertificates = len ;
    if (len > 0) {
        pSignedSoftwareCertificates = reinterpret_cast<OpcUa_SignedSoftwareCertificate *>(calloc(len, sizeof(OpcUa_SignedSoftwareCertificate))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pSignedSoftwareCertificates[i]) ;
    } else {
        pSignedSoftwareCertificates = NULL ;
    }
}

MYDLL TableSignedSoftwareCertificate * TableSignedSoftwareCertificate::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfSignedSoftwareCertificates,OpcUa_SignedSoftwareCertificate const * pSignedSoftwareCertificates)
{
    TableSignedSoftwareCertificate * res = new TableSignedSoftwareCertificate(pNoOfSignedSoftwareCertificates) ;
    for (int i = 0 ; i < pNoOfSignedSoftwareCertificates ; i++)
        res->set(i,SignedSoftwareCertificate::fromCtoCpp(pStatus, pSignedSoftwareCertificates[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableUserTokenPolicy * TableUserTokenPolicy::getTable(int n) const { return new TableUserTokenPolicy(n) ; }

UserTokenPolicy * TableUserTokenPolicy::get(int32_t i) const { return dynamic_cast<UserTokenPolicy *>(tab[i]) ; }

MYDLL void TableUserTokenPolicy::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUserTokenPolicys,OpcUa_UserTokenPolicy *& pUserTokenPolicys) const
{
    pNoOfUserTokenPolicys = len ;
    if (len > 0) {
        pUserTokenPolicys = reinterpret_cast<OpcUa_UserTokenPolicy *>(calloc(len, sizeof(OpcUa_UserTokenPolicy))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pUserTokenPolicys[i]) ;
    } else {
        pUserTokenPolicys = NULL ;
    }
}

MYDLL TableUserTokenPolicy * TableUserTokenPolicy::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUserTokenPolicys,OpcUa_UserTokenPolicy const * pUserTokenPolicys)
{
    TableUserTokenPolicy * res = new TableUserTokenPolicy(pNoOfUserTokenPolicys) ;
    for (int i = 0 ; i < pNoOfUserTokenPolicys ; i++)
        res->set(i,UserTokenPolicy::fromCtoCpp(pStatus, pUserTokenPolicys[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#if (WITH_READ == 1)

MYDLL TableReadValueId * TableReadValueId::getTable(int n) const { return new TableReadValueId(n) ; }

ReadValueId * TableReadValueId::get(int32_t i) const { return dynamic_cast<ReadValueId *>(tab[i]) ; }

MYDLL void TableReadValueId::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfReadValueIds,OpcUa_ReadValueId *& pReadValueIds) const
{
    pNoOfReadValueIds = len ;
    if (len > 0) {
        pReadValueIds = reinterpret_cast<OpcUa_ReadValueId *>(calloc(len, sizeof(OpcUa_ReadValueId))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pReadValueIds[i]) ;
    } else {
        pReadValueIds = NULL ;
    }
}

MYDLL TableReadValueId * TableReadValueId::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfReadValueIds,OpcUa_ReadValueId const * pReadValueIds)
{
    TableReadValueId * res = new TableReadValueId(pNoOfReadValueIds) ;
    for (int i = 0 ; i < pNoOfReadValueIds ; i++)
        res->set(i,ReadValueId::fromCtoCpp(pStatus, pReadValueIds[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_WRITE == 1)

MYDLL TableWriteValue * TableWriteValue::getTable(int n) const { return new TableWriteValue(n) ; }

WriteValue * TableWriteValue::get(int32_t i) const { return dynamic_cast<WriteValue *>(tab[i]) ; }

MYDLL void TableWriteValue::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfWriteValues,OpcUa_WriteValue *& pWriteValues) const
{
    pNoOfWriteValues = len ;
    if (len > 0) {
        pWriteValues = reinterpret_cast<OpcUa_WriteValue *>(calloc(len, sizeof(OpcUa_WriteValue))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pWriteValues[i]) ;
    } else {
        pWriteValues = NULL ;
    }
}

MYDLL TableWriteValue * TableWriteValue::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfWriteValues,OpcUa_WriteValue const * pWriteValues)
{
    TableWriteValue * res = new TableWriteValue(pNoOfWriteValues) ;
    for (int i = 0 ; i < pNoOfWriteValues ; i++)
        res->set(i,WriteValue::fromCtoCpp(pStatus, pWriteValues[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_CALL == 1)

MYDLL TableCallMethodRequest * TableCallMethodRequest::getTable(int n) const { return new TableCallMethodRequest(n) ; }

CallMethodRequest * TableCallMethodRequest::get(int32_t i) const { return dynamic_cast<CallMethodRequest *>(tab[i]) ; }

MYDLL void TableCallMethodRequest::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCallMethodRequests,OpcUa_CallMethodRequest *& pCallMethodRequests) const
{
    pNoOfCallMethodRequests = len ;
    if (len > 0) {
        pCallMethodRequests = reinterpret_cast<OpcUa_CallMethodRequest *>(calloc(len, sizeof(OpcUa_CallMethodRequest))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pCallMethodRequests[i]) ;
    } else {
        pCallMethodRequests = NULL ;
    }
}

MYDLL TableCallMethodRequest * TableCallMethodRequest::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCallMethodRequests,OpcUa_CallMethodRequest const * pCallMethodRequests)
{
    TableCallMethodRequest * res = new TableCallMethodRequest(pNoOfCallMethodRequests) ;
    for (int i = 0 ; i < pNoOfCallMethodRequests ; i++)
        res->set(i,CallMethodRequest::fromCtoCpp(pStatus, pCallMethodRequests[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_CALL == 1)

MYDLL TableCallMethodResult * TableCallMethodResult::getTable(int n) const { return new TableCallMethodResult(n) ; }

CallMethodResult * TableCallMethodResult::get(int32_t i) const { return dynamic_cast<CallMethodResult *>(tab[i]) ; }

MYDLL void TableCallMethodResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCallMethodResults,OpcUa_CallMethodResult *& pCallMethodResults) const
{
    pNoOfCallMethodResults = len ;
    if (len > 0) {
        pCallMethodResults = reinterpret_cast<OpcUa_CallMethodResult *>(calloc(len, sizeof(OpcUa_CallMethodResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pCallMethodResults[i]) ;
    } else {
        pCallMethodResults = NULL ;
    }
}

MYDLL TableCallMethodResult * TableCallMethodResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCallMethodResults,OpcUa_CallMethodResult const * pCallMethodResults)
{
    TableCallMethodResult * res = new TableCallMethodResult(pNoOfCallMethodResults) ;
    for (int i = 0 ; i < pNoOfCallMethodResults ; i++)
        res->set(i,CallMethodResult::fromCtoCpp(pStatus, pCallMethodResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_BROWSE == 1)

MYDLL TableBrowseDescription * TableBrowseDescription::getTable(int n) const { return new TableBrowseDescription(n) ; }

BrowseDescription * TableBrowseDescription::get(int32_t i) const { return dynamic_cast<BrowseDescription *>(tab[i]) ; }

MYDLL void TableBrowseDescription::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowseDescriptions,OpcUa_BrowseDescription *& pBrowseDescriptions) const
{
    pNoOfBrowseDescriptions = len ;
    if (len > 0) {
        pBrowseDescriptions = reinterpret_cast<OpcUa_BrowseDescription *>(calloc(len, sizeof(OpcUa_BrowseDescription))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBrowseDescriptions[i]) ;
    } else {
        pBrowseDescriptions = NULL ;
    }
}

MYDLL TableBrowseDescription * TableBrowseDescription::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowseDescriptions,OpcUa_BrowseDescription const * pBrowseDescriptions)
{
    TableBrowseDescription * res = new TableBrowseDescription(pNoOfBrowseDescriptions) ;
    for (int i = 0 ; i < pNoOfBrowseDescriptions ; i++)
        res->set(i,BrowseDescription::fromCtoCpp(pStatus, pBrowseDescriptions[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_BROWSE == 1)

MYDLL TableBrowseResult * TableBrowseResult::getTable(int n) const { return new TableBrowseResult(n) ; }

BrowseResult * TableBrowseResult::get(int32_t i) const { return dynamic_cast<BrowseResult *>(tab[i]) ; }

MYDLL void TableBrowseResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowseResults,OpcUa_BrowseResult *& pBrowseResults) const
{
    pNoOfBrowseResults = len ;
    if (len > 0) {
        pBrowseResults = reinterpret_cast<OpcUa_BrowseResult *>(calloc(len, sizeof(OpcUa_BrowseResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBrowseResults[i]) ;
    } else {
        pBrowseResults = NULL ;
    }
}

MYDLL TableBrowseResult * TableBrowseResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowseResults,OpcUa_BrowseResult const * pBrowseResults)
{
    TableBrowseResult * res = new TableBrowseResult(pNoOfBrowseResults) ;
    for (int i = 0 ; i < pNoOfBrowseResults ; i++)
        res->set(i,BrowseResult::fromCtoCpp(pStatus, pBrowseResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_BROWSE == 1)

MYDLL TableReferenceDescription * TableReferenceDescription::getTable(int n) const { return new TableReferenceDescription(n) ; }

ReferenceDescription * TableReferenceDescription::get(int32_t i) const { return dynamic_cast<ReferenceDescription *>(tab[i]) ; }

MYDLL void TableReferenceDescription::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfReferenceDescriptions,OpcUa_ReferenceDescription *& pReferenceDescriptions) const
{
    pNoOfReferenceDescriptions = len ;
    if (len > 0) {
        pReferenceDescriptions = reinterpret_cast<OpcUa_ReferenceDescription *>(calloc(len, sizeof(OpcUa_ReferenceDescription))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pReferenceDescriptions[i]) ;
    } else {
        pReferenceDescriptions = NULL ;
    }
}

MYDLL TableReferenceDescription * TableReferenceDescription::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfReferenceDescriptions,OpcUa_ReferenceDescription const * pReferenceDescriptions)
{
    TableReferenceDescription * res = new TableReferenceDescription(pNoOfReferenceDescriptions) ;
    for (int i = 0 ; i < pNoOfReferenceDescriptions ; i++)
        res->set(i,ReferenceDescription::fromCtoCpp(pStatus, pReferenceDescriptions[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

MYDLL TableBrowsePath * TableBrowsePath::getTable(int n) const { return new TableBrowsePath(n) ; }

BrowsePath * TableBrowsePath::get(int32_t i) const { return dynamic_cast<BrowsePath *>(tab[i]) ; }

MYDLL void TableBrowsePath::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePaths,OpcUa_BrowsePath *& pBrowsePaths) const
{
    pNoOfBrowsePaths = len ;
    if (len > 0) {
        pBrowsePaths = reinterpret_cast<OpcUa_BrowsePath *>(calloc(len, sizeof(OpcUa_BrowsePath))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBrowsePaths[i]) ;
    } else {
        pBrowsePaths = NULL ;
    }
}

MYDLL TableBrowsePath * TableBrowsePath::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePaths,OpcUa_BrowsePath const * pBrowsePaths)
{
    TableBrowsePath * res = new TableBrowsePath(pNoOfBrowsePaths) ;
    for (int i = 0 ; i < pNoOfBrowsePaths ; i++)
        res->set(i,BrowsePath::fromCtoCpp(pStatus, pBrowsePaths[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

MYDLL TableRelativePathElement * TableRelativePathElement::getTable(int n) const { return new TableRelativePathElement(n) ; }

RelativePathElement * TableRelativePathElement::get(int32_t i) const { return dynamic_cast<RelativePathElement *>(tab[i]) ; }

MYDLL void TableRelativePathElement::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfRelativePathElements,OpcUa_RelativePathElement *& pRelativePathElements) const
{
    pNoOfRelativePathElements = len ;
    if (len > 0) {
        pRelativePathElements = reinterpret_cast<OpcUa_RelativePathElement *>(calloc(len, sizeof(OpcUa_RelativePathElement))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pRelativePathElements[i]) ;
    } else {
        pRelativePathElements = NULL ;
    }
}

MYDLL TableRelativePathElement * TableRelativePathElement::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfRelativePathElements,OpcUa_RelativePathElement const * pRelativePathElements)
{
    TableRelativePathElement * res = new TableRelativePathElement(pNoOfRelativePathElements) ;
    for (int i = 0 ; i < pNoOfRelativePathElements ; i++)
        res->set(i,RelativePathElement::fromCtoCpp(pStatus, pRelativePathElements[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

MYDLL TableBrowsePathResult * TableBrowsePathResult::getTable(int n) const { return new TableBrowsePathResult(n) ; }

BrowsePathResult * TableBrowsePathResult::get(int32_t i) const { return dynamic_cast<BrowsePathResult *>(tab[i]) ; }

MYDLL void TableBrowsePathResult::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePathResults,OpcUa_BrowsePathResult *& pBrowsePathResults) const
{
    pNoOfBrowsePathResults = len ;
    if (len > 0) {
        pBrowsePathResults = reinterpret_cast<OpcUa_BrowsePathResult *>(calloc(len, sizeof(OpcUa_BrowsePathResult))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBrowsePathResults[i]) ;
    } else {
        pBrowsePathResults = NULL ;
    }
}

MYDLL TableBrowsePathResult * TableBrowsePathResult::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePathResults,OpcUa_BrowsePathResult const * pBrowsePathResults)
{
    TableBrowsePathResult * res = new TableBrowsePathResult(pNoOfBrowsePathResults) ;
    for (int i = 0 ; i < pNoOfBrowsePathResults ; i++)
        res->set(i,BrowsePathResult::fromCtoCpp(pStatus, pBrowsePathResults[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

MYDLL TableBrowsePathTarget * TableBrowsePathTarget::getTable(int n) const { return new TableBrowsePathTarget(n) ; }

BrowsePathTarget * TableBrowsePathTarget::get(int32_t i) const { return dynamic_cast<BrowsePathTarget *>(tab[i]) ; }

MYDLL void TableBrowsePathTarget::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBrowsePathTargets,OpcUa_BrowsePathTarget *& pBrowsePathTargets) const
{
    pNoOfBrowsePathTargets = len ;
    if (len > 0) {
        pBrowsePathTargets = reinterpret_cast<OpcUa_BrowsePathTarget *>(calloc(len, sizeof(OpcUa_BrowsePathTarget))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBrowsePathTargets[i]) ;
    } else {
        pBrowsePathTargets = NULL ;
    }
}

MYDLL TableBrowsePathTarget * TableBrowsePathTarget::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBrowsePathTargets,OpcUa_BrowsePathTarget const * pBrowsePathTargets)
{
    TableBrowsePathTarget * res = new TableBrowsePathTarget(pNoOfBrowsePathTargets) ;
    for (int i = 0 ; i < pNoOfBrowsePathTargets ; i++)
        res->set(i,BrowsePathTarget::fromCtoCpp(pStatus, pBrowsePathTargets[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_QUERY == 1)

MYDLL TableContentFilterElement * TableContentFilterElement::getTable(int n) const { return new TableContentFilterElement(n) ; }

ContentFilterElement * TableContentFilterElement::get(int32_t i) const { return dynamic_cast<ContentFilterElement *>(tab[i]) ; }

MYDLL void TableContentFilterElement::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfContentFilterElements,OpcUa_ContentFilterElement *& pContentFilterElements) const
{
    pNoOfContentFilterElements = len ;
    if (len > 0) {
        pContentFilterElements = reinterpret_cast<OpcUa_ContentFilterElement *>(calloc(len, sizeof(OpcUa_ContentFilterElement))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pContentFilterElements[i]) ;
    } else {
        pContentFilterElements = NULL ;
    }
}

MYDLL TableContentFilterElement * TableContentFilterElement::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfContentFilterElements,OpcUa_ContentFilterElement const * pContentFilterElements)
{
    TableContentFilterElement * res = new TableContentFilterElement(pNoOfContentFilterElements) ;
    for (int i = 0 ; i < pNoOfContentFilterElements ; i++)
        res->set(i,ContentFilterElement::fromCtoCpp(pStatus, pContentFilterElements[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_QUERY == 1)

MYDLL TableQueryDataDescription * TableQueryDataDescription::getTable(int n) const { return new TableQueryDataDescription(n) ; }

QueryDataDescription * TableQueryDataDescription::get(int32_t i) const { return dynamic_cast<QueryDataDescription *>(tab[i]) ; }

MYDLL void TableQueryDataDescription::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfQueryDataDescriptions,OpcUa_QueryDataDescription *& pQueryDataDescriptions) const
{
    pNoOfQueryDataDescriptions = len ;
    if (len > 0) {
        pQueryDataDescriptions = reinterpret_cast<OpcUa_QueryDataDescription *>(calloc(len, sizeof(OpcUa_QueryDataDescription))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pQueryDataDescriptions[i]) ;
    } else {
        pQueryDataDescriptions = NULL ;
    }
}

MYDLL TableQueryDataDescription * TableQueryDataDescription::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfQueryDataDescriptions,OpcUa_QueryDataDescription const * pQueryDataDescriptions)
{
    TableQueryDataDescription * res = new TableQueryDataDescription(pNoOfQueryDataDescriptions) ;
    for (int i = 0 ; i < pNoOfQueryDataDescriptions ; i++)
        res->set(i,QueryDataDescription::fromCtoCpp(pStatus, pQueryDataDescriptions[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif
#if (WITH_DISCOVERY == 1)

MYDLL TableApplicationDescription * TableApplicationDescription::getTable(int n) const { return new TableApplicationDescription(n) ; }

ApplicationDescription * TableApplicationDescription::get(int32_t i) const { return dynamic_cast<ApplicationDescription *>(tab[i]) ; }

MYDLL void TableApplicationDescription::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfApplicationDescriptions,OpcUa_ApplicationDescription *& pApplicationDescriptions) const
{
    pNoOfApplicationDescriptions = len ;
    if (len > 0) {
        pApplicationDescriptions = reinterpret_cast<OpcUa_ApplicationDescription *>(calloc(len, sizeof(OpcUa_ApplicationDescription))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pApplicationDescriptions[i]) ;
    } else {
        pApplicationDescriptions = NULL ;
    }
}

MYDLL TableApplicationDescription * TableApplicationDescription::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfApplicationDescriptions,OpcUa_ApplicationDescription const * pApplicationDescriptions)
{
    TableApplicationDescription * res = new TableApplicationDescription(pNoOfApplicationDescriptions) ;
    for (int i = 0 ; i < pNoOfApplicationDescriptions ; i++)
        res->set(i,ApplicationDescription::fromCtoCpp(pStatus, pApplicationDescriptions[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif

MYDLL TableRange * TableRange::getTable(int n) const { return new TableRange(n) ; }

Range * TableRange::get(int32_t i) const { return dynamic_cast<Range *>(tab[i]) ; }

MYDLL void TableRange::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfRanges,OpcUa_Range *& pRanges) const
{
    pNoOfRanges = len ;
    if (len > 0) {
        pRanges = reinterpret_cast<OpcUa_Range *>(calloc(len, sizeof(OpcUa_Range))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pRanges[i]) ;
    } else {
        pRanges = NULL ;
    }
}

MYDLL TableRange * TableRange::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfRanges,OpcUa_Range const * pRanges)
{
    TableRange * res = new TableRange(pNoOfRanges) ;
    for (int i = 0 ; i < pNoOfRanges ; i++)
        res->set(i,Range::fromCtoCpp(pStatus, pRanges[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableEUInformation * TableEUInformation::getTable(int n) const { return new TableEUInformation(n) ; }

EUInformation * TableEUInformation::get(int32_t i) const { return dynamic_cast<EUInformation *>(tab[i]) ; }

MYDLL void TableEUInformation::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfEUInformations,OpcUa_EUInformation *& pEUInformations) const
{
    pNoOfEUInformations = len ;
    if (len > 0) {
        pEUInformations = reinterpret_cast<OpcUa_EUInformation *>(calloc(len, sizeof(OpcUa_EUInformation))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pEUInformations[i]) ;
    } else {
        pEUInformations = NULL ;
    }
}

MYDLL TableEUInformation * TableEUInformation::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfEUInformations,OpcUa_EUInformation const * pEUInformations)
{
    TableEUInformation * res = new TableEUInformation(pNoOfEUInformations) ;
    for (int i = 0 ; i < pNoOfEUInformations ; i++)
        res->set(i,EUInformation::fromCtoCpp(pStatus, pEUInformations[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#if (WITH_QUERY == 1)

MYDLL TableFilterOperand * TableFilterOperand::getTable(int n) const { return new TableFilterOperand(n) ; }

FilterOperand * TableFilterOperand::get(int32_t i) const { return dynamic_cast<FilterOperand *>(tab[i]) ; }

MYDLL void TableFilterOperand::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfFilterOperands,SOPC_ExtensionObject *& pFilterOperands) const
{
    pNoOfFilterOperands = len ;
    if (len > 0) {
        pFilterOperands = reinterpret_cast<SOPC_ExtensionObject *>(calloc(len, sizeof(SOPC_ExtensionObject))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pFilterOperands[i]) ;
    } else {
        pFilterOperands = NULL ;
    }
}

MYDLL TableFilterOperand * TableFilterOperand::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfFilterOperands,SOPC_ExtensionObject const * pFilterOperands)
{
    TableFilterOperand * res = new TableFilterOperand(pNoOfFilterOperands) ;
    for (int i = 0 ; i < pNoOfFilterOperands ; i++)
        res->set(i,FilterOperand::fromCtoCpp(pStatus, pFilterOperands[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif

MYDLL TableIntegerId * TableIntegerId::getTable(int n) const { return new TableIntegerId(n) ; }

IntegerId * TableIntegerId::get(int32_t i) const { return dynamic_cast<IntegerId *>(tab[i]) ; }

MYDLL void TableIntegerId::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfIntegerIds,uint32_t *& pIntegerIds) const
{
    pNoOfIntegerIds = len ;
    if (len > 0) {
        pIntegerIds = reinterpret_cast<uint32_t *>(calloc(len, sizeof(uint32_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pIntegerIds[i]) ;
    } else {
        pIntegerIds = NULL ;
    }
}

MYDLL TableIntegerId * TableIntegerId::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfIntegerIds,uint32_t const * pIntegerIds)
{
    TableIntegerId * res = new TableIntegerId(pNoOfIntegerIds) ;
    for (int i = 0 ; i < pNoOfIntegerIds ; i++)
        res->set(i,IntegerId::fromCtoCpp(pStatus, pIntegerIds[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableCounter * TableCounter::getTable(int n) const { return new TableCounter(n) ; }

Counter * TableCounter::get(int32_t i) const { return dynamic_cast<Counter *>(tab[i]) ; }

MYDLL void TableCounter::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfCounters,uint32_t *& pCounters) const
{
    pNoOfCounters = len ;
    if (len > 0) {
        pCounters = reinterpret_cast<uint32_t *>(calloc(len, sizeof(uint32_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pCounters[i]) ;
    } else {
        pCounters = NULL ;
    }
}

MYDLL TableCounter * TableCounter::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfCounters,uint32_t const * pCounters)
{
    TableCounter * res = new TableCounter(pNoOfCounters) ;
    for (int i = 0 ; i < pNoOfCounters ; i++)
        res->set(i,Counter::fromCtoCpp(pStatus, pCounters[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#if (WITH_SUBSCRIPTION == 1)

MYDLL TableNotificationData * TableNotificationData::getTable(int n) const { return new TableNotificationData(n) ; }

NotificationData * TableNotificationData::get(int32_t i) const { return dynamic_cast<NotificationData *>(tab[i]) ; }

MYDLL void TableNotificationData::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfNotificationDatas,SOPC_ExtensionObject *& pNotificationDatas) const
{
    pNoOfNotificationDatas = len ;
    if (len > 0) {
        pNotificationDatas = reinterpret_cast<SOPC_ExtensionObject *>(calloc(len, sizeof(SOPC_ExtensionObject))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pNotificationDatas[i]) ;
    } else {
        pNotificationDatas = NULL ;
    }
}

MYDLL TableNotificationData * TableNotificationData::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfNotificationDatas,SOPC_ExtensionObject const * pNotificationDatas)
{
    TableNotificationData * res = new TableNotificationData(pNoOfNotificationDatas) ;
    for (int i = 0 ; i < pNoOfNotificationDatas ; i++)
        res->set(i,NotificationData::fromCtoCpp(pStatus, pNotificationDatas[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}

#endif

MYDLL TableLocaleId * TableLocaleId::getTable(int n) const { return new TableLocaleId(n) ; }

LocaleId * TableLocaleId::get(int32_t i) const { return dynamic_cast<LocaleId *>(tab[i]) ; }

MYDLL void TableLocaleId::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfLocaleIds,SOPC_String *& pLocaleIds) const
{
    pNoOfLocaleIds = len ;
    if (len > 0) {
        pLocaleIds = reinterpret_cast<SOPC_String *>(calloc(len, sizeof(SOPC_String))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pLocaleIds[i]) ;
    } else {
        pLocaleIds = NULL ;
    }
}

MYDLL TableLocaleId * TableLocaleId::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfLocaleIds,SOPC_String const * pLocaleIds)
{
    TableLocaleId * res = new TableLocaleId(pNoOfLocaleIds) ;
    for (int i = 0 ; i < pNoOfLocaleIds ; i++)
        res->set(i,LocaleId::fromCtoCpp(pStatus, pLocaleIds[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableBoolean * TableBoolean::getTable(int n) const { return new TableBoolean(n) ; }

Boolean * TableBoolean::get(int32_t i) const { return dynamic_cast<Boolean *>(tab[i]) ; }

MYDLL void TableBoolean::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfBooleans,uint8_t *& pBooleans) const
{
    pNoOfBooleans = len ;
    if (len > 0) {
        pBooleans = reinterpret_cast<uint8_t *>(calloc(len, sizeof(uint8_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pBooleans[i]) ;
    } else {
        pBooleans = NULL ;
    }
}

MYDLL TableBoolean * TableBoolean::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfBooleans,uint8_t const * pBooleans)
{
    TableBoolean * res = new TableBoolean(pNoOfBooleans) ;
    for (int i = 0 ; i < pNoOfBooleans ; i++)
        res->set(i,Boolean::fromCtoCpp(pStatus, pBooleans[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableInt16 * TableInt16::getTable(int n) const { return new TableInt16(n) ; }

Int16 * TableInt16::get(int32_t i) const { return dynamic_cast<Int16 *>(tab[i]) ; }

MYDLL void TableInt16::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt16s,int16_t *& pInt16s) const
{
    pNoOfInt16s = len ;
    if (len > 0) {
        pInt16s = reinterpret_cast<int16_t *>(calloc(len, sizeof(int16_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pInt16s[i]) ;
    } else {
        pInt16s = NULL ;
    }
}

MYDLL TableInt16 * TableInt16::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt16s,int16_t const * pInt16s)
{
    TableInt16 * res = new TableInt16(pNoOfInt16s) ;
    for (int i = 0 ; i < pNoOfInt16s ; i++)
        res->set(i,Int16::fromCtoCpp(pStatus, pInt16s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableUInt16 * TableUInt16::getTable(int n) const { return new TableUInt16(n) ; }

UInt16 * TableUInt16::get(int32_t i) const { return dynamic_cast<UInt16 *>(tab[i]) ; }

MYDLL void TableUInt16::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt16s,uint16_t *& pUInt16s) const
{
    pNoOfUInt16s = len ;
    if (len > 0) {
        pUInt16s = reinterpret_cast<uint16_t *>(calloc(len, sizeof(uint16_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pUInt16s[i]) ;
    } else {
        pUInt16s = NULL ;
    }
}

MYDLL TableUInt16 * TableUInt16::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt16s,uint16_t const * pUInt16s)
{
    TableUInt16 * res = new TableUInt16(pNoOfUInt16s) ;
    for (int i = 0 ; i < pNoOfUInt16s ; i++)
        res->set(i,UInt16::fromCtoCpp(pStatus, pUInt16s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableInt32 * TableInt32::getTable(int n) const { return new TableInt32(n) ; }

Int32 * TableInt32::get(int32_t i) const { return dynamic_cast<Int32 *>(tab[i]) ; }

MYDLL void TableInt32::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt32s,int32_t *& pInt32s) const
{
    pNoOfInt32s = len ;
    if (len > 0) {
        pInt32s = reinterpret_cast<int32_t *>(calloc(len, sizeof(int32_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pInt32s[i]) ;
    } else {
        pInt32s = NULL ;
    }
}

MYDLL TableInt32 * TableInt32::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt32s,int32_t const * pInt32s)
{
    TableInt32 * res = new TableInt32(pNoOfInt32s) ;
    for (int i = 0 ; i < pNoOfInt32s ; i++)
        res->set(i,Int32::fromCtoCpp(pStatus, pInt32s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableUInt32 * TableUInt32::getTable(int n) const { return new TableUInt32(n) ; }

UInt32 * TableUInt32::get(int32_t i) const { return dynamic_cast<UInt32 *>(tab[i]) ; }

MYDLL void TableUInt32::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt32s,uint32_t *& pUInt32s) const
{
    pNoOfUInt32s = len ;
    if (len > 0) {
        pUInt32s = reinterpret_cast<uint32_t *>(calloc(len, sizeof(uint32_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pUInt32s[i]) ;
    } else {
        pUInt32s = NULL ;
    }
}

MYDLL TableUInt32 * TableUInt32::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt32s,uint32_t const * pUInt32s)
{
    TableUInt32 * res = new TableUInt32(pNoOfUInt32s) ;
    for (int i = 0 ; i < pNoOfUInt32s ; i++)
        res->set(i,UInt32::fromCtoCpp(pStatus, pUInt32s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableInt64 * TableInt64::getTable(int n) const { return new TableInt64(n) ; }

Int64 * TableInt64::get(int32_t i) const { return dynamic_cast<Int64 *>(tab[i]) ; }

MYDLL void TableInt64::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfInt64s,int64_t *& pInt64s) const
{
    pNoOfInt64s = len ;
    if (len > 0) {
        pInt64s = reinterpret_cast<int64_t *>(calloc(len, sizeof(int64_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pInt64s[i]) ;
    } else {
        pInt64s = NULL ;
    }
}

MYDLL TableInt64 * TableInt64::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfInt64s,int64_t const * pInt64s)
{
    TableInt64 * res = new TableInt64(pNoOfInt64s) ;
    for (int i = 0 ; i < pNoOfInt64s ; i++)
        res->set(i,Int64::fromCtoCpp(pStatus, pInt64s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableUInt64 * TableUInt64::getTable(int n) const { return new TableUInt64(n) ; }

UInt64 * TableUInt64::get(int32_t i) const { return dynamic_cast<UInt64 *>(tab[i]) ; }

MYDLL void TableUInt64::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfUInt64s,uint64_t *& pUInt64s) const
{
    pNoOfUInt64s = len ;
    if (len > 0) {
        pUInt64s = reinterpret_cast<uint64_t *>(calloc(len, sizeof(uint64_t))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pUInt64s[i]) ;
    } else {
        pUInt64s = NULL ;
    }
}

MYDLL TableUInt64 * TableUInt64::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfUInt64s,uint64_t const * pUInt64s)
{
    TableUInt64 * res = new TableUInt64(pNoOfUInt64s) ;
    for (int i = 0 ; i < pNoOfUInt64s ; i++)
        res->set(i,UInt64::fromCtoCpp(pStatus, pUInt64s[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableFloat * TableFloat::getTable(int n) const { return new TableFloat(n) ; }

Float * TableFloat::get(int32_t i) const { return dynamic_cast<Float *>(tab[i]) ; }

MYDLL void TableFloat::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfFloats,float *& pFloats) const
{
    pNoOfFloats = len ;
    if (len > 0) {
        pFloats = reinterpret_cast<float *>(calloc(len, sizeof(float))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pFloats[i]) ;
    } else {
        pFloats = NULL ;
    }
}

MYDLL TableFloat * TableFloat::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfFloats,float const * pFloats)
{
    TableFloat * res = new TableFloat(pNoOfFloats) ;
    for (int i = 0 ; i < pNoOfFloats ; i++)
        res->set(i,Float::fromCtoCpp(pStatus, pFloats[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


MYDLL TableDouble * TableDouble::getTable(int n) const { return new TableDouble(n) ; }

Double * TableDouble::get(int32_t i) const { return dynamic_cast<Double *>(tab[i]) ; }

MYDLL void TableDouble::fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOfDoubles,double *& pDoubles) const
{
    pNoOfDoubles = len ;
    if (len > 0) {
        pDoubles = reinterpret_cast<double *>(calloc(len, sizeof(double))) ;
        for (int i = 0 ; i < len ; i++)
            get(i)->fromCpptoC(pStatus, pDoubles[i]) ;
    } else {
        pDoubles = NULL ;
    }
}

MYDLL TableDouble * TableDouble::fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOfDoubles,double const * pDoubles)
{
    TableDouble * res = new TableDouble(pNoOfDoubles) ;
    for (int i = 0 ; i < pNoOfDoubles ; i++)
        res->set(i,Double::fromCtoCpp(pStatus, pDoubles[i])) ;
    if (*pStatus == STATUS_OK)
        return res ;
    delete res ;
    return NULL ;
}


} //namespace opcua
