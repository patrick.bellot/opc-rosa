#ifndef _INITSERVERAPI_H
#define _INITSERVERAPI_H

#include "lib/OpcUa.h"

extern "C" {

#if (WITH_DISCOVERY == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_GetEndpoints_ServiceType ;
#endif
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_ActivateSession_ServiceType ;
#if (WITH_NODESMNGT == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_AddNodes_ServiceType ;
#endif
#if (WITH_NODESMNGT == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_AddReferences_ServiceType ;
#endif
#if (WITH_BROWSE == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Browse_ServiceType ;
#endif
#if (WITH_CALL == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Call_ServiceType ;
#endif
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_CloseSession_ServiceType ;
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_CreateMonitoredItems_ServiceType ;
#endif
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_CreateSession_ServiceType ;
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_CreateSubscription_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_DeleteMonitoredItems_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_DeleteSubscriptions_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_ModifyMonitoredItems_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_ModifySubscription_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Publish_ServiceType ;
#endif
#if (WITH_READ == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Read_ServiceType ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_RegisterNodes_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Republish_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_SetMonitoringMode_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_SetPublishingMode_ServiceType ;
#endif
#if (WITH_SUBSCRIPTION == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_TransferSubscriptions_ServiceType ;
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_TranslateBrowsePathsToNodeIds_ServiceType ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_UnregisterNodes_ServiceType ;
#endif
#if (WITH_WRITE == 1)
extern
#ifdef _WIN32
__declspec( dllimport )
#endif
SOPC_ServiceType OpcUa_Write_ServiceType ;
#endif

void initServerApi()
{
#if (WITH_DISCOVERY == 1)
   OpcUa_GetEndpoints_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_GetEndpoints ;
#endif
   OpcUa_ActivateSession_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_ActivateSession ;
#if (WITH_NODESMNGT == 1)
   OpcUa_AddNodes_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_AddNodes ;
#endif
#if (WITH_NODESMNGT == 1)
   OpcUa_AddReferences_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_AddReferences ;
#endif
#if (WITH_BROWSE == 1)
   OpcUa_Browse_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Browse ;
#endif
#if (WITH_CALL == 1)
   OpcUa_Call_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Call ;
#endif
   OpcUa_CloseSession_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_CloseSession ;
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_CreateMonitoredItems_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_CreateMonitoredItems ;
#endif
   OpcUa_CreateSession_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_CreateSession ;
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_CreateSubscription_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_CreateSubscription ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_DeleteMonitoredItems_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_DeleteMonitoredItems ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_DeleteSubscriptions_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_DeleteSubscriptions ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_ModifyMonitoredItems_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_ModifyMonitoredItems ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_ModifySubscription_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_ModifySubscription ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_Publish_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Publish ;
#endif
#if (WITH_READ == 1)
   OpcUa_Read_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Read ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
   OpcUa_RegisterNodes_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_RegisterNodes ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_Republish_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Republish ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_SetMonitoringMode_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_SetMonitoringMode ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_SetPublishingMode_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_SetPublishingMode ;
#endif
#if (WITH_SUBSCRIPTION == 1)
   OpcUa_TransferSubscriptions_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_TransferSubscriptions ;
#endif
#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
   OpcUa_TranslateBrowsePathsToNodeIds_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_TranslateBrowsePathsToNodeIds ;
#endif
#if (WITH_REGISTER_UNREGISTER_NODES == 1)
   OpcUa_UnregisterNodes_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_UnregisterNodes ;
#endif
#if (WITH_WRITE == 1)
   OpcUa_Write_ServiceType.InvokeService = (SOPC_InvokeService *)OpcUa_ServerApi_Write ;
#endif
}

} // extern "C"
#endif // _INITSERVERAPI_H
