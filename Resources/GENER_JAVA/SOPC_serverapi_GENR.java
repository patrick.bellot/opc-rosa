public class SOPC_serverapi_GENR
{

    static public void main(String[] args)
    {
	String[] services = ImplantedServices.services ;

	System.out.println("#include <sopc_endpoint.h>") ;
	System.out.println() ;
	for (int i=0 ; i <  services.length ; i+=2) {
	    HTable.printif(services[i+1]) ;
	    System.out.println("extern struct SOPC_ServiceType OpcUa_"+services[i]+"_ServiceType ;") ;
	    HTable.printendif(services[i+1]) ;
	}
	System.out.println() ;
	for (int i=0 ; i <  services.length ; i+=2) {
	    System.out.println() ;
	    HTable.printif(services[i+1]) ;
	    System.out.println("void setOpcUa_"+services[i]+"_ServiceType_InvokeService(SOPC_InvokeService * f)") ;
	    System.out.println("{") ;
	    System.out.println("   OpcUa_"+services[i]+"_ServiceType.InvokeService = f ;") ;
	    System.out.println("}") ;
	    HTable.printendif(services[i+1]) ;
	};

    }


}
