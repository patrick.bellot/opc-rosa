#!/bin/zsh

if (( $# != 2 )) ; then
   echo 'What are $1 and $2 ?' ;
   exit 1 ;
fi

echo "Running for $1/$2"
 
perl -pi'.bak' -e"s/$1/$2/g"                   ~/Desktop/opc-rosa/**/*.{h,cpp,c}  
                    
rm ~/Desktop/opc-rosa/**/*.bak 

echo 'Done'
exit 0 ;


