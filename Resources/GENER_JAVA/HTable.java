public class HTable
{
    public static void main(String[] args)
    {
	System.out.println("#ifndef _OPCUA_IPCS_TABLES_H") ;
	System.out.println("#define _OPCUA_IPCS_TABLES_H") ;
	System.out.println() ;
	System.out.println("#include \"OpcUa_IPCS_Table.h\"") ;
	System.out.println() ;
	System.out.println("namespace opcua {") ;
	System.out.println() ;

	printSOPC  (PData.pSOPCDataSDT) ;
	printOpcUa (PData.pOpcUaDataSDT) ;
	printSOPC  (PData.pSOPCDataCPT) ;
	printOpcUa (PData.pOpcUaDataNMT) ;
	printOpcUa (PData.pOpcUaDataNAE) ;
	printOpcUa (PData.pOpcUaDataCPT) ;
	printOpcUa (PData.pOpcUaDataDAS) ;
	printType  (PData.pSpecialCPT) ;
	printType  (PData.pSpecialNAE) ;
	printType  (PData.pSpecialSDT) ;

	System.out.println() ;
	System.out.println("} //namespace opcua") ;
	System.out.println() ;
	System.out.println("#endif // _OPCUA_IPCS_TABLES_H") ;
    }

    private static void printType(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=3) {
	    printif(t[i+2]) ;
	    print(t[i], t[i+1]) ;
	    printendif(t[i+2]) ;
	}
    }

    private static void printOpcUa(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=2) {
	    printif(t[i+1]) ;
	    print(t[i], "OpcUa_"+t[i]) ;
	    printendif(t[i+1]) ;
	}
    }

    private static void printSOPC(String[] t)
    {
	for (int i=0 ; i<t.length ; i+=2) {
	    printif(t[i+1]) ;
	    print(t[i], "SOPC_"+t[i]) ;
	    printendif(t[i+1]) ;
	}
    }

    public static void printif(String selector) {
	if (selector != null)
	    System.out.println("#if ("+selector+" == 1)") ;
    }

    public static void printendif(String selector) {
	if (selector != null)
	    System.out.println("#endif") ;
    }

    private static void print(String c, String t)
    {
	String tid ;
	if (c.equals("ExtensionObject")) {
	    tid = "0" ;
	} else if (c.equals("Variant")) {
	    tid = "0" ;
	} else {
	    tid = "OpcUaId_"+c ;
	}

	System.out.println() ;
	System.out.println("class "+c+" ;") ;
	System.out.println("class MYDLL Table"+c+" : public Table") ;
	System.out.println("{") ;
	System.out.println("public:") ;
	System.out.println() ;
	System.out.println("    Table"+c+"(int32_t _len)") ;
	System.out.println("        : Table(_len)") ;
	System.out.println("    {}") ;
	System.out.println() ;
	System.out.println("    virtual Table"+c+" * getTable(int n) const ;") ;
	System.out.println() ;
	System.out.println("    virtual uint32_t getTypeId() const { return "+tid+" ; }") ;
	System.out.println("    "+c+" * get(int32_t i) const ;") ;
	System.out.println("    void fromCpptoC(SOPC_StatusCode * pStatus, int32_t& pNoOf"+c+"s,"+t+" *& p"+c+"s) const ;") ;
	System.out.println("    static Table"+c+" * fromCtoCpp(SOPC_StatusCode * pStatus, int32_t const& pNoOf"+c+"s,"+t+" const * p"+c+"s) ;") ;
	System.out.println("} ; // class Table"+c) ;
	System.out.println() ;
    }
}
