#!/bin/zsh

echo
echo "DIRECTORY BUILDER"
echo


##########################################################################

source ./variables.sh

##########################################################################

function program_stop {
    echo
    echo $1
    echo "Cannot continue. Must stop !" ;
    exit 0 ;
}

##########################################################################

if [[ "$#" -lt 1 ]] ; then

    program_stop "Usage: $0 <self ip address> [ <server ip address> * ]" ;

fi

##########################################################################

echo
echo "IN \"$openssl\" CONVERTING ALL TO .der OR .pem FORMAT"
echo

echo "cd \"$openssl\""
cd "$openssl"
echo "rm -Rf *.der"
rm -Rf *.der
echo

for file in *.crt ; do

    derfile="$file.der"
    echo "$file => $derfile"

    openssl x509 -in "$file" -out "$derfile" -outform DER

done

for file in *.key ; do

    derfile="$file.der"
    echo "$file => $derfile"

    openssl rsa -in "$file" -out "$derfile" -outform DER

    pemfile="$file.pem"
    echo "$file => $pemfile"

    openssl rsa -in "$file" -out "$pemfile" -outform PEM

done


##########################################################################

echo
echo "IN \"$openssl/$intermediate\" CONVERTING ALL TO .der FORMAT"
echo

echo "cd \"$intermediate\""
cd "$intermediate"
echo "rm -Rf *.der"
rm -Rf *.der
echo

for file in *.crt ; do

    derfile="$file.der"
    echo "$file => $derfile"

    if [[ "$file" != "crl.crt" ]] ; then
	openssl x509 -in "$file" -out "$derfile" -outform DER
    else
	openssl crl -in "$file" -out "$derfile" -outform DER
    fi

done

for file in *.key ; do

    derfile="$file.der"
    echo "$file => $derfile"

    openssl rsa -in "$file" -out "$derfile" -outform DER

    pemfile="$file.pem"
    echo "$file => $pemfile"

    openssl rsa -in "$file" -out "$pemfile" -outform PEM

done

##########################################################################

echo
echo "IN \"$openssl\" PREPARING certficates DIRECTORY"
echo

echo "cd .."
cd ..
echo "rm -Rf certificates"
rm -Rf certificates
echo "mkdir certificates"
mkdir certificates
echo "mkdir certificates/certs"
mkdir certificates/certs
echo "mkdir certificates/keys"
mkdir certificates/keys
echo "mkdir certificates/crl"
mkdir certificates/crl

##########################################################################

echo
echo "IN \"$openssl\" COPYING ALL FILES TO certficate DIRECTORY"
echo

echo "cp \"ca.crt\" \"certificates/certs/ca.crt\""
cp "ca.crt" "certificates/certs/ca.crt"
echo "cp \"ca.crt.der\" \"certificates/certs/ca.der\""
cp "ca.crt.der" "certificates/certs/ca.der"
echo
echo "cp \"$intermediate/signingCa.crt\" \"certificates/certs/signingCa.crt\""
cp "$intermediate/signingCa.crt" "certificates/certs/signingCa.crt"
echo "cp \"$intermediate/signingCa.crt.der\" \"certificates/certs/signingCa.der\""
cp "$intermediate/signingCa.crt.der" "certificates/certs/signingCa.der"
echo
echo "cp \"$intermediate/chainOfTrust.crt\" \"certificates/certs/chainOfTrust.crt\""
cp "$intermediate/chainOfTrust.crt" "certificates/certs/chainOfTrust.crt"
echo "cp \"$intermediate/chainOfTrust.crt.der\" \"certificates/certs/chainOfTrust.der\""
cp "$intermediate/chainOfTrust.crt.der" "certificates/certs/chainOfTrust.der"
echo
echo "cp \"$intermediate/$1.crt\" \"certificates/certs/self.crt\""
cp "$intermediate/$1.crt" "certificates/certs/self.crt"
echo "cp \"$intermediate/$1.crt.der\" \"certificates/certs/self.der\""
cp "$intermediate/$1.crt.der" "certificates/certs/self.der"
echo
echo "cp \"$intermediate/$1.key\" \"certificates/keys/self.key\""
cp "$intermediate/$1.key" "certificates/keys/self.key"
echo "cp \"$intermediate/$1.key.der\" \"certificates/keys/self.der\""
cp "$intermediate/$1.key.der" "certificates/keys/self.der"
echo "cp \"$intermediate/$1.key.pem\" \"certificates/keys/self.pem\""
cp "$intermediate/$1.key.pem" "certificates/keys/self.pem"
echo
echo "cp \"$intermediate/crl.crt\" \"certificates/crl/signingCrl.crt\""
cp "$intermediate/crl.crt" "certificates/crl/signingCrl.crt"
echo "cp \"$intermediate/crl.crt.der\" \"certificates/crl/signingCrl.der\""
cp "$intermediate/crl.crt.der" "certificates/crl/signingCrl.der"

echo

for (( i = 2  ; i <= $#  ; i++  )) ; do

	
	echo "cp \"$intermediate/$argv[$i].crt\" \"certificates/certs/$argv[$i].crt\""
	cp "$intermediate/$argv[$i].crt" "certificates/certs/$argv[$i].crt"
	echo "cp \"$intermediate/$argv[$i].crt.der\" \"certificates/certs/$argv[$i].der\""
	cp "$intermediate/$argv[$i].crt.der" "certificates/certs/$argv[$i].der"

done

##########################################################################

echo
echo "IN \"$openssl\" ZIP DIRECTORY certificates TO \"$1.zip\""
echo

if [[ "$#" -ge 2 ]] ; then

    echo "zip \"$1.client.zip\" certificates"
    zip -r -X "zip/$1.client.zip" certificates

else

    echo "zip \"zip/$1.server.zip\" certificates"
    zip -r -X "zip/$1.server.zip" certificates

fi


##########################################################################

echo
echo "DONE !"
echo

