#!/bin/zsh

echo
echo "CERTIFICATE BUILDER"
echo

##########################################################################

source ./variables.sh

##########################################################################

function program_stop {
    echo
    echo $1
    echo "Cannot continue. Must stop !" ;
    exit 0 ;
}

##########################################################################

if [[ "$#" -ne 1 ]] ; then

    program_stop "Usage: $0 <ip address>" ;PKI/own/certs/uaexpert.der

fi

echo "Building certificate for $1"

echo
echo "cd \"$openssl\""
cd "$openssl"

##########################################################################

echo
echo "Generating CSR for $1"
echo

cmd=\
"openssl req \
 -new \
 -nodes \
 -newkey rsa:2048 \
 -keyout \"$intermediate/$1.key\" \
 -out \"$intermediate/$1.csr\" \
 -config \"$intermediate/ca-config\""

echo $cmd
eval $cmd

##########################################################################

echo
echo "Generating CRT for $1"
echo

cmd=\
"openssl ca \
 -config \"$intermediate/ca-config\" \
 -policy policy_anything \
 -extensions v3_ca \
 -out \"$intermediate/$1.crt\" \
 -infiles \"$intermediate/$1.csr\""

echo $cmd
eval $cmd

##########################################################################

echo
answer="?"
echo -n "In $openssl/$intermediate, verifying $1.crt ? [Y/n] "
read answer
    
if [[ "$answer" == "y" ]] || [[ "$answer" == "Y" ]] || [[ "$answer" == "" ]] ; then
	
    echo
    cmd="openssl x509 -noout -text -in \"$intermediate/$1.crt\""
    echo $cmd
    eval $cmd

    if [[ $? -ne 0 ]] ; then
	program_stop "Failure in information display."
    fi

	
    echo
    cmd="openssl verify -CAfile \"$intermediate/chainOfTrust.crt\" \"$intermediate/$1.crt\""
    echo $cmd
    eval $cmd

    if [[ $? -ne 0 ]] ; then
	program_stop "Failure in verification."
    fi

fi


##########################################################################


echo
echo "DONE !"
echo


