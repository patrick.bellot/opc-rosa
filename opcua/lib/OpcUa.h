
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_H_
#define _OPCUA_H_

/* The main includes */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#ifdef _WIN32
#  include <winsock2.h>
#  include <ws2tcpip.h>
#else
#  include <inttypes.h>
#endif
#include <stdint.h>


/********************************************************************************************************************/
/* For OPC Testing **************************************************************************************************/
/********************************************************************************************************************/

/* 1 if testing, 0 if not testing */

#define JUST_FOR_TEST 0

/********************************************************************************************************************/
/* Logs ? ***********************************************************************************************************/
/********************************************************************************************************************/

/* SIEM_LOG=0 means ordinary log on the console
 *         =1 means SIEM compatible log into a file
 */
#define SIEM_LOG 0

/* This is the maximum number of lines in a log file */
#define MAX_SIEM_LINE_COUNT 1000000

/* SHIFT_SIEM_LOG=0 means that when the log file is full, it is copied and a new log file is created
 *               =1 means that when the log file is full, it is saved and a new log file is created
 */
#define SHIFT_SIEM_LOG 0


/********************************************************************************************************************/
/* Here begin the configurations: 0 or 1  ***************************************************************************/
/********************************************************************************************************************/

/* Do you want node management request ? 1 or 0 (services in 5.7 View Service Set) */
#define WITH_NODEMNGT 1

/* Do you want browse request ? 1 or 0 (services in 5.8 View Service Set) */
#define WITH_BROWSE 1
#define IGNORE_RequestedMaxReferencesPerNode_IN_Browse 1
#define INGORE_IncludeSubtypes_IN_TranslateBrowsePathsToNodeIds 1

/* Do you want translate browsepaths to node ids request ? 1 or 0 (services in 5.8 View Service Set) */
#define WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS 1

/* Do you want register and unregister nodes ? 1 or 0 (services in 5.8 View Service Set) */
#define WITH_REGISTER_UNREGISTER_NODES 1

/* Do you want queries ?  1 or 0 [ MUST BE 0 ] (services in 5.9 Query Service Set) */
#define WITH_QUERY 0

/* Do you want read request ? 1 or 0 (services in 5.10 Attribute Service Set) */
#define WITH_READ 1

/* Do you want write request ? 1 or 0 (services in 5.10 Attribute Service Set) */
#define WITH_WRITE 1

/* Do you want methods ?  1 or 0 (services in 5.11 Call Service Set) */
#define WITH_CALL 1

/* Do you want subscriptions and events ?  1 or 0 (services in 5.12 MonitoredItem Service Set and 5.13 Subscription Service Set) */
#define WITH_SUBSCRIPTION 1

/* Discovery services */
#define WITH_DISCOVERY 1


/********************************************************************************************************************/
/* Do you want some Views ? */

/* A view for Variable nodes */
#define VIEW_VARIABLES 1

/* A view for Method nodes */
#define VIEW_METHODS 1

/********************************************************************************************************************/
/* The size of the hash table for address space */

#define NODES_TABLE_COUNT (12 * 1024)

/********************************************************************************************************************/
/* Cients */

#define MAX_RESPONSES_PER_CLIENT 10

/********************************************************************************************************************/
/* Subscription */

/* Expected subscriptions */
#define EXPECTED_SUBSCRIPTIONS 100

/* Expected publish queue length per subscription */
#define EXPECTED_PUBLISH_REQ_QUEUE_LENGTH 128

//********************************************************************************************************************/
/* Security profiles ************************************************************************************************/
/********************************************************************************************************************/

#define UANONE							0
#define UASIGN_Basic256Sha256   		1
#define UASIGNANDENCRYPT_Basic256Sha256 2

#define SECURITY 0

/* MUST BE ZERO, DON'T TOUCH - The verification of the application URI must be disabled be cause of a bug in mbedtls */
#define VERIF_APPURI_DISABLED			0
#define VERIF_APPURI_ENABLED			1

#define VERIF_APPURI 0

/********************************************************************************************************************/
/* Here end the configurations **************************************************************************************/
/********************************************************************************************************************/

/* Security profiles */

#if (SECURITY == UANONE)
#   define SignatureLength     0
#   define SecurityProfile     SecurityProfileUri::SecurityPolicy_None
#   define NonceLength		   32
#endif

#if (SECURITY == UASIGN_Basic256Sha256 )
#   define SignatureLength     ((uint32_t)256) /*2048 bits, used for the RSA signature algorithm*/
#   define MessageDigestLength 32
#   define NonceLength		   32
#   define SecurityProfile     SecurityProfileUri::SecurityPolicy_256
#endif

#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
#   define SignatureLength     ((uint32_t)256) /*2048 bits, used for the RSA signature algorithm*/
#   define MessageDigestLength 32
#   define NonceLength		   32
#   define SecurityProfile     SecurityProfileUri::SecurityPolicy_256
#endif

/* Deduced size from EXPECTED_SUBSCRIPTIONS */

#define SIZE_OF_SUBSCRIPTIONS_TABLE (2 * (EXPECTED_SUBSCRIPTIONS))

/* About ENDIANESS */

#ifdef _WIN32
#   define IS_BIG_ENDIAN 0
#else
#   if __APPLE__
#      include <architecture/byte_order.h>
#      define IS_BIG_ENDIAN (BYTE_ORDER != __DARWIN_LITTLE_ENDIAN)
#   else
#      include <endian.h>
#      define IS_BIG_ENDIAN (__BYTE_ORDER == __BIG_ENDIAN)
#   endif
#endif

/* About DLL for Windows */

#ifdef USE_DLL
# define MYDLL __declspec( dllimport )
#else
# ifdef COMPILE_DLL
#  define MYDLL __declspec( dllexport )
# else
#  define MYDLL
# endif
#endif

/* Not used parameters */

#define NOT_USED(x) ((void)(x))

/* Debugging flags */

#include "Error/OpcUa_Debug.h"

#define ALL_DBG            0xFFFFFFFF  /* For all error */

#define COM_ERR            0x00000001  /* True error */
#define MAIN_LIFE_DBG      0x00000002  /* Everyday's life */
#define COM_DBG            0x00000004  /* Communication debug */
#define COM_DBG_DETAILED   0x00000008  /* Communication debug, very detailed */
#define MEM_DBG            0x00000010  /* Memory debug */
#define SERVER_DBG         0x00000020  /* Server debug */
#define S2OPC_DBG    	   0x00000040  /* Systerel stack bug*/
#define PROC_INFO          0x00000080  /* Process info */
#define DLL_DBG            0x00000100  /* DLL debugging */
#define XML_DBG            0x00000200  /* XML debugging*/
#define REF_DBG            0x00000400  /* References debugging (printing */
#define IO_DBG             0x00000800  /* Just in and out message */
#define SELF_DBG           0x00001000  /* Simple debugging instruction */
#define ARRAY_DBG          0x00002000  /* Getting and setting array variable */
#define MTH_DBG            0x00004000  /* Methods debugging */
#define SUB_DBG            0x00008000  /* Subscription debugging */
#define S2OPC_ERR          0x00010000  /* Systerel error*/
#define SEC_DBG            0x00020000  /* Security debugging */
#define BRO_DBG            0x00040000  /* Browse and TranslateBrowsePathsToNodeIds debugging */
#define CLIENT_DBG         0x00080000  /* Client debug */
#define BYTE_DBG		   0x00100000  /* For bytes display */
#define _____DBG2  		   0x00200000  /* */
#define HASH_DBG           0x00400000  /* Debugging of hash table */
#define BUG_DBG            0x00800000  /* For debugging great bugs */
#define IPCS_ERR           0x01000000  /* For IPCS great bugs */
#define IPCS_DBG           0x02000000  /* For IPCS debug */
#define STATIC_DBG         0x04000000  /* For static variables initialization debug */
#define _____DBG4          0x08000000  /* */
#define BIN_DBG_DETAIL     0x10000000  /* Binary loading of workspace detailed */
#define BIN_DBG            0x20000000  /* Binary loading of workspace */
#define DISC_DBG           0x40000000  /* Discovery service */
#define SESSION_DBG        0x80000000  /* Session service */

/* Current debug level. */

#define DEBUG_LEVEL (COM_ERR + IPCS_ERR + S2OPC_ERR + MAIN_LIFE_DBG + SERVER_DBG + CLIENT_DBG)

//#define DEBUG_LEVEL (COM_ERR + COM_DBG + IPCS_ERR + S2OPC_ERR + S2OPC_DBG + MAIN_LIFE_DBG + SERVER_DBG + CLIENT_DBG)

// #define DEBUG_LEVEL (COM_ERR + IPCS_ERR + S2OPC_ERR + MAIN_LIFE_DBG + SERVER_DBG + CLIENT_DBG + BIN_DBG + S2OPC_DBG)

/* #define DEBUG_LEVEL (COM_ERR + IPCS_ERR + S2OPC_ERR + MAIN_LIFE_DBG + SERVER_DBG + CLIENT_DBG + BIN_DBG + \
		IPCS_DBG + COM_DBG + COM_DBG_DETAILED + SELF_DBG + SEC_DBG + IO_DBG + S2OPC_DBG)
*/

/* Siem logs */

#if SIEM_LOG == 1
#  ifdef DEBUG_LEVEL
#    undef DEBUG_LEVEL
#  endif

#  define DEBUG_LEVEL ( \
		COM_ERR + \
		MAIN_LIFE_DBG + \
		SERVER_DBG + \
		CLIENT_DBG)

#endif

/* Instruction for debug. */

#define odebug(flag,app,msg) \
	if ((flag) & DEBUG_LEVEL) { \
		opcua::Debug::_debug(flag,app,msg) ; \
	}
#define odebug_i(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		opcua::Debug::_debug_i(flag,app,msg,par1) ; \
	}
#define odebug_ii(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		opcua::Debug::_debug_ii(flag,app,msg,par1,par2) ; \
	}
#define odebug_iii(flag,app,msg,par1,par2,par3) \
	if ((flag) & DEBUG_LEVEL) { \
		opcua::Debug::_debug_iii(flag,app,msg,par1,par2,par3) ; \
	}
#define odebug_is(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		opcua::Debug::_debug_is(flag,app,msg,par1,par2) ; \
	}

namespace opcua {

#define debug(flag,app,msg) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug(flag,app,msg) ; \
	}
#define debug_i(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_i(flag,app,msg,par1) ; \
	}
#define debug_l(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_l(flag,app,msg,par1) ; \
	}
#define debug_s(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_s(flag,app,msg,par1) ; \
	}
#define debug_ss(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_ss(flag,app,msg,par1,par2) ; \
	}
#define debug_sss(flag,app,msg,par1,par2,par3) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_sss(flag,app,msg,par1,par2,par3) ; \
	}
#define debug_ssss(flag,app,msg,par1,par2,par3,par4) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_ssss(flag,app,msg,par1,par2,par3,par4) ; \
	}
#define debug_ii(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_ii(flag,app,msg,par1,par2) ; \
	}
#define debug_si(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_si(flag,app,msg,par1,par2) ; \
	}
#define debug_is(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_is(flag,app,msg,par1,par2) ; \
	}
#define debug_com(flag,app,msg) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_com(flag,app,msg) ; \
	}
#define debug_com_s(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_com_s(flag,app,msg,par1) ; \
	}
#define debug_com_i(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_com_i(flag,app,msg,par1) ; \
	}
#define debug_com_si(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_com_si(flag,app,msg,par1,par2) ; \
	}
#define debug_p(flag,app,msg,par1) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_p(flag,app,msg,par1) ; \
	}
#define debug_pp(flag,app,msg,par1,par2) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_pp(flag,app,msg,par1,par2) ; \
	}
#define debug_ccc(flag,app,msg,par1,par2,par3) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_ccc(flag,app,msg,par1,par2,par3) ; \
	}
#define debug_iii(flag,app,msg,par1,par2,par3) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_iii(flag,app,msg,par1,par2,par3) ; \
	}
#define debug_sii(flag,app,msg,par1,par2,par3) \
	if ((flag) & DEBUG_LEVEL) { \
		Debug::_debug_sii(flag,app,msg,par1,par2,par3) ; \
	}

}

/* Stopping in the debugger */

namespace opcua {

extern void stop_for_debug() ;

extern void stop_for_debug(int n) ;

#define STOPIF(flag) { if (flag) stop_for_debug() ; }

extern void print_uint8_array(const char * msg, int32_t length, uint8_t * array) ;

extern MYDLL void reexit(int n) ;

#define EXIT(n) { fprintf(stderr,"\nExiting\n") ; fflush(stderr) ; Clock::sleep(3000) ; reexit(n) ; }

} /* namespace opcua */


/* Checking functions returns */

#include "Error/OpcUa_Check.h"

/* Status Code */

#include "OpcUa_StatusCodeDefine.h"

/* Type Id */

#include "OpcUa_TypeIdDefine.h"

/* Attribute write mask */

#include "OpcUa_BaseDefine.h"

/* Sometimes missing */

#ifndef INT8_MIN
# define INT8_MIN (-0x7f - 1)
#endif

#ifndef INT16_MIN
# define INT16_MIN (-0x7fff - 1)
#endif

#ifndef INT32_MIN
# define INT32_MIN (-0x7fffffff - 1)
#endif

#ifndef INT64_MIN
# define INT64_MIN (-0x7fffffffffffffff - 1)
#endif

#ifndef INT8_MAX
# define INT8_MAX (0x7f)
#endif

#ifndef INT16_MAX
# define INT16_MAX (0x7fff)
#endif

#ifndef INT32_MAX
# define INT32_MAX (0x07fffffff)
#endif

#ifndef INT64_MAX
# define INT64_MAX (0x7fffffffffffffffLL)
#endif


#ifndef UINT8_MAX
# define UINT8_MAX (0xff)
#endif

#ifndef UINT16_MAX
# define UINT16_MAX (0xffff)
#endif

#ifndef UINT32_MAX
# define UINT32_MAX (0xffffffff)
#endif

#ifndef UINT64_MAX
# ifdef _WIN32
#   define UINT64_MAX (0xffffffffffffffffULL)
# else
#   define UINT64_MAX (0xffffffffffffffffLLU)
# endif
#endif

#ifdef _WIN32
# define FILE_SEPARATOR '/'
#else
# define FILE_SEPARATOR '/'
#endif

#ifdef _MSC_VER
#define strtoll _strtoi64
#endif

#ifdef _MSC_VER
#define strtoull _strtoui64
#endif

/* INCLUDES FOR INGOPCS */

extern "C" {

#   include <sopc_toolkit_constants.h>
#   include <sopc_builtintypes.h>
#   include <sopc_types.h>
#   include <sopc_services_api.h>
#   include <sopc_toolkit_config_internal.h>
#   include <sopc_buffer.h>
#   include <sopc_encoder.h>
#   include <sopc_user_app_itf.h>
#   include <sopc_encodeable.h>
#   include <sopc_crypto_profiles.h>
#   include <sopc_pki_stack.h>
#   include <sopc_toolkit_config.h>
#   include <sopc_toolkit_async_api.h>
#   include <sopc_time.h>
#   include <sopc_secure_channels_api.h>

#   include <opcua_identifiers.h>

#   include <mbedtls/x509_crt.h>

struct SOPC_Certificate {
	mbedtls_x509_crt crt;   /**  Certificate as a lib dependent format */
	uint8_t *crt_der;       /**  Certificate in the DER format, which should be canonical. Points to internal mbedtls buffer.*/
	uint32_t len_der;       /**  Length of crt_der. */
} ; /* struct Certificate */

} // EXTERN "C"

#define STATUS_OK ((SOPC_ReturnStatus)0)

#endif /* OPCUA_H_ */
