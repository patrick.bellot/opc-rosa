
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_TIMEZONEDATATYPE_H_
#define OPCUA_TIMEZONEDATATYPE_H_

/*
 * OPC-UA Part 3, 8.28, p. 66
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_UInt16.h"
#include "OpcUa_IPCS_Boolean.h"
#include "OpcUa_Structure.h"

namespace opcua {

class MYDLL TimeZoneDataType
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_TimeZoneDataType ; }

private:

	UInt16  * offset ;
	Boolean * daylightSavingInOffset ;

public:

	TimeZoneDataType(UInt16  * _offset,
				     Boolean * _daylightSavingInOffset)
		: Structure()
	{
		(offset                 = _offset)                 ->take() ;
		(daylightSavingInOffset = _daylightSavingInOffset) ->take() ;
	}

	virtual ~TimeZoneDataType()
	{
		offset                 ->release() ;
		daylightSavingInOffset ->release() ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_TIMEZONEDATATYPE_H_ */
