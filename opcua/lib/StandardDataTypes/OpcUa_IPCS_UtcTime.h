
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UTCTIME_H_
#define OPCUA_UTCTIME_H_

/*
 * OPC-UA Part3, 8.38, p. 67
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_DateTime.h"
#include "../Utils/OpcUa_Clock.h"

namespace opcua {

class MYDLL UtcTime
		: public DateTime
{
public:

	static UtcTime * null ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_UtcTime ; }

public:

	static UtcTime * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_DateTime const& pDateTime)
	{
		NOT_USED(pStatus) ;
//		uint64_t uvalue64 = (((uint64_t)(pDateTime.High32)) << 32) + (uint64_t)(pDateTime.Low32) ;
//		return new UtcTime(uvalue64) ;
		return new UtcTime(pDateTime) ;
	}

public:

	UtcTime(uint64_t _value)
		: DateTime(_value)
	{}



	virtual ~UtcTime()
	{}

public:

	static UtcTime * now()
	{
		return new UtcTime((uint64_t)(Clock::get_microsecondssince1601() * 10)) ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_UTCTIME_H_ */
