
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_IDTYPE_H_
#define OPCUA_IDTYPE_H_

/*
 * OPC-UA, Part 3, 8.2.3, p. 61
 *                 8.18,  p. 65
 */

#include "../OpcUa.h"
#include "OpcUa_Enumeration.h"

namespace opcua {

// IdType = 256

enum _IdType {
	IdType_NUMERIC_0  = 0,
	IdType_STRING_1   = 1,
	IdType_GUID_2     = 2,
	IdType_OPAQUE_3   = 3
} ;

class MYDLL IdType
		: public Enumeration
{
public:

	static IdType * numeric_0 ;
	static IdType * string_1 ;
	static IdType * guid_2 ;
	static IdType * opaque_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_IdType ; }

public:

	static IdType * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_IdentifierType const& value)
	{
		if ((uint16_t)value <= OpcUa_IdType_Opaque)
			return new IdType((_IdType)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_IdentifierType& pValue) const
	{
		int32_t value = get() ;
		if ((uint16_t)value <= IdType_OPAQUE_3)
			pValue = (SOPC_IdentifierType)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	IdType(_IdType _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* OPCUA_IDTYPE_H_ */
