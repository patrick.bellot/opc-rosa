
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.7, p. 64
 * Enhanced with reference counting.
 */

#include "All.h"
#include "../DataAccess/All.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

# ifdef _WIN32
	BaseDataType * BaseDataType::takeArray(
			class NumericRange  * indexRange,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions)
	{
		CHECK_INT(0) ;

		NOT_USED(indexRange) ;
		NOT_USED(valueRank) ;
		NOT_USED(_arrayDimensions) ;

		return NULL ;
	}
# else
	 BaseDataType * BaseDataType::takeArray(
			class NumericRange * indexRange,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions)
	{
		CHECK_INT(0) ;

		NOT_USED(indexRange) ;
		NOT_USED(valueRank) ;
		NOT_USED(arrayDimensions) ;

		return NULL ;
	}
# endif

# ifdef _WIN32
	void BaseDataType::setArray(
			class NumericRange  * indexRange,
			class BaseDataType  * newValue,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions)
	{
		CHECK_INT(0) ;

		NOT_USED(indexRange) ;
		NOT_USED(newValue) ;
		NOT_USED(valueRank) ;
		NOT_USED(_arrayDimensions) ;
	}
# else
	void BaseDataType::setArray(
			class NumericRange * indexRange,
			class BaseDataType * newValue,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions)
	{
		CHECK_INT(0) ;

		NOT_USED(indexRange) ;
		NOT_USED(newValue) ;
		NOT_USED(valueRank) ;
		NOT_USED(arrayDimensions) ;
	}
# endif

char * BaseDataType::get_Attribute(const char * attribute, node_t * node)
{
	int nba = roxml_get_attr_nb(node) ;

	for (int j = 0 ; j < nba ; j++) {
		node_t * attr = roxml_get_attr(node,NULL,j) ;
		char   * anam = roxml_get_name(attr,NULL,0) ;

		if (StringUtils::strcmpi(anam,attribute) == 0) {
			roxml_release(anam) ;
			return roxml_get_content(attr,NULL,0,NULL) ;
		}

		roxml_release(anam) ;
	}

	return NULL ;
}

BaseDataType * BaseDataType::get_BaseDataType_element(NodeId ** nodeId, const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char     * dataType  = BaseDataType::get_Attribute("DataType",node) ;
	uint32_t   iDataType = 0 ;

	if (dataType == NULL) {
		char * nodeStr = roxml_get_name(node,NULL,0) ;
		debug_ss(COM_ERR,"BaseDataType","File \"%s\", No DataType \"%s\"",file_str,nodeStr) ;
		roxml_release(nodeStr) ;
		return NULL ;
	}

	char * arrayLengthStr = BaseDataType::get_Attribute("ArrayLength",node) ;

	if (arrayLengthStr == NULL) {

		BaseDataType * result ;

		if (strcmp(dataType,"UInt32") == 0) {
			result = UInt32::get_UInt32_element(file_str,node) ;
			iDataType = OpcUaId_UInt32 ;
		} else
		if (strcmp(dataType,"Int32") == 0) {
			result = Int32::get_Int32_element(file_str,node) ;
			iDataType = OpcUaId_Int32 ;
		} else
		if (strcmp(dataType,"UInt16") == 0) {
			result = UInt16::get_UInt16_element(file_str,node) ;
			iDataType = OpcUaId_UInt16 ;
		} else
		if (strcmp(dataType,"Int16") == 0) {
			result = Int16::get_Int16_element(file_str,node) ;
			iDataType = OpcUaId_Int16 ;
		} else
		if (strcmp(dataType,"Byte") == 0) {
			result = Byte::get_Byte_element(file_str,node) ;
			iDataType = OpcUaId_Byte ;
		} else
		if (strcmp(dataType,"SByte") == 0) {
			result = SByte::get_SByte_element(file_str,node) ;
			iDataType = OpcUaId_SByte ;
		} else
		if (strcmp(dataType,"String") == 0) {
			result = String::get_String_element(file_str,node) ;
			iDataType = OpcUaId_String ;
		} else
		if (strcmp(dataType,"ByteString") == 0) {
			result = ByteString::get_ByteString_element(file_str,node) ;
			iDataType = OpcUaId_ByteString ;
		} else
		if (strcmp(dataType,"Boolean") == 0) {
			result = Boolean::get_Boolean_element(file_str,node) ;
			iDataType = OpcUaId_Boolean ;
		} else
		if (strcmp(dataType,"Float") == 0) {
			result = Float::get_Float_element(file_str,node) ;
			iDataType = OpcUaId_Float ;
		} else
		if (strcmp(dataType,"Double") == 0) {
			result = Double::get_Double_element(file_str,node) ;
			iDataType = OpcUaId_Double ;
		} else
		if (strcmp(dataType,"UInt64") == 0) {
			result = UInt64::get_UInt64_element(file_str,node) ;
			iDataType = OpcUaId_UInt64 ;
		} else
		if (strcmp(dataType,"Int64") == 0) {
			result = Int64::get_Int64_element(file_str,node) ;
			iDataType = OpcUaId_Int64 ;
		} else
		if (strcmp(dataType,"LocalizedText") == 0) {
			result = LocalizedText::get_LocalizedText_element(file_str,node) ;
			iDataType = OpcUaId_LocalizedText ;
		} else
		if (strcmp(dataType,"Range") == 0) {
			result = Range::get_Range_element(file_str,node) ;
			iDataType = OpcUaId_Range ;
		} else
		if (strcmp(dataType,"Argument") == 0) {
			result = Argument::get_Argument_element(file_str,node) ;
			iDataType = OpcUaId_Argument ;
		} else
		if (strcmp(dataType,"EUInformation") == 0) {
			result = EUInformation::get_EUInformation_element(file_str,node) ;
			iDataType = OpcUaId_EUInformation ;
		} else {
			debug_ss(COM_ERR,"BaseDataType","File \"%s\", Value reading error (unknown type) \"%s\"",file_str,dataType) ;
			NodesTable::print(file_str,node,1000) ;
			result = NULL ;
		}

		if (result != NULL)
			*nodeId = new NodeId(UInt16::zero,new UInt32(iDataType)) ;

		roxml_release(dataType) ;

		return result ;
	}

	UInt32 * arrayLengthObj = UInt32::get_UInt32(file_str, arrayLengthStr) ;
	roxml_release(arrayLengthStr) ;

	if (arrayLengthObj == NULL) {
		debug_ss(COM_ERR,"BaseDataType","File \"%s\", ArrayLength reading error \"%s\"",file_str,dataType) ;
		NodesTable::print(file_str,node,1000) ;
		roxml_release(dataType) ;
		return NULL ;
	}

	int arrayLength = arrayLengthObj->get() ;
	delete arrayLengthObj ;

	int nb = roxml_get_chld_nb(node) ;

	if (arrayLength > 0 && nb == 0) {
		debug_ss(COM_ERR,"BaseDataType","File \"%s\", No initialiser for array \"%s\"",file_str,dataType) ;
		NodesTable::print(file_str,node,1000) ;
		roxml_release(dataType) ;
		return NULL ;
	}

	BaseDataType * result ;

	int i = 0 ;
	int j = 0 ;

	if (strcmp(dataType,"UInt32") == 0) {
		iDataType = OpcUaId_UInt32 ;
		TableUInt32   * table = new TableUInt32  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,UInt32::get_UInt32_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Int32") == 0) {
		iDataType = OpcUaId_Int32 ;
		TableInt32   * table = new TableInt32  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Int32::get_Int32_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"UInt16") == 0) {
		iDataType = OpcUaId_UInt16 ;
		TableUInt16   * table = new TableUInt16  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,UInt16::get_UInt16_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Int16") == 0) {
		iDataType = OpcUaId_Int16 ;
		TableInt16   * table = new TableInt16  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Int16::get_Int16_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Byte") == 0) {
		iDataType = OpcUaId_Byte ;
		TableByte   * table = new TableByte  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Byte::get_Byte_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"SByte") == 0) {
		iDataType = OpcUaId_SByte ;
		TableSByte   * table = new TableSByte  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,SByte::get_SByte_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"String") == 0) {
		iDataType = OpcUaId_String ;
		TableString   * table = new TableString  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,String::get_String_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"ByteString") == 0) {
		iDataType = OpcUaId_ByteString ;
		TableByteString   * table = new TableByteString  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,ByteString::get_ByteString_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Boolean") == 0) {
		iDataType = OpcUaId_Boolean ;
		TableBoolean   * table = new TableBoolean  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Boolean::get_Boolean_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Double") == 0) {
		iDataType = OpcUaId_Double ;
		TableDouble   * table = new TableDouble  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Double::get_Double_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Float") == 0) {
		iDataType = OpcUaId_Float ;
		TableFloat   * table = new TableFloat  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Float::get_Float_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"UInt64") == 0) {
		iDataType = OpcUaId_UInt64 ;
		TableUInt64   * table = new TableUInt64  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,UInt64::get_UInt64_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Int64") == 0) {
		iDataType = OpcUaId_Int64 ;
		TableInt64   * table = new TableInt64  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Int64::get_Int64_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"LocalizedText") == 0) {
		iDataType = OpcUaId_LocalizedText ;
		TableLocalizedText   * table = new TableLocalizedText  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,LocalizedText::get_LocalizedText_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Argument") == 0) {
		iDataType = OpcUaId_Argument ;
		TableArgument   * table = new TableArgument  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Argument::get_Argument_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"Range") == 0) {
		iDataType = OpcUaId_Range ;
		TableRange   * table = new TableRange  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,Range::get_Range_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else
	if (strcmp(dataType,"EUInformation") == 0) {
		iDataType = OpcUaId_EUInformation ;
		TableEUInformation   * table = new TableEUInformation  (arrayLength) ;
		while (i < arrayLength && j < nb) {
			table->set(i,EUInformation::get_EUInformation_element(file_str,roxml_get_chld(node,NULL,j))) ;
			i++ ; j++ ;
		}
		while (i< arrayLength) {
			table->set(i,table->get(i-1)) ;
			i++ ;
		}
		result = table ;
	} else {
		debug_ss(COM_ERR,"BaseDataType","File \"%s\", Array Value reading error (unknown type) \"%s\"",file_str,dataType) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
	}

	if (result != NULL)
		*nodeId = new NodeId(UInt16::zero,new UInt32(iDataType)) ;

	roxml_release(dataType) ;

	return result ;
}

}




