
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_NAMINGRULETYPE_H_
#define OPCUA_NAMINGRULETYPE_H_

/*
 * OPC-UA, Part 3, 8.29, p. 66
 * IdType = 120
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

enum _NamingRuleType {
	NamingRuleType_MANDATORY_1  = 1,
	NamingRuleType_OPTIONAL_2   = 2,
	NamingRuleType_CONSTRAINT_3 = 3
} ;

class MYDLL NamingRuleType
		: public Enumeration
{
public:

	static NamingRuleType * mandatory_1 ;
	static NamingRuleType * optional_2 ;
	static NamingRuleType * constraint_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_NamingRuleType ; }

private:

	NamingRuleType(_NamingRuleType _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* OPCUA_NAMINGRULETYPE_H_ */
