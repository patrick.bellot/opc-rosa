
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_GUID_H_
#define OPCUA_GUID_H_

/*
 * OPC-UA Part 3, 8.16
 * OPC-UA Part 6, 5.1.3
 */

#include "../OpcUa.h"
#include "OpcUa_BaseDataType.h"
#include "OpcUa_IPCS_UInt32.h"
#include "OpcUa_IPCS_UInt16.h"
#include "OpcUa_IPCS_Byte.h"
#include "OpcUa_IPCS_Variant.h"
#include "../Utils/OpcUa_HashCode.h"
#include "../Utils/OpcUa_IPCS_Table8.h"

#include <typeinfo>

namespace opcua {

class MYDLL Guid
		: public BaseDataType
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_Guid ; }

private:

	UInt32       * data1 ;
	UInt16       * data2 ;
	UInt16       * data3 ;
	Table8Byte   * data4 ;

public:

	static Guid * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_Guid const& pSopcGuid)
	{
		UInt32       * data1 = UInt32    ::fromCtoCpp(pStatus, pSopcGuid.Data1) ;
		UInt16       * data2 = UInt16    ::fromCtoCpp(pStatus, pSopcGuid.Data2) ;
		UInt16       * data3 = UInt16    ::fromCtoCpp(pStatus, pSopcGuid.Data3) ;
		Table8Byte   * data4 = Table8Byte::fromCtoCpp_8(pStatus, pSopcGuid.Data4);

		if (*pStatus == STATUS_OK)
			return
					new Guid(
							data1,
							data2,
							data3,
							data4
							) ;

		if (data1 != NULL)
			data1->checkRefCount() ;
		if (data2 != NULL)
			data2->checkRefCount() ;
		if (data3 != NULL)
			data3->checkRefCount() ;
		if (data4 != NULL)
			data4->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_Guid& pSopcGuid) const
	{
		data1->fromCpptoC (pStatus, pSopcGuid.Data1) ;
		data2->fromCpptoC (pStatus, pSopcGuid.Data2) ;
		data3->fromCpptoC (pStatus, pSopcGuid.Data3) ;
		data4->fromCpptoC8(pStatus, pSopcGuid.Data4) ;
	}

public:

	Guid(UInt32       * _data1,
		 UInt16       * _data2,
		 UInt16       * _data3,
		 Table8Byte   * _data4)
		: BaseDataType()
	{
		(data1 = _data1) ->take()  ;
		(data2 = _data2) ->take()  ;
		(data3 = _data3) ->take()  ;
		(data4 = _data4) ->take()  ;
	}

	virtual ~Guid()
	{
		data1 ->release() ;
		data2 ->release() ;
		data3 ->release() ;
		data4 ->release() ;
	}

public:

	bool isZero()
	{
		return
				data1->isZero()
				&&
				data2->isZero()
				&&
				data3->isZero()
				&&
				data4->get(0)->isZero()
				&&
				data4->get(1)->isZero()
				&&
				data4->get(2)->isZero()
				&&
				data4->get(3)->isZero()
				;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Guid,this) ;
	}

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		hash = data1->hashCode(hash) ;
		hash = data2->hashCode(hash) ;
		hash = data3->hashCode(hash) ;
		return data4->hashCode(hash) ;
	}

public:

	bool equals(Guid * other)
	{
		return
				data1->equals(other->getData1()) &&
				data2->equals(other->getData2()) &&
				data3->equals(other->getData3()) &&
				data4->equals(other->getData4()) ;
	}

public:

	UInt32 * getData1()
	{
		return data1 ;
	}

	UInt16 * getData2()
	{
		return data2 ;
	}

	UInt16 * getData3()
	{
		return data3 ;
	}

	Table8Byte   * getData4()
	{
		return data4 ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_GUID_H_ */
