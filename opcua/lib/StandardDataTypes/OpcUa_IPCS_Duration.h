
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DURATION_H_
#define OPCUA_DURATION_H_

/*
 * OPC-AU Part 3, 8.13, p. 65
 */

#include "../OpcUa.h"
#include "OpcUa_IPCS_UInt32.h"

namespace opcua {

#define Duration Double
#define Builtin_Duration 11

} /* namespace opcua */
#endif /* OPCUA_DURATION_H_ */
