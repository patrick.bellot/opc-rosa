
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_DATETIME_H_
#define OPCUA_DATETIME_H_

/*
 * OPC-UA Part 3, 8.11,    p. 64
 * OPC-UA Part 6, 5.2.2.5, p. 10
 */

#include "../OpcUa.h"
#include "OpcUa_BaseDataType.h"

namespace opcua {

class MYDLL DateTime
		: public BaseDataType
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_DateTime ; }

private:

	uint64_t value ;

public:

	static inline DateTime * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_DateTime const& pDateTime)
	{
		NOT_USED(pStatus) ;
//		uint64_t uvalue64 = (((uint64_t)(pDateTime.High32)) << 32) + (uint64_t)(pDateTime.Low32) ;
//		return new DateTime(uvalue64) ;
		return new DateTime(pDateTime) ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_DateTime& pDateTime) const
	{
		NOT_USED(pStatus) ;
//		pDateTime.Low32  = (uint32_t)(value & 0xffffffff) ;
//		pDateTime.High32 = (uint32_t)((value >> 32) & 0xffffffff) ;
		pDateTime = value ;
	}

public:

	DateTime(uint64_t _value)
		: BaseDataType(),
		  value(_value)
	{}

	virtual ~DateTime()
	{}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_DateTime,this) ;
	}

public:

	inline uint64_t get() const
	{
		return value ;
	}

public:

	inline bool isNotZero()
	{
		return (value != 0L) ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_DATETIME_H_ */
