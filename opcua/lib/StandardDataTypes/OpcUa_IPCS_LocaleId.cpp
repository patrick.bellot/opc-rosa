
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.4, p. 63
 */

#include "OpcUa_IPCS_LocalizedText.h"

#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

LocaleId * LocaleId::get_LocaleId_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		return LocaleId::empty ;
	}

	LocaleId * result = new LocaleId(value) ;
	roxml_release(value) ;
	return result ;
}

}
