
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_NODEID_H_
#define OPCUA_NODEID_H_

/*
 * OPC-UA Part3, 8.2, p. 61
 *        Part6, 5.2.2.9, p. 11
 */

#include "../OpcUa.h"
#include "OpcUa_Structure.h"
#include "OpcUa_Enumeration.h"
#include "OpcUa_IPCS_IdType.h"
#include "OpcUa_IPCS_UInt16.h"
#include "OpcUa_IPCS_UInt32.h"
#include "OpcUa_IPCS_UInt64.h"
#include "OpcUa_IPCS_Int32.h"
#include "OpcUa_IPCS_String.h"
#include "OpcUa_IPCS_Guid.h"
#include "OpcUa_IPCS_ByteString.h"
#include "../Utils/OpcUa_HashCode.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL NodeId
		: public BaseDataType
{
public:

	static NodeId * nullNodeId ;

public:

	bool isNullNodeId()
	{
		if (namespaceIndex->isNotZero())
			return false ;

		switch (identifierType->get())
		{
		case IdType_NUMERIC_0:
			return getNumeric()->isZero() ;
		case IdType_STRING_1:
			return getString()->isNullOrEmpty() ;
		case IdType_GUID_2:
			return getGuid()->isZero() ;
		case IdType_OPAQUE_3:
			return getOpaque()->isNullOrEmpty() ;
		}

		return false ;
	}

	inline bool isNotNullNodeId()
	{
		return ! isNullNodeId() ;
	}

public:

	virtual uint32_t getTypeId() const { return OpcUaId_NodeId ; }

private:

	UInt16       * namespaceIndex ;
	IdType       * identifierType ;
	BaseDataType * identifier ;

	uint8_t		   expandedNodeId ;

public:

	inline void setExpandedNodeId(uint8_t _expandedNodeId)
	{
		expandedNodeId = _expandedNodeId ;
	}

	inline uint8_t getExpandedNodeId()
	{
		return expandedNodeId ;
	}

private:

	void _init(
			UInt16       * _namespaceIndex,
			IdType       * _identifierType,
			BaseDataType * _identifier
			)
	{
		(namespaceIndex = _namespaceIndex) ->take() ;
		(identifierType = _identifierType) ->take() ;
		(identifier     = _identifier)     ->take() ;

		expandedNodeId = (uint8_t)0 ;
	}

public:

	NodeId(uint32_t authenticationToken)
		: BaseDataType()
	{
		_init(UInt16::zero,IdType::numeric_0,new UInt32(authenticationToken)) ;
	}

	NodeId(UInt16 * _namespaceIndex,
		   UInt32 * _identifier)
		: BaseDataType()
	{
		_init(_namespaceIndex,IdType::numeric_0,_identifier) ;
	}

	NodeId(UInt16 * _namespaceIndex,
		   String * _identifier)
		: BaseDataType()
	{
		_init(_namespaceIndex,IdType::string_1,_identifier) ;
	}

	NodeId(UInt16 * _namespaceIndex,
		   Guid   * _identifier)
		: BaseDataType()
	{
		_init(_namespaceIndex,IdType::guid_2,_identifier) ;
	}

	NodeId(UInt16     * _namespaceIndex,
		   ByteString * _identifier)
		: BaseDataType()
	{
		_init(_namespaceIndex,IdType::opaque_3,_identifier) ;
	}

	virtual ~NodeId()
	{
		namespaceIndex  ->release() ;
		identifierType  ->release() ;
		identifier      ->release() ;
	}

public: // Temporary

	inline uint32_t get() const
	{
		switch(identifierType->get())
		{
		case IdType_NUMERIC_0:
			return dynamic_cast<UInt32 *>(identifier)->get() ;
		case IdType_STRING_1:
		case IdType_GUID_2:
		case IdType_OPAQUE_3:
		default:
			CHECK_INT(0) ;
		}
		return (uint32_t)0 ;
	}

public:

	static NodeId * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_NodeId const& pNodeId) ;

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_NodeId& pNodeId) const ;

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_NodeId,this) ;
	}

public:

	inline bool isNumeric()
	{
		return (identifierType->get() == IdType_NUMERIC_0) ;
	}

public:

	String * toString()
	{
		String   *  result ;
		uint16_t   _namespaceIndex = namespaceIndex->get() ;

		switch(identifierType->get())
		{
		case IdType_NUMERIC_0:
		{
			UInt32 * id = dynamic_cast<UInt32 *>(identifier) ;
			char * str = new char[32+32+32] ;
			sprintf(str,"NodeId(%d,%d)",_namespaceIndex,id->get()) ;
			result = new String(str) ;
			delete[] str ;
		} ; break ;
		case IdType_STRING_1:
		{
			String * id = dynamic_cast<String *>(identifier) ;
			const char * _str = id->get() ;
			char * str ;
			if (_str == NULL) {
				str = new char[32+32] ;
				sprintf(str,"NodeId(%d:NULL)",_namespaceIndex) ;
			} else {
				str = new char[32+32+strlen(_str)] ;
				sprintf(str,"NodeId(%d:\"%s\")",_namespaceIndex,_str) ;
			} ;
			result = new String(str) ;
			delete[] str ;
		} ; break ;
		case IdType_GUID_2:
		{
			Guid * id = dynamic_cast<Guid *>(identifier) ;
			char * str = new char[32+32+32+16+16+16] ;
			sprintf(str,"NodeId(%d:[%08x-%04x,%04x,...])",_namespaceIndex,id->getData1()->get(),id->getData2()->get(),id->getData3()->get()) ;
			result = new String(str) ;
			delete[] str ;
		} ; break ;
		case IdType_OPAQUE_3:
		{
			char * str = new char[32+32] ;
			sprintf(str,"NodeId(%d:\"...\")",_namespaceIndex) ;
			result = new String(str) ;
			delete[] str ;
		} ; break;
		default:
			result = NULL ;
			CHECK_INT(0) ;
		}

		return result ;
	}



public:

	uint32_t hashCode(uint32_t hash) const
	{
		hash = namespaceIndex->hashCode(hash) ;

		switch(identifierType->get())
		{
		case IdType_NUMERIC_0:
			return ((UInt32 *)identifier)->hashCode(hash) ;
		case IdType_STRING_1:
			return ((String *)identifier)->hashCode(hash) ;
		case IdType_GUID_2:
			return ((Guid *)identifier)->hashCode(hash) ;
		case IdType_OPAQUE_3:
			return ((ByteString *)identifier)->hashCode(hash) ;
		default:
			CHECK_INT(0) ;
		}

		return (uint32_t)0 ;
	}

public:

	bool equals(NodeId * other)
	{
		if (! getNamespaceIndex()->equals(other->getNamespaceIndex()))
			return false ;

		switch(identifierType->get())
		{
		case IdType_NUMERIC_0:
			if (other->getIdentifierType()->get() != IdType_NUMERIC_0)
				return false ;
			return getNumeric()->equals(other->getNumeric()) ;

		case IdType_STRING_1:
			if (other->getIdentifierType()->get() != IdType_STRING_1)
				return false ;
			return getString()->equals(other->getString()) ;

		case IdType_GUID_2:
			if (other->getIdentifierType()->get() != IdType_GUID_2)
				return false ;
			return getGuid()->equals(other->getGuid()) ;

		case IdType_OPAQUE_3:
			if (other->getIdentifierType()->get() != IdType_OPAQUE_3)
				return false ;
			return getOpaque()->equals(other->getOpaque()) ;

		default:
			CHECK_INT(0) ;
		}
		return (uint32_t)0 ;
	}

public:

	inline UInt16 * getNamespaceIndex() const
	{
		return namespaceIndex ;
	}

	inline IdType * getIdentifierType() const
	{
		return identifierType ;
	}

	inline UInt32 * getNumeric() const
	{
		return dynamic_cast<UInt32 *>(identifier) ;
	}

	inline String * getString() const
	{
		return (String *)identifier ;
	}

	inline Guid * getGuid() const
	{
		return dynamic_cast<Guid *>(identifier) ;
	}

	inline ByteString * getOpaque() const
	{
		return dynamic_cast<ByteString *>(identifier) ;
	}

public:

	static NodeId * get_NodeId_element(const char * const file_str, node_t * node) ;

};

} /* namespace opcua */
#endif /* OPCUA_NODEID_H_ */
