
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BYTE_H_
#define OPCUA_BYTE_H_

/*
 * OPC-UA Part 3, 8.9, p. 64
 */

#include "../OpcUa.h"
#include "OpcUa_Integer.h"
#include "OpcUa_IPCS_Boolean.h"
#include "OpcUa_IPCS_Variant.h"
#include "../Utils/OpcUa_HashCode.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL Byte
	: public Integer
{
public:

	static Byte * zero;
	static Byte * un;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Byte ; }

private:

	uint8_t  value ;

public:

	static inline Byte * fromCtoCpp(SOPC_StatusCode * pStatus, uint8_t const& value)
	{
		NOT_USED(pStatus) ;
		return new Byte(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, uint8_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtObjectBodyEncoding& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = static_cast<SOPC_ExtObjectBodyEncoding>(value) ;
	}

public:

	Byte(uint8_t _value)
		: Integer(),
		  value(_value)
	{}

	virtual ~Byte()
	{}

private:

	void setBit(Boolean * bit, int n)
	{
		if (bit != NULL && bit->isSet())
			value |= (((uint8_t)1) << n) ;
	}

public:

	Byte(
			Boolean * bit0, Boolean * bit1, Boolean * bit2, Boolean * bit3, Boolean * bit4,
			Boolean * bit5, Boolean * bit6, Boolean * bit7
		)
	{
		value = (uint8_t) 0 ;

		setBit(bit0,0) ; setBit(bit1,1) ; setBit(bit2,2) ; setBit(bit3,3) ; setBit(bit4,4) ;
		setBit(bit5,5) ; setBit(bit6,6) ; setBit(bit7,7) ;
	}


public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		return HashCode::hashCode(hash,(uint8_t *)&value,(int32_t)sizeof(uint8_t)) ;
	}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Byte,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	inline uint8_t get()
	{
		return value ;
	}

	inline void unset(uint8_t mask)
	{
		value &= (~ mask) ;
	}

	inline bool isSet(uint8_t mask)
	{
		return ((value & mask) != (uint8_t)0) ;
	}

	inline bool isUnset(uint8_t mask)
	{
		return ((value & mask) == (uint8_t)0) ;
	}

	inline bool isZero()
	{
		return (value == (uint8_t)0) ;
	}

public:

	inline void selfAnd(Byte * other)
	{
		selfAnd(other->get()) ;
	}

	inline void selfAnd(uint8_t other)
	{
		value &= other ;
	}

	inline void selfOr(Byte * other)
	{
		selfOr(other->get()) ;
	}

	inline void selfOr(uint8_t other)
	{
		value |= other ;
	}

public:

	inline bool less(uint8_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(uint8_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(uint8_t other)
	{
		return (value == other) ;
 	}

	inline bool notEquals(uint8_t other)
	{
		return (value != other) ;
 	}

	inline bool greaterOrEquals(uint8_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(uint8_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(Byte * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(Byte * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(Byte * other)
	{
		return other->equals(value) ;
 	}

	inline bool notEquals(Byte * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(Byte * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(Byte * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static Byte * get_Byte_element(const char * const file_str, node_t * node) ;

	static Byte * get_Byte(const char * const file_str, const char * const content) ;

	static Byte * get_UserAccessLevel_element(class String * browseName, const char * const file_str, node_t * node) ;

	static Byte * get_AccessLevel_element(class String * browseName, const char * const file_str, node_t * node) ;

	static Byte * get_EventNotifier_element(const char * const file_str, node_t * node) ;

};

} /* namespace opcua */
#endif /* OPCUA_BYTE_H_ */
