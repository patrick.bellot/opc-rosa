
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 3, 8.17, p. 65
 */

#include "OpcUa_IPCS_SByte.h"

#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

	SByte * SByte::get_SByte_element(const char * const file_str, node_t * node)
	{
		if (NodesTable::bad_ns(node,file_str))
			return NULL ;

		char * value = BaseDataType::get_Attribute("Value",node) ;

		if (value == NULL) {
			return SByte::zero ;
		}

		SByte * result = get_SByte(file_str,value) ;
		roxml_release(value) ;
		return result ;
	}

	SByte * SByte::get_SByte(const char * const file_str, const char * const content)
	{
		if (content == NULL) {
			debug_s(COM_ERR,"NodesTable","File \"%s\", SByte reading error (NULL value)",file_str) ;
			return NULL ;
		}

		errno = 0 ;

		int64_t value = strtoll(content,(char **)NULL,0) ;

		if (errno != 0) {
			debug_ss(COM_ERR,"NodesTable","File \"%s\", SByte reading error (not an int) \"%s\"",file_str,content) ;
			errno = 0 ;
			return NULL ;
		}

		if (static_cast<int64_t>(INT8_MIN) <= value && value <= static_cast<int64_t>(INT8_MAX)) {
			debug_ss(XML_DBG,"NodesTable","get SByte in file \"%s\", value is \"%s\"",file_str,content) ;
			return new SByte(static_cast<int8_t>(value)) ;
		}

		debug_ss(COM_ERR,"NodesTable","File \"%s\", SByte reading error (not a SByte) \"%s\"",file_str,content) ;
		return NULL ;
	}

}




