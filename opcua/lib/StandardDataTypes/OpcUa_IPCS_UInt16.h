
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UINT16_H_
#define OPCUA_UINT16_H_

/*
 * OPC-UA 3, 8.35, p. 67
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_UInteger.h"
#include "../Utils/OpcUa_HashCode.h"
#include "../Utils/OpcUa_Bits.h"
#include "OpcUa_IPCS_Variant.h"
extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL UInt16
		: public UInteger
{
public:

	static UInt16 * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_UInt16 ; }

private:

	uint16_t value ;

public:

	static inline UInt16 * fromCtoCpp(SOPC_StatusCode * pStatus, uint16_t const& value)
	{
		NOT_USED(pStatus) ;
		return new UInt16(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, uint16_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	UInt16(uint16_t _value)
		: UInteger(),
		  value(_value)
	{}

	virtual ~UInt16()
	{}

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		uint16_t regValue = Bits::to_little_endian_uint16(value) ;
		return HashCode::hashCode(hash,(uint8_t *)&regValue,(int32_t)sizeof(uint16_t)) ;
	}

public:

	inline uint16_t get()
	{
		return value ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_UInt16,this) ;
	}

public:

	inline bool isZero()
	{
		return (value == (uint16_t)0) ;
	}

	inline bool isNotZero()
	{
		return (value != (uint16_t)0) ;
	}

	inline bool less(uint16_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(uint16_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(uint16_t other)
	{
		return (value == other) ;
 	}

	inline bool greaterOrEquals(uint16_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(uint16_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(UInt16 * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(UInt16 * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(UInt16 * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(UInt16 * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(UInt16 * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static UInt16 * get_UInt16_element(const char * const file_str, node_t * node) ;

	static UInt16 * get_UInt16(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_UINT16_H_ */
