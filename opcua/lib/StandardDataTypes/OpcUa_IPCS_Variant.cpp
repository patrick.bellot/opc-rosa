
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA Part 6, 5.2.2.16, p. 16
 *                table 1,  p. 7
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/OpcUa_IPCS_StatusCode.h"
#include "../CommonParametersTypes/OpcUa_IPCS_DataValue.h"
#include "../CommonParametersTypes/OpcUa_IPCS_DiagnosticInfo.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_Variant.h"

namespace opcua {

Variant * Variant::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_Variant const& pVariant)
{
	uint32_t builtinTypeId = pVariant.BuiltInTypeId ;
	debug_i(IPCS_DBG,"Variant","fromCtoCpp: builtinTypeId = %d",builtinTypeId) ;

	if (builtinTypeId == 0)
		return NULL ;

	BaseDataType * value           = NULL ;
	TableInt32   * arrayDimensions ;

	switch (pVariant.ArrayType) {
	case SOPC_VariantArrayType_SingleValue:
		arrayDimensions = NULL ;
		break ;
	case SOPC_VariantArrayType_Array:
		arrayDimensions = NULL ;
		break ;
	case SOPC_VariantArrayType_Matrix:
		arrayDimensions = TableInt32::fromCtoCpp(pStatus,pVariant.Value.Matrix.Dimensions, pVariant.Value.Matrix.ArrayDimensions) ;
		if (*pStatus != STATUS_OK)
			return NULL ;
		break ;
	}

	int32_t dummyLength ;

	switch (builtinTypeId) {

	case Builtin_Boolean:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = TableBoolean::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.BooleanArr) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableBoolean::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.BooleanArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = Boolean::fromCtoCpp(pStatus,pVariant.Value.Boolean) ;
			break ;
		}
		break ;
	case Builtin_SByte:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = SByte::fromCtoCpp(pStatus,pVariant.Value.Sbyte) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableSByte::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.SbyteArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableSByte::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.SbyteArr) ;
			break ;
		}
		break ;
	case Builtin_Byte:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Byte::fromCtoCpp(pStatus,pVariant.Value.Byte) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableByte::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ByteArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableByte::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.ByteArr) ;
			break ;
		}
		break ;
	case Builtin_Int16:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_Int16") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Int16::fromCtoCpp(pStatus,pVariant.Value.Int16) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableInt16::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int16Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableInt16::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int16Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt16:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_UInt16") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = UInt16::fromCtoCpp(pStatus,pVariant.Value.Uint16) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableUInt16::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint16Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableUInt16::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint16Arr) ;
			break ;
		}
		break ;
	case Builtin_Int32:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_Int32") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Int32::fromCtoCpp(pStatus,pVariant.Value.Int32) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableInt32::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int32Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableInt32::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int32Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt32:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_UInt32") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = UInt32::fromCtoCpp(pStatus,pVariant.Value.Uint32) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableUInt32::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint32Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableUInt32::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint32Arr) ;
			break ;
		}
		break ;
	case Builtin_Int64:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Int64::fromCtoCpp(pStatus,pVariant.Value.Int64) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableInt64::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int64Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableInt64::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int64Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt64:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = UInt64::fromCtoCpp(pStatus,pVariant.Value.Uint64) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableUInt64::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint64Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableUInt64::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint64Arr) ;
			break ;
		}
		break ;
	case Builtin_Float:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Float::fromCtoCpp(pStatus,pVariant.Value.Floatv) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableFloat::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.FloatvArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableFloat::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.FloatvArr) ;
			break ;
		}
		break ;
	case Builtin_Double:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = Double::fromCtoCpp(pStatus,pVariant.Value.Doublev) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableDouble::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DoublevArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableDouble::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.DoublevArr) ;
			break ;
		}
		break ;
	case Builtin_String:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_String") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = String::fromCtoCpp(pStatus,pVariant.Value.String) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableString::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.StringArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableString::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.StringArr) ;
			break ;
		}
		break ;
	case Builtin_DateTime:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_DateTime") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = DateTime::fromCtoCpp(pStatus,pVariant.Value.Date) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableDateTime::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DateArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableDateTime::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.DateArr) ;
			break ;
		}
		break ;
	case Builtin_Guid:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_Guid") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.Guid != NULL)
				value = Guid::fromCtoCpp(pStatus,*pVariant.Value.Guid) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp no pVariant.Value.Guid") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableGuid::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.GuidArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableGuid::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.GuidArr) ;
			break ;
		}
		break ;
	case Builtin_ByteString:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_ByteString") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = ByteString::fromCtoCpp(pStatus,pVariant.Value.Bstring) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableByteString::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.BstringArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableByteString::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.BstringArr) ;
			break ;
		}
		break ;
	case Builtin_XmlElement:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_XmlElement (error)") ;
		*pStatus = _Bad_DecodingError ;
		break ;
	case Builtin_NodeId:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_NodeId") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.NodeId != NULL)
				value = NodeId::fromCtoCpp(pStatus,*pVariant.Value.NodeId) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.NodeId") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableNodeId::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.NodeIdArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableNodeId::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.NodeIdArr) ;
			break ;
		}
		break ;
	case Builtin_ExpandedNodeId:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_ExpandedNodeId") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.ExpNodeId != NULL)
				value = ExpandedNodeId::fromCtoCpp(pStatus,*pVariant.Value.ExpNodeId) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.ExpNodeId") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableExpandedNodeId::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ExpNodeIdArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableExpandedNodeId::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.ExpNodeIdArr) ;
			break ;
		}
		break ;
	case Builtin_StatusCode:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_StatusCode") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			value = StatusCode::fromCtoCpp(pStatus,pVariant.Value.Status) ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableStatusCode::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.StatusArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableStatusCode::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.StatusArr) ;
			break ;
		}
		break ;
	case Builtin_QualifiedName:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_QualifiedName") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.Qname != NULL)
				value = QualifiedName::fromCtoCpp(pStatus,*pVariant.Value.Qname) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.Qname") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableQualifiedName::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.QnameArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableQualifiedName::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.QnameArr) ;
			break ;
		}
		break ;
	case Builtin_LocalizedText:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_LocalizedText") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.LocalizedText != NULL)
				value = LocalizedText::fromCtoCpp(pStatus,*pVariant.Value.LocalizedText) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.LocalizedText") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableLocalizedText::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.LocalizedTextArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableLocalizedText::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.LocalizedTextArr) ;
			break ;
		}
		break ;
	case Builtin_ExtensionObject:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_ExtensionObject") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.ExtObject != NULL)
				value = ExtensionObject::fromCtoCpp(pStatus,*pVariant.Value.ExtObject) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.ExtObject") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableExtensionObject::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ExtObjectArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableExtensionObject::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.ExtObjectArr) ;
			break ;
		}
		break ;
	case Builtin_DataValue:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_DataValue") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.DataValue != NULL)
				value = DataValue::fromCtoCpp(pStatus,*pVariant.Value.DataValue) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.DataValue") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableDataValue::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DataValueArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableDataValue::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.DataValueArr) ;
			break ;
		}
		break ;
	case Builtin_Variant:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_Variant") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			debug(IPCS_ERR,"Variant","fromCtoCpp: Variant inside Variant") ;
			*pStatus = _Bad_DecodingError ;
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableVariant::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.VariantArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableVariant::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.VariantArr) ;
			break ;
		}
		break ;
	case Builtin_DiagnosticInfo:
		debug(IPCS_DBG,"Variant","fromCtoCpp: Builtin_DiagnosticInfo") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			if (pVariant.Value.DiagInfo != NULL)
				value = DiagnosticInfo::fromCtoCpp(pStatus,*pVariant.Value.DiagInfo) ;
			else {
				debug(IPCS_ERR,"Variant","fromCtoCpp: no pVariant.Value.DiagInfo") ;
				*pStatus = _Bad_DecodingError ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			value = TableDiagnosticInfo::fromCtoCpp (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DiagInfoArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			value = TableDiagnosticInfo::fromCtoCpp (pStatus, dummyLength, pVariant.Value.Matrix.Content.DiagInfoArr) ;
			break ;
		}
		break ;
	default:
		debug_i(IPCS_ERR,"Variant","fromCtoCpp: type not translated (%d)",builtinTypeId) ;
		*pStatus = _Bad_DecodingError ;
		break ;
	}

	if (*pStatus != STATUS_OK) {

		if (value != NULL)
			delete value ;
		if (arrayDimensions != NULL)
			delete arrayDimensions ;

		return NULL ;
	}

	return new Variant(builtinTypeId,value,arrayDimensions) ;
}


MYDLL void Variant::fromCpptoC(SOPC_StatusCode * pStatus, SOPC_Variant& pVariant) const
{
	debug_i(IPCS_DBG,"Variant","fromCpptoC: builtinTypeId=%d",builtinTypeId) ;

	SOPC_Variant_Initialize(&pVariant) ;

	pVariant.BuiltInTypeId = (SOPC_BuiltinId)builtinTypeId ;

	bool isMatrix = (arrayDimensions != NULL) ;
	debug_i(IPCS_DBG,"Variant","fromCpptoC: isMatrix=%d",isMatrix) ;

	bool isArray = (value->getLength() != -1) ;
	debug_i(IPCS_DBG,"Variant","fromCpptoC: isArray=%d",isArray) ;

	if (isMatrix) {
		pVariant.ArrayType = SOPC_VariantArrayType_Matrix ;
		arrayDimensions->fromCpptoC (pStatus, pVariant.Value.Matrix.Dimensions, pVariant.Value.Matrix.ArrayDimensions) ;
	} else if (isArray)
		pVariant.ArrayType = SOPC_VariantArrayType_Array ;
	else
		pVariant.ArrayType = SOPC_VariantArrayType_SingleValue ;

	int32_t dummyLength ;

	switch (builtinTypeId) {

	case Builtin_Boolean:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Boolean * const>(value)->fromCpptoC(pStatus,pVariant.Value.Boolean) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableBoolean   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.BooleanArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableBoolean   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.BooleanArr) ;
			break ;
		}
		break ;
	case Builtin_SByte:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<SByte * const>(value)->fromCpptoC(pStatus,pVariant.Value.Sbyte) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableSByte   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.SbyteArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableSByte   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.SbyteArr) ;
			break ;
		}
		break ;
	case Builtin_Byte:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Byte * const>(value)->fromCpptoC(pStatus,pVariant.Value.Byte) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableByte   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ByteArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableByte   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.ByteArr) ;
			break ;
		}
		break ;
	case Builtin_Int16:
		debug(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_Int16") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Int16 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Int16) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableInt16   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int16Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableInt16   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int16Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt16:
		debug(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_UInt16") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<UInt16 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Uint16) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableUInt16   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint16Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableUInt16   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint16Arr) ;
			break ;
		}
		break ;
	case Builtin_Int32:
		debug(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_Int32") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Int32 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Int32) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableInt32   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int32Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableInt32   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int32Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt32:
		debug_i(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_UInt32 %d",pVariant.ArrayType) ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<UInt32 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Uint32) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableUInt32   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint32Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableUInt32   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint32Arr) ;
			break ;
		}
		break ;
	case Builtin_Int64:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Int64 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Int64) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableInt64   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Int64Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableInt64   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Int64Arr) ;
			break ;
		}
		break ;
	case Builtin_UInt64:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<UInt64 * const>(value)->fromCpptoC(pStatus,pVariant.Value.Uint64) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableUInt64   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.Uint64Arr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableUInt64   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.Uint64Arr) ;
			break ;
		}
		break ;
	case Builtin_Float:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Float * const>(value)->fromCpptoC(pStatus,pVariant.Value.Floatv) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableFloat   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.FloatvArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableFloat   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.FloatvArr) ;
			break ;
		}
		break ;
	case Builtin_Double:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<Double * const>(value)->fromCpptoC(pStatus,pVariant.Value.Doublev) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableDouble   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DoublevArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableDouble   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.DoublevArr) ;
			break ;
		}
		break ;
	case Builtin_String:
		debug(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_String") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<String * const>(value)->fromCpptoC(pStatus,pVariant.Value.String) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableString   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.StringArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableString   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.StringArr) ;
			break ;
		}
		break ;
	case Builtin_DateTime:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<DateTime * const>(value)->fromCpptoC(pStatus,pVariant.Value.Date) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableDateTime   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DateArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableDateTime   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.DateArr) ;
			break ;
		}
		break ;
	case Builtin_Guid:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.Guid = (SOPC_Guid *)calloc(1,sizeof(SOPC_Guid)) ;
			reinterpret_cast<Guid * const>(value)->fromCpptoC(pStatus, *pVariant.Value.Guid) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.Guid) ;
				pVariant.Value.Guid = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableGuid   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.GuidArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableGuid   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.GuidArr) ;
			break ;
		}
		break ;
	case Builtin_ByteString:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<ByteString * const>(value)->fromCpptoC(pStatus,pVariant.Value.Bstring) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableByteString   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.BstringArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableByteString   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.BstringArr) ;
			break ;
		}
		break ;
	case Builtin_XmlElement:
		*pStatus = _Bad_EncodingError ;
		break ;
	case Builtin_NodeId:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.NodeId = (SOPC_NodeId *)calloc(1,sizeof(SOPC_NodeId)) ;
			reinterpret_cast<NodeId * const>(value)->fromCpptoC(pStatus, *pVariant.Value.NodeId) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.NodeId) ;
				pVariant.Value.NodeId = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableNodeId   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.NodeIdArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableNodeId   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.NodeIdArr) ;
			break ;
		}
		break ;
	case Builtin_ExpandedNodeId:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.ExpNodeId = (SOPC_ExpandedNodeId *)calloc(1,sizeof(SOPC_ExpandedNodeId)) ;
			reinterpret_cast<ExpandedNodeId * const>(value)->fromCpptoC(pStatus, *pVariant.Value.ExpNodeId) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.ExpNodeId) ;
				pVariant.Value.ExpNodeId = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableExpandedNodeId   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ExpNodeIdArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableExpandedNodeId   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.ExpNodeIdArr) ;
			break ;
		}
		break ;
	case Builtin_StatusCode:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			reinterpret_cast<StatusCode * const>(value)->fromCpptoC(pStatus,pVariant.Value.Status) ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableStatusCode   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.StatusArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableStatusCode   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.StatusArr) ;
			break ;
		}
		break ;
	case Builtin_QualifiedName:
		debug(IPCS_DBG,"Variant","Retour fromCpptoC: Builtin_QualifiedName") ;
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.Qname = (SOPC_QualifiedName *)calloc(1,sizeof(SOPC_QualifiedName)) ;
			reinterpret_cast<QualifiedName * const>(value)->fromCpptoC(pStatus, *pVariant.Value.Qname) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.Qname) ;
				pVariant.Value.Qname = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableQualifiedName   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.QnameArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableQualifiedName   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.QnameArr) ;
			break ;
		}
		break ;
	case Builtin_LocalizedText:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.LocalizedText = (SOPC_LocalizedText *)calloc(1,sizeof(SOPC_LocalizedText)) ;
			reinterpret_cast<LocalizedText * const>(value)->fromCpptoC(pStatus, *pVariant.Value.LocalizedText) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.LocalizedText) ;
				pVariant.Value.LocalizedText = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableLocalizedText   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.LocalizedTextArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableLocalizedText   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.LocalizedTextArr) ;
			break ;
		}
		break ;
	case Builtin_ExtensionObject:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.ExtObject = (SOPC_ExtensionObject *)calloc(1,sizeof(SOPC_ExtensionObject)) ;
			reinterpret_cast<ExtensionObject * const>(value)->fromCpptoC(pStatus, *pVariant.Value.ExtObject) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.ExtObject) ;
				pVariant.Value.ExtObject = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableExtensionObject   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.ExtObjectArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableExtensionObject   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.ExtObjectArr) ;
			break ;
		}
		break ;
	case Builtin_DataValue:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.DataValue = (SOPC_DataValue *)calloc(1,sizeof(SOPC_DataValue)) ;
			reinterpret_cast<DataValue * const>(value)->fromCpptoC(pStatus, *pVariant.Value.DataValue) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.DataValue) ;
				pVariant.Value.DataValue = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableDataValue   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DataValueArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableDataValue   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.DataValueArr) ;
			break ;
		}
		break ;
	case Builtin_Variant:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			debug(IPCS_ERR,"Variant","fromCtoCpp: Variant inside Variant")
			*pStatus = _Bad_EncodingError ;
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableVariant   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.VariantArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableVariant   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.VariantArr) ;
			break ;
		}
		break ;
	case Builtin_DiagnosticInfo:
		switch (pVariant.ArrayType) {
		case SOPC_VariantArrayType_SingleValue:
			pVariant.Value.DiagInfo = (SOPC_DiagnosticInfo *)calloc(1,sizeof(SOPC_DiagnosticInfo)) ;
			reinterpret_cast<DiagnosticInfo * const>(value)->fromCpptoC(pStatus, *pVariant.Value.DiagInfo) ;
			if (*pStatus != STATUS_OK) {
				free(pVariant.Value.DiagInfo) ;
				pVariant.Value.DiagInfo = NULL ;
			}
			break ;
		case SOPC_VariantArrayType_Array:
			reinterpret_cast<TableDiagnosticInfo   * const>(value)->fromCpptoC (pStatus, pVariant.Value.Array.Length, pVariant.Value.Array.Content.DiagInfoArr) ;
			break ;
		case SOPC_VariantArrayType_Matrix:
			reinterpret_cast<TableDiagnosticInfo   * const>(value)->fromCpptoC (pStatus, dummyLength, pVariant.Value.Matrix.Content.DiagInfoArr) ;
			break ;
		}
		break ;
	default:
		debug_i(IPCS_ERR,"Variant","fromCpptoC: type not translated (builtinTypeId = %d)",builtinTypeId) ;
		*pStatus = _Bad_EncodingError ;
		break ;
	}
}




MYDLL Variant::Variant(
		uint32_t             _builtinTypeId,
		BaseDataType       * _value
)
	: BaseDataType()
{
	builtinTypeId = _builtinTypeId ;
	(value = _value)->take() ;
	arrayDimensions = NULL ;
}

MYDLL Variant::Variant(
		uint32_t       _builtinTypeId,
		BaseDataType * _value,
		TableInt32   * _arrayDimensions
)
	: BaseDataType()
{
	builtinTypeId = _builtinTypeId ;
	(value = _value)->take() ;
	if ((arrayDimensions = _arrayDimensions))
		arrayDimensions->take() ;
}

MYDLL Variant::~Variant()
{
	value->release() ;
	if (arrayDimensions != NULL)
		arrayDimensions->release() ;
}



MYDLL double Variant::getDouble()
{
	CHECK_INT(value->getLength() == -1) ;

	switch (builtinTypeId) {
	case Builtin_Byte:
		return static_cast<double>((dynamic_cast<Byte *>(value))->get()) ;
	case Builtin_SByte:
		return static_cast<double>((dynamic_cast<SByte *>(value))->get()) ;
	case Builtin_Int16:
		return static_cast<double>((dynamic_cast<Int16 *>(value))->get()) ;
	case Builtin_UInt16:
		return static_cast<double>((dynamic_cast<UInt16 *>(value))->get()) ;
	case Builtin_Int32:
		return static_cast<double>((dynamic_cast<Int32 *>(value))->get()) ;
	case Builtin_UInt32:
		return static_cast<double>((dynamic_cast<UInt32 *>(value))->get()) ;
	case Builtin_Int64:
		return static_cast<double>((dynamic_cast<Int64 *>(value))->get()) ;
	case Builtin_UInt64:
		return static_cast<double>((dynamic_cast<UInt64 *>(value))->get()) ;
	case Builtin_Float:
		return static_cast<double>((dynamic_cast<Float *>(value))->get()) ;
	case Builtin_Double:
		return static_cast<double>((dynamic_cast<Double *>(value))->get()) ;
	}

	return 0.0 ;
}

MYDLL bool Variant::isNumeric()
{
	switch (builtinTypeId) {
	case Builtin_Byte:
	case Builtin_SByte:
	case Builtin_Int16:
	case Builtin_UInt16:
	case Builtin_Int32:
	case Builtin_UInt32:
	case Builtin_Int64:
	case Builtin_UInt64:
	case Builtin_Float:
	case Builtin_Double:
		return true ;
	}
	return false ;
}


} // namespace opcua


