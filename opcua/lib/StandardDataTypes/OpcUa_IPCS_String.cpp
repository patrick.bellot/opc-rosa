
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPC-UA Part3, 8.32, p.67
 */

#include "OpcUa_IPCS_String.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {


MYDLL Variant * String::getVariant()
{
	return new Variant(Builtin_String,this) ;
}

NodeId * String::get_NodeId_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		return NodeId::nullNodeId ;
	}

	if (StringUtils::isNumeric(value)) {
		errno = 0 ;
		int64_t ivalue = strtoll(value,(char **)NULL,0) ;
		if (errno == 0 && static_cast<int64_t>(0) <= ivalue && ivalue <= static_cast<int64_t>(UINT32_MAX)) {
			return new NodeId(UInt16::zero,new UInt32(static_cast<uint32_t>(ivalue))) ;
		}
	}

	NodeId * result = new NodeId(UInt16::zero,new String(value)) ;
	roxml_release(value) ;
	return result ;
}

String * String::get_String_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		return String::empty ;
	}

	String * result = new String(value) ;
	roxml_release(value) ;
	return result ;
}

String * String::get_File_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		debug_s(COM_ERR,"Byte","String \"%s\", File reading error : it's NULL",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		return NULL ;
	}

	String * result = new String(NodesTable::fileName(file_str,value)) ;
	roxml_release(value) ;
	return result ;
}

}
