
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIANT_H_
#define OPCUA_VARIANT_H_

/*
 * OPC-UA Part 6, 5.2.2.16, p. 16
 *                5.1.5, p. 8
 */

#include "../OpcUa.h"
#include "../OpcUa_BuiltinDefine.h"
#include "OpcUa_BaseDataType.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL Variant
		: public BaseDataType
{
public:

	virtual uint32_t getTypeId() const { return 0 ; }

private:

	uint32_t       builtinTypeId ;
	BaseDataType * value ;
	TableInt32   * arrayDimensions ;

public:

	static Variant * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_Variant const& pVariant) ;

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_Variant& pVariant) const ;

public:

	Variant(
			uint32_t       _builtinTypeId,
			BaseDataType * _value
			) ;

	Variant(
			uint32_t       _builtinTypeId,
			BaseDataType * _value,
			TableInt32   * _arrayDimensions
			) ;

	virtual ~Variant() ;

public:

	double getDouble() ;
	bool   isNumeric() ;

public:

	inline int32_t getArrayLength()
	{
		return value->getLength() ;
	}

	inline uint32_t getBuiltinTypeId()
	{
		return builtinTypeId ;
	}

	inline BaseDataType * getValue()
	{
		return value ;
	}

	inline TableInt32 * getArrayDimensions()
	{
		return arrayDimensions ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_LOCALIZEDTEXT_H_ */
