
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_IMAGEJPG_H_
#define OPCUA_IMAGEJPG_H_

/*
 * OPC-UA, Part 3, 8.22, p. 65
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Image.h"

namespace opcua {

class MYDLL ImageJPG
		: public Image
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ImageJPG ; }

public:

	ImageJPG(int32_t _length, uint8_t * _array)
		: Image(_length,_array)
	{}

};
} /* namespace opcua */
#endif /* OPCUA_IMAGEJPG_H_ */
