
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_LOCALEID_H_
#define OPCUA_LOCALEID_H_

/*
 * OPC-UA Part 3, 8.4, p. 63
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_IPCS_String.h"

namespace opcua {

class MYDLL LocaleId
		: public String
{
public:

	static LocaleId * empty ;
	static LocaleId * en ;
	static LocaleId * fr ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_LocaleId ; }

public:

	LocaleId(const char * const _str)
		: String(_str)
	{}

	LocaleId(char * const _str, int takeItForYou)
		: String(_str,takeItForYou)
	{}

public:

	static LocaleId * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_String const& pLocaleId)
	{
		NOT_USED(pStatus) ;
		if (pLocaleId.Length <= 0)
			return empty ;
		return new LocaleId((char *)pLocaleId.Data) ;
	}

public:

	static LocaleId * get_LocaleId_element(const char * const file_str, node_t * node) ;

};
} /* namespace opcua */
#endif /* OPCUA_LOCALEID_H_ */
