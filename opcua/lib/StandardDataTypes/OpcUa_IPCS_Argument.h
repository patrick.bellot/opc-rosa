
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_ARGUMENT_H_
#define OPCUA_ARGUMENT_H_

/*
 * OPC-UA Part 3, 8.6, p. 64
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int32.h"
#include "../StandardDataTypes/OpcUa_Structure.h"
#include "../StandardDataTypes/OpcUa_IPCS_String.h"
#include "../StandardDataTypes/OpcUa_IPCS_NodeId.h"
#include "../StandardDataTypes/OpcUa_IPCS_UInt32.h"
#include "../StandardDataTypes/OpcUa_IPCS_LocalizedText.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL Argument
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_Argument ; }

private:

	String        * name ;
	NodeId        * dataType ;
	Int32         * valueRank ;
	TableUInt32   * arrayDimensions ;
	LocalizedText * description ;

public:

	static Argument * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_Argument const& pArgument)
	{
		String        * name            = String        ::fromCtoCpp( pStatus, pArgument.Name) ;
		NodeId        * dataType        = NodeId        ::fromCtoCpp( pStatus, pArgument.DataType) ;
		Int32         * valueRank       = Int32         ::fromCtoCpp( pStatus, pArgument.ValueRank) ;
		TableUInt32   * arrayDimensions = TableUInt32   ::fromCtoCpp( pStatus, pArgument.NoOfArrayDimensions, pArgument.ArrayDimensions) ;
		LocalizedText * description     = LocalizedText ::fromCtoCpp( pStatus, pArgument.Description) ;

		if (*pStatus == STATUS_OK)
			return
					new Argument(
							name,
							dataType,
							valueRank,
							arrayDimensions,
							description
							) ;

		if (name != NULL)
			name->checkRefCount() ;
		if (dataType != NULL)
			dataType->checkRefCount() ;
		if (valueRank != NULL)
			valueRank->checkRefCount() ;
		if (arrayDimensions != NULL)
			arrayDimensions->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_Argument& pArgument) const
	{
		name            ->fromCpptoC (pStatus, pArgument.Name) ;
		dataType        ->fromCpptoC (pStatus, pArgument.DataType) ;
		valueRank       ->fromCpptoC (pStatus, pArgument.ValueRank) ;
		arrayDimensions ->fromCpptoC (pStatus, pArgument.NoOfArrayDimensions, pArgument.ArrayDimensions) ;
		description     ->fromCpptoC (pStatus, pArgument.Description) ;
	}

public:

	Argument(String        * _name,
			 NodeId        * _dataType,
			 Int32         * _valueRank,
			 TableUInt32   * _arrayDimensions,
			 LocalizedText * _description)
		: Structure()
	{
		(name            = _name)            ->take() ;
		(dataType        = _dataType)        ->take() ;
		(valueRank       = _valueRank)       ->take() ;
		(arrayDimensions = _arrayDimensions) ->take() ;
		(description     = _description)     ->take() ;
	}

	virtual ~Argument()
	{
	 	name           ->release()  ;
		dataType       ->release()  ;
		valueRank      ->release() ;
		arrayDimensions ->release() ;
		description    ->release() ;
	}

public:

	inline String * getName()
	{
		return name ;
	}

	inline NodeId * getDataType()
	{
		return dataType ;
	}

	inline Int32 * getValueRank()
	{
		return valueRank ;
	}

	inline TableUInt32 * getArrayDimensions()
	{
		return arrayDimensions ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}

public:

	static Argument * get_Argument_element(const char * const file_str, node_t * node) ;


};

} /* namespace opcua */
#endif /* OPCUA_ARGUMENT_H_ */
