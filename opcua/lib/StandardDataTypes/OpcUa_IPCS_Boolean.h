
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BOOLEAN_H_
#define OPCUA_BOOLEAN_H_

/*
 * OPC-UA Part 3, 8.8, p. 64
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_BaseDataType.h"
#include "OpcUa_IPCS_Variant.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL Boolean
	: public BaseDataType
{
public:

	static Boolean * booleanTrue ;
	static Boolean * booleanFalse ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Boolean ; }

private:

	uint8_t value ;

public:

	static inline Boolean * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_Boolean const& value)
	{
		NOT_USED(pStatus) ;
		return (value) ? Boolean::booleanTrue : Boolean::booleanFalse ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_Boolean& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = (SOPC_Boolean)value ;
	}

public:

	Boolean(int _value)
		: BaseDataType(),
		  value((uint8_t)(_value != 0))
	{}

	virtual ~Boolean()
	{}

public: // Must be redefined in all data that can be variant

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_Boolean,this) ;
	}

public: // Can be redefined in all base data

	static class NodeId * nodeId ;

	virtual class NodeId * getDataType()
	{
		return nodeId ;
	}

public:

	inline bool get()
	{
		return (value != 0) ;
	}

	inline bool isTrue()
	{
		return (value != 0) ;
	}

	inline bool isSet()
	{
		return (value != 0) ;
	}

public:

	static Boolean * get_Boolean_element(const char * const file_str, node_t * node) ;

	static Boolean * get_Boolean(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_BOOLEAN_H_ */
