/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPC UA Part 3, 8.6, p. 64
 */

#include "OpcUa_IPCS_Argument.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

Argument * Argument::get_Argument_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	String        * iname           = NULL ;
	NodeId        * dataType        = NULL ;
	Int32         * valueRank       = NULL ;
	TableUInt32   * arrayDimensions = NULL ;

	int nb = roxml_get_chld_nb(node) ;

	for (int i = 0 ; i < nb ; i++) {

		node_t * chld = roxml_get_chld(node,NULL,i) ;
		char   * name = roxml_get_name(chld,NULL,0) ;

		debug_ss(XML_DBG,"Argument","Configuration file \"%s\", LocalizedText element = \"%s\"",file_str,name) ;

		if (StringUtils::strcmpi(name,"Name") == 0 && iname == NULL) {
			if ((iname = String::get_String_element(file_str,chld)))
				iname->take() ;
		} else
		if (StringUtils::strcmpi(name,"DataType") == 0 && dataType == NULL) {
			if ((dataType = String::get_NodeId_element(file_str,chld)))
				dataType->take() ;
		} else
		if (StringUtils::strcmpi(name,"ValueRank") == 0 && valueRank == NULL) {
			if ((valueRank = Int32::get_Int32_element(file_str,chld)))
				valueRank->take() ;
		} else
		if (StringUtils::strcmpi(name,"ArrayDimensions") == 0 && arrayDimensions == NULL) {
			if ((arrayDimensions = UInt32::get_UInt32Array_element(file_str,chld)))
				arrayDimensions->take() ;
		} else {
			debug_ss(COM_ERR,"Argument","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"Argument\"",file_str,name) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(name) ;
	}

	Argument * result ;

	if (iname == NULL) {
		debug_s(COM_ERR,"Argument","Configuration file \"%s\", Argument has no Name element",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}

	if (dataType == NULL) {
		debug_s(COM_ERR,"Argument","Configuration file \"%s\", Argument has no DataType element",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}

	if (valueRank == NULL)
		(valueRank = Int32::minusOne)->take() ;

	if (arrayDimensions == NULL)
		(arrayDimensions = new TableUInt32  (0))->take() ;

	result
		= new Argument(
			iname,
			dataType,
			valueRank,
			arrayDimensions,
			LocalizedText::null
			) ;

error:

	if (iname != NULL)
		iname->release() ;
	if (dataType != NULL)
		dataType->release() ;
	if (valueRank != NULL)
		valueRank->release() ;
	if (arrayDimensions != NULL)
		arrayDimensions->release() ;

	return result ;
}

}
