
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SBYTE_H_
#define OPCUA_SBYTE_H_

/*
 * OPC-UA Part 3, 8.17, p. 65
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_Integer.h"
#include "OpcUa_IPCS_Variant.h"

extern "C" {
#   include "../libroxml/roxml.h"
}

namespace opcua {

class MYDLL SByte
		: public Integer
{
public:

	static SByte * zero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_SByte ; }

private:

	int8_t  value ;

public:

	static inline SByte * fromCtoCpp(SOPC_StatusCode * pStatus, int8_t const& value)
	{
		NOT_USED(pStatus) ;
		return new SByte(value) ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, int8_t& pValue) const
	{
		NOT_USED(pStatus) ;
		pValue = value ;
	}

public:

	SByte(int8_t _value)
		: Integer(),
		  value(_value)
	{}

	virtual ~SByte()
	{}

public:

	inline int8_t get()
	{
		return value ;
	}

public:

	virtual inline Variant * getVariant()
	{
		return new Variant(Builtin_SByte,this) ;
	}

public:

	inline bool less(int8_t other)
	{
		return (value < other) ;
 	}

	inline bool lessOrEquals(int8_t other)
	{
		return (value <= other) ;
 	}

	inline bool equals(int8_t other)
	{
		return (value == other) ;
 	}

	inline bool greaterOrEquals(int8_t other)
	{
		return (value >= other) ;
 	}

	inline bool greater(int8_t other)
	{
		return (value > other) ;
 	}

public:

	inline bool less(SByte * other)
	{
		return other->greaterOrEquals(value) ;
 	}

	inline bool lessOrEquals(SByte * other)
	{
		return other->greater(value) ;
 	}

	inline bool equals(SByte * other)
	{
		return other->equals(value) ;
 	}

	inline bool greaterOrEquals(SByte * other)
	{
		return other->less(value) ;
 	}

	inline bool greater(SByte * other)
	{
		return other->lessOrEquals(value) ;
 	}

public:

	static SByte * get_SByte_element(const char * const file_str, node_t * node) ;

	static SByte * get_SByte(const char * const file_str, const char * const content) ;

};

} /* namespace opcua */
#endif /* OPCUA_SBYTE_H_ */
