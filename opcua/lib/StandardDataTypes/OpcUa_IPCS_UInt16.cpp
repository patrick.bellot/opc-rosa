
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC-UA 3, 8.25, p. 66
 */

#include "OpcUa_IPCS_UInt16.h"

#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

UInt16 * UInt16::get_UInt16_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	char * value = BaseDataType::get_Attribute("Value",node) ;

	if (value == NULL) {
		return UInt16::zero ;
	}

	UInt16 * result = get_UInt16(file_str,value) ;
	roxml_release(value) ;
	return result ;
}

UInt16 * UInt16::get_UInt16(const char * const file_str, const char * const content)
{
	if (content == NULL) {
		debug_s(COM_ERR,"NodesTable","File \"%s\", UInt16 reading error (NULL value)",file_str) ;
		return NULL ;
	}

	errno = 0 ;

	uint64_t value = strtoull(content,(char **)NULL,0) ;

	if (errno != 0) {
		debug_ss(COM_ERR,"NodesTable","File \"%s\", UInt16 reading error (not an int) \"%s\"",file_str,content) ;
		errno = 0 ;
		return NULL ;
	}

	if (value <= static_cast<uint64_t>(UINT16_MAX)) {
		debug_ss(XML_DBG,"NodesTable","get UInt16 in file \"%s\", value is \"%s\"",file_str,content) ;
		return new UInt16(static_cast<uint16_t>(value)) ;
	}

	debug_ss(COM_ERR,"NodesTable","File \"%s\", UInt16 reading error (not a UInt16) \"%s\"",file_str,content) ;
	return NULL ;
}

}


