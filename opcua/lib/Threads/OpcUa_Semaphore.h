
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUASEMAPHORE_H_
#define OPCUASEMAPHORE_H_

#include "../OpcUa.h"

#include "../Utils/OpcUa_Alea.h"
#include "../Utils/OpcUa_Clock.h"

#ifdef _WIN32
# include "Windows.h"
# include <process.h>
#else
# include <unistd.h>    /* getpid() */
# include <time.h>      /* time() */
# include <fcntl.h>     /* For O_* constants */
# include <semaphore.h>
# include <sys/types.h>
#endif

namespace opcua
{

//extern int           x_total ;
//extern int           x_current ;
//extern class Mutex * x_mutex ;

class MYDLL Semaphore
{
private:

#ifdef _WIN32
	HANDLE   semaphore ;
#else
	sem_t  * psemaphore ;
#endif

	char sem_name[64] ;

public:

	Semaphore(int val)
	{
#ifdef _WIN32
		semaphore = CreateSemaphore(NULL,val,(long)30000,NULL) ;
		CHECK_LIB(semaphore != NULL) ;
#else
		sprintf(sem_name,"OPC-ROSA-%08x-%08x",getpid(),Alea::alea->random_uint32()) ;
		psemaphore = sem_open(sem_name,O_CREAT | O_EXCL,0x777,val) ;
		// try to close the semaphore first if open failed
		if ( psemaphore == (sem_t *)SEM_FAILED)
		{
		    sem_unlink(sem_name);
		    psemaphore = sem_open(sem_name,O_CREAT | O_EXCL,0x777,val) ;
			CHECK_LIB(psemaphore != (sem_t *)SEM_FAILED) ;
		}
#endif
	}

	virtual ~Semaphore()
	{
#ifdef _WIN32
		CHECK_LIB(CloseHandle(semaphore) != 0) ;
#else
		CHECK_LIB(sem_close(psemaphore) == 0) ;
		sem_unlink(sem_name) ;
#endif
	}

public:

	void post()
	{
#ifdef _WIN32
		CHECK_LIB(ReleaseSemaphore(semaphore,1,NULL) != 0) ;
#else
		CHECK_LIB(sem_post(psemaphore) == 0) ;
#endif
	}

	void wait()
	{
#ifdef _WIN32
		CHECK_LIB(WaitForSingleObject(semaphore,INFINITE) == WAIT_OBJECT_0) ;
#else
		CHECK_LIB(sem_wait(psemaphore) == 0) ;
#endif
	}

	bool wait(uint32_t timeoutMillis)
	{

#ifdef _WIN32
		return (WaitForSingleObject(semaphore,timeoutMillis) == WAIT_OBJECT_0) ;
#else

#   ifdef __APPLE__
		struct timespec fiveMillisecond ;
		fiveMillisecond.tv_sec  = 0 ;
		fiveMillisecond.tv_nsec = 5000000 ;
		uint64_t lastTime = Clock::get_millisecondssince1601() + timeoutMillis ;
		do {
			if (sem_trywait(psemaphore) == 0)
				return true ;
			CHECK_INT(nanosleep(&fiveMillisecond,NULL) == 0) ;
		} while (Clock::get_millisecondssince1601() < lastTime) ;
		return false ;
#   else
		struct timespec duration ;
		if (clock_gettime(CLOCK_REALTIME, &duration) == -1){
			return false;
		}
		duration.tv_sec  += timeoutMillis / 1000 ;
		duration.tv_nsec = (timeoutMillis % 1000) * 1000000 ;
		return (sem_timedwait(psemaphore,&duration) == 0) ;
#   endif

#endif
}

} ;

} /* namespace opcua */
#endif /* OPCUASEMAPHORE_H_ */
