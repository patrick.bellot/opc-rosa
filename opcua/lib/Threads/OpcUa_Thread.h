
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_THREAD_H_
#define OPCUA_THREAD_H_

#include "../OpcUa.h"

#ifdef _WIN32
# include <windows.h>
#else
# include <pthread.h>
#endif

namespace opcua
{

class MYDLL Thread
{
private:

	static void * run_the_run_method(void * arg)
	{
		((Thread *)arg)->run() ;
		return (void *)0 ;
	}

private:

#ifdef _WIN32
	HANDLE    thread ;
#else
	pthread_t thread ;
#endif

public:
	Thread()
#ifdef _WIN32
		: thread(NULL)
#else
		: thread(0)
#endif
	{
#ifdef _WIN32
#else
#endif
	}

	virtual ~Thread()
	{
#ifdef _WIN32
		if (thread != NULL)
			CloseHandle(thread) ;
#else
#endif

	}

public:

	virtual void run() = 0 ;

public:

	inline void start()
	{
		_start(&Thread::run_the_run_method) ;
	}

private:

	void _start(void * (*f)(void *))
	{
#ifdef _WIN32
		thread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)f,this,0,NULL) ;
		CHECK_LIB(thread != NULL) ;
#else
		CHECK_LIB(pthread_create(&thread,NULL,f,this) == 0) ;
#endif
	}

public:

	void join()
	{
#ifdef _WIN32
		CHECK_LIB(WaitForSingleObject(thread,INFINITE) == WAIT_OBJECT_0) ;
#else
		CHECK_LIB(pthread_join(thread,NULL) == 0) ;
#endif
	}

};

} /* namespace opcua */
#endif /* OPCUA_THREAD_H_ */
