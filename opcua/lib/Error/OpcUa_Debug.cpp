
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "../Error/OpcUa_Debug.h"
#include "../Utils/OpcUa_Clock.h"

#ifdef _WIN32
# include <Windows.h>
#include <cmath>
#else
# include <pthread.h>
#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#ifdef __GNUC__
__extension__
#endif

namespace opcua
{

#if SIEM_LOG==1
MYDLL const char * Debug::to_litteral(uint32_t log2flag)
{
	switch (log2flag) {
	case  0:	return "FATAL"   ; // COM_ERR            0x00000001  /* True error */
	case  1:	return "INFO"    ; // MAIN_LIFE_DBG      0x00000002  /* Everyday's life */
	case  2:	return "INFO"    ; // COM_DBG            0x00000004  /* Communication debug */
	case  3:	return "DEBUG"   ; // COM_DBG_DETAILED   0x00000008  /* Communication debug, very detailed */
	case  4:	return "DEBUG"   ; // MEM_DBG            0x00000010  /* Memory debug */
	case  5:	return "INFO"    ; // SERVER_DBG         0x00000020  /* Server debug */
	case  6:	return "DEBUG"   ; // S2OPC_DBG          0x00000040  /* Stack usage debug */
	case  7:	return "INFO"    ; // PROC_INFO          0x00000080  /* Process info */
	case  8:	return "INFO"    ; // DLL_DBG            0x00000100  /* DLL debugging */
	case  9:	return "INFO"    ; // XML_DBG            0x00000200  /* XML debugging*/
	case 10:	return "INFO"    ; // REF_DBG            0x00000400  /* References debugging (printing) */
	case 11:	return "INFO"    ; // IO_DBG             0x00000800  /* Just in and out message */
 	case 12:	return "DEBUG"   ; // SELF_DBG			 0x00001000  /* General debugging */
 	case 13:	return "DEBUG"   ; // ARRAY_DBG          0x00002000  /* Getting and setting array variable */
 	case 14:	return "INFO"    ; // MTH_DBG            0x00004000  /* Methods debugging */
 	case 15:	return "INFO"    ; // SUB_DBG            0x00008000  /* Subscription debugging */
 	case 16:	return "FATAL"   ; // S2OPC_ERR          0x00010000  /* Stack usage error */
 	case 17:	return "WARNING" ; // SEC_DBG            0x00020000  /* Security debugging */
 	case 18:	return "INFO"    ; // BRO_DBG            0x00040000  /* Browse debugging */
 	case 19:	return "INFO"    ; // CLIENT_DBG         0x00080000  /* Client bug */
 	case 20:	return "DEBUG"   ; // BYTE_DBG		     0x00100000  /* for bytes display */
 	case 21:	return "UNKW"    ; // 0x00200000
 	case 22:	return "DEBUG"   ; // HASH_DBG           0x00400000  /* debugging of hash table */
 	case 23:	return "INFO"    ; // BUG_DBG            0x00800000  /* Debugging great bugs */
	case 24:	return "FATAL"   ; // IPCS_ERR           0x01000000  /* Error in manipulating stack */
 	case 25:	return "DEBUG"   ; // IPCS_DBG           0x02000000  /* Debug in manipulating stack */
 	case 26:	return "DEBUG"   ; // STATIC_DBG         0x04000000  /* Static variable initialization */
  	case 27:	return "UNKW"    ; // 0x0800000
 	case 28:	return "DEBUG"   ; // BIN_DBG_DETAIL     0x10000000  /* Detailed debug of binary loading */
 	case 29:	return "DEBUG"   ; // BIN_DBG 	         0x20000000  /* Debug of binary loading */
 	case 30:	return "DEBUG"   ; // DISC_DBG		     0x40000000  /* Discovery debugging */
 	case 31:	return "DEBUG"   ; // SESSION_DBG        0x80000000  /* Session debugging */
 	default:	return "UNDEF"   ;
	}
}
#endif

#if SIEM_LOG==1
MYDLL const char * Debug::to_typology(uint32_t log2flag)
{
	switch (log2flag) {
	case  0:	return "COM_ERR"           ; // COM_ERR            0x00000001  /* True error */
	case  1:	return "MAIN_LIFE_DBG"     ; // MAIN_LIFE_DBG      0x00000002  /* Everyday's life */
	case  2:	return "COM_DBG"           ; // COM_DBG            0x00000004  /* Communication debug */
	case  3:	return "COM_DBG_DETAILED"  ; // COM_DBG_DETAILED   0x00000008  /* Communication debug, very detailed */
	case  4:	return "MEM_DBG"           ; // MEM_DBG            0x00000010  /* Memory debug */
	case  5:	return "SERVER_DBG"        ; // SERVER_DBG         0x00000020  /* Server debug */
	case  6:	return "S2OPC_DBG"         ; // SER_DBG            0x00000040  /* Stack usage debug */
	case  7:	return "PROC_INFO"         ; // PROC_INFO          0x00000080  /* Process info */
	case  8:	return "DLL_DBG"           ; // DLL_DBG            0x00000100  /* DLL debugging */
	case  9:	return "XML_DBG"           ; // XML_DBG            0x00000200  /* XML debugging*/
	case 10:	return "REF_DBG"           ; // REF_DBG            0x00000400  /* References debugging (printing) */
	case 11:	return "IO_DBG"            ; // IO_DBG             0x00000800  /* Just in and out message */
 	case 12:	return "SELF_DBG"          ; // SELF_DBG		   0x00001000  /* General debugging */
 	case 13:	return "ARRAY_DBG"         ; // ARRAY_DBG          0x00002000  /* getting and setting array variable */
 	case 14:	return "MTH_DBG"           ; // MTH_DBG            0x00004000  /* Methods debugging */
 	case 15:	return "SUB_DBG"           ; // SUB_DBG            0x00008000  /* Subscription debugging */
 	case 16:	return "S2OPC_ERR"         ; // S2OPC_ERR          0x00010000  /* Stack usage error */
 	case 17:	return "SEC_DBG"           ; // SEC_DBG            0x00020000  /* Security debugging */
 	case 18:	return "BRO_DBG"           ; // BRO_DBG            0x00040000  /* Browse debugging */
 	case 19:	return "CLIENT_DBG"        ; // CLIENT_DBG         0x00080000  /* Client bug */
 	case 20:	return "BYTE_DBG"          ; // BYTE_DBG		   0x00100000  /* for bytes display */
 	case 21:	return "UNKW"              ; // 0x0020000
 	case 22:	return "HASH_DBG"          ; // HASH_DBG           0x00400000  /* debugging of hash table */
 	case 23:	return "BUG_DBG"           ; // BUG_DBG            0x00800000  /* Debugging great bugs */
 	case 24:	return "IPCS_ERR"          ; // IPCS_ERR           0x01000000  /* Error in manipulating stack */
 	case 25:	return "IPCS_DBG"          ; // IPCS_DBG     	   0x02000000  /* Debug in manipulating stack */
 	case 26:	return "STATIC_DBG"        ; // STATIC_DBG         0x04000000  /* Static variable initialization */
 	case 27:	return "UNKW"              ; // 0x08000000
 	case 28:	return "BIN_DBG_DETAIL"    ; // BIN_DBG_DETAIL     0x10000000  /* Detailed debug of binary loading */
 	case 29:	return "BIN_DBG"           ; // BIN_DBG 	       0x20000000  /* Debug of binary loading */
 	case 30:	return "DISC_DBG"          ; // DISC_DBG		   0x40000000  /* Discovery debugging */
 	case 31:	return "SESSION_DBG"       ; // SESSION_DBG        0x80000000  /* Session debugging */
 	default:	return "UNDEF"             ;
	}
}
#endif

/*
 * The log file.
 */

#if SIEM_LOG==1
MYDLL char       * Debug::siem_file_name = NULL ;
MYDLL Mutex      * Debug::siem_mutex = new Mutex() ;
MYDLL FILE       * Debug::siem_file = NULL ;
MYDLL uint64_t     Debug::siem_line_count = 0 ;
#endif


#if SIEM_LOG==1
MYDLL void Debug::init_siem_file_name(const char *ip, const char *port)
{
	siem_file_name = new char[strlen(ip)+strlen(port)+128] ;
	sprintf(siem_file_name,"opc-ua_%s_%s.log",ip,port) ;
}
#endif


#if SIEM_LOG==1
MYDLL void Debug::lock_siem_file()
{
	siem_mutex->lock() ;

	if (siem_file == NULL) {
		siem_file = fopen(siem_file_name,"r") ;
		siem_line_count = 0 ;
		if (siem_file != NULL) {
			char c ;
			while ((c = fgetc(siem_file)) != EOF) {
				if (c == '\n')
					siem_line_count++ ;
			}
			fclose(siem_file) ;
		}
		siem_file = fopen(siem_file_name,"a") ;
	}
}
#endif


#if SIEM_LOG==1
MYDLL void Debug::unlock_siem_file()
{
	if (siem_line_count  >= MAX_SIEM_LINE_COUNT) {
		if (siem_file != NULL) {
			fclose(siem_file) ;
			siem_file = NULL ;
			char * new_siem_file_name = new char[strlen(siem_file_name) + 128] ;
			if (SHIFT_SIEM_LOG ==1) {
				char date[256] ;
				Clock::getLocalDate(date,256) ;
				sprintf(new_siem_file_name,"%s_%s",date, siem_file_name) ;
			} else {
				sprintf(new_siem_file_name,"copy_%s",siem_file_name) ;
			}
			char * c = new_siem_file_name ;
			while (*c != '\0') {
				if (*c == ' ' || *c == '/' || *c == ':')
					*c = '_' ;
				c++ ;
			}
			if (rename(siem_file_name,new_siem_file_name)) {
				fprintf(stderr,"Cannot rename \"%s\" in \"%s\".\n",siem_file_name,new_siem_file_name) ;
				fflush(stderr) ;
			}
			delete[] new_siem_file_name ;
		}
		siem_line_count = 0 ;
	}

	siem_mutex->unlock() ;
}
#endif


#if SIEM_LOG==1
MYDLL void Debug::print_siem(
			      char * date,
			const char * category,
			const char * typology,
			const void * thread_id,
			const char * app,
			const char * msg)
{
	lock_siem_file() ;
	if (siem_file != NULL) {
		fprintf(siem_file,"%s : %s : %s : (%p,%s) : %s\n",date,category,typology,thread_id,app,msg) ;
		siem_line_count++ ;
	}
	fflush(siem_file) ;
	unlock_siem_file() ;
}
#endif


#ifdef _WIN32
double Log2( float flag )
{
    return log( flag ) / log(2.);
}
#endif

MYDLL void Debug::_debug(uint32_t flag, const char * const app, const char * const msg)
{
#if SIEM_LOG==1
#ifdef _WIN32
	uint32_t log2flag = (int)Log2((float)flag) ;
#else
	uint32_t log2flag = (int)log2((float)flag) ;
#endif
	char date[128] ;
	Clock::getLocalDate(date,128) ;
#  ifdef _WIN32
	print_siem(date,to_litteral(log2flag),to_typology(log2flag),(void *)GetCurrentThreadId(),app,msg) ;
#  else
	print_siem(date,to_litteral(log2flag),to_typology(log2flag),(void *)pthread_self(),app,msg) ;
#  endif
#else
#  ifdef _WIN32
	fprintf(stderr,
			"%llu : %16p : %24s : %s\n",
			(long long unsigned)Clock::get_microsecondssince1601(),
			(void *)GetCurrentThreadId(),
			app,
			msg) ;
#  else
	fprintf(stderr,
			"%llu : %16p : %24s : %s\n",
			(long long unsigned)Clock::get_microsecondssince1601(),
			(void *)pthread_self(),
			app,
			msg) ;
#  endif
	fflush(stderr) ;
	NOT_USED(flag) ;
#endif
}

MYDLL char * Debug::_com_error_message(int const err_num)
{
#ifdef _WIN32
	char * str_error ;
	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			err_num,
			0,
			(LPSTR)(&str_error),
			0,
			NULL) ;
	return str_error ;
#else
	return strerror(err_num) ;
#endif
}

MYDLL void Debug::_debug_l(uint32_t flag, const char * const app, const char * const msg, long long const par1)
{
	char *msgaux = new char[strlen(msg)+512] ;
	sprintf(msgaux,msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_i(uint32_t flag, const char * const app, const char * const msg, int const par1)
{
	char *msgaux = new char[strlen(msg)+512] ;
	sprintf(msgaux,msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_s(uint32_t flag, const char * const app, const char * const msg, const char * const par1)
{
	char *msgaux = new char[strlen(msg)+strlen(par1)+512] ;
	sprintf(msgaux,msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_ss(uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2)
{
	char *msgaux = new char[strlen(msg)+strlen(par2)+512] ;
	sprintf(msgaux,msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_sss(uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2, const char * const par3)
{
	char *msgaux = new char[strlen(msg)+strlen(par1)+strlen(par2)+strlen(par3)+512] ;
	sprintf(msgaux,msg,par1,par2,par3) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_ssss(uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2, const char * const par3, const char * const par4)
{
	char *msgaux = new char[strlen(msg)+strlen(par1)+strlen(par2)+strlen(par3)+strlen(par4)+512] ;
	sprintf(msgaux,msg,par1,par2,par3,par4) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_p(uint32_t flag, const char * const app, const char * const msg, const void * const par1)
{
	char *msgaux = new char[strlen(msg)+512] ;
	sprintf(msgaux,msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_pp(uint32_t flag, const char * const app, const char * const msg, const void * const par1, const void * const par2)
{
	char *msgaux = new char[strlen(msg)+512] ;
	sprintf(msgaux,msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_ccc(uint32_t flag, const char * const app, const char * const msg, char par1, char par2, char par3)
{
	char *msgaux = new char[strlen(msg)+512] ;
	sprintf(msgaux,msg,par1,par2,par3) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_ii(uint32_t flag, const char * const app, const char * const msg, int const par1, int const par2)
{
	char *msgaux = new char[strlen(msg)+1024] ;
	sprintf(msgaux,msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_si(uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2)
{
	char * msgaux = new char[strlen(msg)+strlen(par1)+512] ;
	sprintf(msgaux,msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_is(uint32_t flag, const char * const app, const char * const msg, int const par1, const char * const par2)
{
	char * msgaux = new char[strlen(msg)+512+strlen(par2)] ;
	sprintf(msgaux,msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_sii(uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2, int const par3)
{
	char * msgaux = new char[strlen(msg)+strlen(par1)+512] ;
	sprintf(msgaux,msg,par1,par2,par3) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}

MYDLL void Debug::_debug_com(uint32_t flag, const char * const app, const char * const msg)
{
#ifdef _WIN32
	char * err_msg = Debug::_com_error_message(WSAGetLastError()) ;
#else
	char * err_msg = Debug::_com_error_message(errno) ;
#endif

	char * msgaux = new char[strlen(msg)+strlen(err_msg)+512] ;
	sprintf(msgaux,"%s -> %s",msg,err_msg) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;

#ifdef _WIN32
	LocalFree(err_msg) ;
#endif
}

MYDLL void Debug::_debug_com_s(uint32_t flag, const char * const app, const char * const msg, const char * const par1)
{
#ifdef _WIN32
	char * err_msg = Debug::_com_error_message(WSAGetLastError()) ;
#else
	char * err_msg = Debug::_com_error_message(errno) ;
#endif

	char * msgaux = new char[strlen(msg)+strlen(err_msg)+strlen(par1)+512] ;
	sprintf(msgaux,"%s -> %s -> %s",msg,err_msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;

#ifdef _WIN32
	LocalFree(err_msg) ;
#endif
}

MYDLL void Debug::_debug_com_i(uint32_t flag, const char * const app, const char * const msg, int const par1)
{
#ifdef _WIN32
	char * err_msg = Debug::_com_error_message(WSAGetLastError()) ;
#else
	char * err_msg = Debug::_com_error_message(errno) ;
#endif

	char * msgaux = new char[strlen(msg)+strlen(err_msg)+512] ;
	sprintf(msgaux,"%s -> %s -> %d",msg,err_msg,par1) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;

#ifdef _WIN32
	LocalFree(err_msg) ;
#endif
}

MYDLL void Debug::_debug_com_si(uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2)
{
#ifdef _WIN32
	char * err_msg = Debug::_com_error_message(WSAGetLastError()) ;
#else
	char * err_msg = Debug::_com_error_message(errno) ;
#endif

	char * msgaux = new char[strlen(msg)+strlen(err_msg)+strlen(par1)+512] ;
	sprintf(msgaux,"%s -> %s -> %s -> %d",msg,err_msg,par1,par2) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;

#ifdef _WIN32
	LocalFree(err_msg) ;
#endif
}

MYDLL void Debug::_debug_iii(uint32_t flag, const char * const app, const char * const msg, int const par1, int const par2, int const par3)
{
	char *msgaux = new char[strlen(msg)+1024] ;
	sprintf(msgaux,msg,par1,par2,par3) ;
	_debug(flag,app,msgaux) ;
	delete[] msgaux ;
}


} /* namespace opcua */
