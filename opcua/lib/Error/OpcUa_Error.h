
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_ERROR_H_
#define _OPCUA_ERROR_H_

#include "../OpcUa.h"

/*
 * Ways to write the error messages and EXITing the program.
 * Used by Check.h.
 */

namespace opcua
{

class MYDLL Error
{
public:

	static int int_error(const char * const file, int line) ;

	static int lib_error(const char * const file, int line) ;

	static int com_error(const char * const file, int line) ;

private:

	static int _error(const char * const type, const char* const file, int line, int error_num) ;

public:

	static void error_message(const char * const type, const char* const file, int line, int error_num) ;
};

} /* namespace opcua */
#endif /* UAERROR_H_ */
