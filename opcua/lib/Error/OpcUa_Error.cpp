
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "../Error/OpcUa_Error.h"

#ifdef _WIN32
# include <Windows.h>
#endif

#include <string.h>
#include <errno.h>

namespace opcua
{

/*
 * The generic error message.
 */

MYDLL void Error::error_message(const char * const type, const char * const file, int line, int error_num)
{
	char message[10*1024] ;

#ifdef _WIN32
	char * str_error ;
	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL,
			error_num,
			0,
			(LPSTR)(&str_error),
			0,
			NULL) ;
	sprintf(message,"%s %d: file %s, line %d, error \"%s",type,error_num,file,line,str_error) ;
	message[strlen(message)-2]='"' ;
	message[strlen(message)-1]='\0' ;
	LocalFree(str_error) ;
#else
	sprintf(message,"%s %d: file %s, line %d, error \"%s\"",type,error_num,file,line,strerror(error_num)) ;
#endif

	debug(COM_ERR,"Error",message) ;
}

/*
 * The generic error message then EXIT.
 */

MYDLL int Error::_error(const char * const type, const char* const file, int line, int error_num)
{
	error_message(type,file,line,error_num) ;
	return 0 ; /* to comply with the || in the CHECK_* macros */
}

/*
 * An error in a system library.
 */

MYDLL int Error::lib_error(const char* const file, int32_t line)
{
#ifdef _WIN32
	return Error::_error("OS Error",file,line,GetLastError()) ;
#else
	return Error::_error("OS Error",file,line,errno) ;
#endif
}

/*
 * An error in communication.
 */

MYDLL int Error::com_error(const char* const file, int32_t line)
{
#ifdef _WIN32
	return Error::_error("Socket Error",file,line,WSAGetLastError()) ;
#else
	return Error::_error("Socket Error",file,line,errno) ;
#endif
}

/*
 * An internal error of the program.
 */

MYDLL int Error::int_error(const char * const file, int line)
{
	debug_si(COM_ERR,"Error","Internal Error: file %s, line %d\n",file,line) ;
	return 0 ; /* to comply with the || in the CHECK_* macros */
}

} /* namespace opcua */
