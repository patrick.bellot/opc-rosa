
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DEBUG_H_
#define OPCUA_DEBUG_H_

#include "../OpcUa.h"
#include "../Threads/OpcUa_Mutex.h"

namespace opcua
{

/*
 * Used by ../opcua.h.
 */

class MYDLL Debug
{
#if SIEM_LOG==1
public:

	static void init_siem_file_name(const char *ip, const char *port) ;

private:

	static const char * to_litteral (uint32_t log2flag) ;
	static const char * to_typology (uint32_t log2flag) ;

	static char     * siem_file_name ;
	static Mutex    * siem_mutex ;
	static FILE     * siem_file ;
	static uint64_t   siem_line_count ;

	static void lock_siem_file() ;
	static void unlock_siem_file() ;

	static void print_siem(
			      char * date,
			const char * category,
			const char * typologie,
			const void * thread_id,
			const char * app,
			const char * msg) ;
#endif

private:

	static char * _com_error_message(int err_num) ;

public:

	static void _debug      (uint32_t flag, const char * const app, const char * const msg) ;
	static void _debug_l    (uint32_t flag, const char * const app, const char * const msg, long long const par1) ;
	static void _debug_i    (uint32_t flag, const char * const app, const char * const msg, int const par1) ;
	static void _debug_s    (uint32_t flag, const char * const app, const char * const msg, const char * const par1) ;
	static void _debug_ss   (uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2) ;
	static void _debug_sss  (uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2, const char * const par3) ;
	static void _debug_ssss (uint32_t flag, const char * const app, const char * const msg, const char * const par1, const char * const par2, const char * const par3, const char * const par4) ;
	static void _debug_p    (uint32_t flag, const char * const app, const char * const msg, const void * const par1) ;
	static void _debug_pp   (uint32_t flag, const char * const app, const char * const msg, const void * const par1, const void * const par2) ;
	static void _debug_ii   (uint32_t flag, const char * const app, const char * const msg, int const par1, int const par2) ;
	static void _debug_si   (uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2) ;
	static void _debug_is   (uint32_t flag, const char * const app, const char * const msg, int const par1, const char * const par2) ;
	static void _debug_ccc  (uint32_t flag, const char * const app, const char * const msg, const char par1, const char par2, const char par3) ;
	static void _debug_iii  (uint32_t flag, const char * const app, const char * const msg, int const  par1, int const  par2, int const par3) ;
	static void _debug_sii  (uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2, int const par3) ;

	static void _debug_com     (uint32_t flag, const char * const app, const char * const msg) ;
	static void _debug_com_i   (uint32_t flag, const char * const app, const char * const msg, int const par1) ;
	static void _debug_com_s   (uint32_t flag, const char * const app, const char * const msg, const char * const par1) ;
	static void _debug_com_si  (uint32_t flag, const char * const app, const char * const msg, const char * const par1, int const par2) ;

};

} /* namespace opcua */
#endif /* OPCUA_DEBUG_H_ */
