
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SIMPLEATTRIBUTEOPERAND_H_
#define OPCUA_SIMPLEATTRIBUTEOPERAND_H_

/*
 * OPC-UA Part 4, 7.4.4.5, p. 116
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_FilterOperand.h"

namespace opcua {

class MYDLL SimpleAttributeOperand
	: public FilterOperand
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_SimpleAttributeOperand ; }

private:

	NodeId               * typeId ;
	TableQualifiedName   * browsePath ;
	IntegerId            * attributeId ;
	NumericRange         * indexRange ;

public:

	static SimpleAttributeOperand * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_SimpleAttributeOperand const& pSimpleAttributeOperand)
	{
		NodeId               * typeId      = NodeId             ::fromCtoCpp(pStatus,pSimpleAttributeOperand.TypeDefinitionId) ;
		TableQualifiedName   * browsePath  = TableQualifiedName ::fromCtoCpp(pStatus,pSimpleAttributeOperand.NoOfBrowsePath,pSimpleAttributeOperand.BrowsePath) ;
		IntegerId            * attributeId = IntegerId          ::fromCtoCpp(pStatus,pSimpleAttributeOperand.AttributeId) ;
		NumericRange         * indexRange  = NumericRange       ::fromCtoCpp(pStatus,pSimpleAttributeOperand.IndexRange) ;

		if (*pStatus == STATUS_OK)
			return
					new SimpleAttributeOperand(
							typeId,
							browsePath,
							attributeId,
							indexRange
							) ;

		if (typeId != NULL)
			typeId->checkRefCount() ;
		if (browsePath != NULL)
			browsePath->checkRefCount() ;
		if (attributeId != NULL)
			attributeId->checkRefCount() ;
		if (indexRange != NULL)
			indexRange->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pSimpleAttributeOperand) const
	{
		expandedNodeId->fromCpptoC(pStatus,pSimpleAttributeOperand.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pSimpleAttributeOperand.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pSimpleAttributeOperand.Body.Object.ObjType = &OpcUa_SimpleAttributeOperand_EncodeableType ;
			pSimpleAttributeOperand.Body.Object.Value = (void *)malloc(sizeof(OpcUa_SimpleAttributeOperand)) ;
			OpcUa_SimpleAttributeOperand_Initialize(pSimpleAttributeOperand.Body.Object.Value) ;
			SimpleAttributeOperand::fromCpptoC(pStatus,*static_cast<OpcUa_SimpleAttributeOperand *>(pSimpleAttributeOperand.Body.Object.Value)) ;
			pSimpleAttributeOperand.Length = sizeof(OpcUa_SimpleAttributeOperand) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_SimpleAttributeOperand& pSimpleAttributeOperand) const
	{
		typeId      ->fromCpptoC(pStatus,pSimpleAttributeOperand.TypeDefinitionId) ;
		browsePath  ->fromCpptoC(pStatus,pSimpleAttributeOperand.NoOfBrowsePath,pSimpleAttributeOperand.BrowsePath) ;
		attributeId ->fromCpptoC(pStatus,pSimpleAttributeOperand.AttributeId) ;
		indexRange  ->fromCpptoC(pStatus,pSimpleAttributeOperand.IndexRange) ;
	}

public:

	SimpleAttributeOperand(
			NodeId               * _typeId,
			TableQualifiedName   * _browsePath,
			IntegerId            * _attributeId,
			NumericRange         * _indexRange
			)
		: FilterOperand()
	{
		(typeId      = _typeId)      ->take() ;
		(browsePath  = _browsePath)  ->take() ;
		(attributeId = _attributeId) ->take() ;
		(indexRange  = _indexRange)  ->take() ;
	}

	virtual ~SimpleAttributeOperand()
	{
		typeId      ->release() ;
		browsePath  ->release() ;
		attributeId ->release() ;
		indexRange  ->release() ;
	}

public:

	inline NodeId * getTypeId()
	{
		return typeId ;
	}

	inline TableQualifiedName   * getBrowsePath()
	{
		return browsePath ;
	}

	inline IntegerId * getAttributeId()
	{
		return attributeId ;
	}

	inline NumericRange * getIndexRange()
	{
		return indexRange ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_SIMPLEATTRIBUTEOPERAND_H_ */
