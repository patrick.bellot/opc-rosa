
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_RELATIVEPATHELEMENT_H_
#define OPCUA_RELATIVEPATHELEMENT_H_

/*
 * OPC-UA, Part 4, 7.25, p. 137 in table 156
 */

#include "../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL RelativePathElement
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RelativePathElement ; }

private:

	NodeId        * referenceTypeId ;
	Boolean       * isInverse ;
	Boolean       * includeSubtypes ;
	QualifiedName * targetName ;

public:

	static RelativePathElement * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RelativePathElement const& pRelativePathElement)
	{
		NodeId        * referenceTypeId = NodeId        ::fromCtoCpp( pStatus, pRelativePathElement.ReferenceTypeId) ;
		Boolean       * isInverse       = Boolean       ::fromCtoCpp( pStatus, pRelativePathElement.IsInverse) ;
		Boolean       * includeSubtypes = Boolean       ::fromCtoCpp( pStatus, pRelativePathElement.IncludeSubtypes) ;
		QualifiedName * targetName      = QualifiedName ::fromCtoCpp( pStatus, pRelativePathElement.TargetName) ;

		if (*pStatus == STATUS_OK)
			return
					new RelativePathElement(
							referenceTypeId,
							isInverse,
							includeSubtypes,
							targetName
							) ;

		if (referenceTypeId != NULL)
			referenceTypeId->checkRefCount() ;
		if (isInverse != NULL)
			isInverse->checkRefCount() ;
		if (includeSubtypes != NULL)
			includeSubtypes->checkRefCount() ;
		if (targetName != NULL)
			targetName->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RelativePathElement& pRelativePathElement) const
	{
		referenceTypeId ->fromCpptoC (pStatus, pRelativePathElement.ReferenceTypeId) ;
		isInverse       ->fromCpptoC (pStatus, pRelativePathElement.IsInverse) ;
		includeSubtypes ->fromCpptoC (pStatus, pRelativePathElement.IncludeSubtypes) ;
		targetName      ->fromCpptoC (pStatus, pRelativePathElement.TargetName) ;
	}

public:

	RelativePathElement(
			NodeId        * _referenceTypeId,
			Boolean       * _isInverse,
			Boolean       * _includeSubtypes,
			QualifiedName * _targetName
			)
		: Structure()
	{
		(referenceTypeId = _referenceTypeId) ->take() ;
		(isInverse       = _isInverse)       ->take() ;
		(includeSubtypes = _includeSubtypes) ->take() ;
		(targetName      = _targetName)      ->take() ;
	}

	virtual ~RelativePathElement()
	{
		referenceTypeId ->release() ;
		isInverse       ->release() ;
		includeSubtypes ->release() ;
		targetName      ->release() ;
	}

public:

	inline NodeId * getReferenceTypeId()
	{
		return referenceTypeId ;
	}

	inline Boolean * getIsInverse()
	{
		return isInverse ;
	}

	inline Boolean * getIncludeSubtypes()
	{
		return includeSubtypes ;
	}

	inline QualifiedName * getTargetName()
	{
		return targetName ;
	}

};     /* class RelativePathElement */
}      /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_RELATIVEPATHELEMENT_H_ */
