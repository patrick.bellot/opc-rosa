
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CHANNELSECURITYTOKEN_H_
#define OPCUA_CHANNELSECURITYTOKEN_H_

/*
 * Part 4, 5.2.2, p. 22
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua
{

class MYDLL ChannelSecurityToken
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ChannelSecurityToken ; }

private:

	uint32_t     channelId ;
	uint32_t     tokenId ;
	UtcTime    * createdAt ;
	int32_t   	 revisedLifetime ;

public:

	ChannelSecurityToken(
			uint32_t    _channelId,
			uint32_t     _tokenId,
			UtcTime    * _createdAt,
			int32_t      _revisedLifetime
			)
		: Structure()
	{
		channelId        = _channelId ;
		tokenId          = _tokenId ;
		(createdAt       = _createdAt)->take() ;
		revisedLifetime  = _revisedLifetime ;
	}

	virtual ~ChannelSecurityToken()
	{
		createdAt       ->release() ;
	}

public:

	inline uint32_t getChannelId()
	{
		return channelId ;
	}

	inline void setChannelId(uint32_t _channelId)
	{
		channelId = _channelId;
	}

	inline uint32_t getTokenId()
	{
		return tokenId ;
	}

	inline UtcTime * getCreatedAt()
	{
		return createdAt ;
	}

	inline int32_t getRevisedLifetime()
	{
		return revisedLifetime ;
	}

};     /* class */
}      /* namespace opcua */
#endif /* OPCXUA_CHANNELSECURITYTOKEN_H_ */
