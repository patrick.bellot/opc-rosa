
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_USERNAMEIDENTITYTOKEN_H_
#define OPCUA_USERNAMEIDENTITYTOKEN_H_

/*
 * OPC-UA Part 4, 7.35.3, p. 148
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_UserIdentityToken.h"

namespace opcua {

class MYDLL UserNameIdentityToken
	: public UserIdentityToken
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_UserNameIdentityToken ; }

private:

	String     * policyId ;
	String     * userName ;
	ByteString * password ;
	String     * encryptionAlgorithm ; // Part 7

public:

	static UserNameIdentityToken * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_UserNameIdentityToken const& pUserNameIdentityToken)
	{
		String     * policyId            = String     ::fromCtoCpp(pStatus,pUserNameIdentityToken.PolicyId) ;
		String     * userName            = String     ::fromCtoCpp(pStatus,pUserNameIdentityToken.UserName) ;
		ByteString * password            = ByteString ::fromCtoCpp(pStatus,pUserNameIdentityToken.Password) ;
		String     * encryptionAlgorithm = String     ::fromCtoCpp(pStatus,pUserNameIdentityToken.EncryptionAlgorithm) ;

		if (*pStatus == STATUS_OK)
			return
					new UserNameIdentityToken(
							policyId,
							userName,
							password,
							encryptionAlgorithm
							) ;

		if (policyId != NULL)
			policyId->checkRefCount() ;
		if (userName != NULL)
			userName->checkRefCount() ;
		if (password != NULL)
			password->checkRefCount() ;
		if (encryptionAlgorithm != NULL)
			encryptionAlgorithm->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pUserNameIdentityToken) const
	{
		expandedNodeId->fromCpptoC(pStatus,pUserNameIdentityToken.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pUserNameIdentityToken.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pUserNameIdentityToken.Body.Object.ObjType = &OpcUa_UserNameIdentityToken_EncodeableType ;
			pUserNameIdentityToken.Body.Object.Value = (void *)malloc(sizeof(OpcUa_UserNameIdentityToken)) ;
			OpcUa_UserNameIdentityToken_Initialize(pUserNameIdentityToken.Body.Object.Value) ;
			UserNameIdentityToken::fromCpptoC(pStatus,*static_cast<OpcUa_UserNameIdentityToken *>(pUserNameIdentityToken.Body.Object.Value)) ;
			pUserNameIdentityToken.Length = sizeof(OpcUa_UserNameIdentityToken) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_UserNameIdentityToken& pUserNameIdentityToken) const
	{
		policyId            ->fromCpptoC(pStatus,pUserNameIdentityToken.PolicyId) ;
		userName            ->fromCpptoC(pStatus,pUserNameIdentityToken.UserName) ;
		password            ->fromCpptoC(pStatus,pUserNameIdentityToken.Password) ;
		encryptionAlgorithm ->fromCpptoC(pStatus,pUserNameIdentityToken.EncryptionAlgorithm) ;
	}

public:

	UserNameIdentityToken(
			String     * _policyId,
			String     * _userName,
			ByteString * _password,
			String     * _encryptionAlgorithm // Part 7
			)
		: UserIdentityToken()
	{
		(policyId            = _policyId)            ->take() ;
		(userName            = _userName)            ->take() ;
		(password            = _password)            ->take() ;
		(encryptionAlgorithm = _encryptionAlgorithm) ->take() ;
	}

	virtual ~UserNameIdentityToken()
	{
		policyId            ->release() ;
		userName            ->release() ;
		password            ->release() ;
		encryptionAlgorithm ->release() ;
	}

public:

	inline String * getPolicyId()
	{
		return policyId ;
	}

	inline String * getUserName()
	{
		return userName ;
	}

	inline ByteString * getPassword()
	{
		return password ;
	}

	inline String * getEncryptionAlgorithm() // Part 7
	{
		return encryptionAlgorithm ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_USERNAMEIDENTITYTOKEN_H_ */
