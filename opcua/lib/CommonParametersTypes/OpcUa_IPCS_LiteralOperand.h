
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_LITERALOPERAND_H_
#define OPCUA_LITERALOPERAND_H_

/*
 * OPC-UA Part 4, 7.4.4.2, p. 115
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_FilterOperand.h"

namespace opcua {

class MYDLL LiteralOperand
	: public FilterOperand
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_LiteralOperand ; }

private:

	Variant * value ;

public:

	static LiteralOperand * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_LiteralOperand const& pLiteralOperand)
	{
		Variant * value = Variant ::fromCtoCpp(pStatus,pLiteralOperand.Value) ;

		if (*pStatus == STATUS_OK)
			return
					new LiteralOperand(
							value
							) ;

		if (value != NULL)
			value->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pLiteralOperand) const
	{
		expandedNodeId->fromCpptoC(pStatus,pLiteralOperand.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pLiteralOperand.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pLiteralOperand.Body.Object.ObjType = &OpcUa_LiteralOperand_EncodeableType ;
			pLiteralOperand.Body.Object.Value = (void *)malloc(sizeof(OpcUa_LiteralOperand)) ;
			OpcUa_LiteralOperand_Initialize(pLiteralOperand.Body.Object.Value) ;
			LiteralOperand::fromCpptoC(pStatus,*static_cast<OpcUa_LiteralOperand *>(pLiteralOperand.Body.Object.Value)) ;
			pLiteralOperand.Length = sizeof(OpcUa_LiteralOperand) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_LiteralOperand& pLiteralOperand) const
	{
		value ->fromCpptoC(pStatus,pLiteralOperand.Value) ;
	}

public:

	LiteralOperand(
			Variant * _value
			)
		: FilterOperand()
	{
		(value  = _value) ->take() ;
	}

	virtual ~LiteralOperand()
	{
		value  ->release() ;
	}

public:

	inline Variant * getValue()
	{
		return value ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_LITERALOPERAND_H_ */
