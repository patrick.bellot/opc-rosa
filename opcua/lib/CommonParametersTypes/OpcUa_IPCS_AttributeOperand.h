
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ATTRIBUTEOPERAND_H_
#define OPCUA_ATTRIBUTEOPERAND_H_

/*
 * OPC-UA Part 4, 7.4.4.4, p. 115-116
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_FilterOperand.h"
#include "OpcUa_IPCS_NumericRange.h"
#include "OpcUa_IPCS_RelativePath.h"
#include "OpcUa_IPCS_IntegerId.h"

namespace opcua {

class MYDLL AttributeOperand
	: public FilterOperand
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AttributeOperand ; }

private:

	NodeId       * nnodeId ;
	String       * alias ;
	RelativePath * browsePath ;
	IntegerId    * attributeId ;
	NumericRange * indexRange ;

public:

	static AttributeOperand * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_AttributeOperand const& pAttributeOperand)
	{
		NodeId       * nnodeId     = NodeId       ::fromCtoCpp(pStatus,pAttributeOperand.NodeId) ;
		String       * alias       = String       ::fromCtoCpp(pStatus,pAttributeOperand.Alias) ;
		RelativePath * browsePath  = RelativePath ::fromCtoCpp(pStatus,pAttributeOperand.BrowsePath) ;
		IntegerId    * attributeId = IntegerId ::fromCtoCpp(pStatus,pAttributeOperand.AttributeId) ;
		NumericRange * indexRange  = NumericRange ::fromCtoCpp(pStatus,pAttributeOperand.IndexRange) ;

		if (*pStatus == STATUS_OK)
			return
					new AttributeOperand(
							nnodeId,
							alias,
							browsePath,
							attributeId,
							indexRange
							) ;

		if (nnodeId != NULL)
			nnodeId->checkRefCount() ;
		if (alias != NULL)
			alias->checkRefCount() ;
		if (browsePath != NULL)
			browsePath->checkRefCount() ;
		if (attributeId != NULL)
			attributeId->checkRefCount() ;
		if (indexRange != NULL)
			indexRange->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pAttributeOperand) const
	{
		expandedNodeId->fromCpptoC(pStatus,pAttributeOperand.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pAttributeOperand.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pAttributeOperand.Body.Object.ObjType = &OpcUa_AttributeOperand_EncodeableType ;
			pAttributeOperand.Body.Object.Value = (void *)malloc(sizeof(OpcUa_AttributeOperand)) ;
			OpcUa_AttributeOperand_Initialize(pAttributeOperand.Body.Object.Value) ;
			AttributeOperand::fromCpptoC(pStatus,*static_cast<OpcUa_AttributeOperand *>(pAttributeOperand.Body.Object.Value)) ;
			pAttributeOperand.Length = sizeof(OpcUa_AttributeOperand) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_AttributeOperand& pAttributeOperand) const
	{
		nnodeId     ->fromCpptoC(pStatus,pAttributeOperand.NodeId) ;
		alias       ->fromCpptoC(pStatus,pAttributeOperand.Alias) ;
		browsePath  ->fromCpptoC(pStatus,pAttributeOperand.BrowsePath) ;
		attributeId ->fromCpptoC(pStatus,pAttributeOperand.AttributeId) ;
		indexRange  ->fromCpptoC(pStatus,pAttributeOperand.IndexRange) ;
	}

public:

	AttributeOperand(
			NodeId       * _nnodeId,
			String       * _alias,
			RelativePath * _browsePath,
			IntegerId    * _attributeId,
			NumericRange * _indexRange
			)
		: FilterOperand()
	{
		(nnodeId     = _nnodeId)    ->take() ;
		(alias       = _alias)       ->take() ;
		(browsePath  = _browsePath)  ->take() ;
		(attributeId = _attributeId) ->take() ;
		(indexRange  = _indexRange)  ->take() ;
	}

	virtual ~AttributeOperand()
	{
		nnodeId     ->release() ;
		alias       ->release() ;
		browsePath  ->release() ;
		attributeId ->release() ;
		indexRange  ->release() ;
	}

public:

	inline NodeId * getNNodeId()
	{
		return nnodeId ;
	}

	inline String * getAlias()
	{
		return alias ;
	}

	inline RelativePath * getBrowsePath()
	{
		return browsePath ;
	}

	inline IntegerId * getAttributeId()
	{
		return attributeId ;
	}

	inline NumericRange * getIndexRange()
	{
		return indexRange ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};     /* class AtributeOperand */
}      /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_ATTRIBUTEOPERAND_H_ */
