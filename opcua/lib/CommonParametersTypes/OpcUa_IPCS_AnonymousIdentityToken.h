
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATACHANGENOTIFICAYION_H_
#define OPCUA_DATACHANGENOTIFICAYION_H_

/*
 * OPC-UA Part 4, 7.35, p. 147
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_UserIdentityToken.h"

namespace opcua {

class MYDLL AnonymousIdentityToken
	: public UserIdentityToken
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AnonymousIdentityToken ; }

private:

	String * policyId ;

public:

	static AnonymousIdentityToken * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_AnonymousIdentityToken const& pAnonymousIdentityToken)
	{
		String * policyId = String::fromCtoCpp(pStatus,pAnonymousIdentityToken.PolicyId) ;

		if (*pStatus == STATUS_OK)
			return
					new AnonymousIdentityToken(policyId) ;

		if (policyId != NULL)
			policyId->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pUserIdentityToken) const
	{
		expandedNodeId->fromCpptoC(pStatus,pUserIdentityToken.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pUserIdentityToken.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pUserIdentityToken.Body.Object.ObjType = &OpcUa_AnonymousIdentityToken_EncodeableType ;
			pUserIdentityToken.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_AnonymousIdentityToken)) ;
			OpcUa_AnonymousIdentityToken_Initialize(pUserIdentityToken.Body.Object.Value) ;
			AnonymousIdentityToken::fromCpptoC(pStatus,*static_cast<_OpcUa_AnonymousIdentityToken *>(pUserIdentityToken.Body.Object.Value)) ;
			pUserIdentityToken.Length = sizeof(_OpcUa_AnonymousIdentityToken) ;
		}
	}

private:

	inline void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_AnonymousIdentityToken& pAnonymousIdentityToken) const
	{
		policyId->fromCpptoC(pStatus,pAnonymousIdentityToken.PolicyId) ;
	}

public:

	AnonymousIdentityToken(
			String * _policyId)
		: UserIdentityToken()
	{
		(policyId    = _policyId)    ->take() ;
	}

	virtual ~AnonymousIdentityToken()
	{
		policyId    ->release() ;
	}

public:

	inline String * getPolicyId()
	{
		return policyId ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_ANONYMOUSIDENTITYTOKEN_H_ */
