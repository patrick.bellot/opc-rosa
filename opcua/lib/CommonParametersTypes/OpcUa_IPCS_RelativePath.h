
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_RELATIVEPATH_H_
#define OPCUA_RELATIVEPATH_H_

/*
 * OPC-UA, Part 4, 7.25, p. 137
 */

#include "../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_RelativePathElement.h"

namespace opcua {

class MYDLL RelativePath
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RelativePath ; }

private:

	TableRelativePathElement * elements ;

public:

	static RelativePath * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RelativePath const& pRelativePath)
	{
		TableRelativePathElement * elements = TableRelativePathElement::fromCtoCpp( pStatus, pRelativePath.NoOfElements, pRelativePath.Elements) ;

		if (*pStatus == STATUS_OK)
			return
					new RelativePath(
							elements
							) ;

		if (elements != NULL)
			elements->checkRefCount() ;

		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RelativePath& pRelativePath) const
	{
		elements->fromCpptoC(pStatus, pRelativePath.NoOfElements, pRelativePath.Elements) ;
	}

public:

	RelativePath(
			TableRelativePathElement * _elements
			)
		: Structure()
	{
		(elements = _elements) ->take() ;
	}

	virtual ~RelativePath()
	{
		elements ->release() ;
	}

public:

	inline TableRelativePathElement * getElements()
	{
		return elements ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_RELATIVEPATH_H_ */
