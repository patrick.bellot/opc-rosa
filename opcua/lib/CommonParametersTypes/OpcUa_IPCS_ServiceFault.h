
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SERVICEFAULT_H_
#define OPCUA_SERVICEFAULT_H_

/*
 * OPC-UA, Part 4, 7.28, p. 139
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_ResponseHeader.h"

namespace opcua {

class MYDLL ServiceFault
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ServiceFault ; }


	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ServiceFault_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;

public:

	static ServiceFault * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_ServiceFault const& pServiceFault)
	{
		ResponseHeader * responseHeader = ResponseHeader ::fromCtoCpp (pStatus, pResponseHeader) ;

		if (*pStatus == STATUS_OK)
			return
					new ServiceFault(
							responseHeader
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;

		NOT_USED(pServiceFault) ;

		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_ServiceFault& pServiceFault) const
	{
		responseHeader ->fromCpptoC (pStatus, pResponseHeader) ;

		NOT_USED(pServiceFault) ;
	}

public:

	ServiceFault(ResponseHeader * _responseHeader)
		: Structure()
	{
		(responseHeader = _responseHeader) ->take() ;
	}

	virtual ~ServiceFault()
	{
		responseHeader->release() ;
	}

public:

	virtual ResponseHeader * getResponseHeader() { return responseHeader ; }

};
} /* namespace opcua */
#endif /* OPCUA_SERVICEFAULT_H_ */
