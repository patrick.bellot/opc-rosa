
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ENDPOINTDESCRIPTION_H_
#define OPCUA_ENDPOINTDESCRIPTION_H_

/*
 * Part 4, 7.9, p. 121
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_ApplicationDescription.h"
#include "OpcUa_IPCS_ApplicationInstanceCertificate.h"
#include "OpcUa_IPCS_MessageSecurityMode.h"
#include "OpcUa_SecurityProfileUri.h"
#include "OpcUa_IPCS_UserTokenPolicy.h"
#include "OpcUa_TransportProfileUri.h"

namespace opcua {

class MYDLL EndpointDescription
		: public Structure
{
public:

	static TableEndpointDescription   * selfEndpoints ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_EndpointDescription ; }

private:

	String                         * enpointUrl ;
	ApplicationDescription         * server ;
	ApplicationInstanceCertificate * serverCertificate ;
	MessageSecurityMode            * securityMode ;
	String                         * securityPolicyUri ;

	TableUserTokenPolicy           * userIdentityTokens ;
	String                         * transportProfileUri ;
	Byte                           * securityLevel ;

public:

	static EndpointDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_EndpointDescription const& pEndpointDescription)
	{
		String                         * enpointUrl          = String                         ::fromCtoCpp(pStatus, pEndpointDescription.EndpointUrl) ;
		ApplicationDescription         * server              = ApplicationDescription         ::fromCtoCpp(pStatus, pEndpointDescription.Server) ;
		ApplicationInstanceCertificate * serverCertificate   = ApplicationInstanceCertificate ::fromCtoCpp(pStatus, pEndpointDescription.ServerCertificate) ;
		MessageSecurityMode            * securityMode        = MessageSecurityMode            ::fromCtoCpp(pStatus, pEndpointDescription.SecurityMode) ;
		String                         * securityPolicyUri   = String                         ::fromCtoCpp(pStatus, pEndpointDescription.SecurityPolicyUri);

		TableUserTokenPolicy           * userIdentityTokens  = TableUserTokenPolicy::fromCtoCpp(pStatus, pEndpointDescription.NoOfUserIdentityTokens,pEndpointDescription.UserIdentityTokens) ;
		String                         * transportProfileUri = String                         ::fromCtoCpp(pStatus, pEndpointDescription.TransportProfileUri);
		Byte                           * securityLevel       = Byte						      ::fromCtoCpp(pStatus, pEndpointDescription.SecurityLevel) ;

		if (*pStatus == STATUS_OK)
			return
					new EndpointDescription(
							enpointUrl,
							server,
							serverCertificate,
							securityMode,
							securityPolicyUri,

							userIdentityTokens,
							transportProfileUri,
							securityLevel
							) ;

		if (enpointUrl != NULL)
			enpointUrl->checkRefCount() ;
		if (server != NULL)
			server->checkRefCount() ;
		if (serverCertificate != NULL)
			serverCertificate->checkRefCount() ;
		if (securityMode != NULL)
			securityMode->checkRefCount() ;
		if (securityPolicyUri != NULL)
			securityPolicyUri->checkRefCount() ;

		if (userIdentityTokens != NULL)
			userIdentityTokens->checkRefCount() ;
		if (transportProfileUri != NULL)
			transportProfileUri->checkRefCount() ;
		if (securityLevel != NULL)
			securityLevel->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_EndpointDescription& pEndpointDescription) const
	{
		enpointUrl          ->fromCpptoC(pStatus, pEndpointDescription.EndpointUrl) ;
		server              ->fromCpptoC(pStatus, pEndpointDescription.Server) ;
		serverCertificate   ->fromCpptoC(pStatus, pEndpointDescription.ServerCertificate) ;
		securityMode        ->fromCpptoC(pStatus, pEndpointDescription.SecurityMode) ;
		securityPolicyUri   ->fromCpptoC(pStatus, pEndpointDescription.SecurityPolicyUri) ;
		userIdentityTokens  ->fromCpptoC(pStatus, pEndpointDescription.NoOfUserIdentityTokens,pEndpointDescription.UserIdentityTokens) ;
		transportProfileUri ->fromCpptoC(pStatus, pEndpointDescription.TransportProfileUri) ;
		securityLevel       ->fromCpptoC(pStatus, pEndpointDescription.SecurityLevel) ;
	}

public:

	EndpointDescription(
			String                         * _enpointUrl,
			ApplicationDescription         * _server,
			ApplicationInstanceCertificate * _serverCertificate,
			MessageSecurityMode            * _securityMode,
			String                         * _securityPolicyUri,
			TableUserTokenPolicy           * _userIdentityTokens,
			String                         * _transportProfileUri,
			Byte                           * _securityLevel
			)
		: Structure()
	{
		(enpointUrl          = _enpointUrl)          ->take() ;
		(server              = _server)              ->take() ;
		(serverCertificate   = _serverCertificate)   ->take() ;
		(securityMode        = _securityMode)        ->take() ;
		(securityPolicyUri   = _securityPolicyUri)   ->take() ;
		(userIdentityTokens  = _userIdentityTokens)  ->take() ;
		(transportProfileUri = _transportProfileUri) ->take() ;
		(securityLevel       = _securityLevel)       ->take() ;
	}

	virtual ~EndpointDescription()
	{
		enpointUrl          ->release() ;
		server              ->release() ;
		serverCertificate   ->release() ;
		securityMode        ->release() ;
		securityPolicyUri   ->release() ;
		userIdentityTokens  ->release() ;
		transportProfileUri ->release() ;
		securityLevel       ->release() ;
	}

public:

	inline ApplicationDescription * getServer() {
		return server ;
	}

};
} /* namespace opcua */
#endif /* ENDPOINTDESCRIPTION_H_ */
