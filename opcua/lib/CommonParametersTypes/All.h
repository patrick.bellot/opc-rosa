
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ALL_COMMONPARAMETERSTYPES_H_
#define _ALL_COMMONPARAMETERSTYPES_H_

#include "OpcUa_IPCS_AnonymousIdentityToken.h"
#include "OpcUa_IPCS_ApplicationDescription.h"
#include "OpcUa_IPCS_ApplicationInstanceCertificate.h"
#include "OpcUa_IPCS_ApplicationType.h"
#include "OpcUa_IPCS_AttributeOperand.h"
#include "OpcUa_IPCS_BrowseDescription.h"
#include "OpcUa_IPCS_BrowseDirection.h"
#include "OpcUa_IPCS_BrowsePath.h"
#include "OpcUa_IPCS_BrowsePathResult.h"
#include "OpcUa_IPCS_BrowsePathTarget.h"
#include "OpcUa_IPCS_BrowseResult.h"
#include "OpcUa_IPCS_CallMethodRequest.h"
#include "OpcUa_IPCS_CallMethodResult.h"
#include "OpcUa_ChannelSecurityToken.h"
#include "OpcUa_ContentFilter.h"
#include "OpcUa_IPCS_ContentFilterElement.h"
#include "OpcUa_IPCS_ContinuationPoint.h"
#include "OpcUa_IPCS_Counter.h"
#include "OpcUa_IPCS_DataValue.h"
#include "OpcUa_IPCS_DiagnosticInfo.h"
#include "OpcUa_IPCS_ElementOperand.h"
#include "OpcUa_IPCS_EndpointDescription.h"
#include "OpcUa_IPCS_FilterOperand.h"
#include "OpcUa_IPCS_FilterOperator.h"
#include "OpcUa_Index.h"
#include "OpcUa_IPCS_IntegerId.h"
#include "OpcUa_IPCS_IssuedIdentityToken.h"
#include "OpcUa_IPCS_LiteralOperand.h"
#include "OpcUa_IPCS_MessageSecurityMode.h"
#include "OpcUa_NodeTypeDescription.h"
#include "OpcUa_IPCS_NumericRange.h"
#include "OpcUa_ParsingResult.h"
#include "OpcUa_IPCS_QueryDataDescription.h"
#include "OpcUa_QueryDataSet.h"
#include "OpcUa_IPCS_ReadValueId.h"
#include "OpcUa_ReadValueIdDefine.h"
#include "OpcUa_IPCS_ReferenceDescription.h"
#include "OpcUa_IPCS_RelativePath.h"
#include "OpcUa_IPCS_RelativePathElement.h"
#include "OpcUa_IPCS_RequestHeader.h"
#include "OpcUa_IPCS_ResponseHeader.h"
#include "OpcUa_SecurityProfileUri.h"
#include "OpcUa_ServerState.h"
#include "OpcUa_IPCS_SecurityTokenRequestType.h"
#include "OpcUa_IPCS_ServiceFault.h"
#include "OpcUa_SessionAuthenticationToken.h"
#include "OpcUa_IPCS_SignatureData.h"
#include "OpcUa_IPCS_SignatureData.h"
#include "OpcUa_IPCS_SignedSoftwareCertificate.h"
#include "OpcUa_IPCS_SimpleAttributeOperand.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_IPCS_TimestampsToReturn.h"
#include "OpcUa_TransportProfileUri.h"
#include "OpcUa_IPCS_UserIdentityToken.h"
#include "OpcUa_IPCS_UserIdentityTokenType.h"
#include "OpcUa_IPCS_UserNameIdentityToken.h"
#include "OpcUa_IPCS_UserTokenPolicy.h"
#include "OpcUa_IPCS_ViewDescription.h"
#include "OpcUa_IPCS_WriteValue.h"
#include "OpcUa_IPCS_X509IdentityToken.h"

#endif /* _ALL_COMMONPARAMETERSTYPES_H_ */
