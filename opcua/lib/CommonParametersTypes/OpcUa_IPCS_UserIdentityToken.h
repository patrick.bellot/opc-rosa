
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_USERIDENTITYTOKEN_H_
#define OPCUA_USERIDENTITYTOKEN_H_

/*
 * OPC-UA Part 4, 7.35, p. 147
 * An extensible parameters.
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL UserIdentityToken
	: public Structure
{
public:

	virtual ExpandedNodeId * getNodeId() const = 0 ;

public:

	static UserIdentityToken * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pUserIdentityToken) ;

	virtual void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pUserIdentityToken) const = 0 ;

};
} /* namespace opcua */
#endif /* OPCUA_USERIDENTITYTOKEN_H_ */
