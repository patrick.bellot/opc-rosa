
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSEPATHTARGET_H_
#define OPCUA_BROWSEPATHTARGET_H_

/*
 * OPC-UA, Part 4, 5.8.4.2, p. 43
 */

#include "../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_Index.h"

namespace opcua {

class MYDLL BrowsePathTarget
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowsePathTarget ; }

private:

	ExpandedNodeId          * targetId ;
	Index                   * remainingPathIndex ;

public:

	static BrowsePathTarget * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_BrowsePathTarget const& pBrowsePathTarget)
	{
		ExpandedNodeId * targetId           = ExpandedNodeId ::fromCtoCpp (pStatus, pBrowsePathTarget.TargetId) ;
		Index          * remainingPathIndex = Index          ::fromCtoCpp (pStatus, pBrowsePathTarget.RemainingPathIndex) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowsePathTarget(
							targetId,
							remainingPathIndex
							) ;

		if (targetId != NULL)
			targetId->checkRefCount() ;
		if (remainingPathIndex != NULL)
			remainingPathIndex->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_BrowsePathTarget& pBrowsePathTarget) const
	{
		targetId           ->fromCpptoC (pStatus, pBrowsePathTarget.TargetId) ;
		remainingPathIndex ->fromCpptoC (pStatus, pBrowsePathTarget.RemainingPathIndex) ;
	}

public:

	BrowsePathTarget(
			ExpandedNodeId          * _targetId,
			Index                   * _remainingPathIndex
			)
		: Structure()
	{
		(targetId           = _targetId)           ->take() ;
		(remainingPathIndex = _remainingPathIndex) ->take() ;
	}

	virtual ~BrowsePathTarget()
	{
		targetId           ->release() ;
		remainingPathIndex ->release() ;
	}

public:

	inline ExpandedNodeId * getTargetId()
	{
		return targetId ;
	}
	inline Index * getRemainingPathIndex()
	{
		return remainingPathIndex ;
	}

};
} /* namespace opcua */
#endif /* WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS */
#endif /* OPCUA_BROWSEPATHTARGET_H_ */

