
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_WRITEVALUE_H_
#define OPCUA_WRITEVALUE_H_

/*
 * OPC-UA Part 4, 5.10.4.2, p. 56, Table 55
 */

#include "../OpcUa.h"

#if (WITH_WRITE == 1)

#include "../StandardDataTypes/All.h"

#include "OpcUa_IPCS_IntegerId.h"
#include "OpcUa_IPCS_NumericRange.h"
#include "OpcUa_IPCS_DataValue.h"

namespace opcua {

class MYDLL WriteValue
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_WriteValue ; }

private:

	NodeId        * nodeId ;
	IntegerId     * attributeId ;
	NumericRange  * indexRange ;
	DataValue     * value ;

public:

	static WriteValue * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_WriteValue const& pWriteValue)
	{
		NodeId       * nodeId       = NodeId       ::fromCtoCpp(pStatus, pWriteValue.NodeId) ;
		IntegerId    * attributeId  = IntegerId    ::fromCtoCpp(pStatus, pWriteValue.AttributeId) ;
		NumericRange * indexRange   = NumericRange ::fromCtoCpp(pStatus, pWriteValue.IndexRange) ;
		DataValue    * value        = DataValue    ::fromCtoCpp(pStatus, pWriteValue.Value) ;

		if (*pStatus == STATUS_OK)
			return
					new WriteValue(
							nodeId,
							attributeId,
							indexRange,
							value
							) ;

		if (nodeId != NULL)
			nodeId->checkRefCount() ;
		if (attributeId != NULL)
			attributeId->checkRefCount() ;
		if (indexRange != NULL)
			indexRange->checkRefCount() ;
		if (value != NULL)
			value->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_WriteValue& pWriteValue) const
	{
		nodeId      ->fromCpptoC(pStatus, pWriteValue.NodeId) ;
		attributeId ->fromCpptoC(pStatus, pWriteValue.AttributeId) ;
		indexRange  ->fromCpptoC(pStatus, pWriteValue.IndexRange) ;
		value       ->fromCpptoC(pStatus, pWriteValue.Value) ;
	}

public:

	WriteValue(
			NodeId        * _nodeId,
			IntegerId     * _attributeId,
			NumericRange  * _indexRange,
			DataValue     * _value
			)
		: Structure()
	{
		(nodeId       = _nodeId)       ->take() ;
		(attributeId  = _attributeId)  ->take() ;
		(indexRange   = _indexRange)   ->take() ;
		(value        = _value)        ->take() ;
	}

	virtual ~WriteValue()
	{
		nodeId       ->release() ;
		attributeId  ->release() ;
		indexRange   ->release() ;
		value        ->release() ;
	}

public:

	inline NodeId * getNodeId()
	{
		return nodeId ;
	}

	inline IntegerId * getAttributeId()
	{
		return attributeId ;
	}

	inline NumericRange * getIndexRange()
	{
		return indexRange ;
	}

	inline DataValue * getValue()
	{
		return value ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_WRITEVALUE_H_ */
