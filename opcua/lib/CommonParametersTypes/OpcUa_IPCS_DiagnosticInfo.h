
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DIAGNOSTICINFO_H_
#define OPCUA_DIAGNOSTICINFO_H_

/*
 * OPC-UA Part 4, 7.8, p.120
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_StatusCode.h"

namespace opcua {

class MYDLL DiagnosticInfo
	: public Structure
{
public:

	static DiagnosticInfo * fakeDiagnosticInfo ;

	static TableDiagnosticInfo   * tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_DiagnosticInfo ; }

private:

	Int32          * namespaceUri ;
	Int32          * symbolicId ;
	Int32          * locale ;
	Int32          * localizedText ;
	String         * additionalInfo ;

	StatusCode     * innerStatusCode ;
	DiagnosticInfo * innerDiagnosticInfo ;

public:

	static DiagnosticInfo * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_DiagnosticInfo const& pDiagnosticInfo)
	{
		Int32          * namespaceUri        = Int32           ::fromCtoCpp(pStatus, pDiagnosticInfo.NamespaceUri) ;
		Int32          * symbolicId          = Int32           ::fromCtoCpp(pStatus, pDiagnosticInfo.SymbolicId) ;
		Int32          * locale              = Int32           ::fromCtoCpp(pStatus, pDiagnosticInfo.Locale) ;
		Int32          * localizedText       = Int32           ::fromCtoCpp(pStatus, pDiagnosticInfo.LocalizedText) ;
		String         * additionalInfo      = String          ::fromCtoCpp(pStatus, pDiagnosticInfo.AdditionalInfo) ;

		StatusCode     * innerStatusCode     = StatusCode      ::fromCtoCpp(pStatus, pDiagnosticInfo.InnerStatusCode) ;
		DiagnosticInfo * innerDiagnosticInfo = (pDiagnosticInfo.InnerDiagnosticInfo == NULL) ? NULL : DiagnosticInfo::fromCtoCpp(pStatus, *pDiagnosticInfo.InnerDiagnosticInfo);

		if (*pStatus == STATUS_OK)
			return
					new DiagnosticInfo(
							namespaceUri,
							symbolicId,
							locale,
							localizedText,
							additionalInfo,

							innerStatusCode,
							innerDiagnosticInfo
							) ;

		if (namespaceUri != NULL)
			namespaceUri->checkRefCount() ;
		if (symbolicId != NULL)
			symbolicId->checkRefCount() ;
		if (locale != NULL)
			locale->checkRefCount() ;
		if (localizedText != NULL)
			localizedText->checkRefCount() ;
		if (additionalInfo != NULL)
			additionalInfo->checkRefCount() ;

		if (innerStatusCode != NULL)
			innerStatusCode->checkRefCount() ;
		if (innerDiagnosticInfo != NULL)
			innerDiagnosticInfo->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_DiagnosticInfo& pDiagnosticInfo) const
	{
		if (namespaceUri == NULL)
			pDiagnosticInfo.NamespaceUri = (int32_t)0 ;
		else
			namespaceUri->fromCpptoC(pStatus, pDiagnosticInfo.NamespaceUri) ;

		if (symbolicId == NULL)
			pDiagnosticInfo.SymbolicId = (int32_t)0 ;
		else
			symbolicId->fromCpptoC(pStatus, pDiagnosticInfo.SymbolicId) ;

		if (locale == NULL)
			pDiagnosticInfo.Locale = (int32_t)0 ;
		else
			locale ->fromCpptoC(pStatus, pDiagnosticInfo.Locale) ;

		if (localizedText == NULL)
			pDiagnosticInfo.LocalizedText = (int32_t)0 ;
		else
			localizedText ->fromCpptoC(pStatus, pDiagnosticInfo.LocalizedText) ;

		if (additionalInfo == NULL)
			SOPC_String_Initialize(&pDiagnosticInfo.AdditionalInfo) ;
		else
			additionalInfo->fromCpptoC(pStatus, pDiagnosticInfo.AdditionalInfo) ;

		if (innerStatusCode == NULL)
			pDiagnosticInfo.InnerStatusCode = (SOPC_StatusCode)0 ;
		else
			innerStatusCode->fromCpptoC(pStatus, pDiagnosticInfo.InnerStatusCode) ;

		if (innerDiagnosticInfo == NULL)
			pDiagnosticInfo.InnerDiagnosticInfo = NULL ;
		else {
			pDiagnosticInfo.InnerDiagnosticInfo = static_cast<SOPC_DiagnosticInfo *>(calloc(1,sizeof(SOPC_DiagnosticInfo))) ;
			innerDiagnosticInfo ->fromCpptoC(pStatus, *pDiagnosticInfo.InnerDiagnosticInfo) ;
		}
	}

public:

	DiagnosticInfo(
			Int32          * _namespaceUri,
			Int32          * _symbolicId,
			Int32          * _locale,
			Int32          * _localizedText,
			String         * _additionalInfo,
			StatusCode     * _innerStatusCode,
			DiagnosticInfo * _innerDiagnosticInfo
			)
		: Structure()
	{
		if ((namespaceUri        = _namespaceUri))
			namespaceUri        ->take() ;
		if ((symbolicId          = _symbolicId))
			symbolicId          ->take() ;
		if ((locale              = _locale))
			locale              ->take() ;
		if ((localizedText       = _localizedText))
			localizedText       ->take() ;
		if ((additionalInfo      = _additionalInfo))
			additionalInfo      ->take() ;
		if ((innerStatusCode     = _innerStatusCode))
			innerStatusCode     ->take() ;
		if ((innerDiagnosticInfo = _innerDiagnosticInfo))
			innerDiagnosticInfo ->take() ;
	}

	virtual ~DiagnosticInfo()
	{
		if (namespaceUri != NULL)
			namespaceUri->release() ;
		if (symbolicId != NULL)
			symbolicId->release() ;
		if (locale != NULL)
			locale->release() ;
		if (localizedText != NULL)
			localizedText->release() ;
		if (additionalInfo != NULL)
			additionalInfo->release() ;
		if (innerStatusCode != NULL)
			innerStatusCode->release() ;
		if (innerDiagnosticInfo != NULL)
			innerDiagnosticInfo->release() ;
	}

public:

	virtual inline Variant * getDiagnosticInfo()
	{
		return new Variant(Builtin_DataValue,this) ;
	}

};
} /* namespace opcua */
#endif /* OPCAU_DIAGNOSTICINFO_H_ */
