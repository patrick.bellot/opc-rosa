
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_NUMERICRANGE_H_
#define OPCUA_NUMERICRANGE_H_

/*
 * OPC-UA Part4, 7.21, p. 133
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL NumericRange
	: public String
{
public:

	static NumericRange * null ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_NumericRange ; }

public:

	static NumericRange * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_String const& pNumericRange)
	{
		NOT_USED(pStatus) ;
		if (pNumericRange.Length <= 0)
			return null ;
		return new NumericRange((char *)pNumericRange.Data) ;
	}

public:

	NumericRange(const char * const _str)
		: String(_str)
	{}

};
} /* namespace opcua */
#endif /* NUMERICRANGE_H_ */
