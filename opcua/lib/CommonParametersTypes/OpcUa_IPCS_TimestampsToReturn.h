
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_TIMESTAMPSTORETURN_H_
#define OPCUA_TIMESTAMPSTORETURN_H_

#include "../OpcUa.h"

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 7.34, p. 147
 */

enum _TimestampsToReturn {
	TimestampsToReturn_SOURCE_0  = 0,
	TimestampsToReturn_SERVER_1  = 1,
	TimestampsToReturn_BOTH_2    = 2,
	TimestampsToReturn_NEITHER_3 = 3
} ;

class MYDLL TimestampsToReturn
		: public Enumeration
{
public:

	static TimestampsToReturn * source_0 ;
	static TimestampsToReturn * server_1 ;
	static TimestampsToReturn * both_2 ;
	static TimestampsToReturn * neither_3 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_TimestampsToReturn ; }

public:

	static TimestampsToReturn * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_TimestampsToReturn const& value)
	{
		if (OpcUa_TimestampsToReturn_Source <= value && value <= OpcUa_TimestampsToReturn_Neither)
			return new TimestampsToReturn((_TimestampsToReturn)value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_TimestampsToReturn& pValue) const
	{
		int32_t value = get() ;
		if (TimestampsToReturn_SOURCE_0 <= value && value <= TimestampsToReturn_NEITHER_3)
			pValue = (OpcUa_TimestampsToReturn)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	TimestampsToReturn(int _value)
		: Enumeration(_value)
	{}

public:

	bool isInvalid()
	{
		return (get() < 0) || (3 < get()) ;
	}

};     /* class TimestampsToReturn */
}      /* namespace opcua */
#endif /* OPCUA_TIMESTAMPSTORETURN_H_ */
