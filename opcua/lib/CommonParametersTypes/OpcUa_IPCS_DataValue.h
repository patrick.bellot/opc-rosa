
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATAVALUE_H_
#define OPCUA_DATAVALUE_H_

/*
 * Problem : these two reference don't say the same thing
 * OPC-UA Part 6, 5.2.2.17, p. 17
 * OPC-UA Part 4, 7.7, p. 118
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_BaseDataType.h"
#include "../StandardDataTypes/OpcUa_IPCS_Int32.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL DataValue
		: public BaseDataType
{
public:

	static DataValue * const Bad_AttributeIdInvalid ;
	static DataValue * const Bad_IndexRangeInvalid ;
	static DataValue * const Bad_NotImplemented ;
	static DataValue * const Bad_NodeIdUnknown ;
	static DataValue * const Bad_NotReadable ;
	static DataValue * const Bad_UserAccessDenied ;
	static DataValue * const Bad_TimestampsToReturnInvalid ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_DataValue ; }

private:

	Variant    * variant ;
	StatusCode * statusCode ;
	UtcTime    * sourceTimeStamp ;
	UInt16     * sourcePicoSeconds ;
	UtcTime    * serverTimeStamp ;

	UInt16     * serverPicoSeconds ;

public:

	static DataValue * fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_DataValue const& pDataValue)
	{
		debug(IPCS_DBG,"DataValue","fromCtoCpp") ;

		Variant    * variant           = Variant    ::fromCtoCpp(pStatus, pDataValue.Value) ;
		StatusCode * statusCode        = StatusCode ::fromCtoCpp(pStatus, pDataValue.Status) ;
		UtcTime    * sourceTimeStamp   = UtcTime    ::fromCtoCpp(pStatus, pDataValue.SourceTimestamp) ;
		UInt16     * sourcePicoSeconds = UInt16     ::fromCtoCpp(pStatus, pDataValue.SourcePicoSeconds) ;
		UtcTime    * serverTimeStamp   = UtcTime    ::fromCtoCpp(pStatus, pDataValue.ServerTimestamp) ;

		UInt16     * serverPicoSeconds = UInt16     ::fromCtoCpp(pStatus, pDataValue.ServerPicoSeconds) ;

		if (*pStatus == STATUS_OK)
			return
					new DataValue(
							variant,
							statusCode,
							sourceTimeStamp,
							sourcePicoSeconds,
							serverTimeStamp,

							serverPicoSeconds
							) ;

		if (variant != NULL)
			variant->checkRefCount() ;
		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (sourceTimeStamp != NULL)
			sourceTimeStamp->checkRefCount() ;
		if (sourcePicoSeconds != NULL)
			sourcePicoSeconds->checkRefCount() ;
		if (serverTimeStamp != NULL)
			serverTimeStamp->checkRefCount() ;

		if (serverPicoSeconds != NULL)
			serverPicoSeconds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_DataValue& pDataValue) const
	{
		debug(IPCS_DBG,"DataValue","fromCpptoC") ;

		if (variant != NULL)
			variant ->fromCpptoC(pStatus, pDataValue.Value) ;
		else
			SOPC_Variant_Clear(&(pDataValue.Value)) ;

		if (statusCode != NULL)
			statusCode ->fromCpptoC(pStatus, pDataValue.Status) ;
		else
			SOPC_StatusCode_Clear(&(pDataValue.Status)) ;

		if (sourceTimeStamp != NULL)
			sourceTimeStamp ->fromCpptoC(pStatus, pDataValue.SourceTimestamp) ;
		else
			UtcTime::null ->fromCpptoC(pStatus, pDataValue.SourceTimestamp) ;

		if (sourcePicoSeconds != NULL)
			sourcePicoSeconds ->fromCpptoC(pStatus, pDataValue.SourcePicoSeconds) ;
		else
			UInt16::zero ->fromCpptoC(pStatus, pDataValue.SourcePicoSeconds) ;

		if (serverTimeStamp != NULL)
			serverTimeStamp ->fromCpptoC(pStatus, pDataValue.ServerTimestamp) ;
		else
			UtcTime::null ->fromCpptoC(pStatus, pDataValue.ServerTimestamp) ;

		if (serverPicoSeconds != NULL)
			serverPicoSeconds ->fromCpptoC(pStatus, pDataValue.ServerPicoSeconds) ;
		else
			UInt16::zero ->fromCpptoC(pStatus, pDataValue.ServerPicoSeconds) ;
}

public:

	DataValue(
			StatusCode   * _statusCode
			)
		: BaseDataType()
	{
		variant = NULL ;
		if ((statusCode = _statusCode))
			statusCode->take() ;
		sourceTimeStamp = NULL ;
		sourcePicoSeconds = NULL ;
		serverTimeStamp = NULL ;
		serverPicoSeconds = NULL ;
	}

	DataValue(
			Variant      * _variant
			)
		: BaseDataType()
	{
		if ((variant = _variant))
			variant->take() ;
		statusCode = NULL ;
		sourceTimeStamp = NULL ;
		sourcePicoSeconds = NULL ;
		serverTimeStamp = NULL ;
		serverPicoSeconds = NULL ;
	}

	DataValue(
			Variant      * _variant,
			StatusCode   * _statusCode
			)
		: BaseDataType()
	{
		if ((variant = _variant))
			variant->take() ;
		if ((statusCode = _statusCode))
			statusCode->take() ;
		sourceTimeStamp = NULL ;
		sourcePicoSeconds = NULL ;
		serverTimeStamp = NULL ;
		serverPicoSeconds = NULL ;
	}

	DataValue(
			Variant      * _variant,
			StatusCode   * _statusCode,
			UtcTime      * _sourceTimeStamp,
			UtcTime      * _serverTimeStamp
			)
		: BaseDataType()
	{
		if ((variant = _variant))
			variant->take() ;
		if ((statusCode = _statusCode))
			statusCode->take() ;
		if ((sourceTimeStamp = _sourceTimeStamp))
			sourceTimeStamp->take() ;
		sourcePicoSeconds = NULL ;
		if ((serverTimeStamp = _serverTimeStamp))
			serverTimeStamp->take() ;
		serverPicoSeconds = NULL ;
	}

	DataValue(
			Variant      * _variant,
			StatusCode   * _statusCode,
			UtcTime      * _sourceTimeStamp,
			UInt16       * _sourcePicoSeconds,
			UtcTime      * _serverTimeStamp,
			UInt16       * _serverPicoSeconds
			)
		: BaseDataType()
	{
		if ((variant = _variant))
			variant->take() ;
		if ((statusCode = _statusCode))
			statusCode->take() ;
		if ((sourceTimeStamp = _sourceTimeStamp))
			sourceTimeStamp->take() ;
		if ((sourcePicoSeconds = _sourcePicoSeconds))
			sourcePicoSeconds->take() ;
		if ((serverTimeStamp = _serverTimeStamp))
			serverTimeStamp->take() ;
		if ((serverPicoSeconds = _serverPicoSeconds))
			serverPicoSeconds->take() ;
	}

	virtual ~DataValue()
	{
		if (variant != NULL)
			variant->release() ;
		if (statusCode != NULL)
			statusCode->release() ;
		if (sourceTimeStamp != NULL)
			sourceTimeStamp->release() ;
		if (sourcePicoSeconds != NULL)
			sourcePicoSeconds->release() ;
		if (serverTimeStamp != NULL)
			serverTimeStamp->release() ;
		if (serverPicoSeconds != NULL)
			serverPicoSeconds->release() ;
	}

public:

	inline void set_Overflow_StatusCode()
	{
		if (statusCode != NULL) {
			StatusCode * statusCode2 = new StatusCode(statusCode->get() | StatusCode_Overflow) ;
			statusCode->release() ;
			(statusCode = statusCode2)->take() ;
		}
	}

public:

	virtual inline Variant * getVariantA()
	{
		return new Variant(Builtin_DataValue,this) ;
	}

public:

	inline Variant * getVariantAttr()
	{
		return variant ;
	}

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline UtcTime  * getSourceTimeStamp()
	{
		return sourceTimeStamp ;
	}

	inline UtcTime  * getServerTimeStamp()
	{
		return serverTimeStamp ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_LOCALIZEDTEXT_H_ */
