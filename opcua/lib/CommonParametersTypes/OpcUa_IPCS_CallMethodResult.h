/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CALLMETHODRESULT_H_
#define OPCUA_CALLMETHODRESULT_H_

/*
 * OPC-UA Part 4, 5.11.2.2, p. 60
 */

#include "../OpcUa.h"

#if (WITH_CALL == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_IPCS_DiagnosticInfo.h"

namespace opcua {

class MYDLL CallMethodResult
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CallMethodResult ; }

private:

	StatusCode            * statusCode ;
	TableStatusCode       * inputArgumentResults ;
	TableDiagnosticInfo   * inputArgumentDiagnosticInfos ;
	TableVariant          * outputArguments ;

public:

	static CallMethodResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_CallMethodResult const& pCallMethodResult)
	{
		StatusCode            * statusCode                   = StatusCode ::fromCtoCpp       (pStatus, pCallMethodResult.StatusCode) ;
		TableStatusCode       * inputArgumentResults         = TableStatusCode::fromCtoCpp     (pStatus, pCallMethodResult.NoOfInputArgumentResults,         pCallMethodResult.InputArgumentResults) ;
		TableDiagnosticInfo   * inputArgumentDiagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pCallMethodResult.NoOfInputArgumentDiagnosticInfos, pCallMethodResult.InputArgumentDiagnosticInfos) ;
		TableVariant          * outputArguments              = TableVariant::fromCtoCpp        (pStatus, pCallMethodResult.NoOfOutputArguments,              pCallMethodResult.OutputArguments) ;

		if (*pStatus == STATUS_OK)
			return
					new CallMethodResult(
							statusCode,
							inputArgumentResults,
							inputArgumentDiagnosticInfos,
							outputArguments
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (inputArgumentResults != NULL)
			inputArgumentResults->checkRefCount() ;
		if (inputArgumentDiagnosticInfos != NULL)
			inputArgumentDiagnosticInfos->checkRefCount() ;
		if (outputArguments != NULL)
			outputArguments->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_CallMethodResult& pCallMethodResult) const
	{
		statusCode                   ->fromCpptoC(pStatus, pCallMethodResult.StatusCode) ;
		inputArgumentResults         ->fromCpptoC(pStatus, pCallMethodResult.NoOfInputArgumentResults,         pCallMethodResult.InputArgumentResults) ;
		inputArgumentDiagnosticInfos ->fromCpptoC(pStatus, pCallMethodResult.NoOfInputArgumentDiagnosticInfos, pCallMethodResult.InputArgumentDiagnosticInfos) ;
		outputArguments              ->fromCpptoC(pStatus, pCallMethodResult.NoOfOutputArguments,              pCallMethodResult.OutputArguments) ;
	}

public:

	CallMethodResult(
			StatusCode            * _statusCode,
			TableStatusCode       * _inputArgumentResults,
			TableDiagnosticInfo   * _inputArgumentDiagnosticInfos,
			TableVariant          * _outputArguments
			)
		: Structure()
	{
		(statusCode                   = _statusCode)                   ->take() ;
		(inputArgumentResults         = _inputArgumentResults)         ->take() ;
		(inputArgumentDiagnosticInfos = _inputArgumentDiagnosticInfos) ->take() ;
		(outputArguments              = _outputArguments)              ->take() ;
	}

	virtual ~CallMethodResult()
	{
		statusCode                   ->release() ;
		inputArgumentResults         ->release() ;
		inputArgumentDiagnosticInfos ->release() ;
		outputArguments              ->release() ;
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline TableStatusCode   * getInputArgumentResults()
	{
		return inputArgumentResults ;
	}

	inline TableDiagnosticInfo   * getInputArgumentDiagnosticInfos()
	{
		return inputArgumentDiagnosticInfos ;
	}

	inline TableVariant   * getOutputArguments()
	{
		return outputArguments ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_CALLMETHODRESULT_H_ */
