
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CONTENTFILTER_H_
#define OPCUA_CONTENTFILTER_H_

/*
 * OPC-UA Part 4, 7.4.1, p. 107,
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_ContentFilterElement.h"

namespace opcua {

class MYDLL ContentFilter
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ContentFilter ; }

private:

	TableContentFilterElement   * elements ;

public:

	ContentFilter(
			TableContentFilterElement   * _elements
			)
		: Structure()
	{
		(elements = _elements) ->take() ;
	}

	virtual ~ContentFilter()
	{
		elements ->release() ;
	}

public:

	inline TableContentFilterElement   * getElements()
	{
		return elements ;
	}

public:

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_CONTENTFILTER_H_ */
