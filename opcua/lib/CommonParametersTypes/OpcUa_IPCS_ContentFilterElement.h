
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CONTENTFILTERELEMENT_H_
#define OPCUA_CONTENTFILTERELEMENT_H_

/*
 * OPC-UA Part 4, 7.4.1, p. 107, inside Table 106
 */

#include "../OpcUa.h"

#if (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_IPCS_FilterOperator.h"
#include "OpcUa_IPCS_FilterOperand.h"

namespace opcua {

class MYDLL ContentFilterElement
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ContentFilterElement ; }

private:

	FilterOperator     * filterOperator ;
	TableFilterOperand * filterOperands ;

public:

	static ContentFilterElement * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ContentFilterElement const& pContentFilterElement)
	{
		FilterOperator     * filterOperator = FilterOperator     ::fromCtoCpp(pStatus, pContentFilterElement.FilterOperator) ;
		TableFilterOperand * filterOperands = TableFilterOperand ::fromCtoCpp(pStatus, pContentFilterElement.NoOfFilterOperands, pContentFilterElement.FilterOperands) ;

		if (*pStatus == STATUS_OK)
			return
					new ContentFilterElement(
							filterOperator,
							filterOperands
							) ;

		if (filterOperator != NULL)
			filterOperator->checkRefCount() ;
		if (filterOperands != NULL)
			filterOperands->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ContentFilterElement& pContentFilterElement) const
	{
		filterOperator ->fromCpptoC(pStatus, pContentFilterElement.FilterOperator) ;
		filterOperands ->fromCpptoC(pStatus, pContentFilterElement.NoOfFilterOperands, pContentFilterElement.FilterOperands) ;
	}

public:

	ContentFilterElement(
			FilterOperator       * _filterOperator,
			TableFilterOperand   * _filterOperands
			)
		: Structure()
	{
		(filterOperator = _filterOperator) ->take() ;
		(filterOperands = _filterOperands) ->take() ;
	}

	virtual ~ContentFilterElement()
	{
		filterOperator ->release() ;
		filterOperands ->release() ;
	}

public:

	inline FilterOperator * getFilterOperator()
	{
		return filterOperator ;
	}

	inline TableFilterOperand   * getFilterOperands()
	{
		return filterOperands ;
	}

};
} /* namespace opcua */
#endif /* WITH_QUERY */
#endif /* OPCUA_CONTENTFILTERELEMENT_H_ */
