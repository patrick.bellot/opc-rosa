
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VIEWDESCRIPTION_H_
#define OPCUA_VIEWDESCRIPTION_H_

/*
 * OPC-UA, Part 4, 7.37, p. 150
 */

#include "../OpcUa.h"

#if (WITH_BROWSE == 1) || (WITH_QUERY == 1)

#include "../StandardDataTypes/All.h"

namespace opcua {

class MYDLL ViewDescription
		: public Structure
{
public:

	static ViewDescription * nullView ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ViewDescription ; }

private:

	NodeId  * viewId ;
	UtcTime * timestamp ;
	UInt32  * viewVersion ;

public:

	static ViewDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ViewDescription const& pViewDescription)
	{
		NodeId  * viewId      = NodeId  ::fromCtoCpp (pStatus, pViewDescription.ViewId) ;
		UtcTime * timestamp   = UtcTime ::fromCtoCpp (pStatus, pViewDescription.Timestamp) ;
		UInt32  * viewVersion = UInt32  ::fromCtoCpp (pStatus, pViewDescription.ViewVersion) ;

		if (*pStatus == STATUS_OK)
			return
					new ViewDescription(
							viewId,
							timestamp,
							viewVersion
							) ;

		if (viewId != NULL)
			viewId->checkRefCount() ;
		if (timestamp != NULL)
			timestamp->checkRefCount() ;
		if (viewVersion != NULL)
			viewVersion->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ViewDescription& pViewDescription) const
	{
		viewId      ->fromCpptoC (pStatus, pViewDescription.ViewId) ;
		timestamp   ->fromCpptoC (pStatus, pViewDescription.Timestamp) ;
		viewVersion ->fromCpptoC (pStatus, pViewDescription.ViewVersion) ;
	}

public:

	ViewDescription(
			NodeId  * _viewId,
			UtcTime * _timestamp,
			UInt32  * _viewVersion
			)
		: Structure()
	{
		(viewId      = _viewId     ) ->take() ;
		(timestamp   = _timestamp  ) ->take() ;
		(viewVersion = _viewVersion) ->take() ;
	}

	virtual ~ViewDescription()
	{
		viewId      ->release() ;
		timestamp   ->release() ;
		viewVersion ->release() ;
	}

public:

	inline NodeId * getViewId()
	{
		return viewId ;
	}

	inline UtcTime * getTimestamp()
	{
		return timestamp ;
	}

	inline UInt32 * getViewVersion()
	{
		return viewVersion ;
	}

};
} /* namespace opcua */
#endif /* WITH_BROWSE || WITH_QUERY */
#endif /* OPCUA_VIEWDESCRIPTION_H_ */
