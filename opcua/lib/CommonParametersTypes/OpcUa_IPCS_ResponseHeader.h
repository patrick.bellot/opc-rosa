
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_RESPONSEHEADER_H_
#define OPCUA_RESPONSEHEADER_H_

/*
 * OPC-UA Part 7, 7.27, p. 139
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_IntegerId.h"
#include "OpcUa_IPCS_StatusCode.h"
#include "OpcUa_IPCS_DiagnosticInfo.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ResponseHeader
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ResponseHeader ; }

private:

	UtcTime         * timestamp ;
	IntegerId       * requestHandle ;
	StatusCode      * serviceResult ;
	DiagnosticInfo  * serviceDiagnostic ;
    TableString     * stringTable ;

	ExtensionObject * extensionObject ;

public:

	static ResponseHeader * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader)
	{
		UtcTime         * timestamp         = UtcTime	     ::fromCtoCpp(pStatus, pResponseHeader.Timestamp) ;
		IntegerId       * requestHandle     = IntegerId      ::fromCtoCpp(pStatus, pResponseHeader.RequestHandle) ;
		StatusCode      * serviceResult     = StatusCode     ::fromCtoCpp(pStatus, pResponseHeader.ServiceResult) ;
		DiagnosticInfo  * serviceDiagnostic = DiagnosticInfo ::fromCtoCpp(pStatus, pResponseHeader.ServiceDiagnostics) ;
	    TableString     * stringTable       = TableString::fromCtoCpp(pStatus, pResponseHeader.NoOfStringTable, pResponseHeader.StringTable) ;

		ExtensionObject * extensionObject   = ExtensionObject ::fromCtoCpp(pStatus, pResponseHeader.AdditionalHeader);

		if (*pStatus == STATUS_OK)
			return
					new ResponseHeader(
							timestamp,
							requestHandle,
							serviceResult,
							serviceDiagnostic,
							stringTable,

							extensionObject
							) ;

		if (timestamp != NULL)
			timestamp->checkRefCount() ;
		if (requestHandle != NULL)
			requestHandle->checkRefCount() ;
		if (serviceResult != NULL)
			serviceResult->checkRefCount() ;
		if (serviceDiagnostic != NULL)
			serviceDiagnostic->checkRefCount() ;
		if (stringTable != NULL)
			stringTable->checkRefCount() ;

		if (extensionObject != NULL)
			extensionObject->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader) const
	{
		timestamp         ->fromCpptoC(pStatus, pResponseHeader.Timestamp) ;
		requestHandle     ->fromCpptoC(pStatus, pResponseHeader.RequestHandle) ;
		serviceResult     ->fromCpptoC(pStatus, pResponseHeader.ServiceResult) ;
		serviceDiagnostic ->fromCpptoC(pStatus, pResponseHeader.ServiceDiagnostics) ;
		stringTable       ->fromCpptoC(pStatus, pResponseHeader.NoOfStringTable,pResponseHeader.StringTable) ;

		extensionObject   ->fromCpptoC(pStatus, pResponseHeader.AdditionalHeader) ;
	}

public:

	ResponseHeader(
			UtcTime         * _timestamp,
			IntegerId       * _requestHandle,
			StatusCode      * _serviceResult,
			DiagnosticInfo  * _serviceDiagnostic,
		    TableString     * _stringTable,
			ExtensionObject * _extensionObject
			)
		: Structure()
	{
		(timestamp         = _timestamp)         ->take() ;
		(requestHandle     = _requestHandle)     ->take() ;
		(serviceResult     = _serviceResult)     ->take() ;
		(serviceDiagnostic = _serviceDiagnostic) ->take() ;
		(stringTable       = _stringTable)       ->take() ;
		(extensionObject   = _extensionObject)   ->take() ;
	}

	ResponseHeader(
			UtcTime        * _timestamp,
			IntegerId      * _requestHandle,
			StatusCode     * _serviceResult,
			DiagnosticInfo * _serviceDiagnostic,
		    TableString    * _stringTable
			)
		: Structure()
	{
		(timestamp         = _timestamp)            ->take() ;
		(requestHandle     = _requestHandle)        ->take() ;
		(serviceResult     = _serviceResult)        ->take() ;
		(serviceDiagnostic = _serviceDiagnostic)    ->take() ;
		(stringTable       = _stringTable)          ->take() ;
		(extensionObject   = ExtensionObject::null) ->take() ;
	}

	virtual ~ResponseHeader()
	{
		timestamp         ->release() ;
		requestHandle     ->release() ;
		serviceResult     ->release() ;
		serviceDiagnostic ->release() ;
		stringTable       ->release() ;
		extensionObject   ->release() ;
	}

public:

	void setStatusCode(StatusCode * _serviceResult)
	{
		serviceResult->release() ;
		(serviceResult = _serviceResult)->take() ;
	}

public:

	inline IntegerId  * getRequestHandle()
	{
		return requestHandle ;
	}

	inline StatusCode * getServiceResult()
	{
		return serviceResult ;
	}

};
} /* namespace opcua */
#endif /* OPCUA_RESPONSEHEADER_H_ */
