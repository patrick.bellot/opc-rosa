
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_COUNTER_H_
#define OPCUA_COUNTER_H_

/*
 * OPC-UA Part 4, ?.5, p. 117
 */

#include "../OpcUa.h"

#include "../StandardDataTypes/OpcUa_IPCS_DateTime.h"
#include "../Utils/OpcUa_Clock.h"

namespace opcua {

class MYDLL Counter
		: public UInt32
{
public:

	static Counter * zero ;

	static TableCounter   * tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_Counter ; }

public:

	static inline Counter * fromCtoCpp(SOPC_StatusCode * pStatus, uint32_t const& value)
	{
		NOT_USED(pStatus) ;
		return new Counter(value) ;
	}

public:

	Counter(uint32_t _value)
		: UInt32(_value)
	{}

};     /* class MYSQL Counter */
}      /* namespace opcua */
#endif /* OPCUA_COUNTER_H_ */
