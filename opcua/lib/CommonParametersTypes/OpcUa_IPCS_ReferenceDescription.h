
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCEDESCRIPTION_H_
#define OPCUA_REFERENCEDESCRIPTION_H_

/*
 * OPC-UA, Part 4, 7.24, p. 136
 */

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ReferenceDescription
		: public Structure
{
public:

	static TableReferenceDescription   * tableZero ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_ReferenceDescription ; }

private:

	NodeId         * referenceTypeId ;
	Boolean        * isForward ;
	ExpandedNodeId * nodeId ;
	QualifiedName  * browseName ;
	LocalizedText  * displayName ;

	NodeClass      * nodeClass ;
	ExpandedNodeId * typeDefinition ;

public:

	static ReferenceDescription * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ReferenceDescription const& pReferenceDescription)
	{
		NodeId         * referenceTypeId = NodeId         ::fromCtoCpp (pStatus, pReferenceDescription.ReferenceTypeId) ;
		Boolean        * isForward       = Boolean        ::fromCtoCpp (pStatus, pReferenceDescription.IsForward) ;
		ExpandedNodeId * nodeId          = ExpandedNodeId ::fromCtoCpp (pStatus, pReferenceDescription.NodeId) ;
		QualifiedName  * browseName      = QualifiedName  ::fromCtoCpp (pStatus, pReferenceDescription.BrowseName) ;
		LocalizedText  * displayName     = LocalizedText  ::fromCtoCpp (pStatus, pReferenceDescription.DisplayName) ;

		NodeClass      * nodeClass       = NodeClass      ::fromCtoCpp (pStatus, pReferenceDescription.NodeClass) ;
		ExpandedNodeId * typeDefinition  = ExpandedNodeId ::fromCtoCpp (pStatus, pReferenceDescription.TypeDefinition) ;

		if (*pStatus == STATUS_OK)
			return
					new ReferenceDescription(
							referenceTypeId,
							isForward,
							nodeId,
							browseName,
							displayName,

							nodeClass,
							typeDefinition
							) ;

		if (referenceTypeId != NULL)
			referenceTypeId->checkRefCount() ;
		if (isForward != NULL)
			isForward->checkRefCount() ;
		if (nodeId != NULL)
			nodeId->checkRefCount() ;
		if (browseName != NULL)
			browseName->checkRefCount() ;
		if (displayName != NULL)
			displayName->checkRefCount() ;

		if (nodeClass != NULL)
			nodeClass->checkRefCount() ;
		if (typeDefinition != NULL)
			typeDefinition->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ReferenceDescription& pReferenceDescription) const
	{
		referenceTypeId ->fromCpptoC (pStatus, pReferenceDescription.ReferenceTypeId) ;
		isForward       ->fromCpptoC (pStatus, pReferenceDescription.IsForward) ;
		nodeId          ->fromCpptoC (pStatus, pReferenceDescription.NodeId) ;
		browseName      ->fromCpptoC (pStatus, pReferenceDescription.BrowseName) ;
		displayName     ->fromCpptoC (pStatus, pReferenceDescription.DisplayName) ;

		nodeClass       ->fromCpptoC (pStatus, pReferenceDescription.NodeClass) ;
		typeDefinition  ->fromCpptoC (pStatus, pReferenceDescription.TypeDefinition) ;
}

public:

	ReferenceDescription(
			NodeId         * _referenceTypeId,
			Boolean        * _isForward,
			ExpandedNodeId * _nodeId,
			QualifiedName  * _browseName,
			LocalizedText  * _displayName,

			NodeClass      * _nodeClass,
			ExpandedNodeId * _typeDefinition
			)
		: Structure()
	{
		(referenceTypeId = _referenceTypeId) ->take() ;
		(isForward       = _isForward)       ->take() ;
		(nodeId          = _nodeId)          ->take() ;
		(browseName      = _browseName)      ->take() ;
		(displayName     = _displayName)     ->take() ;

		(nodeClass       = _nodeClass)       ->take() ;
		(typeDefinition  = _typeDefinition)  ->take() ;
	}

	virtual ~ReferenceDescription()
	{
		referenceTypeId ->release() ;
		isForward       ->release() ;
		nodeId          ->release() ;
		browseName      ->release() ;
		displayName     ->release() ;

		nodeClass       ->release() ;
		typeDefinition  ->release() ;
	}

public:

	inline NodeId * getReferenceTypeId()
	{
		return referenceTypeId ;
	}

	inline Boolean * getIsForward()
	{
		return isForward ;
	}

	inline ExpandedNodeId * getNodeId()
	{
		return nodeId ;
	}

	inline QualifiedName * getBrowseName()
	{
		return browseName ;
	}

	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline NodeClass * getNodeClass()
	{
		return nodeClass ;
	}

	inline ExpandedNodeId * getTypeDefinition()
	{
		return typeDefinition ;
	}


};     /* class ReferenceDescription */
}      /* namespace opcua */
#endif /* WITH_BROWSE */
#endif /* OPCUA_REFERENCEDESCRIPTION_H_ */
