
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BASEDEFINE_H_
#define OPCUA_BASEDEFINE_H_

/*
 * OPC UA PART 3, 5.2, P. 13
 * OPC UA Part 5, 5.1, p. 5
 */

namespace opcua {

#define WriteMask_AccessLevel             ((uint32_t)   1)
#define WriteMask_ArrayDimensions         ((uint32_t)   1*2)
#define WriteMask_BrowseName              ((uint32_t)   1*2*2)
#define WriteMask_ContainsNoLoops         ((uint32_t)   1*2*2*2)
#define WriteMask_DataType                ((uint32_t)   1*2*2*2*2)
#define WriteMask_Description             ((uint32_t)   1*2*2*2*2*2)
#define WriteMask_DisplayName             ((uint32_t)   1*2*2*2*2*2*2)
#define WriteMask_EventNotifier           ((uint32_t)   1*2*2*2*2*2*2*2)
#define WriteMask_Executable              ((uint32_t)   1*2*2*2*2*2*2*2*2)
#define WriteMask_Historizing             ((uint32_t)   1*2*2*2*2*2*2*2*2*2)
#define WriteMask_InverseName             ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_IsAbstract              ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_MinimumSamplingInterval ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_NodeClass               ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_NodeId                  ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_Symmetric               ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_UserAccessLevel         ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_UserExecutable          ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_UserWriteMask           ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_ValueRank               ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_WriteMask               ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)
#define WriteMask_ValueForVariableType    ((uint32_t)   1*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2*2)

#define UserWriteMask_AccessLevel             WriteMask_AccessLevel
#define UserWriteMask_ArrayDimensions         WriteMask_ArrayDimensions
#define UserWriteMask_BrowseName              WriteMask_BrowseName
#define UserWriteMask_ContainsNoLoops         WriteMask_ContainsNoLoops
#define UserWriteMask_DataType                WriteMask_DataType
#define UserWriteMask_Description             WriteMask_Description
#define UserWriteMask_DisplayName             WriteMask_DisplayName
#define UserWriteMask_EventNotifier           WriteMask_EventNotifier
#define UserWriteMask_Executable              WriteMask_Executable
#define UserWriteMask_Historizing             WriteMask_Historizing
#define UserWriteMask_InverseName             WriteMask_InverseName
#define UserWriteMask_IsAbstract              WriteMask_IsAbstract
#define UserWriteMask_MinimumSamplingInterval WriteMask_MinimumSamplingInterval
#define UserWriteMask_NodeClass               WriteMask_NodeClass
#define UserWriteMask_NodeId                  WriteMask_NodeId
#define UserWriteMask_Symmetric               WriteMask_Symmetric
#define UserWriteMask_UserAccessLevel         WriteMask_UserAccessLevel
#define UserWriteMask_UserExecutable          WriteMask_UserExecutable
#define UserWriteMask_UserWriteMask           WriteMask_UserWriteMask
#define UserWriteMask_ValueRank               WriteMask_ValueRank
#define UserWriteMask_WriteMask               WriteMask_WriteMask
#define UserWriteMask_ValueForVariableType    WriteMask_ValueForVariableType

} /* namespace opcua */
#endif /* OPCUA_BASEDEFINE_H_ */
