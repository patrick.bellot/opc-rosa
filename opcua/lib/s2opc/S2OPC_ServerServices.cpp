
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../s2opc/S2OPC_ServerServices.h"


MYDLL void ServerService_SendResponse(
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
		SOPC_EncodeableType  * encodeable,
	    OpcUa_ResponseHeader * responseHeader,
	    void                 * response)
{
	SOPC_Buffer * buffer = SOPC_Buffer_Create(SOPC_MAX_MESSAGE_LENGTH) ;

	if (buffer == NULL) {
		odebug_i(S2OPC_ERR,"ServerService_SendResponse","Cannot SOPC_Buffer_Create a buffer of length %d",SOPC_MAX_MESSAGE_LENGTH) ;
		return ;
	}

	SOPC_StatusCode status = SOPC_Buffer_SetDataLength(
			buffer,
			SOPC_UA_SECURE_MESSAGE_HEADER_LENGTH +
            SOPC_UA_SYMMETRIC_SECURITY_HEADER_LENGTH +
            SOPC_UA_SECURE_MESSAGE_SEQUENCE_LENGTH);

	if (status != SOPC_STATUS_OK) {
		odebug(S2OPC_ERR,"ServerService_SendResponse","Cannot SOPC_Buffer_SetDataLength on a buffer") ;
		return ;
	}

	status = SOPC_Buffer_SetPosition(
			buffer,
			SOPC_UA_SECURE_MESSAGE_HEADER_LENGTH +
            SOPC_UA_SYMMETRIC_SECURITY_HEADER_LENGTH +
            SOPC_UA_SECURE_MESSAGE_SEQUENCE_LENGTH);

	if (status != SOPC_STATUS_OK) {
		odebug(S2OPC_ERR,"ServerService_SendResponse","Cannot SOPC_Buffer_SetPosition on a buffer") ;
		return ;
	}

    // Encode OpcUa message (header + body)

	status = SOPC_EncodeMsg_Type_Header_Body(buffer,encodeable,&OpcUa_ResponseHeader_EncodeableType,(void*)responseHeader,(void*)response);

 	if (status != SOPC_STATUS_OK) {
 		odebug_i(S2OPC_ERR,"ServerService_SendResponse","Cannot SOPC_EncodeMsg_Type_Header_Body in the buffer with typeId=%d",encodeable->TypeId) ;
 		return ;
 	}

    // SEND EVENT TO SC LAYER: ASK TO SEND MSG ON SC
 	odebug_ii(S2OPC_DBG,"ServerService_SendResponse","Sending with scId=%d and requestId=%d",scConnectionId,requestId) ;
    SOPC_SecureChannels_EnqueueEvent(SC_SERVICE_SND_MSG,scConnectionId,(void*) buffer,requestId);

 	if (status != SOPC_STATUS_OK) {
 		odebug_i(S2OPC_ERR,"ServerService_SendResponse","Cannot SOPC_SecureChannels_EnqueueEvent the buffer with scConnectionId=%d",scConnectionId) ;
 		return ;
 	}
}

namespace opcua {

MYDLL void ServerService_SendError(
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
	    uint32_t               requestHandle,
		SOPC_StatusCode        status)
{
	OpcUa_ResponseHeader faultHeader ;
	OpcUa_ResponseHeader_Initialize(&faultHeader) ;

	SOPC_DiagnosticInfo  diagnosticInfo ;
	SOPC_DiagnosticInfo_Initialize(&diagnosticInfo) ;

	faultHeader.RequestHandle      = requestHandle ;
	faultHeader.Timestamp          = (uint64_t)(Clock::get_microsecondssince1601() * 10) ;
	faultHeader.ServiceResult      = status ;
	faultHeader.ServiceDiagnostics = diagnosticInfo ;
	faultHeader.NoOfStringTable    = 0 ;
	faultHeader.StringTable        = NULL ;

	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_ServiceFault_EncodeableType,&faultHeader,NULL) ;

    OpcUa_ResponseHeader_Clear(&faultHeader) ;
}

MYDLL void ServerService_SendError(
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
	    OpcUa_RequestHeader  * requestHeader,
		SOPC_StatusCode        status)
{
	ServerService_SendError(scConnectionId,requestId,requestHeader->RequestHandle,status) ;
}



#if (WITH_DISCOVERY == 1)
MYDLL void ServerService_FindServers(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_FindServersRequest  * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_FindServersResponse response ;
	OpcUa_FindServersResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_FindServers(requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	debug(S2OPC_DBG,"ServerService_FindServers","Succeded with status=0") ;
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_FindServersResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	debug_i(S2OPC_ERR,"ServerService_FindServers","Failed with status=0x%08x",status) ;
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_FindServersRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_FindServersResponse_Clear(&response);
}
#endif


#if (WITH_DISCOVERY == 1)
MYDLL void ServerService_GetEndpoints(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_GetEndpointsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_GetEndpointsResponse response ;
	OpcUa_GetEndpointsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_GetEndpoints(requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	debug(S2OPC_DBG,"ServerService_GetEndpoints","Succeded with status=0") ;
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_GetEndpointsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	debug_i(S2OPC_ERR,"ServerService_GetEndpoints","Failed with status=0x%08x",status) ;
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_GetEndpointsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_GetEndpointsResponse_Clear(&response);
}
#endif


#if (WITH_CALL == 1)
MYDLL void ServerService_Call(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_CallRequest         * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_CallResponse response ;
	OpcUa_CallResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_Call(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_CallResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_CallRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_CallResponse_Clear(&response);
}
#endif


#if (WITH_NODEMNGT == 1)
MYDLL void ServerService_AddNodes(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_AddNodesRequest     * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_AddNodesResponse response ;
	OpcUa_AddNodesResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_AddNodes(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_AddNodesResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_AddNodesRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_AddNodesResponse_Clear(&response);
}
#endif


#if (WITH_NODEMNGT == 1)
MYDLL void ServerService_AddReferences(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_AddReferencesRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_AddReferencesResponse response ;
	OpcUa_AddReferencesResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_AddReferences(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_AddReferencesResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_AddReferencesRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_AddReferencesResponse_Clear(&response);
}
#endif


#if (WITH_READ == 1)
MYDLL void ServerService_Read(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_ReadRequest         * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_ReadResponse response ;
	OpcUa_ReadResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_Read(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_ReadResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_ReadRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_ReadResponse_Clear(&response);
}
#endif


#if (WITH_WRITE == 1)
MYDLL void ServerService_Write(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_WriteRequest        * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_WriteResponse response ;
	OpcUa_WriteResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_Write(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_WriteResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_WriteRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_WriteResponse_Clear(&response);
}
#endif


MYDLL void ServerService_CreateSession(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_CreateSessionRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_CreateSessionResponse response ;
	OpcUa_CreateSessionResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_CreateSession(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_CreateSessionResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_CreateSessionRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_CreateSessionResponse_Clear(&response);
}


MYDLL void ServerService_ActivateSession(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_ActivateSessionRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_ActivateSessionResponse response ;
	OpcUa_ActivateSessionResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_ActivateSession(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_ActivateSessionResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_ActivateSessionRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_ActivateSessionResponse_Clear(&response);
}


MYDLL void ServerService_CloseSession(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_CloseSessionRequest  * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_CloseSessionResponse response ;
	OpcUa_CloseSessionResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_CloseSession(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_CloseSessionResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_CloseSessionRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_CloseSessionResponse_Clear(&response);
}


#if (WITH_BROWSE == 1)
MYDLL void ServerService_Browse(
    uint32_t                    scConnectionId,
	uint32_t			        requestId,
    OpcUa_RequestHeader       * requestHeader,
    OpcUa_BrowseRequest       * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_BrowseResponse response ;
	OpcUa_BrowseResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_Browse(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_BrowseResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_BrowseRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_BrowseResponse_Clear(&response);
}
#endif


#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ServerService_RegisterNodes(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_RegisterNodesRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_RegisterNodesResponse response ;
	OpcUa_RegisterNodesResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_RegisterNodes(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_RegisterNodesResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
     	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_RegisterNodesRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_RegisterNodesResponse_Clear(&response);
}
#endif


#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
MYDLL void ServerService_TranslateBrowsePathsToNodeIds(
    uint32_t                     scConnectionId,
	uint32_t			         requestId,
    OpcUa_RequestHeader        * requestHeader,
    OpcUa_TranslateBrowsePathsToNodeIdsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_TranslateBrowsePathsToNodeIdsResponse response ;
	OpcUa_TranslateBrowsePathsToNodeIdsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_TranslateBrowsePathsToNodeIds(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_TranslateBrowsePathsToNodeIdsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_TranslateBrowsePathsToNodeIdsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_TranslateBrowsePathsToNodeIdsResponse_Clear(&response);
}
#endif


#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ServerService_UnregisterNodes(
    uint32_t                       scConnectionId,
	uint32_t			           requestId,
    OpcUa_RequestHeader          * requestHeader,
    OpcUa_UnregisterNodesRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_UnregisterNodesResponse response ;
	OpcUa_UnregisterNodesResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_UnregisterNodes(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_UnregisterNodesResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_UnregisterNodesRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_UnregisterNodesResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_CreateMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_CreateMonitoredItemsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_CreateMonitoredItemsResponse response ;
	OpcUa_CreateMonitoredItemsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_CreateMonitoredItems(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_CreateMonitoredItemsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_CreateMonitoredItemsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_CreateMonitoredItemsResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_DeleteMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_DeleteMonitoredItemsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_DeleteMonitoredItemsResponse response ;
	OpcUa_DeleteMonitoredItemsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_DeleteMonitoredItems(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_DeleteMonitoredItemsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_DeleteMonitoredItemsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_DeleteMonitoredItemsResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_ModifyMonitoredItems(
    uint32_t                            scConnectionId,
	uint32_t			                requestId,
    OpcUa_RequestHeader               * requestHeader,
    OpcUa_ModifyMonitoredItemsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_ModifyMonitoredItemsResponse response ;
	OpcUa_ModifyMonitoredItemsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_ModifyMonitoredItems(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_ModifyMonitoredItemsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_ModifyMonitoredItemsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_ModifyMonitoredItemsResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_SetMonitoringMode(
    uint32_t                         scConnectionId,
	uint32_t			             requestId,
    OpcUa_RequestHeader            * requestHeader,
    OpcUa_SetMonitoringModeRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_SetMonitoringModeResponse response ;
	OpcUa_SetMonitoringModeResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_SetMonitoringMode(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_SetMonitoringModeResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_SetMonitoringModeRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_SetMonitoringModeResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_CreateSubscription(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_CreateSubscriptionRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_CreateSubscriptionResponse response ;
	OpcUa_CreateSubscriptionResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_CreateSubscription(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_CreateSubscriptionResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_CreateSubscriptionRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_CreateSubscriptionResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_DeleteSubscriptions(
    uint32_t                           scConnectionId,
	uint32_t			               requestId,
    OpcUa_RequestHeader              * requestHeader,
    OpcUa_DeleteSubscriptionsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_DeleteSubscriptionsResponse response ;
	OpcUa_DeleteSubscriptionsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_DeleteSubscriptions(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_DeleteSubscriptionsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_DeleteSubscriptionsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_DeleteSubscriptionsResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_ModifySubscription(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_ModifySubscriptionRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_ModifySubscriptionResponse response ;
	OpcUa_ModifySubscriptionResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_ModifySubscription(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_ModifySubscriptionResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_ModifySubscriptionRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_ModifySubscriptionResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_Republish(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_RepublishRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_RepublishResponse response ;
	OpcUa_RepublishResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_Republish(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_RepublishResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_RepublishRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_RepublishResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_SetPublishingMode(
    uint32_t                          scConnectionId,
	uint32_t			              requestId,
    OpcUa_RequestHeader             * requestHeader,
    OpcUa_SetPublishingModeRequest  * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_SetPublishingModeResponse response ;
	OpcUa_SetPublishingModeResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_SetPublishingMode(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_SetPublishingModeResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_SetPublishingModeRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_SetPublishingModeResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_TransferSubscriptions(
    uint32_t                             scConnectionId,
	uint32_t			                 requestId,
    OpcUa_RequestHeader                * requestHeader,
    OpcUa_TransferSubscriptionsRequest * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_TransferSubscriptionsResponse response ;
	OpcUa_TransferSubscriptionsResponse_Initialize(&response) ;

    SOPC_StatusCode status = OpcUa_ServerApi_TransferSubscriptions(scConnectionId,requestHeader,request,&responseHeader,&response) ;

    if (status == STATUS_OK) {
    	ServerService_SendResponse(scConnectionId,requestId,&OpcUa_TransferSubscriptionsResponse_EncodeableType,&responseHeader,(void*)&response) ;
    } else {
    	ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_TransferSubscriptionsRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_TransferSubscriptionsResponse_Clear(&response);
}
#endif


#if (WITH_SUBSCRIPTION == 1)
MYDLL void ServerService_Publish(
    uint32_t                             scConnectionId,
	uint32_t			                 requestId,
    OpcUa_RequestHeader                * requestHeader,
    OpcUa_PublishRequest               * request)
{
	OpcUa_ResponseHeader responseHeader ;
	OpcUa_ResponseHeader_Initialize(&responseHeader) ;

	OpcUa_PublishResponse response ;
	OpcUa_PublishResponse_Initialize(&response) ;

	bool answered = false ;

    SOPC_StatusCode status = OpcUa_ServerApi_Publish(scConnectionId,requestId, requestHeader,request,&responseHeader,&response,&answered) ;

    if (answered) {
    	if (status == STATUS_OK) {
    		ServerService_SendResponse(scConnectionId,requestId,&OpcUa_PublishResponse_EncodeableType,&responseHeader,(void*)&response) ;
    	} else {
    		ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    	}
    } else {
    	if (status != STATUS_OK) {
    		ServerService_SendError(scConnectionId,requestId,requestHeader,status) ;
    	}
    }

    SOPC_Encodeable_Delete(&OpcUa_RequestHeader_EncodeableType,(void **)&requestHeader);
    SOPC_Encodeable_Delete(&OpcUa_PublishRequest_EncodeableType,(void **)&request);
    OpcUa_ResponseHeader_Clear(&responseHeader) ;
    OpcUa_PublishResponse_Clear(&response);
}
#endif



} // namespace opcua


