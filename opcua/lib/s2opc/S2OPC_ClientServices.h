
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_S2OPC_S2OPC_CLIENTSERVICES_H_
#define LIB_S2OPC_S2OPC_CLIENTSERVICES_H_

#include "../OpcUa.h"
#include "../Services/All.h"

MYDLL void ClientService_SendRequest(
    uint32_t               scConnectionId,
	uint32_t			   requestId,
	SOPC_EncodeableType  * encodeable,
    OpcUa_RequestHeader  * requestHeader,
    void                 * response) ;

MYDLL void ClientService_SendResponse(
    uint32_t               scConnectionId,
	uint32_t			   requestId,
	SOPC_EncodeableType  * encodeable,
    OpcUa_ResponseHeader * responseHeader,
    void                 * response) ;


namespace opcua {

MYDLL void ClientService_SendError( /* Because of a link probleme. */
	    uint32_t               scConnectionId,
		uint32_t			   requestId,
	    uint32_t               requestHandle,
		SOPC_StatusCode        status) ;

MYDLL void ClientService_ServiceFault(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responseHeader,
	    OpcUa_ServiceFault                * response) ;



MYDLL void ClientService_CreateSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_CreateSessionResponse       * response) ;

MYDLL void ClientService_ActivateSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_ActivateSessionResponse     * response) ;

MYDLL void ClientService_CloseSession(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_CloseSessionResponse        * response) ;

#if (WITH_READ == 1)
MYDLL void ClientService_Read(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_ReadResponse                * response) ;
#endif

#if (WITH_WRITE == 1)
MYDLL void ClientService_Write(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_WriteResponse               * response) ;
#endif

#if (WITH_CALL == 1)
MYDLL void ClientService_Call(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_CallResponse                * response) ;
#endif

#if (WITH_BROWSE == 1)
MYDLL void ClientService_Browse(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_BrowseResponse              * response) ;
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ClientService_RegisterNodes(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_RegisterNodesResponse       * response) ;
#endif

#if (WITH_REGISTER_UNREGISTER_NODES == 1)
MYDLL void ClientService_UnregisterNodes(
	    uint32_t                            scConnectionId,
		uint32_t			                requestId,
	    OpcUa_ResponseHeader              * responsetHeader,
	    OpcUa_UnregisterNodesResponse     * response) ;
#endif

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)
MYDLL void ClientService_TranslateBrowsePathsToNodeIds(
	    uint32_t                                      scConnectionId,
		uint32_t			                          requestId,
	    OpcUa_ResponseHeader                        * responsetHeader,
	    OpcUa_TranslateBrowsePathsToNodeIdsResponse * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_CreateMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_CreateMonitoredItemsResponse * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_ModifyMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_ModifyMonitoredItemsResponse * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_SetMonitoringMode(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_SetMonitoringModeResponse    * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_DeleteMonitoredItems(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_DeleteMonitoredItemsResponse * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_CreateSubscription(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_CreateSubscriptionResponse   * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_ModifySubscription(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_ModifySubscriptionResponse   * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_SetPublishingMode(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_SetPublishingModeResponse    * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_Publish(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_PublishResponse              * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_Republish(
	    uint32_t                             scConnectionId,
		uint32_t			                 requestId,
	    OpcUa_ResponseHeader               * responsetHeader,
	    OpcUa_RepublishResponse            * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_TransferSubscriptions(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responsetHeader,
	    OpcUa_TransferSubscriptionsResponse * response) ;
#endif

#if (WITH_SUBSCRIPTION == 1)
MYDLL void ClientService_DeleteSubscriptions(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responsetHeader,
	    OpcUa_DeleteSubscriptionsResponse   * response) ;
#endif

#if (WITH_NODEMNGT == 1)
MYDLL void ClientService_AddNodes(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responsetHeader,
	    OpcUa_AddNodesResponse              * response) ;
#endif

#if (WITH_NODEMNGT == 1)
MYDLL void ClientService_AddReferences(
	    uint32_t                              scConnectionId,
		uint32_t			                  requestId,
	    OpcUa_ResponseHeader                * responsetHeader,
	    OpcUa_AddReferencesResponse         * response) ;
#endif

}

#endif /* LIB_S2OPC_S2OPC_CLIENTSERVICES_H_ */
