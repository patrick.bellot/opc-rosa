
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_ADDRESSSPACE_ALL_OPCUA_REFERENCEHIERARCHY_H_
#define LIB_ADDRESSSPACE_ALL_OPCUA_REFERENCEHIERARCHY_H_


#include "../../OpcUa.h"
#include "OpcUa_ReferenceLink.h"

namespace opcua {

// 31 -> 32,33
// 32 -> 37,38,39,40,41,51,52,53,54,117,9004,9005,9006
// 33 -> 34,35,36,14936
// 34 -> 44,45
// 36 -> 48
// 41 -> 3065
// 44 -> 46,47,56
// 47 -> 49,14476,15296,15297,16361

class ReferenceHierarchy
{
public:

	static bool sonOf(int y, int x)
	{
		while (true) {
			if (x == y)
				return true ;
			switch (y) {
			case 31: 		return false ;
			case 32:
			case 33:
				y = 31 ; break ;
			case 37:
			case 38:
			case 39:
			case 40:
			case 41:
			case 51:
			case 52:
			case 53:
			case 54:
			case 117:
			case 9004:
			case 9005:
			case 9006:
				y = 32 ; break ;
			case 34:
			case 35:
			case 36:
			case 14936:
				y = 33 ; break ;
			case 44:
			case 45:
				y = 34 ; break ;
			case 48:
				y = 36 ; break ;
			case 3065:
				y = 41 ; break ;
			case 46:
			case 47:
			case 56:
				y = 44 ; break ;
			case 49:
			case 14476:
			case 15296:
			case 15297:
			case 16361:
				y = 47 ; break ;
			return false ;
			}
		}
		return false ;
	}
};


}      /* namespace opcua */
#endif /* LIB_ADDRESSSPACE_ALL_OPCUA_REFERENCEHIERARCHY_H_ */
