
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "../../OpcUa.h"
#include "../OpcUa_Base.h"

#include "OpcUa_ReferenceLink.h"
#include "../../Server/OpcUa_AddressSpace.h"

namespace opcua {

MYDLL ReferenceType * ReferenceLink::Organizes            = NULL ;
MYDLL ReferenceType * ReferenceLink::HasComponent         = NULL ;
MYDLL ReferenceType * ReferenceLink::HasOrderedComponent  = NULL ;
MYDLL ReferenceType * ReferenceLink::HasProperty          = NULL ;
MYDLL ReferenceType * ReferenceLink::HasSubtype           = NULL ;

MYDLL ReferenceType * ReferenceLink::HasModellingRule     = NULL ;
MYDLL ReferenceType * ReferenceLink::HasTypeDefinition    = NULL ;
MYDLL ReferenceType * ReferenceLink::HasEncoding          = NULL ;
MYDLL ReferenceType * ReferenceLink::HasDescription       = NULL ;
MYDLL ReferenceType * ReferenceLink::HasEventSource       = NULL ;

MYDLL ReferenceType * ReferenceLink::HasNotifier          = NULL ;
MYDLL ReferenceType * ReferenceLink::GeneratesEvent       = NULL ;
MYDLL ReferenceType * ReferenceLink::AlwaysGeneratesEvent = NULL ;

/* Static function
 * - if *pReferenceType != NULL,
 *     - returns *pReferenceType
 * - if *pReferenceType == NULL
 *     - find the referenceType associated to reference_str
 *     - put it in * pReferenceType
 *     - return *pReferenceType
 * USED in fromCode below
 */

MYDLL ReferenceType * ReferenceLink::getReferenceType(ReferenceType ** pReferenceType, uint32_t code)
{
	if (*pReferenceType == NULL) {
		NodeId * referenceTypeId = new NodeId(UInt16::zero,new UInt32(code)) ;
		referenceTypeId->take() ;
	    Base * referenceType = AddressSpace::self->get(referenceTypeId) ;
	    referenceTypeId->release() ;
	    if (referenceType == NULL) {
	    	debug_i(COM_ERR,"ReferenceLink","getReferenceType(**,%d) is null",code) ;
	    	return NULL ;
	    }
	    if (referenceType->getNodeClass()->get() != NodeClass_REFERENCE_TYPE_32) {
	    	debug_i(COM_ERR,"ReferenceLink","getReferenceType(**,%i) not a reference type",code) ;
	    	return NULL ;
	    }
	    (*pReferenceType = dynamic_cast<ReferenceType *>(referenceType))->take() ;
	}
	return *pReferenceType ;
}

/* Static function to get the ReferenceType from the code */

MYDLL ReferenceType * ReferenceLink::fromCode(uint32_t code)
{
	switch (code) {

	case OpcUaRef_Organizes :
		if (Organizes != NULL)
			return Organizes ;
		return getReferenceType(&Organizes,OpcUaRef_Organizes) ;
	case OpcUaRef_HasComponent :
		if (HasComponent != NULL)
			return HasComponent ;
		return getReferenceType(&HasComponent,OpcUaRef_HasComponent) ;
	case OpcUaRef_HasOrderedComponent :
		if (HasOrderedComponent != NULL)
			return HasOrderedComponent ;
		return getReferenceType(&HasOrderedComponent,OpcUaRef_HasOrderedComponent) ;
	case OpcUaRef_HasProperty :
		if (HasProperty != NULL)
			return HasProperty ;
		return getReferenceType(&HasProperty,OpcUaRef_HasProperty) ;
	case OpcUaRef_HasSubtype :
		if (HasSubtype != NULL)
			return HasSubtype ;
		return getReferenceType(&HasSubtype,OpcUaRef_HasSubtype) ;

	case OpcUaRef_HasModellingRule :
		if (HasModellingRule != NULL)
			return HasModellingRule ;
		return getReferenceType(&HasModellingRule,OpcUaRef_HasModellingRule) ;
	case OpcUaRef_HasTypeDefinition :
		if (HasTypeDefinition != NULL)
			return HasTypeDefinition ;
		return getReferenceType(&HasTypeDefinition,OpcUaRef_HasTypeDefinition) ;
	case OpcUaRef_HasEncoding :
		if (HasEncoding != NULL)
			return HasEncoding ;
		return getReferenceType(&HasEncoding,OpcUaRef_HasEncoding) ;
	case OpcUaRef_HasDescription :
		if (HasDescription != NULL)
			return HasDescription ;
		return getReferenceType(&HasDescription,OpcUaRef_HasDescription) ;
	case OpcUaRef_HasEventSource :
		if (HasEventSource != NULL)
			return HasEventSource ;
		return getReferenceType(&HasEventSource,OpcUaRef_HasEventSource) ;

	case OpcUaRef_HasNotifier :
		if (HasNotifier != NULL)
			return HasNotifier ;
		return getReferenceType(&HasNotifier,OpcUaRef_HasNotifier) ;
	case OpcUaRef_GeneratesEvent :
		if (GeneratesEvent != NULL)
			return GeneratesEvent ;
		return getReferenceType(&GeneratesEvent,OpcUaRef_GeneratesEvent) ;
	case OpcUaRef_AlwaysGeneratesEvent :
		if (AlwaysGeneratesEvent != NULL)
			return AlwaysGeneratesEvent ;
		return getReferenceType(&AlwaysGeneratesEvent,OpcUaRef_AlwaysGeneratesEvent) ;

	default:
    	debug_i(COM_ERR,"ReferenceLink","ReferenceType * fromCode(%d) not a repertoried reference code",code) ;
    	CHECK_INT(0) ;
	}

	return NULL ;
}


MYDLL ReferenceLink::ReferenceLink(
		uint32_t   _linkId,
		Base     * _source,
		Base     * _target
		)
	: linkId(_linkId),
	  source(_source),
	  target(_target)
{
	source->setForward(this) ;
	target->setBackward(this) ;
}


MYDLL ReferenceLink::~ReferenceLink()
{
	source->unsetForward(this) ;
	target->unsetBackward(this) ;
}

} /* namespace opcua */
