
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC UA Part 3, 5.3, p. 15
 */

#include "OpcUa_ReferenceType.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "../Utils/OpcUa_StringUtils.h"

namespace opcua {

MYDLL ReferenceType * ReferenceType::explore_element(
			QualifiedName     * browseName,
			NodeId            * nodeId,
			const char        * file_str,
			node_t            * node,
			LinkDescriptions ** pLinkDescriptions
			)
	{
		String * _browseNameStr = browseName->toString() ;
		_browseNameStr->take() ;

		debug_ss(XML_DBG,"ReferenceType","Configuration file \"%s\", ReferenceType = \"%s\"",file_str,_browseNameStr->get()) ;

		if (NodesTable::bad_ns(node,file_str)) {
			_browseNameStr->release() ;
			return NULL ;
		}

		String        * symbolicName  = NULL ;
		LocalizedText * displayName	  = NULL ;
		LocalizedText * description   = NULL ;
		UInt32        * writeMask     = NULL ;
		UInt32        * userWriteMask = NULL ;

		LocalizedText * inverseName = NULL ;
		Boolean       * symmetric   = NULL ;
		Boolean       * isAbstract  = NULL ;

		ReferenceType * result = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"ReferenceType","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMask == NULL) {
				if ((writeMask = UInt32::get_WriteMask_element(_browseNameStr ,file_str,chld))) {
					writeMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMask == NULL) {
				if ((userWriteMask = UInt32::get_UserWriteMask_element(_browseNameStr ,file_str,chld))) {
					userWriteMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"InverseName") == 0 && inverseName == NULL) {
				if ((inverseName = LocalizedText::get_LocalizedText_element(file_str,chld))) {
					inverseName->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"Symmetric") == 0 && symmetric == NULL) {
				if ((symmetric = Boolean::get_Boolean_element(file_str,chld))) {
					symmetric->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"IsAbstract") == 0 && isAbstract == NULL) {
				if ((isAbstract = Boolean::get_Boolean_element(file_str,chld))) {
					isAbstract->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasProperty") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasSubtype") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else {
				debug_sss(COM_ERR,"ReferenceType","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s\"",file_str,name,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		if (displayName == NULL) {
			(displayName = new LocalizedText(_browseNameStr,LocaleId::en))->take() ;
		}

		if (isAbstract == NULL) {
			debug_ss(COM_ERR,"ReferenceType","Configuration file \"%s\", Error : no IsAbstract in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (symmetric == NULL) {
			debug_ss(COM_ERR,"ReferenceType","Configuration file \"%s\", Error : no Symmetric in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		debug_s(XML_DBG,"ReferenceType","Configuration file \"%s\", registering element",file_str) ;

		result =
				new ReferenceType(
						nodeId,
						browseName,
						symbolicName,
						displayName,
						description,
						writeMask,
						userWriteMask,

						isAbstract,
						symmetric,
						inverseName
				) ;

	error:

		if (symbolicName != NULL)
			symbolicName->release() ;
		if (displayName != NULL)
			displayName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;

		if (inverseName != NULL)
			inverseName->release() ;
		if (symmetric != NULL)
			symmetric->release() ;
		if (isAbstract != NULL)
			isAbstract->release() ;

		if (_browseNameStr != NULL)
			_browseNameStr->release() ;

		return result ;
	}

}

