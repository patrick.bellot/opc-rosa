/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * OPC UA Part 3, 5.7, p. 31
 */

#include "OpcUa_Method.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "../Utils/OpcUa_StringUtils.h"

namespace opcua {

MYDLL Method * Method::explore_element(
			QualifiedName     * browseName,
			NodeId            * nodeId,
			const char        * file_str,
			node_t            * node,
			LinkDescriptions ** pLinkDescriptions
			)
	{
		String * _browseNameStr = browseName->toString() ;
		_browseNameStr->take() ;

		debug_ss(XML_DBG,"Method","Configuration file \"%s\", Method = \"%s\"",file_str,_browseNameStr->get()) ;

		if (NodesTable::bad_ns(node,file_str)) {
			_browseNameStr->release() ;
			return NULL ;
		}

		String        * symbolicName  = NULL ;
		LocalizedText * displayName	  = NULL ;
		LocalizedText * description   = NULL ;
		UInt32        * writeMask     = NULL ;
		UInt32        * userWriteMask = NULL ;

		Boolean * executable           = NULL ;
		Boolean * userExecutable       = NULL ;
		NodeId  * methodDeclarationId  = NULL ;

		String * dllName = NULL ;

		int hasModellingRule = 0 ;
		int hasModelParent   = 0 ;

		Method * result = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Method","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMask == NULL) {
				if ((writeMask = UInt32::get_WriteMask_element(_browseNameStr ,file_str,chld))) {
					writeMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMask == NULL) {
				if ((userWriteMask = UInt32::get_UserWriteMask_element(_browseNameStr ,file_str,chld))) {
					userWriteMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"Executable") == 0 && executable == NULL) {
				if ((executable = Boolean::get_Boolean_element(file_str,chld))) {
					executable->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserExecutable") == 0 && userExecutable == NULL) {
				if ((userExecutable = Boolean::get_Boolean_element(file_str,chld))) {
					userExecutable->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"DLLName") == 0 && dllName == NULL) {
				if ((dllName = String::get_File_element(file_str,chld))) {
					dllName->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasProperty") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasModellingRule") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasModellingRule++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasModelParent") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasModelParent++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"GeneratesEvent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"AlwaysGenerateEvent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else {
				debug_sss(COM_ERR,"Method","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s\"",file_str,name,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		if (displayName == NULL) {
			(displayName = new LocalizedText(_browseNameStr,LocaleId::en))->take() ;
		}

		if (executable == NULL) {
			debug_ss(COM_ERR,"Method","Configuration file \"%s\", Error : no Executable in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (userExecutable == NULL) {
			debug_ss(COM_ERR,"Method","Configuration file \"%s\", Error : no UserExecutable in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		if (hasModellingRule > 1) {
			debug_ss(COM_ERR,"Method","Configuration file \"%s\", Error : hasModellingRule > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasModelParent > 1) {
			debug_ss(COM_ERR,"Method","Configuration file \"%s\", Error : hasModelParent > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		debug_s(XML_DBG,"Method","Configuration file \"%s\", registering element",file_str) ;

		result =
				new Method(
						nodeId,
						browseName,
						symbolicName,
						displayName,
						description,
						writeMask,
						userWriteMask,

						executable,
						userExecutable,
						methodDeclarationId,

						dllName
					) ;

	error:

		if (symbolicName != NULL)
			symbolicName->release() ;
		if (displayName != NULL)
			displayName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;

		if (executable != NULL)
			executable->release() ;
		if (userExecutable != NULL)
			userExecutable->release() ;
		if (methodDeclarationId != NULL)
			methodDeclarationId->release() ;

		if (dllName != NULL)
			dllName->release() ;

		if (_browseNameStr != NULL)
			_browseNameStr->release() ;

		return result ;
	}

}




