
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC UA Part 3, 5.6, p. 23
 */

#include "OpcUa_Variable.h"
#include "OpcUa_ExternalVariable.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "../Utils/OpcUa_StringUtils.h"

namespace opcua {

MYDLL Variable * Variable::explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			LinkDescriptions       ** pLinkDescriptions
			)
	{
		String * _browseNameStr = browseName->toString() ;
		_browseNameStr->take() ;

		debug_ss(XML_DBG,"Variable","Configuration file \"%s\", DataType = \"%s\"",file_str,_browseNameStr->get()) ;

		if (NodesTable::bad_ns(node,file_str)) {
			_browseNameStr->release() ;
			return NULL ;
		}

		String        * symbolicName  = NULL ;
		LocalizedText * displayName	  = NULL ;
		LocalizedText * description   = NULL ;
		UInt32        * writeMask     = NULL ;
		UInt32        * userWriteMask = NULL ;

		NodeId        * parentNodeId             = NULL ;
		BaseDataType  * value                    = NULL ;
		NodeId        * dataType                 = NULL ;
		Int32         * valueRank                = NULL ;
		TableUInt32   * arrayDimensions          = NULL ;
		Byte          * accessLevel              = NULL ;
		Byte          * userAccessLevel          = NULL ;
		Duration      * minimumSamplingInterval  = NULL ;
		Boolean       * historizing              = NULL ;

		String        * dllName = NULL ;

		uint32_t        hasModellingRule  = 0 ;
		uint32_t        hasTypeDefinition = 0 ;
		uint32_t        hasModelParent    = 0 ;

		Variable * result = NULL ;

		int nb = roxml_get_chld_nb(node) ;


		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Variable","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMask == NULL) {
				if ((writeMask = UInt32::get_WriteMask_element(_browseNameStr ,file_str,chld))) {
					writeMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMask == NULL) {
				if ((userWriteMask = UInt32::get_UserWriteMask_element(_browseNameStr ,file_str,chld))) {
					userWriteMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"DataValue") == 0 && value == NULL && dataType == NULL) {
				if ((value = BaseDataType::get_BaseDataType_element(&dataType,file_str,chld))) {
					value->take() ;
					dataType->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"ValueRank") == 0 && valueRank == NULL) {
				if ((valueRank = Int32::get_Int32_element(file_str,chld))) {
					valueRank->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"ArrayDimensions") == 0 && arrayDimensions == NULL) {
				if ((arrayDimensions = UInt32::get_UInt32Array_element(file_str,chld))) {
					arrayDimensions->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"AccessLevel") == 0 && accessLevel == NULL) {
				if ((accessLevel = Byte::get_AccessLevel_element(_browseNameStr,file_str,chld))) {
					accessLevel->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserAccessLevel") == 0 && userAccessLevel == NULL) {
				if ((userAccessLevel = Byte::get_UserAccessLevel_element(_browseNameStr,file_str,chld))) {
					userAccessLevel->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"Historizing") == 0 && historizing == NULL) {
				if ((historizing = Boolean::get_Boolean_element(file_str,chld))) {
					historizing->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"DLLName") == 0 && dllName == NULL) {
				if ((dllName = String::get_File_element(file_str,chld))) {
					dllName->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasModellingRule") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
				hasModellingRule++ ;
			} else
			if (StringUtils::strcmpi(name,"HasProperty") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasComponent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasTypeDefinition") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasTypeDefinition++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasModelParent") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasModelParent++ ;
				}
			} else {
				debug_sss(COM_ERR,"Variable","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s\"",file_str,name,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		if (displayName == NULL) {
			(displayName = new LocalizedText(_browseNameStr,LocaleId::en))->take() ;
		}

		if (value == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no Value in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (dataType == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no DataType in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (valueRank == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no ValueRank in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (accessLevel == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no AccessLevel in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (userAccessLevel == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no UserAccessLevel in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (historizing == NULL) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : no Historizing in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		if (hasModellingRule > 1) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : hasModellingRule > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasTypeDefinition != 1) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : hasTypeDefinition <> 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasModelParent > 1) {
			debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : hasModelParent > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		if (valueRank->get() <= 0) {
			if (arrayDimensions != NULL) {
				debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank <= 0 but arrayDimensions not null in \"%s\"",file_str,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
				goto error ;
			}
		} else {
			if (arrayDimensions != NULL) {
				if (valueRank->notEquals(arrayDimensions->getLength())) {
					debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank>0 but arrayDimensions!=valueRank in \"%s\"",file_str,_browseNameStr->get()) ;
					NodesTable::print(file_str,node,1000) ;
					goto error ;
				}
			} else {
				debug_ss(COM_ERR,"Variable","Configuration file \"%s\", Error : valueRank > 0 but arrayDimensions is null in \"%s\"",file_str,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
				goto error ;
			}
		}

		debug_s(XML_DBG,"Variable","Configuration file \"%s\", registering element",file_str) ;


		if (dllName != NULL) {
			result =
					new ExternalVariable(
							nodeId,
							browseName,
							symbolicName,
							displayName,
							description,
							writeMask,
							userWriteMask,

							parentNodeId,
							value,
							dataType,
							valueRank,
							arrayDimensions,
							accessLevel,
							userAccessLevel,
							minimumSamplingInterval,
							historizing,

							dllName
					) ;
		} else {
			result =
					new Variable(
							nodeId,
							browseName,
							symbolicName,
							displayName,
							description,
							writeMask,
							userWriteMask,

							parentNodeId,
							value,
							dataType,
							valueRank,
							arrayDimensions,
							accessLevel,
							userAccessLevel,
							minimumSamplingInterval,
							historizing
					) ;
		}

	error:

		if (displayName != NULL)
			displayName->release() ;
		if (symbolicName != NULL)
			symbolicName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;

		if (value != NULL)
			value->release() ;
		if (dataType != NULL)
			dataType->release() ;
		if (valueRank != NULL)
			valueRank->release() ;
		if (arrayDimensions != NULL)
			arrayDimensions->release() ;
		if (accessLevel != NULL)
			accessLevel->release() ;
		if (userAccessLevel != NULL)
			userAccessLevel->release() ;
		if (minimumSamplingInterval != NULL)
			minimumSamplingInterval->release() ;
		if (historizing != NULL)
			historizing->release() ;

		if (dllName != NULL)
			dllName->release() ;

		if (_browseNameStr != NULL)
			_browseNameStr->release() ;

		return result ;
	}

}


