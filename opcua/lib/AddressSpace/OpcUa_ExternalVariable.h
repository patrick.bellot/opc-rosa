/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_EXTERNALVARIABLE_H_
#define OPCUA_EXTERNALVARIABLE_H_

/*
 * OPC UA Part 3, 5.6, p. 23
 */

#include "../OpcUa.h"

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_Clock.h"
#include "../Error/OpcUa_Error.h"

#include "OpcUa_Base.h"
#include "OpcUa_Variable.h"
#include "OpcUa_VariableDefine.h"

#include <float.h>
#include <stdio.h>

#ifdef _WIN32
# include <Windows.h>
#else
# include <dlfcn.h>
#endif

#ifdef __GNUC__
__extension__
#endif

namespace opcua {

typedef int32_t   start_t() ;
typedef int32_t   stop_t() ;
typedef int32_t   get_t(class ExternalVariable * node, class NumericRange * indexRange, BaseDataType ** value, int64_t * microseconds, int32_t * rc) ;
typedef int32_t   put_t(class ExternalVariable * node, class NumericRange * indexRange, BaseDataType  * value, int32_t * rc) ;

class MYDLL ExternalVariable
	: public Variable
{
private:

	int64_t   serverTimestampMilli ;
	int64_t   sourceTimestampMicro ;
	String  * dllName ;

#ifdef _WIN32
	HINSTANCE    dll_handle ;
#else
	void       * dll_handle ;
	const char * dlerr ;
#endif

	start_t * start ;
	stop_t  * stop ;
	get_t   * get ;
	put_t   * put ;

public:

	ExternalVariable(
			NodeId		   * _nodeId,        // M
			QualifiedName  * _browseName,    // M
			String         * _symbolicName,  //
			LocalizedText  * _displayName,   // M
			LocalizedText  * _description,   // O
			UInt32    	   * _writeMask,     // O
			UInt32	       * _userWriteMask, // O

			NodeId           * _parentNodeId,
			BaseDataType     * _value,                   // M
			NodeId           * _dataType,                // M
			Int32            * _valueRank,               // M
			TableUInt32      * _arrayDimensions,         // O
			Byte             * _accessLevel,             // M
			Byte             * _userAccessLevel,         // M
			Duration         * _minimumSamplingInterval, // O
			Boolean          * _historizing,             // M

			String       * _dllName
			)
		: Variable(
				_nodeId,
				_browseName,
				_symbolicName,
				_displayName,
				_description,
				_writeMask,
				_userWriteMask,

				_parentNodeId,
				_value,
				_dataType,
				_valueRank,
				_arrayDimensions,
				_accessLevel,
				_userAccessLevel,
				_minimumSamplingInterval,
				_historizing
			)
	{
		serverTimestampMilli = (int64_t)0 ;
		sourceTimestampMicro = (int64_t)0 ;

		(dllName = _dllName)->take() ;

		dll_handle = NULL ;

		if (load_dll()) {
			if (start != NULL) {
				int rc = (*start)() ;
				if (rc != 0) {
					debug_i(COM_ERR,"ExternalVariable","start() function failed (error code = %d)",rc) ;
					debug_s(COM_ERR,"ExternalVariable","load_dll() function failed for \"%s\"",_dllName->get()) ;
					unload_dll() ;
				}
			}
		} else {
			debug_s(COM_ERR,"ExternalVariable","load_dll() function failed for \"%s\"",_dllName->get()) ;
			unload_dll() ;
		}

#ifdef _WIN32
#else
		dlerr = NULL ;
#endif
	}


	virtual ~ExternalVariable()
	{
		if (dll_handle != NULL) {

			if (stop != NULL) {
				int rc = (*stop)() ;
				if (rc != 0) {
					debug_i(COM_ERR,"ExternalVariable","stop() function failed (error code = %d)",rc) ;
				}
			}

			if (! unload_dll()) {
				debug(COM_ERR,"ExternalVariable","unload_dll() function failed") ;
			}
		}
	}

private:

	bool load_dll()
	{
		const char * str_base = dllName->get() ;

		if (str_base == NULL) {
			debug(COM_ERR,"ExternalVariable","dll name is NULL") ;
			dll_handle = NULL ;
			return 1 ;
		}

		int str_len  = (int)strlen(str_base) ;

		char * dll_name = new char[str_len+16] ;
		strcpy(dll_name,str_base) ;

#if _WIN32
		strcpy(dll_name+str_len,".dll") ;
#else
# ifdef __APPLE__
		strcpy(dll_name+str_len,".dylib") ;
# else
		strcpy(dll_name+str_len,".so") ;
# endif
#endif

#ifdef _WIN32
		dll_handle = LoadLibrary(TEXT(dll_name)) ;
#else
		dll_handle = dlopen(dll_name,RTLD_LAZY | RTLD_LOCAL) ;
#endif


#ifdef _WIN32
		delete[] dll_name ;

		if (dll_handle == NULL) {
			if (COM_ERR) {
				Error::error_message("ExternalVariable : dll not loadable",__FILE__,__LINE__,GetLastError()) ;
			}
			return false ;
		}
#else
		if (dll_handle == NULL && (dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"ExternalVariable","dll not loadable (dll error = %s) in \"%s\"",dlerr,dll_name) ;
			delete[] dll_name ;
			return false ;
		}
		errno = 0 ; // TBD
#endif

#ifdef _WIN32
		start = reinterpret_cast<start_t *>(GetProcAddress(dll_handle,"start")) ;

		if (start == NULL) {
			if (COM_ERR & DEBUG_LEVEL) {
				Error::error_message("ExternalVariable : cannot find start()",__FILE__,__LINE__,GetLastError()) ;
			}
		}
#else
		start = reinterpret_cast<start_t *>(dlsym(dll_handle,"start")) ;

		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"ExternalVariable","cannot find start() (dll error = %s) in \"%s\"",dlerr,str_base) ;
		}
#endif

#ifdef _WIN32
		stop = reinterpret_cast<stop_t *>(GetProcAddress(dll_handle,"stop")) ;

		if (stop == NULL) {
			if (COM_ERR & DEBUG_LEVEL) {
				Error::error_message("ExternalVariable : cannot find stop()",__FILE__,__LINE__,GetLastError()) ;
			}
		}
#else
		stop = reinterpret_cast<stop_t *>(dlsym(dll_handle,"stop")) ;

		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"ExternalVariable","cannot find stop() (dll error = %s) in \"%s\"",dlerr,str_base) ;
		}
#endif

#ifdef _WIN32
		get = reinterpret_cast<get_t *>(GetProcAddress(dll_handle,"get")) ;

		if (get == NULL) {
			if (COM_ERR & DEBUG_LEVEL) {
				Error::error_message("ExternalVariable : cannot find get()",__FILE__,__LINE__,GetLastError()) ;
			}
			getAccessLevel()->unset(AccessLevel_CurrentRead) ;
		}
#else
		get = reinterpret_cast<get_t *>(dlsym(dll_handle,"get")) ;

		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"ExternalVariable","cannot find get() (dll error = %s) in \"%s\"",dlerr,str_base) ;
			getAccessLevel()->unset(AccessLevel_CurrentRead) ;
		}
#endif

#ifdef _WIN32
		put = reinterpret_cast<put_t *>(GetProcAddress(dll_handle,"put")) ;

		if (put == NULL) {
			if (COM_ERR & DEBUG_LEVEL) {
				Error::error_message("ExternalVariable : cannot find put()",__FILE__,__LINE__,GetLastError()) ;
			}
			getAccessLevel()->unset(AccessLevel_CurrentWrite) ;
		}
#else
		put = reinterpret_cast<put_t *>(dlsym(dll_handle,"put")) ;

		if ((dlerr = dlerror()) != NULL) {
			debug_ss(COM_ERR,"ExternalVariable","cannot find put() (dll error = %s) in \"%s\"",dlerr,str_base) ;
			getAccessLevel()->unset(AccessLevel_CurrentWrite) ;
		}
#endif

		return true ;
	}

	bool unload_dll()
	{
		start = NULL ;
		stop  = NULL ;
		get   = NULL ;
		put   = NULL ;

#ifdef _WIN32
		if (dll_handle == 0) {
			if (FreeLibrary(dll_handle) == 0) {
				if (COM_ERR)
					Error::error_message("ExternalVariable : cannot unload dll",__FILE__,__LINE__,GetLastError()) ;
			}
		}
#else
		if (dlerr == NULL && dlclose(dll_handle)) {
			debug_ss(COM_ERR,"ExternalVariable","cannot unload dll (dll error = %s) in \"%s\"",dlerror(),dllName->get()) ;
		}
#endif

		dll_handle = NULL ;

		return true ;
	}

private:

	int32_t do_get(NumericRange * indexRange, int32_t * p_opcua_rc)
	{
		if (get != NULL) {

			BaseDataType * value = NULL ;
			int64_t        microseconds = 0 ;

			int32_t rc = (*get)(this,indexRange,&value,&microseconds,p_opcua_rc) ;

			serverTimestampMilli = static_cast<int64_t>(Clock::get_millisecondssince1601()) ;

			if (value != NULL)
				value->take() ;

			if (rc != 0) {
				debug_i(COM_ERR,"ExternalVariable","get() function failed (error code = %d)",rc) ;
				if (value != NULL)
					value->release() ;
				return rc ;
			}

			if (indexRange->isNullOrEmpty()) {
				setValue(value) ;
			} else {
				setArray(indexRange,value) ;
			}

			value->release() ;

			sourceTimestampMicro = microseconds ;

			return (int32_t)0 ;
		}

		debug(COM_ERR,"ExternalVariable","get not loaded") ;
		*p_opcua_rc = _Bad_InternalError ;
		return (int32_t)1 ;
	}

private:

	int32_t do_put(NumericRange *indexRange, BaseDataType * value, int32_t * p_opcua_rc)
	{
		if (put != NULL) {

			int32_t rc = (*put)(this,indexRange,value,p_opcua_rc) ;

			if (rc != 0) {
				debug_i(COM_ERR,"ExternalVariable","put() function failed (error code = %d)",rc) ;
				return rc ;
			}

			return (int32_t) 0 ;
		}

		debug(COM_ERR,"ExternalVariable","put not loaded") ;
		*p_opcua_rc = _Bad_InternalError ;
		return (int32_t)1 ;
	}

public:

	virtual StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			uint32_t             builtinTypeId,
			BaseDataType       * newValue
			)
	{
		int32_t opcua_rc = _Good ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::putAttribute(attributeId,indexRange,builtinTypeId,newValue) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			return StatusCode::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			getMutex()->lock() ;
			if (userAccessLevel->isSet(UserAccessLevel_CurrentWrite)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (accessLevel->isUnset(AccessLevel_CurrentWrite)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			if (timesMatch(builtinTypeId,getDataType())) {
				if (indexRange->isNullOrEmpty()) {
					if (do_put(indexRange,newValue,&opcua_rc) != 0) {
						getMutex()->unlock() ;
						return new StatusCode(opcua_rc) ;
					}
					setValue(newValue) ;
				} else {
					StatusCode * rc = checkArray(indexRange,newValue) ;
					if (rc != NULL) {
						getMutex()->unlock() ;
						return rc ;
					}
					if (do_put(indexRange,newValue,&opcua_rc) != 0) {
						getMutex()->unlock() ;
						return new StatusCode(opcua_rc) ;
					}
					setArray(indexRange,newValue) ;
				}
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_TypeMismatch ;
			}
			getMutex()->unlock() ;
			return new StatusCode(opcua_rc) ;

		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
			return Variable::putAttribute(attributeId,indexRange,builtinTypeId,newValue) ;

		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		return new StatusCode(opcua_rc) ;
	}

public:

	virtual DataValue * getAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			Duration           * maxAge,
			TimestampsToReturn * timestampsToReturn
			)
	{
		int32_t opcua_rc = _Good ;

		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_IsAbstract:
		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			return DataValue::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			getMutex()->lock() ;
			if (userAccessLevel->isSet(UserAccessLevel_CurrentRead)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return DataValue::Bad_UserAccessDenied ;
			}
			if (accessLevel->isUnset(AccessLevel_CurrentRead)) {
				getMutex()->unlock() ;
				return DataValue::Bad_NotReadable ;
			}
			if (maxAge->greaterOrEquals(INT32_MAX)) {
				if (serverTimestampMilli == (int64_t)0) {
					if (indexRange->isNullOrEmpty()) {
						if (do_get(indexRange,&opcua_rc) != 0) {
							getMutex()->unlock() ;
							return new DataValue(new StatusCode(opcua_rc)) ;
						}
					} else {
						DataValue * rc = checkArray(indexRange) ;
						if (rc != NULL) {
							getMutex()->unlock() ;
							return rc ;
						}
						if (do_get(indexRange,&opcua_rc) != 0) {
							getMutex()->unlock() ;
							return new DataValue(new StatusCode(opcua_rc)) ;
						}
					}
				}
			} else if (maxAge->equals((int32_t)0) || maxAge->less(static_cast<double>(static_cast<int64_t>(Clock::get_millisecondssince1601()) - serverTimestampMilli))) {
				if (indexRange->isNullOrEmpty()) {
					if (do_get(indexRange,&opcua_rc) != 0) {
						getMutex()->unlock() ;
						return new DataValue(new StatusCode(opcua_rc)) ;
					}
				} else {
					DataValue * rc = checkArray(indexRange) ;
					if (rc != NULL) {
						getMutex()->unlock() ;
						return rc ;
					}
					if (do_get(indexRange,&opcua_rc) != 0) {
						getMutex()->unlock() ;
						return new DataValue(new StatusCode(opcua_rc)) ;
					}
				}
			}
			if (indexRange->isNullOrEmpty()) {
				if ((data = getValue()))
					data->take() ;
			} else {
				if ((data = takeArray(indexRange)))
					data->take() ;
			}
			getMutex()->unlock() ;
			break ;

		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
			return Variable::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * sourceTimestamp = NULL ;
		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = new UtcTime(10 * sourceTimestampMicro) ;
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = new UtcTime(10000 * serverTimestampMilli) ;
			break ;
		case TimestampsToReturn_BOTH_2:
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = new UtcTime(10 * sourceTimestampMicro) ;
			serverTimestamp = new UtcTime(10000 * serverTimestampMilli) ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		DataValue * dataValue
			= new DataValue((data==NULL)?(reinterpret_cast<Variant *>(NULL)):(data->getVariant()),
							new StatusCode(opcua_rc),
							sourceTimestamp,
							serverTimestamp
			) ;

		if (data != NULL)
			data->release() ;

		return dataValue ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_EXTERNALVARIABLE_H_ */
