
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATATYPE_H_
#define OPCUA_DATATYPE_H_

/*
 * OPC UA Part 3, 5.8.3, p. 35
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_Base.h"

namespace opcua {

class MYDLL DataType
	: public Base
{
private:

	Boolean * isAbstract ; // M

public:

	DataType(
			NodeId		   * _nodeId,        // M
			QualifiedName  * _browseName,    // M
			String         * _symbolicName,  //
			LocalizedText  * _displayName,   // M
			LocalizedText  * _description,   // O
			UInt32    	   * _writeMask,     // O
			UInt32	       * _userWriteMask, // O

			Boolean        * _isAbstract     // M
			)
		: Base(
				_nodeId,                 // M
				NodeClass::data_type_64, // M
				_browseName,             // M
				_symbolicName,
				_displayName,            // M
				_description,            // O
				_writeMask,              // O
				_userWriteMask           // O
				)
	{
		(isAbstract = _isAbstract)->take() ;
	}

	virtual ~DataType()
	{
		isAbstract->release() ;
	}

public:

	virtual StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			uint32_t             builtinTypeId,
			BaseDataType       * value
			)
	{
		if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
			// true for all attribute id
			return StatusCode::Bad_IndexRangeInvalid ;
		}

		BaseDataType * tmp ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::putAttribute(attributeId,indexRange,builtinTypeId,value) ;

		case AttributeId_IsAbstract:
			if (builtinTypeId != Builtin_Boolean)
				return StatusCode::Bad_TypeMismatch ;
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_IsAbstract)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_IsAbstract)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp        = isAbstract ;
			isAbstract = reinterpret_cast<Boolean *>(value) ;
			if (value != NULL)
				value->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		if (tmp != NULL)
			tmp->release() ;

		return StatusCode::Good ;
	}

public:

	virtual DataValue * getAttribute(
							IntegerId          * attributeId,
							NumericRange       * indexRange,
							Duration           * maxAge,
							TimestampsToReturn * timestampsToReturn
						)
	{
		if (! indexRange->isNullOrEmpty()) {
			// true for all attribute id
			return DataValue::Bad_IndexRangeInvalid ;
		}

		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_IsAbstract:
			getMutex()->lock() ;
			if ((data = isAbstract))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
		case AttributeId_Value:
		case AttributeId_DataType:
		case AttributeId_ValueRank:
		case AttributeId_ArrayDimensions:
		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_BOTH_2:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		DataValue * dataValue
				= new DataValue(
						(data==NULL)?(reinterpret_cast<Variant *>(NULL)):(data->getVariant()),
						StatusCode::Good,
						NULL, serverTimestamp
						) ;

		if (data != NULL)
			data->release() ;

		return dataValue ;
	}

public:

	static DataType * explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			class LinkDescriptions ** pLinkDescriptions
			) ;

};

} /* namespace opcua */
#endif /* OPCUA_DATATYPE_H_ */
