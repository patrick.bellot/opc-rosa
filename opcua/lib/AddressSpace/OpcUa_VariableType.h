
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIABLETYPE_H_
#define OPCUA_VARIABLETYPE_H_

/*
 * OPC UA Part 3, 5.6.5, p. 29
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "OpcUa_Base.h"

namespace opcua {

class MYDLL VariableType
	: public Base
{
private:

	BaseDataType     * value ;                   // O
	NodeId           * dataType ;                // M
	Int32            * valueRank ;               // M
	TableUInt32      * arrayDimensions ;         // O
	Boolean          * isAbstract ;              // M

public:

	VariableType(
			NodeId		   * _nodeId,        // M
			QualifiedName  * _browseName,    // M
			String         * _symbolicName,  //
			LocalizedText  * _displayName,   // M
			LocalizedText  * _description,   // O
			UInt32    	   * _writeMask,     // O
			UInt32	       * _userWriteMask, // O

			BaseDataType     * _value,           // O
			NodeId           * _dataType,        // M
			Int32            * _valueRank,       // M
			TableUInt32      * _arrayDimensions, // O
			Boolean          * _isAbstract       // M
			)
		: Base(
				_nodeId,                     // M
				NodeClass::variable_type_16, // M
				_browseName,                 // M
				_symbolicName,
				_displayName,                // M
				_description,                // O
				_writeMask,                  // O
				_userWriteMask               // O
				)
	{
		if ((value = _value))
			value->take() ;
		if ((dataType = _dataType))
			dataType->take() ;
		(valueRank = _valueRank)->take() ;
		if ((arrayDimensions = _arrayDimensions))
			arrayDimensions->take() ;
		(isAbstract = _isAbstract)->take() ;
	}

	virtual ~VariableType()
	{
		if ((value))
			value->release() ;
		if ((dataType))
			dataType->release() ;
		valueRank->release() ;
		if ((arrayDimensions))
			arrayDimensions->release() ;
		isAbstract->release() ;
	}

public:

	virtual StatusCode * putAttribute(
			IntegerId          * attributeId,
			NumericRange       * indexRange,
			uint32_t             builtinTypeId,
			BaseDataType       * newValue
			)
	{
		BaseDataType * tmp = NULL ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::putAttribute(attributeId,indexRange,builtinTypeId,newValue) ;

		case AttributeId_IsAbstract:
			if (builtinTypeId != Builtin_Boolean)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_IsAbstract)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_IsAbstract)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp        = isAbstract ;
			isAbstract = reinterpret_cast<Boolean *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			return StatusCode::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_ValueForVariableType)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_ValueForVariableType)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			if (timesMatch(builtinTypeId,dataType)) {
				if (indexRange->isNullOrEmpty()) {
					tmp   = value ;
					value = reinterpret_cast<BaseDataType *>(newValue) ;
					if (newValue != NULL)
						newValue->take() ;
				} else {
					// TBD implement index range
					getMutex()->unlock() ;
					return StatusCode::Bad_NotImplemented ;
				}
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_TypeMismatch ;
			}
			getMutex()->unlock() ;
			break ;

		case AttributeId_DataType:
			if (builtinTypeId != Builtin_NodeId)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_DataType)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_DataType)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp      = dataType ;
			dataType = reinterpret_cast<NodeId *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ValueRank:
			if (builtinTypeId != Builtin_Int32)
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_ValueRank)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_ValueRank)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp       = valueRank ;
			valueRank = reinterpret_cast<Int32 *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ArrayDimensions:
			if (builtinTypeId != Builtin_UInt32)
				return StatusCode::Bad_TypeMismatch ;
			if (! newValue->isArray())
				return StatusCode::Bad_TypeMismatch ;
			if (indexRange != NULL && ! indexRange->isNullOrEmpty()) {
				return StatusCode::Bad_NotImplemented ;
			}
			getMutex()->lock() ;
			if (userWriteMask == NULL || userWriteMask->isSet(UserWriteMask_AccessLevel)) {
				// TBD access control
			} else {
				getMutex()->unlock() ;
				return StatusCode::Bad_UserAccessDenied ;
			}
			if (writeMask != NULL && writeMask->isUnset(UserWriteMask_AccessLevel)) {
				getMutex()->unlock() ;
				return StatusCode::Bad_NotWritable ;
			}
			tmp             = arrayDimensions ;
			arrayDimensions = reinterpret_cast<TableUInt32   *>(newValue) ;
			if (newValue != NULL)
				newValue->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return StatusCode::Bad_AttributeIdInvalid ;
		}

		if (tmp != NULL)
			tmp->release() ;

		return StatusCode::Good ;
	}

public:

	virtual DataValue * getAttribute(
							IntegerId          * attributeId,
							NumericRange       * indexRange,
							Duration           * maxAge,
							TimestampsToReturn * timestampsToReturn
						)
	{
		BaseDataType * data ;

		switch (attributeId->get())
		{
		case AttributeId_NodeId:
		case AttributeId_NodeClass:
		case AttributeId_BrowseName:
		case AttributeId_DisplayName:
		case AttributeId_Description:
		case AttributeId_WriteMask:
		case AttributeId_UserWriteMask:
			return Base::getAttribute(attributeId,indexRange,maxAge,timestampsToReturn) ;

		case AttributeId_IsAbstract:
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = isAbstract))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_Symmetric:
		case AttributeId_InverseName:
		case AttributeId_ContainsNoLoops:
		case AttributeId_EventNotifier:
			return DataValue::Bad_AttributeIdInvalid ;

		case AttributeId_Value:
			if (indexRange->isNullOrEmpty()) {
				getMutex()->lock() ;
				if ((data = value))
					data->take() ;
				getMutex()->unlock() ;
			} else {
				// TBD implement index range
				return DataValue::Bad_NotImplemented ;
			}
			break ;

		case AttributeId_DataType:
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = dataType))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ValueRank:
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = valueRank))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_ArrayDimensions:
			if (! indexRange->isNullOrEmpty()) {
				return DataValue::Bad_IndexRangeInvalid ;
			}
			getMutex()->lock() ;
			if ((data = arrayDimensions))
				data->take() ;
			getMutex()->unlock() ;
			break ;

		case AttributeId_AccessLevel:
		case AttributeId_UserAccessLevel:
		case AttributeId_MinimumSamplingInterval:
		case AttributeId_Historizing:
		case AttributeId_Executable:
		case AttributeId_UserExecutable:
		default:
			return DataValue::Bad_AttributeIdInvalid ;
		}

		UtcTime * sourceTimestamp = NULL ;
		UtcTime * serverTimestamp = NULL ;

		switch(timestampsToReturn->get())
		{
		case TimestampsToReturn_SOURCE_0:
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_SERVER_1:
			serverTimestamp = UtcTime::now() ;
			break ;
		case TimestampsToReturn_BOTH_2:
			serverTimestamp = UtcTime::now() ;
			if (attributeId->get() == AttributeId_Value)
				sourceTimestamp = serverTimestamp ;
			break ;
		case TimestampsToReturn_NEITHER_3:
			break ;
		default:
			if (data != NULL)
				data->release() ;
			return DataValue::Bad_TimestampsToReturnInvalid ;
		}

		DataValue * dataValue
			= new DataValue((data==NULL)?(reinterpret_cast<Variant *>(NULL)):(data->getVariant()),
							StatusCode::Good,
							sourceTimestamp, serverTimestamp
							) ;

		if (data != NULL)
			data->release() ;

		return dataValue ;
	}

public:

	static VariableType * explore_element(
			QualifiedName           * browseName,
			NodeId                  * nodeId,
			const char              * file_str,
			node_t                  * node,
			class LinkDescriptions ** pLinkDescriptions
			) ;

};

} /* namespace opcua */
#endif /* OPCUA_VARIABLE_H_ */
