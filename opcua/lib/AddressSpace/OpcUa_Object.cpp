
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * OPC UA Part 3, 5.5, p. 20
 * OPC UA Part 5, 5.2, p. 6
 */

#include "OpcUa_Object.h"
#include "../StandardDataTypes/All.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "OpcUa_Base.h"
#include "../Utils/OpcUa_StringUtils.h"

namespace opcua {


MYDLL Object * Object::explore_element(
			QualifiedName     * browseName,
			NodeId            * nodeId,
			const char        * file_str,
			node_t            * node,
			LinkDescriptions ** pLinkDescriptions
			)
	{
		String * _browseNameStr = browseName->toString() ;
		_browseNameStr->take() ;

		debug_ss(XML_DBG,"Object","Configuration file \"%s\", Object = \"%s\"",file_str,_browseNameStr->get()) ;

		if (NodesTable::bad_ns(node,file_str)) {
			_browseNameStr->release() ;
			return NULL ;
		}

		String        * symbolicName  = NULL ;
		LocalizedText * displayName	  = NULL ;
		LocalizedText * description   = NULL ;
		UInt32        * writeMask     = NULL ;
		UInt32        * userWriteMask = NULL ;

		Byte * eventNotifier = NULL;

		int hasModellingRule  = 0 ;
		int hasTypeDefinition = 0 ;
		int hasModelParent    = 0 ;
		int hasDescription    = 0 ;

		Object * result = NULL ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Object","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"WriteMask") == 0 && writeMask == NULL) {
				if ((writeMask = UInt32::get_WriteMask_element(_browseNameStr ,file_str,chld))) {
					writeMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"UserWriteMask") == 0 && userWriteMask == NULL) {
				if ((userWriteMask = UInt32::get_UserWriteMask_element(_browseNameStr ,file_str,chld))) {
					userWriteMask->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"EventNotifier") == 0 && eventNotifier == NULL) {
				if ((eventNotifier = Byte::get_EventNotifier_element(file_str,chld))) {
					eventNotifier->take() ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasComponent") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasProperty") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasModellingRule") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasModellingRule++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasTypeDefinition") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasTypeDefinition++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasModelParent") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasModelParent++ ;
				}
			} else
			if (StringUtils::strcmpi(name,"HasEventSource") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasNotifier") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"Organizes") == 0) {
				NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions) ;
			} else
			if (StringUtils::strcmpi(name,"HasDescription") == 0) {
				if (NodesTable::get_link_element(name,nodeId,file_str,chld,pLinkDescriptions)) {
					hasDescription++ ;
				}
			} else {
				debug_sss(COM_ERR,"Object","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"%s\"",file_str,name,_browseNameStr->get()) ;
				NodesTable::print(file_str,node,1000) ;
			}

			roxml_release(name) ;
		}

		if (displayName == NULL) {
			(displayName = new LocalizedText(_browseNameStr,LocaleId::en))->take() ;
		}

		if (eventNotifier == NULL) {
			debug_ss(COM_ERR,"Object","Configuration file \"%s\", Error : no EventNotifier in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		if (hasModellingRule > 1) {
			debug_ss(COM_ERR,"Object","Configuration file \"%s\", Error : hasModellingRule > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasTypeDefinition != 1) {
			debug_ss(COM_ERR,"Object","Configuration file \"%s\", Error : hasTypeDefinition <> 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasModelParent > 1) {
			debug_ss(COM_ERR,"Object","Configuration file \"%s\", Error : hasModelParent > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}
		if (hasDescription > 1) {
			debug_ss(COM_ERR,"Object","Configuration file \"%s\", Error : hasDescription > 1 in \"%s\"",file_str,_browseNameStr->get()) ;
			NodesTable::print(file_str,node,1000) ;
			goto error ;
		}

		debug_s(XML_DBG,"Object","Configuration file \"%s\", registering element",file_str) ;

		result =
				new Object(
						nodeId,
						browseName,
						symbolicName,
						displayName,
						description,
						writeMask,
						userWriteMask,

						eventNotifier
				) ;

	error:

		if (symbolicName != NULL)
			symbolicName->release() ;
		if (displayName != NULL)
			displayName->release() ;
		if (description != NULL)
			description->release() ;
		if (writeMask != NULL)
			writeMask->release() ;
		if (userWriteMask != NULL)
			userWriteMask->release() ;

		if (eventNotifier != NULL)
			eventNotifier->release() ;

		if (_browseNameStr != NULL)
			_browseNameStr->release() ;

		return result ; ;
	}


}


