
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef INFORMATIONMODEL_ALL_H_
#define INFORMATIONMODEL_ALL_H_

#include 	   "OpcUa_Base.h"
#include       "OpcUa_Reference.h"
#include       "OpcUa_ReferenceType.h"
#include       "OpcUa_View.h"
#include       "OpcUa_Object.h"
#include       "OpcUa_ObjectType.h"
#include       "OpcUa_Variable.h"
#include       "OpcUa_ExternalVariable.h"
#include       "OpcUa_VariableType.h"
#include       "OpcUa_Method.h"
#include       "OpcUa_DataType.h"

#endif /* INFORMATIONMODEL_ALL_H_ */
