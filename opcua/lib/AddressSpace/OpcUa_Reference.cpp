
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpcUa_View.h"
#include "../Server/OpcUa_LinkDescriptions.h"
#include "../Server/OpcUa_NodesTable.h"
#include "../Utils/OpcUa_StringUtils.h"
#include "../StandardDataTypes//OpcUa_IPCS_NodeId.h"

#ifndef _WIN32
#   include <inttypes.h>
#endif

namespace opcua {

	MYDLL void Reference::explore_element(
			QualifiedName           * browseName,
			const char              * file_str,
			node_t                  * node,
			LinkDescriptions       ** pLinkDescriptions
			)
	{


		debug_ss(XML_DBG,"Reference","Configuration file \"%s\", Reference = \"%s\"",file_str,browseName->getName()->get()) ;

		if (NodesTable::bad_ns(node,file_str))
			return ;

		NodeId * source    = NULL ;
		NodeId * target    = NULL ;
		char   * nodeClass = NULL ;

		bool go = true ;

		int nb = roxml_get_chld_nb(node) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * chld = roxml_get_chld(node,NULL,i) ;
			char *   name = roxml_get_name(chld,NULL,0) ;

			debug_ss(XML_DBG,"Reference","Configuration file \"%s\", element = \"%s\"",file_str,name) ;

			if (StringUtils::strcmpi(name,"Source") == 0 && source == NULL) {
				if (!Reference::getSource(file_str,chld,&source))
					go = false ;
				if (source != NULL)
					source->take() ;
			} else
			if (StringUtils::strcmpi(name,"Target") == 0 && target == NULL) {
				if (!Reference::getTarget(file_str,chld,&target,&nodeClass))
					go = false ;
				if (target != NULL)
					target->take() ;
			} else {
				go = false ;
				debug_ss(COM_ERR,"Reference","Configuration file \"%s\", Error : unknown or duplicate element \"%s\"",file_str,name) ;
			}

			roxml_release(name) ;
		}

		if (source == NULL) {
			debug_s(COM_ERR,"Reference","Configuration file \"%s\", Error : no Source element",file_str) ;
			go = false ;
		}

		if (target == NULL) {
			debug_s(COM_ERR,"Reference","Configuration file \"%s\", Error : no Target element",file_str) ;
			go = false ;
		}

		if (nodeClass == NULL) {
			debug_s(COM_ERR,"Reference","Configuration file \"%s\", Error : no NodeClass attribute in Target",file_str) ;
			go = false ;
		}

		if (go) {
			*pLinkDescriptions = new LinkDescriptions(new LinkDescription(browseName->getName()->get(),nodeClass,source,target),*pLinkDescriptions) ;
		} else {
			NodesTable::print(file_str,node,1000) ;
		}

		if (source != NULL)
			source->release() ;
		if (target != NULL)
			target->release() ;
		if (nodeClass != NULL)
			delete[] nodeClass ;
	}

	MYDLL bool Reference::getSource(
			const char    * file_str,
			node_t        * node,
			class NodeId ** pNodeId
			)
	{
		if (NodesTable::bad_ns(node,file_str))
			return false ;

		int nb = roxml_get_chld_nb(node) ; // child element

		if (nb != 0) {
			debug_si(COM_ERR,"Reference","Configuration file \"%s\", Error : Source element with %d child(ren), must be zero",file_str,nb) ;
			return false ;
		}

		nb = roxml_get_attr_nb(node) ; // attributes

		if (nb != 1) {
			debug_si(COM_ERR,"Reference","Configuration file \"%s\", Error : Source element with %d element(s), must be one",file_str,nb) ;
			return false ;
		}

		node_t * attr = roxml_get_attr(node,NULL,0) ;

		if (NodesTable::bad_ns(attr,file_str))
			return false ;

		char * nodeIdStr = roxml_get_name(attr,NULL,0) ;

		if (StringUtils::strcmpi(nodeIdStr,"NodeId") != 0) {
			roxml_release(nodeIdStr) ;
			return false ;
		}

		roxml_release(nodeIdStr) ;

		char * _nodeId = roxml_get_content(attr,NULL,0,NULL) ;

		if (_nodeId == NULL)
			return false ;

		if (StringUtils::isNumeric(_nodeId)) {
			errno = 0 ;
			int64_t value = strtoll(_nodeId,(char **)NULL,0) ;
			if (errno == 0 && static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
				*pNodeId = new NodeId(UInt16::zero,new UInt32(static_cast<uint32_t>(value))) ;
			} else {
				*pNodeId = new NodeId(UInt16::zero,new String(_nodeId)) ;
			}
		} else {
			*pNodeId = new NodeId(UInt16::zero,new String(_nodeId)) ;
		}

		roxml_release(_nodeId) ;

		return true ;
	}

	MYDLL bool Reference::getTarget(
			const char    * file_str,
			node_t        * node,
			class NodeId ** pNodeId,
			char         ** pNodeClass
			)
	{
		if (NodesTable::bad_ns(node,file_str))
			return false ;

		int nb = roxml_get_chld_nb(node) ; // child element

		if (nb != 0) {
			debug_si(COM_ERR,"Reference","Configuration file \"%s\", Error : Target element with %d child(ren), must be zero",file_str,nb) ;
			return false ;
		}

		nb = roxml_get_attr_nb(node) ; // attributes

		if (nb != 2) {
			debug_si(COM_ERR,"Reference","Configuration file \"%s\", Error : Target element with %d element(s), must be two",file_str,nb) ;
			return false ;
		}

		bool error = false ;

		for (int i = 0 ; i < 2 ; i++) {

			node_t * attr = roxml_get_attr(node,NULL,i) ;

			if (NodesTable::bad_ns(attr,file_str))
				return false ;

			char * attr_name = roxml_get_name(attr,NULL,0) ;
			char * attr_val  = roxml_get_content(attr,NULL,0,NULL) ;

			if (StringUtils::strcmpi(attr_name,"NodeId") == 0) {
				if (*pNodeId == NULL) {
					if (StringUtils::isNumeric(attr_val)) {
						errno = 0 ;
						int64_t value = strtoll(attr_val,(char **)NULL,0) ;
						if (errno == 0 && static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
							*pNodeId = new NodeId(UInt16::zero,new UInt32(static_cast<uint32_t>(value))) ;
						} else {
							*pNodeId = new NodeId(UInt16::zero,new String(attr_val)) ;
						}
					} else {
						*pNodeId = new NodeId(UInt16::zero,new String(attr_val)) ;
					}
				} else {
					debug_s(COM_ERR,"Reference","Configuration file \"%s\", Error : Target element with duplicate NodeId attribute",file_str) ;
					error = true ;
				}
			} else
			if (StringUtils::strcmpi(attr_name,"NodeClass") == 0) {
				if (*pNodeClass == NULL) {
					*pNodeClass = StringUtils::strdup(attr_val) ;
				} else {
					debug_s(COM_ERR,"Reference","Configuration file \"%s\", Error : Target element with duplicate NodeClass attribute",file_str) ;
					error = true ;
				}
			} else {
				debug_ss(COM_ERR,"Reference","Configuration file \"%s\", Error : Target element with unknown attribute \"%s\"",file_str,attr_name) ;
				error = true ;
			}

			roxml_release(attr_name) ;
			roxml_release(attr_val) ;

			if (error)
				return false ;
		}

		return true ;
	}

}



