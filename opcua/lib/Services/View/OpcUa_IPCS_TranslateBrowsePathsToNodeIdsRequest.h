
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_
#define OPCUA_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_

/*
 * Part 4, 5.8.4.2, p. 43
 */

#include "../../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL TranslateBrowsePathsToNodeIdsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_TranslateBrowsePathsToNodeIdsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_TranslateBrowsePathsToNodeIdsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader     * requestHeader ;
	TableBrowsePath   * browsePaths ;

public:

	static TranslateBrowsePathsToNodeIdsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_TranslateBrowsePathsToNodeIdsRequest const& pTranslateBrowsePathsToNodeIdsRequest)
	{
		RequestHeader     * requestHeader = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		TableBrowsePath   * browsePaths   = TableBrowsePath::fromCtoCpp ( pStatus, pTranslateBrowsePathsToNodeIdsRequest.NoOfBrowsePaths, pTranslateBrowsePathsToNodeIdsRequest.BrowsePaths) ;

		if (*pStatus == STATUS_OK)
			return
					new TranslateBrowsePathsToNodeIdsRequest(
							requestHeader,
							browsePaths
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (browsePaths != NULL)
			browsePaths->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_TranslateBrowsePathsToNodeIdsRequest& pTranslateBrowsePathsToNodeIdsRequest) const
	{
		requestHeader ->fromCpptoC (pStatus, pRequestHeader) ;
		browsePaths   ->fromCpptoC (pStatus, pTranslateBrowsePathsToNodeIdsRequest.NoOfBrowsePaths, pTranslateBrowsePathsToNodeIdsRequest.BrowsePaths) ;
	}

public:

	TranslateBrowsePathsToNodeIdsRequest(
			RequestHeader     * _requestHeader,
			TableBrowsePath   * _browsePaths
			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(browsePaths   = _browsePaths)   ->take() ;
	}

	virtual ~TranslateBrowsePathsToNodeIdsRequest()
	{
		requestHeader ->release() ;
		browsePaths   ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableBrowsePath   * getBrowsePaths()
	{
		return browsePaths ;
	}

};
} /* namespace opcua */
#endif /* WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS */
#endif /* OPCUA_TRANSLATEBROWSEPATHSTONODEIDSREQUEST_H_ */
