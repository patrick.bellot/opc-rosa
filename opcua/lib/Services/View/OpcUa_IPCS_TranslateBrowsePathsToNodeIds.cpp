/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIdsRequest.h"
#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIdsResponse.h"
#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIds.h"
#include "../../Stacks/OpcUa_BaseList.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_TranslateBrowsePathsToNodeIds(
		uint32_t				  	                  secureChannelIdNum,
		OpcUa_RequestHeader                         * pRequestHeader,
		OpcUa_TranslateBrowsePathsToNodeIdsRequest  * pRequest,
		OpcUa_ResponseHeader                        * pResponseHeader,
		OpcUa_TranslateBrowsePathsToNodeIdsResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	TranslateBrowsePathsToNodeIdsRequest * request  = NULL ;
	request = TranslateBrowsePathsToNodeIdsRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	TranslateBrowsePathsToNodeIdsResponse * response = NULL ;
	TranslateBrowsePathsToNodeIds::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void TranslateBrowsePathsToNodeIds::answer(
		SOPC_StatusCode                              * pStatus,
		uint32_t					                   secureChannelIdNum,
	    class TranslateBrowsePathsToNodeIdsRequest   * request,
	    class TranslateBrowsePathsToNodeIdsResponse ** pResponse)
{
	AddressSpace * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"TranslateBrowsePathsToNodeIds","processing request") ;


	RequestHeader                        * requestHeader       = request->getRequestHeader() ;
	IntegerId                            * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken           * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		             * channel ;
	Session                              * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"TranslateBrowsePathsToNodeIds","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer Request : getting browsePaths.") ;
	TableBrowsePath   * browsePaths = request->getBrowsePaths() ;

	int32_t length = browsePaths->getLength() ;
	debug_i(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer Request : browsePaths length=%d",length) ;

	TableBrowsePathResult   * results = new TableBrowsePathResult  (length) ;

	debug_i(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer Request : getting %d results.",length) ;

	for (int32_t i = 0 ; i < length ; i++) {

		BrowsePath * browsePath   = browsePaths->get(i) ;
		NodeId     * startingNode = browsePath->getStartingNode() ;

		if (DEBUG_LEVEL & BRO_DBG) {
			String * startingNodeStr = startingNode->getString() ;
			debug_is(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer Request [%d]:  startingNode = \"%s\"",i,startingNodeStr->get()) ;
			startingNodeStr->checkRefCount() ;
		}

		debug(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer  searching base for startingNode found") ;

		Base * base = addressSpace->get(startingNode) ;

		debug_p(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer  base for startingNode : %p",base) ;

		BrowsePathResult * browsePathResult = NULL ;

		if (base == NULL) {

			browsePathResult
				= new BrowsePathResult(
						StatusCode::Bad_NodeIdInvalid,
						new TableBrowsePathTarget  (0)
						) ;

			String * startingNodeStr = startingNode->getString() ;
			debug_is(COM_ERR,"TranslateBrowsePathsToNodeIds","answer Request nodeId [%d]:  NodeId Invalid = \"%s\"",i,startingNodeStr->get()) ;
			startingNodeStr->checkRefCount() ;

		} else {


			BaseList                * startingBases = new BaseList(base,NULL) ;
			RelativePath            * relativePath  = browsePath->getRelativePath() ;
			TableBrowsePathTarget   * targetsTable  = NULL ;

			StatusCode * statusCode = translate(startingBases,relativePath,&targetsTable) ;

			browsePathResult
				= new BrowsePathResult(
						statusCode,
						targetsTable) ;
		}

		results->set(i,browsePathResult) ;
	}

	debug(BRO_DBG,"TranslateBrowsePathsToNodeIds","answer Request succeeds : returning Response.") ;

	*pResponse =
			new TranslateBrowsePathsToNodeIdsResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}


MYDLL StatusCode * TranslateBrowsePathsToNodeIds::translate(
		BaseList                 * startingBases,
		RelativePath             * relativePath,
		TableBrowsePathTarget   ** pTargetsList)
{
	debug(BRO_DBG,"TranslateBrowsePathsToNodeIds","translate begins") ;

	TableRelativePathElement * elements   = relativePath->getElements() ;
	int32_t                    pathLength = elements->getLength() ;

	debug_i(BRO_DBG,"TranslateBrowsePathsToNodeIds","translate loop begins, pathLength = %d",pathLength) ;

	for (int32_t i = 0 ; i < pathLength ; i++) {

		if (BaseList::getLength(startingBases) == 0)
			goto fin ;

		RelativePathElement * element = elements->get(i) ;

		NodeId * referenceTypeId = element->getReferenceTypeId() ;

#if (INGORE_IncludeSubtypes_IN_TranslateBrowsePathsToNodeIds == 1)
#else
		if ( !referenceTypeId->equals(NodeId::nullNodeId) && element->getIncludeSubtypes()->get()) {
			debug(COM_ERR,"TranslateBrowsePathsToNodeIds","includeSubtypes = true not implemented") ;
			BaseList::deleteAll(startingBases) ;
			*pTargetsList = new TableBrowsePathTarget  (0) ;
			return StatusCode::Bad_NotImplemented ;
		}
#endif

		QualifiedName * targetName = element->getTargetName() ;

		BaseList * newStartingBases = NULL ;
		BaseList * curStartingBases = startingBases ;

		if (referenceTypeId->equals(NodeId::nullNodeId)) {
			while (curStartingBases != NULL) {
				Base * base = curStartingBases->getCar() ;
				if (element->getIsInverse()->get()) {
					ReferenceLinkList * backwards = base->getBackward() ;
					while (backwards != NULL) {
						ReferenceLink * backward = backwards->getCar() ;
						Base          * target   = backward->getSource() ;
						if (target->equals(targetName) || (i = pathLength-1 && targetName->isNull()))
							newStartingBases = new BaseList(target,newStartingBases) ;
						backwards = backwards->getCdr() ;
					}
				} else {
					ReferenceLinkList * forwards = base->getForward() ;
					while (forwards != NULL) {
						ReferenceLink * forward = forwards->getCar() ;
						Base          * target  = forward->getTarget() ;
						if (target->equals(targetName) || (i = pathLength-1 && targetName->isNull()))
							newStartingBases = new BaseList(target,newStartingBases) ;
						forwards = forwards->getCdr() ;
					}
				}
				curStartingBases = curStartingBases->getCdr() ;
			}
			BaseList::deleteAll(startingBases) ;
			startingBases = newStartingBases ;

		} else {

			if (! referenceTypeId->isNumeric()) {
				String * str = referenceTypeId->getString() ;
				debug_s(COM_ERR,"TranslateBrowsePathsToNodeIds","referenceTypeId not a numeric NodeId = %s",str->get()) ;
				str->checkRefCount() ;
				BaseList::deleteAll(startingBases) ;
				*pTargetsList = new TableBrowsePathTarget  (0) ;
				return StatusCode::Bad_NotImplemented ;
			}

			uint32_t code = referenceTypeId->getNumeric()->get() ;

			while (curStartingBases != NULL) {
				Base * base = curStartingBases->getCar() ;
				if (element->getIsInverse()->get()) {
					ReferenceLinkList * backwards = base->getBackward() ;
					while (backwards != NULL) {
						ReferenceLink * backward = backwards->getCar() ;
						if (backward->getLinkId() == code) {
							Base          * target   = backward->getSource() ;
							if (target->equals(targetName) || (i = pathLength-1 && targetName->isNull()))
								newStartingBases = new BaseList(target,newStartingBases) ;
						}
						backwards = backwards->getCdr() ;
					}
				} else {
					ReferenceLinkList * forwards = base->getForward() ;
					while (forwards != NULL) {
						ReferenceLink * forward = forwards->getCar() ;
						if (forward->getLinkId() == code) {
							Base          * target  = forward->getTarget() ;
							if (target->equals(targetName) || (i = pathLength-1 && targetName->isNull()))
								newStartingBases = new BaseList(target,newStartingBases) ;
						}
						forwards = forwards->getCdr() ;
					}
				}
				curStartingBases = curStartingBases->getCdr() ;
			}
			BaseList::deleteAll(startingBases) ;
			startingBases = newStartingBases ;

		}
	}

fin:

	debug(BRO_DBG,"TranslateBrowsePathsToNodeIds","translate ends with StatusCode::Good") ;
	int32_t length = BaseList::getLength(startingBases) ;
	debug_i(BRO_DBG,"TranslateBrowsePathsToNodeIds","translate length result is %d",length) ;

	*pTargetsList = new TableBrowsePathTarget  (length) ;

	BaseList * curr = startingBases ;
	for (int32_t i = 0 ; i < length ; i++) {
		(*pTargetsList)->set(i, new BrowsePathTarget(new ExpandedNodeId(curr->getCar()->getNodeId()),Index::zero)) ;
		curr = curr->getCdr() ;
	}

	BaseList::deleteAll(startingBases) ;

	return (length == 0) ? StatusCode::Bad_NoMatch : StatusCode::Good ;
}


} /* namespace opcua */

#endif // WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS
