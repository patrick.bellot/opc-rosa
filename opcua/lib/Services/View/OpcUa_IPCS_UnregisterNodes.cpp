
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_UnregisterNodesRequest.h"
#include "OpcUa_IPCS_UnregisterNodesResponse.h"
#include "OpcUa_IPCS_UnregisterNodes.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_UnregisterNodes(
		uint32_t				  	    secureChannelIdNum,
		OpcUa_RequestHeader           * pRequestHeader,
		OpcUa_UnregisterNodesRequest  * pRequest,
		OpcUa_ResponseHeader          * pResponseHeader,
		OpcUa_UnregisterNodesResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	UnregisterNodesRequest * request  = NULL ;
	request = UnregisterNodesRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	UnregisterNodesResponse * response = NULL ;
	UnregisterNodes::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void UnregisterNodes::answer(
		SOPC_StatusCode                * pStatus,
		uint32_t					     secureChannelIdNum,
	    class UnregisterNodesRequest   * request,
	    class UnregisterNodesResponse ** pResponse)
{
	AddressSpace  * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"UnregisterNodes","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good)
		return ;

	debug(COM_DBG,"UnregisterNodes","Request : getting parameters.") ;

	TableNodeId   * nodesToUnregister = request->getNodesToUnregister() ;
	int32_t         length            = nodesToUnregister->getLength() ;

	debug(COM_DBG,"UnregisterNodes","Request : unregistering nodes.") ;

	for (int i = 0 ; i < length; i++) {
		NodeId * nodeId = nodesToUnregister->get(i) ;
		Base   * base   = addressSpace->get(nodeId) ;
		session->deleteRegisteredNode(base) ;
	}

	debug(COM_DBG,"UnregisterNodes","Request succeeds : returning Response.") ;

	*pResponse = // Part 4, 5.6.3.2, p. 29
			new UnregisterNodesResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					)
			) ;
}

} /* namespace opcua */

#endif

