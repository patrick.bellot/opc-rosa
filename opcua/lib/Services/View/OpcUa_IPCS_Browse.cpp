/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../AddressSpace/All/OpcUa_ReferenceHierarchy.h"

#include "OpcUa_IPCS_BrowseRequest.h"
#include "OpcUa_IPCS_BrowseResponse.h"
#include "OpcUa_IPCS_Browse.h"

#include "../../NotificationsAndEvents/OpcUa_ReferenceDescriptionList.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Browse(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_BrowseRequest        * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_BrowseResponse       * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	BrowseRequest * request  = NULL ;
	request = BrowseRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	BrowseResponse * response = NULL ;
	Browse::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void Browse::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class BrowseRequest         * request,
	    class BrowseResponse       ** pResponse)
{
	AddressSpace  * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"Browse","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Browse","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(BRO_DBG,"Browse","Request : getting view description.") ;
	ViewDescription * view = request->getView() ;

	if (view->getTimestamp()->isNotZero()) {
		debug(COM_ERR,"Browse","Request : ViewDescription->timeStamp != 0.") ;
		*pStatus = _Bad_NotImplemented ;
		return ;
	}

	if (view->getViewVersion()->isNotZero()) {
		debug(COM_ERR,"Browse","Request : ViewDescription->viewVersion != 0.") ;
		*pStatus = _Bad_NotImplemented ;
		return ;
	}

	NodeId * viewId = view->getViewId() ;

	if (viewId->isNullNodeId())
		viewId = NULL ;

	if (viewId != NULL) {
		debug(COM_ERR,"Browse","Request : ViewDescription->viewId != NULL not implemented.") ;
		*pStatus = _Bad_NotImplemented ;
		return ;
	}

	Counter * requestedMaxReferencesPerNode = request->getRequestedMaxReferencesPerNode() ;
	debug_i(BRO_DBG,"Browse","Request : getting requested max references per node = %d",requestedMaxReferencesPerNode->get()) ;

#if (IGNORE_RequestedMaxReferencesPerNode_IN_Browse == 1)
#else
	if (requestedMaxReferencesPerNode->isNotZero()) {
		debug(COM_ERR,"Browse","Request : ViewDescription->requestedMaxReferencesPerNode != 0.") ;
		*pStatus = _Bad_NotImplemented ;
		return ;
	}
#endif

	debug(BRO_DBG,"Browse","Request : getting nodes to Browse.") ;
	TableBrowseDescription   * nodesToBrowse = request->getNodesToBrowse() ;
	debug(BRO_DBG,"Browse","Request : building result.") ;
	int32_t length = nodesToBrowse->getLength() ;

	TableBrowseResult     * results         = new TableBrowseResult  (length) ;

	debug_i(BRO_DBG,"Browse","Request : getting %d results.",length) ;

	for (int32_t i = 0 ; i < length ; i++) {

		debug_i(BRO_DBG,"Browse","Results begins (%d)",i) ;

		BrowseDescription * browseDescription = nodesToBrowse->get(i) ;

		NodeId * nodeId = browseDescription->getNodeId() ;

		Base * node = addressSpace->get(nodeId) ;

		if (node == NULL) {
			String * nodeIdStr = nodeId->toString() ;
			debug_s(COM_ERR,"Browse","nodeId don't exists \"%s\"",nodeIdStr->get()) ;
			nodeIdStr->checkRefCount() ;
			results->set(i, BrowseResult::Bad_NodeIdInvalid) ;
			continue ;
		}

#if (BRO_DBG & DEBUG_LEVEL)
		String * tmpString = nodeId->toString() ;
		debug_s(BRO_DBG,"Browse","Browsing Node (%s)",tmpString->get()) ;
		tmpString->checkRefCount() ;
#endif

		int32_t _browseDirection = browseDescription->getBrowseDirection()->get() ;

		debug_i(BRO_DBG,"Browse","Browsing Direction (%d)",_browseDirection) ;

		NodeId * referenceTypeId = browseDescription->getReferenceTypeId() ;

		uint32_t referenceLinkCode = 0 ;

		if (referenceTypeId->isNotNullNodeId()) {

			if (referenceTypeId->getNamespaceIndex()->isNotZero()) {
				debug(COM_ERR,"Browse","referenceTypeId not local") ;
				results         ->set(i, BrowseResult::Bad_ReferenceTypeIdInvalid) ;
				continue ;
			}

			if (referenceTypeId->getIdentifierType()->get() != IdType_NUMERIC_0) {
				debug(COM_ERR,"Browse","referenceTypeId not a Numeric") ;
				results         ->set(i, BrowseResult::Bad_ReferenceTypeIdInvalid) ;
				continue ;
			}

//			Base * referenceType = BaseService::get(referenceTypeId,session,addressSpace) ;
//
//			if (referenceType == NULL) {
//				debug(COM_ERR,"Browse","referenceType don't exists") ;
//				results         ->set(i, BrowseResult::Bad_ReferenceTypeIdInvalid) ;
//				continue ;
//			}
//
//			if (referenceType->getNodeClass()->get() != NodeClass_REFERENCE_TYPE_32) {
//				debug(COM_ERR,"Browse","referenceType not a reference type") ;
//				results         ->set(i, BrowseResult::Bad_ReferenceTypeIdInvalid) ;
//				continue ;
//			}

			referenceLinkCode = referenceTypeId->getNumeric()->get() ;

			debug_i(BRO_DBG,"Browse","Browsing Reference (%d)",referenceLinkCode) ;
		} else {
			debug(BRO_DBG,"Browse","Browsing Reference (NULL)") ;
		}

		Boolean * includeSubtypes = browseDescription->getIncludeSubtypes() ;

		debug(BRO_DBG,"Browse","Browsing Reference IncludeSubtypes (false)") ;

		uint32_t _nodeClassMask =  browseDescription->getNodeClassMask()->get() ;

		debug_i(BRO_DBG,"Browse","Browsing NodeClass (%ux)",_nodeClassMask) ;

		uint32_t _resultMask = browseDescription->getResultMask()->get() ;

		debug_i(BRO_DBG,"Browse","Browsing ResultMask (%ux)",_resultMask) ;

		ReferenceDescriptionList * referenceDescriptionList = NULL ;

		if (_browseDirection != BrowseDirection_INVERSE_1) {
			debug(BRO_DBG,"Browse","Request scanning forward list (exploit).") ;
			exploit(
					includeSubtypes->isSet(),
					&referenceDescriptionList,
					node->getForward(),
					referenceLinkCode,
					_nodeClassMask,
					_resultMask,
					false
					) ;
		}

		if (_browseDirection != BrowseDirection_FORWARD_0) {
			debug(BRO_DBG,"Browse","Request scanning backward list (exploit).") ;
			exploit(
					includeSubtypes->isSet(),
					&referenceDescriptionList,
					node->getBackward(),
					referenceLinkCode,
					_nodeClassMask,
					_resultMask,
					true
					) ;
		}

		TableReferenceDescription   * references ;

		if (referenceDescriptionList != NULL) {
			ReferenceDescriptionList    * tmpReferenceDescriptionList = referenceDescriptionList ;
			int32_t 				      referenceLength             = ReferenceDescriptionList::getLength(tmpReferenceDescriptionList) ;

			references = new TableReferenceDescription  (referenceLength) ;

			for (int32_t j = 0 ; j < referenceLength ; j++) {
				ReferenceDescription * referenceDescription = tmpReferenceDescriptionList->getCar() ;
#if 0
				ExpandedNodeId * expandedNodeId = referenceDescription->getNodeId() ;
				String * str = expandedNodeId->toString() ;
				str->take() ;
				debug_s(BRO_DBG,"Browse","new ReferenceDescription = %s",str->get()) ;
				str->release() ;
#endif
				references->set(j,referenceDescription) ;
				tmpReferenceDescriptionList = tmpReferenceDescriptionList->getCdr() ;
			}

			referenceDescriptionList->deleteAll() ;
		} else {
			references = ReferenceDescription::tableZero ;
		}

		BrowseResult * browseResult = new BrowseResult(references) ;

		results         ->set(i, browseResult) ;
	}

	debug(BRO_DBG,"Browse","Request succeeds : returning Response.") ;

	*pResponse =
			new BrowseResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}


void Browse::exploit(
		bool                        includeSubtypes,
		ReferenceDescriptionList ** referenceDescriptionList,
		ReferenceLinkList         * referenceLinkList,
		uint32_t                    referenceLinkCode,
		uint32_t                    _nodeClassMask,
		uint32_t                    _resultMask,
		bool						isInverse)
{
	debug(BRO_DBG,"Browse","exploit : beginning") ;

	while (referenceLinkList != NULL) {

		ReferenceLink * referenceLink = referenceLinkList->getCar() ;
		debug(BRO_DBG,"Browse","exploit : next link") ;

		uint32_t linkCode = referenceLink->getLinkId() ;

		if (referenceLinkCode != 0) {
			if (linkCode != referenceLinkCode) {
				if (includeSubtypes) {
					if (! ReferenceHierarchy::sonOf(linkCode,referenceLinkCode)) {
						debug(BRO_DBG,"Browse","exploit : reference link not good") ;
						referenceLinkList = referenceLinkList->getCdr() ;
						continue ;
					}
				} else {
					referenceLinkList = referenceLinkList->getCdr() ;
					continue ;
				}
			}
		}

		Base * target ;

		if (isInverse) {
			target = referenceLink->getSource() ;
		} else {
			target = referenceLink->getTarget() ;
		}

		NodeClass  * targetNodeClass = target->getNodeClass() ;
		int32_t     _targetNodeClass = targetNodeClass->get() ;

		if (_nodeClassMask != (uint32_t)0) {
			if ((_targetNodeClass & _nodeClassMask) == 0) {
				referenceLinkList = referenceLinkList->getCdr() ;
				debug(BRO_DBG,"Browse","exploit : nodeClass not good") ;
				continue ;
			}
		}

#if (BRO_DBG & DEBUG_LEVEL)
		{
			String * str = target->getNodeId()->toString() ;
			str->take() ;
			debug_s(BRO_DBG,"Browse","exploit : get one target (forward) = %s",str->get()) ;
			str->release() ;
		}
#endif

		ReferenceType * referenceType = ReferenceLink::fromCode(linkCode) ;

		QualifiedName * browseName ;
		if (_resultMask & (((uint32_t)1) << 3))
			browseName = target->getBrowseName() ;
		else
			browseName = QualifiedName::null ;

		LocalizedText * displayName ;
		if (_resultMask & (((uint32_t)1) << 4))
			displayName = target->getDisplayName() ;
		else
			displayName = LocalizedText::null ;

		ExpandedNodeId * typeDefinition ;
		if (_resultMask & (((uint32_t)1) << 5)) {
			if (_targetNodeClass == NodeClass_OBJECT_1 || _targetNodeClass == NodeClass_VARIABLE_2) {
				typeDefinition = target->hasTypeDefinition() ;
			} else {
				typeDefinition = ExpandedNodeId::nullExpandedNodeId ;
			}
		} else {
			typeDefinition = ExpandedNodeId::nullExpandedNodeId ;
		}

		ExpandedNodeId * expandedNodeId = new ExpandedNodeId(target->getNodeId()) ;

#if 0
		String * str = expandedNodeId->toString() ;
		str->take() ;
		debug_s(BRO_DBG,"Browse","exploit : new ReferenceDescription = %s",str->get()) ;
		str->release() ;
#endif

		ReferenceDescription * referenceDescription
			= new ReferenceDescription(
					referenceType->getNodeId(),
					(isInverse) ? (Boolean::booleanFalse) : (Boolean::booleanTrue),
					expandedNodeId,
					browseName,
					displayName,
					targetNodeClass,
					typeDefinition
					) ;

		*referenceDescriptionList = new ReferenceDescriptionList(referenceDescription,*referenceDescriptionList) ;

		referenceLinkList = referenceLinkList->getCdr() ;
	}

	debug(BRO_DBG,"Browse","exploit : exiting") ;
}


} /* namespace opcua */

#endif
