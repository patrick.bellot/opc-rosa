
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_VIEW_ALL_H
#define _OPCUA_VIEW_ALL_H

#include "OpcUa_IPCS_BrowseRequest.h"
#include "OpcUa_IPCS_BrowseResponse.h"
#include "OpcUa_IPCS_Browse.h"

#include "OpcUa_IPCS_RegisterNodesRequest.h"
#include "OpcUa_IPCS_RegisterNodesResponse.h"
#include "OpcUa_IPCS_RegisterNodes.h"

#include "OpcUa_IPCS_UnregisterNodesRequest.h"
#include "OpcUa_IPCS_UnregisterNodesResponse.h"
#include "OpcUa_IPCS_UnregisterNodes.h"

#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIdsRequest.h"
#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIdsResponse.h"
#include "OpcUa_IPCS_TranslateBrowsePathsToNodeIds.h"

#endif



