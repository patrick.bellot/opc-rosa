
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UNREGISTERNODESREQUEST_H_
#define OPCUA_UNREGISTERNODESREQUEST_H_

/*
 * Part3, 5.8.6, p. 45
 */

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL UnregisterNodesRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_UnregisterNodesRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_UnregisterNodesRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	TableNodeId   * nodesToUnregister ;

public:

	static UnregisterNodesRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_UnregisterNodesRequest const& pUnregisterNodesRequest)
	{
		RequestHeader * requestHeader     = RequestHeader ::fromCtoCpp(pStatus, pRequestHeader) ;
		TableNodeId   * nodesToUnregister = TableNodeId::fromCtoCpp(pStatus, pUnregisterNodesRequest.NoOfNodesToUnregister,pUnregisterNodesRequest.NodesToUnregister) ;

		if (*pStatus == STATUS_OK)
			return
					new UnregisterNodesRequest(
							requestHeader,
							nodesToUnregister
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (nodesToUnregister != NULL)
			nodesToUnregister->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_UnregisterNodesRequest& pUnregisterNodesRequest) const
	{
		requestHeader     ->fromCpptoC(pStatus, pRequestHeader) ;
		nodesToUnregister ->fromCpptoC (pStatus, pUnregisterNodesRequest.NoOfNodesToUnregister,pUnregisterNodesRequest.NodesToUnregister) ;
	}

public:

	UnregisterNodesRequest(
			RequestHeader * _requestHeader,
			TableNodeId   * _nodesToUnregister
			)
		: Structure()
	{
		(requestHeader     = _requestHeader)   ->take() ;
		(nodesToUnregister = _nodesToUnregister) ->take() ;
	}

	virtual ~UnregisterNodesRequest()
	{
		requestHeader     ->release() ;
		nodesToUnregister ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableNodeId   * getNodesToUnregister()
	{
		return nodesToUnregister;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_UNREGISTERNODESREQUEST_H_ */
