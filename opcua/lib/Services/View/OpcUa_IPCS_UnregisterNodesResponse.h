
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_UNREGISTERNODESRESPONSE_H_
#define OPCUA_UNREGISTERNODESRESPONSE_H_

/*
 * Part3, 5.8.6, p. 45
 */

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL UnregisterNodesResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_UnregisterNodesResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_UnregisterNodesResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader * responseHeader ;

public:

	static UnregisterNodesResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader,  OpcUa_UnregisterNodesResponse const& pUnregisterNodesResponse)
	{
		ResponseHeader * responseHeader    = ResponseHeader ::fromCtoCpp(pStatus, pResponseHeader) ;

		if (*pStatus == STATUS_OK)
			return
					new UnregisterNodesResponse(
							responseHeader
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;

		NOT_USED(pUnregisterNodesResponse) ;

		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_UnregisterNodesResponse& pUnregisterNodesResponse) const
	{
		responseHeader    ->fromCpptoC(pStatus, pResponseHeader) ;

		NOT_USED(pUnregisterNodesResponse) ;
	}

public:

	UnregisterNodesResponse(
			ResponseHeader * _responseHeader
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)     ->take() ;
	}

	virtual ~UnregisterNodesResponse()
	{
		responseHeader ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_UNREGISTERNODESRESPONSE_H_ */
