
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BROWSERESPONSE_H_
#define OPCUA_BROWSERESPONSE_H_

/*
 * Part 4, 5.8.2.2, p. 38-39
 */

#include "../../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL BrowseResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_BrowseResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_BrowseResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableBrowseResult     * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static BrowseResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_BrowseResponse const& pBrowseResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp   (pStatus, pResponseHeader) ;
		TableBrowseResult     * results         = TableBrowseResult::fromCtoCpp   (pStatus, pBrowseResponse.NoOfResults,         pBrowseResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pBrowseResponse.NoOfDiagnosticInfos, pBrowseResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new BrowseResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_BrowseResponse& pBrowseResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pBrowseResponse.NoOfResults,         pBrowseResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pBrowseResponse.NoOfDiagnosticInfos, pBrowseResponse.DiagnosticInfos) ;
	}

public:

	BrowseResponse(
			ResponseHeader        * _responseHeader,
			TableBrowseResult     * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~BrowseResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableBrowseResult   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* BROWSE */
#endif /* OPCUA_BROWSERESPONSE_H_ */
