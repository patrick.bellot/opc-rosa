
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REGISTERNODES_H_
#define OPCUA_REGISTERNODES_H_

#include "../../OpcUa.h"

#if (WITH_REGISTER_UNREGISTER_NODES == 1)

#include "../OpcUa_BaseService.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_RegisterNodes(
		uint32_t					  secureChannelIdNum,
		OpcUa_RequestHeader         * pRequestHeader,
		OpcUa_RegisterNodesRequest  * pRequest,
		OpcUa_ResponseHeader        * pResponseHeader,
		OpcUa_RegisterNodesResponse * pResponse) ;

class MYDLL RegisterNodes
	: public BaseService
{
public:

	static void answer(
			SOPC_StatusCode              * pStatus,
			uint32_t					   secureChannelIdNum,
		    class RegisterNodesRequest   * request,
		    class RegisterNodesResponse ** pResponse) ;

};

} /* namespace opcua */
#endif
#endif /* REGISTERNODES_H_ */
