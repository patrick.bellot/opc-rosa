/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_TRANSFERSUBSCRIPTIONSREQUEST_H_
#define OPCUA_TRANSFERSUBSCRIPTIONSREQUEST_H_

/*
 * Part 4, 5.13.7.2, p. 88
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL TransferSubscriptionsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_TransferSubscriptionsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_TransferSubscriptionsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader    * requestHeader ;
	TableIntegerId   * subscriptionIds ;
	Boolean          * sendInitialValues ;

public:

	static TransferSubscriptionsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_TransferSubscriptionsRequest const& pTransferSubscriptionsRequest)
	{
		RequestHeader    * requestHeader     = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		TableIntegerId   * subscriptionIds   = TableIntegerId::fromCtoCpp( pStatus, pTransferSubscriptionsRequest.NoOfSubscriptionIds, pTransferSubscriptionsRequest.SubscriptionIds) ;
		Boolean          * sendInitialValues = Boolean       ::fromCtoCpp( pStatus, pTransferSubscriptionsRequest.SendInitialValues) ;

		if (*pStatus == STATUS_OK)
			return
					new TransferSubscriptionsRequest(
							requestHeader,
							subscriptionIds,
							sendInitialValues
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionIds != NULL)
			subscriptionIds->checkRefCount() ;
		if (sendInitialValues != NULL)
			sendInitialValues->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_TransferSubscriptionsRequest& pTransferSubscriptionsRequest) const
	{
		requestHeader     ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionIds   ->fromCpptoC (pStatus, pTransferSubscriptionsRequest.NoOfSubscriptionIds, pTransferSubscriptionsRequest.SubscriptionIds) ;
		sendInitialValues ->fromCpptoC (pStatus, pTransferSubscriptionsRequest.SendInitialValues) ;
	}

public:

	TransferSubscriptionsRequest(
			RequestHeader    * _requestHeader,
			TableIntegerId   * _subscriptionIds,
			Boolean          * _sendInitialValues
			)
		: Structure()
	{
		(requestHeader     = _requestHeader)     ->take() ;
		(subscriptionIds   = _subscriptionIds)   ->take() ;
		(sendInitialValues = _sendInitialValues) ->take() ;
	}

	virtual ~TransferSubscriptionsRequest()
	{
		requestHeader     ->release() ;
		subscriptionIds   ->release() ;
		sendInitialValues ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableIntegerId   * getSubscriptionIds()
	{
		return subscriptionIds ;
	}

	inline Boolean * getSendInitialValues()
	{
		return sendInitialValues ;
	}

};

} /* namespace opcua */

#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_TRANSFERSUBSCRIPTIONSREQUEST_H_ */
