/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REPUBLISHREQUEST_H_
#define OPCUA_REPUBLISHREQUEST_H_

/*
 * Part 4, 5.13.6.2, p. 87
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL RepublishRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RepublishRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_RepublishRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	IntegerId     * subscriptionId ;
	Counter       * retransmitSequenceNumber ;

public:

	static RepublishRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_RepublishRequest const& pRepublishRequest)
	{
		RequestHeader * requestHeader            = RequestHeader ::fromCtoCpp ( pStatus, pRequestHeader) ;
		IntegerId     * subscriptionId           = IntegerId     ::fromCtoCpp ( pStatus, pRepublishRequest.SubscriptionId) ;
		Counter       * retransmitSequenceNumber = Counter       ::fromCtoCpp ( pStatus, pRepublishRequest.RetransmitSequenceNumber) ;

		if (*pStatus == STATUS_OK)
			return
					new RepublishRequest(
							requestHeader,
							subscriptionId,
							retransmitSequenceNumber
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (retransmitSequenceNumber != NULL)
			retransmitSequenceNumber->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_RepublishRequest& pRepublishRequest) const
	{
		requestHeader            ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId           ->fromCpptoC (pStatus, pRepublishRequest.SubscriptionId) ;
		retransmitSequenceNumber ->fromCpptoC (pStatus, pRepublishRequest.RetransmitSequenceNumber) ;
	}

public:

	RepublishRequest(
			RequestHeader * _requestHeader,
			IntegerId     * _subscriptionId,
			Counter       * _retransmitSequenceNumber
			)
		: Structure()
	{
		(requestHeader            = _requestHeader)            ->take() ;
		(subscriptionId           = _subscriptionId)           ->take() ;
		(retransmitSequenceNumber = _retransmitSequenceNumber) ->take() ;
	}

	virtual ~RepublishRequest()
	{
		requestHeader            ->release() ;
		subscriptionId           ->release() ;
		retransmitSequenceNumber ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline Counter * getRetransmitSequenceNumber()
	{
		return retransmitSequenceNumber ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_REPUBLISHREQUEST_H_ */
