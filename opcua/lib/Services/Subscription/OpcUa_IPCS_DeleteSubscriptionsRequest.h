/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DELETESUBSCRIPTIONSREQUEST_H_
#define OPCUA_DELETESUBSCRIPTIONSREQUEST_H_

/*
 * Part 4, 5.13.8.2, p. 89
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL DeleteSubscriptionsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_DeleteSubscriptionsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_DeleteSubscriptionsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader    * requestHeader ;
	TableIntegerId   * subscriptionIds ;

public:

	static DeleteSubscriptionsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_DeleteSubscriptionsRequest const& pDeleteSubscriptionsRequest)
	{
		RequestHeader    * requestHeader     = RequestHeader ::fromCtoCpp( pStatus, pRequestHeader) ;
		TableIntegerId   * subscriptionIds   = TableIntegerId::fromCtoCpp( pStatus, pDeleteSubscriptionsRequest.NoOfSubscriptionIds, pDeleteSubscriptionsRequest.SubscriptionIds) ;

		if (*pStatus == STATUS_OK)
			return
					new DeleteSubscriptionsRequest(
							requestHeader,
							subscriptionIds
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionIds != NULL)
			subscriptionIds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_DeleteSubscriptionsRequest& pDeleteSubscriptionsRequest) const
	{
		requestHeader     ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionIds   ->fromCpptoC (pStatus, pDeleteSubscriptionsRequest.NoOfSubscriptionIds, pDeleteSubscriptionsRequest.SubscriptionIds) ;
	}

public:

	DeleteSubscriptionsRequest(
			RequestHeader    * _requestHeader,
			TableIntegerId   * _subscriptionIds
			)
		: Structure()
	{
		(requestHeader      = _requestHeader)     ->take() ;
		(subscriptionIds    = _subscriptionIds)   ->take() ;
	}

	virtual ~DeleteSubscriptionsRequest()
	{
		requestHeader     ->release() ;
		subscriptionIds   ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableIntegerId   * getSubscriptionIds()
	{
		return subscriptionIds ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_DELETESUBSCRIPTIONSREQUEST_H_ */
