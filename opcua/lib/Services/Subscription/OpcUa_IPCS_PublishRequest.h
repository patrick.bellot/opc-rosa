/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_PUBLISHREQUEST_H_
#define OPCUA_PUBLISHREQUEST_H_

/*
 * Part 4, 5.13.5.2, p. 86
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_SubscriptionAcknowledgement.h"

namespace opcua {

class MYDLL PublishRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_PublishRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_PublishRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader                      * requestHeader ;
	TableSubscriptionAcknowledgement   * subscriptionAcknowledgements ;

public:

	static PublishRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_PublishRequest const& pPublishRequest)
	{
		RequestHeader                      * requestHeader                = RequestHeader                ::fromCtoCpp   ( pStatus, pRequestHeader) ;
		TableSubscriptionAcknowledgement   * subscriptionAcknowledgements = TableSubscriptionAcknowledgement::fromCtoCpp( pStatus, pPublishRequest.NoOfSubscriptionAcknowledgements, pPublishRequest.SubscriptionAcknowledgements) ;

		if (*pStatus == STATUS_OK)
			return
					new PublishRequest(
							requestHeader,
							subscriptionAcknowledgements
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionAcknowledgements != NULL)
			subscriptionAcknowledgements->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_PublishRequest& pPublishRequest) const
	{
		requestHeader                ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionAcknowledgements ->fromCpptoC (pStatus, pPublishRequest.NoOfSubscriptionAcknowledgements, pPublishRequest.SubscriptionAcknowledgements) ;
	}

public:

	PublishRequest(
			RequestHeader                      * _requestHeader,
			TableSubscriptionAcknowledgement   * _subscriptionAcknowledgements
			)
		: Structure()
	{
		(requestHeader                = _requestHeader)                ->take() ;
		(subscriptionAcknowledgements = _subscriptionAcknowledgements) ->take() ;
	}

	virtual ~PublishRequest()
	{
		requestHeader                ->release() ;
		subscriptionAcknowledgements ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableSubscriptionAcknowledgement   * getSubscriptionAcknowledgements()
	{
		return subscriptionAcknowledgements ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_PUBLISHREQUEST_H_ */
