/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SUBSCRIPTION_ALL_H_
#define _SUBSCRIPTION_ALL_H_

#include "OpcUa_IPCS_CreateSubscription.h"
#include "OpcUa_IPCS_CreateSubscriptionRequest.h"
#include "OpcUa_IPCS_CreateSubscriptionResponse.h"
#include "OpcUa_IPCS_DeleteSubscriptions.h"

#include "OpcUa_IPCS_DeleteSubscriptionsRequest.h"
#include "OpcUa_IPCS_DeleteSubscriptionsResponse.h"
#include "OpcUa_IPCS_ModifySubscription.h"
#include "OpcUa_IPCS_ModifySubscriptionRequest.h"
#include "OpcUa_IPCS_ModifySubscriptionResponse.h"
#include "OpcUa_IPCS_Publish.h"
#include "OpcUa_IPCS_PublishRequest.h"
#include "OpcUa_IPCS_PublishResponse.h"
#include "OpcUa_IPCS_Republish.h"
#include "OpcUa_IPCS_RepublishRequest.h"
#include "OpcUa_IPCS_RepublishResponse.h"
#include "OpcUa_IPCS_SetPublishingMode.h"
#include "OpcUa_IPCS_SetPublishingModeRequest.h"
#include "OpcUa_IPCS_SetPublishingModeResponse.h"
#include "OpcUa_IPCS_TransferSubscriptions.h"
#include "OpcUa_IPCS_TransferSubscriptionsRequest.h"
#include "OpcUa_IPCS_TransferSubscriptionsResponse.h"



#endif /* _SUBSCRIPTION_ALL_H_ */
