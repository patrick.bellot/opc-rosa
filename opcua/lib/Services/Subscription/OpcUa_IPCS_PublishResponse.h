/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_PUBLISHRESPONSE_H_
#define OPCUA_PUBLISHRESPONSE_H_

/*
 * Part 4, 5.13.5.2, p. 86
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_NotificationMessage.h"

namespace opcua {

class MYDLL PublishResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_PublishResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_PublishResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	IntegerId             * subscriptionId ;
	TableCounter          * availableSequenceNumbers ;
	Boolean               * moreNotifications ;
	NotificationMessage   * notificationMessage ;

	TableStatusCode       * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static PublishResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_PublishResponse const& pPublishResponse)
	{
		ResponseHeader        * responseHeader           = ResponseHeader      ::fromCtoCpp (pStatus, pResponseHeader) ;
		IntegerId             * subscriptionId           = IntegerId           ::fromCtoCpp (pStatus, pPublishResponse.SubscriptionId) ;
		TableCounter          * availableSequenceNumbers = TableCounter        ::fromCtoCpp (pStatus, pPublishResponse.NoOfAvailableSequenceNumbers, pPublishResponse.AvailableSequenceNumbers) ;
		Boolean               * moreNotifications        = Boolean             ::fromCtoCpp (pStatus, pPublishResponse.MoreNotifications) ;
		NotificationMessage   * notificationMessage      = NotificationMessage ::fromCtoCpp (pStatus, pPublishResponse.NotificationMessage) ;

		TableStatusCode       * results                  = TableStatusCode::fromCtoCpp        (pStatus, pPublishResponse.NoOfResults,         pPublishResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos          = TableDiagnosticInfo::fromCtoCpp    (pStatus, pPublishResponse.NoOfDiagnosticInfos, pPublishResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new PublishResponse(
							responseHeader,
							subscriptionId,
							availableSequenceNumbers,
							moreNotifications,
							notificationMessage,

							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (availableSequenceNumbers != NULL)
			availableSequenceNumbers->checkRefCount() ;
		if (moreNotifications != NULL)
			moreNotifications->checkRefCount() ;
		if (notificationMessage != NULL)
			notificationMessage->checkRefCount() ;

		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_PublishResponse& pPublishResponse) const
	{
		responseHeader           ->fromCpptoC (pStatus, pResponseHeader) ;
		subscriptionId           ->fromCpptoC (pStatus, pPublishResponse.SubscriptionId) ;
		availableSequenceNumbers ->fromCpptoC (pStatus, pPublishResponse.NoOfAvailableSequenceNumbers, pPublishResponse.AvailableSequenceNumbers) ;
		moreNotifications        ->fromCpptoC (pStatus, pPublishResponse.MoreNotifications) ;
		notificationMessage      ->fromCpptoC (pStatus, pPublishResponse.NotificationMessage) ;

		results                  ->fromCpptoC (pStatus, pPublishResponse.NoOfResults,         pPublishResponse.Results) ;
		diagnosticInfos          ->fromCpptoC (pStatus, pPublishResponse.NoOfDiagnosticInfos, pPublishResponse.DiagnosticInfos) ;
	}

public:

	PublishResponse(
			ResponseHeader        * _responseHeader,
			IntegerId             * _subscriptionId,
			TableCounter          * _availableSequenceNumbers,
			Boolean               * _moreNotifications,
			NotificationMessage   * _notificationMessage,
			TableStatusCode       * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader           = _responseHeader)           ->take() ;
		(subscriptionId           = _subscriptionId)           ->take() ;
		(availableSequenceNumbers = _availableSequenceNumbers) ->take() ;
		(moreNotifications        = _moreNotifications)        ->take() ;
		(notificationMessage      = _notificationMessage)      ->take() ;
		(results                  = _results)                  ->take() ;
		(diagnosticInfos          = _diagnosticInfos)          ->take() ;
	}

	virtual ~PublishResponse()
	{
		responseHeader           ->release() ;
		subscriptionId           ->release() ;
		availableSequenceNumbers ->release() ;
		moreNotifications        ->release() ;
		notificationMessage      ->release() ;
		results                  ->release() ;
		diagnosticInfos          ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline TableCounter   * getAvailableSequenceNumbers()
	{
		return availableSequenceNumbers ;
	}

	inline Boolean * getMoreNotifications()
	{
		return moreNotifications ;
	}

	inline NotificationMessage * getNotificationMessage()
	{
		return notificationMessage ;
	}

	inline TableStatusCode   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_PUBLISHRESPONSE_H_ */
