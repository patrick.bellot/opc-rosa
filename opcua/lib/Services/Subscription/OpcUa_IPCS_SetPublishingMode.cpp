/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"
#include "../../NotificationsAndEvents/All.h"

#include "OpcUa_IPCS_SetPublishingModeRequest.h"
#include "OpcUa_IPCS_SetPublishingModeResponse.h"
#include "OpcUa_IPCS_SetPublishingMode.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_SetPublishingMode(
		uint32_t					      secureChannelIdNum,
		OpcUa_RequestHeader             * pRequestHeader,
		OpcUa_SetPublishingModeRequest  * pRequest,
		OpcUa_ResponseHeader            * pResponseHeader,
		OpcUa_SetPublishingModeResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	SetPublishingModeRequest * request  = NULL ;
	request = SetPublishingModeRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	SetPublishingModeResponse * response = NULL ;
	SetPublishingMode::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void SetPublishingMode::answer(
		SOPC_StatusCode                  * pStatus,
		uint32_t					       secureChannelIdNum,
	    class SetPublishingModeRequest   * request,
	    class SetPublishingModeResponse ** pResponse)
{
	debug(MAIN_LIFE_DBG,"SetPublishingMode","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"SetPublishingMode","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"SetPublishingMode","Request : continue Request decoding.") ;

	bool publishingEnabled = request->getPublishingEnabled() ;

	TableIntegerId   * subscriptionIds = request->getSubscriptionIds() ;

	int32_t length = subscriptionIds->getLength() ;

	debug(COM_DBG,"SetPublishingMode","Request : building response structure.") ;

	TableStatusCode   * results  = new TableStatusCode  (length) ;

	StatusCode * r = NULL ;

	for (int32_t i = 0 ; i < length ; i++) {

		SubscriptionsTable::subscriptionsLock() ;

		Subscription * subscription = SubscriptionsTable::subscriptionsTable->getSubscription(subscriptionIds->get(i)) ;

		if (subscription != NULL)
			r = subscription->onReceiveSetPublishingModeRequest(publishingEnabled) ;

		SubscriptionsTable::subscriptionsUnlock() ;

		if (subscription == NULL)
			r = StatusCode::Bad_SubscriptionIdInvalid ;

		results->set(i,r) ;
	}

	* pResponse =
			new SetPublishingModeResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			);
}

} /* namespace opcua */

#endif
