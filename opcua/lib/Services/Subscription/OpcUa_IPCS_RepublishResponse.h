/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REPUBLISHRESPONSE_H_
#define OPCUA_REPUBLISHRESPONSE_H_

/*
 * Part 4, 5.13.6.2, p. 87
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL RepublishResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_RepublishResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_RepublishResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader      * responseHeader ;
	NotificationMessage * notificationMessage ;

public:

	static RepublishResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_RepublishResponse const& pRepublishResponse)
	{
		ResponseHeader        * responseHeader      = ResponseHeader      ::fromCtoCpp (pStatus, pResponseHeader) ;
		NotificationMessage   * notificationMessage = NotificationMessage ::fromCtoCpp (pStatus, pRepublishResponse.NotificationMessage) ;

		if (*pStatus == STATUS_OK)
			return
					new RepublishResponse(
							responseHeader,
							notificationMessage
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (notificationMessage != NULL)
			notificationMessage->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_RepublishResponse& pRepublishResponse) const
	{
		responseHeader      ->fromCpptoC (pStatus, pResponseHeader) ;
		notificationMessage ->fromCpptoC (pStatus, pRepublishResponse.NotificationMessage) ;
	}

public:

	RepublishResponse(
			ResponseHeader      * _responseHeader,
			NotificationMessage * _notificationMessage
			)
		: Structure()
	{
		(responseHeader      = _responseHeader)      ->take() ;
		(notificationMessage = _notificationMessage) ->take() ;
	}

	virtual ~RepublishResponse()
	{
		responseHeader      ->release() ;
		notificationMessage ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline NotificationMessage * getNotificationMessage()
	{
		return notificationMessage ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_REPUBLISHRESPONSE_H_ */
