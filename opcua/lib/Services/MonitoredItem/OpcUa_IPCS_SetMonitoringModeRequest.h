/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SETMONITORINGMODEREQUEST_H_
#define OPCUA_SETMONITORINGMODEREQUEST_H_

/*
 * Part 4, 5.12.4.2, Table 70, p. 70
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_MonitoringMode.h"

namespace opcua {

class MYDLL SetMonitoringModeRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_SetMonitoringModeRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_SetMonitoringModeRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader    * requestHeader ;
	IntegerId        * subscriptionId ;
	MonitoringMode   * monitoringMode ;
	TableIntegerId   * monitoredItemIds ;

public:

	static SetMonitoringModeRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_SetMonitoringModeRequest const& pSetMonitoringModeRequest)
	{
		RequestHeader    * requestHeader    = RequestHeader  ::fromCtoCpp (pStatus, pRequestHeader) ;
		IntegerId        * subscriptionId   = IntegerId      ::fromCtoCpp (pStatus, pSetMonitoringModeRequest.SubscriptionId) ;
		MonitoringMode   * monitoringMode   = MonitoringMode ::fromCtoCpp (pStatus, pSetMonitoringModeRequest.MonitoringMode) ;
		TableIntegerId   * monitoredItemIds = TableIntegerId ::fromCtoCpp (pStatus, pSetMonitoringModeRequest.NoOfMonitoredItemIds,pSetMonitoringModeRequest.MonitoredItemIds) ;

		if (*pStatus == STATUS_OK)
			return
					new SetMonitoringModeRequest(
							requestHeader,
							subscriptionId,
							monitoringMode,
							monitoredItemIds
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (monitoringMode != NULL)
			monitoringMode->checkRefCount() ;
		if (monitoredItemIds != NULL)
			monitoredItemIds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_SetMonitoringModeRequest& pSetMonitoringModeRequest) const
	{
		requestHeader    ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId   ->fromCpptoC (pStatus, pSetMonitoringModeRequest.SubscriptionId) ;
		monitoringMode   ->fromCpptoC (pStatus, pSetMonitoringModeRequest.MonitoringMode) ;
		monitoredItemIds ->fromCpptoC (pStatus, pSetMonitoringModeRequest.NoOfMonitoredItemIds,pSetMonitoringModeRequest.MonitoredItemIds) ;
	}

public:

	SetMonitoringModeRequest(
			RequestHeader    * _requestHeader,
			IntegerId        * _subscriptionId,
			MonitoringMode   * _monitoringMode,
			TableIntegerId   * _monitoredItemIds
			) : Structure() {
		(requestHeader    = _requestHeader)    ->take() ;
		(subscriptionId   = _subscriptionId)   ->take() ;
		(monitoringMode   = _monitoringMode)   ->take() ;
		(monitoredItemIds = _monitoredItemIds) ->take() ;
	}

	virtual ~SetMonitoringModeRequest()
	{
		requestHeader    ->release() ;
		subscriptionId   ->release() ;
		monitoringMode   ->release() ;
		monitoredItemIds ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline MonitoringMode * getMonitoringMode()
	{
		return monitoringMode ;
	}

	inline TableIntegerId   * getMonitoredItemIds()
	{
		return monitoredItemIds ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SETMONITORINGMODEREQUEST_H_ */
