/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MODIFYMONITOREDITEMSRESPONSE_H_
#define OPCUA_MODIFYMONITOREDITEMSRESPONSE_H_

/*
 * Part 4, 5.12.3.2, Table 67, p. 69
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemModifyResult.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ModifyMonitoredItemsResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ModifyMonitoredItemsResponse; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ModifyMonitoredItemsResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader                   * responseHeader ;
	TableMonitoredItemModifyResult   * results ;
	TableDiagnosticInfo              * diagnosticInfos ;

public:

	static ModifyMonitoredItemsResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_ModifyMonitoredItemsResponse const& pModifyMonitoredItemsResponse)
	{
		ResponseHeader                   * responseHeader  = ResponseHeader ::fromCtoCpp                (pStatus, pResponseHeader) ;
		TableMonitoredItemModifyResult   * results         = TableMonitoredItemModifyResult::fromCtoCpp (pStatus, pModifyMonitoredItemsResponse.NoOfResults,         pModifyMonitoredItemsResponse.Results) ;
		TableDiagnosticInfo              * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp            (pStatus, pModifyMonitoredItemsResponse.NoOfDiagnosticInfos, pModifyMonitoredItemsResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new ModifyMonitoredItemsResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_ModifyMonitoredItemsResponse& pModifyMonitoredItemsResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pModifyMonitoredItemsResponse.NoOfResults,         pModifyMonitoredItemsResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pModifyMonitoredItemsResponse.NoOfDiagnosticInfos, pModifyMonitoredItemsResponse.DiagnosticInfos) ;
	}

public:

	ModifyMonitoredItemsResponse(
			ResponseHeader                   * _responseHeader,
			TableMonitoredItemModifyResult   * _results,
			TableDiagnosticInfo              * _diagnosticInfos
			) : Structure() {
		(responseHeader  = _responseHeader)  ->take();
		(results         = _results)         ->take();
		(diagnosticInfos = _diagnosticInfos) ->take();
	}

	virtual ~ModifyMonitoredItemsResponse()
	{
		responseHeader  ->release();
		results         ->release();
		diagnosticInfos ->release();
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader;
	}

	inline TableMonitoredItemModifyResult   * getResults()
	{
		return results;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfos()
	{
		return diagnosticInfos ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MODIFYMONITOREDITEMSRESPONSE_H_ */
