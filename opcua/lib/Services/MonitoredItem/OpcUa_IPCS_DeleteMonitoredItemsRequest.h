/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DELETEMONITOREDITEMSREQUEST_H_
#define OPCUA_DELETEMONITOREDITEMSREQUEST_H_

/*
 * Part 4, 5.12.6.2, Table 76, p. 72
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL DeleteMonitoredItemsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_DeleteMonitoredItemsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_DeleteMonitoredItemsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader    * requestHeader ;
	IntegerId        * subscriptionId ;
	TableIntegerId   * monitoredItemIds ;

public:

	static DeleteMonitoredItemsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_DeleteMonitoredItemsRequest const& pDeleteMonitoredItemsRequest)
	{
		RequestHeader    * requestHeader    = RequestHeader ::fromCtoCpp (pStatus, pRequestHeader) ;
		IntegerId        * subscriptionId   = IntegerId     ::fromCtoCpp (pStatus, pDeleteMonitoredItemsRequest.SubscriptionId) ;
		TableIntegerId   * monitoredItemIds = TableIntegerId::fromCtoCpp (pStatus, pDeleteMonitoredItemsRequest.NoOfMonitoredItemIds,pDeleteMonitoredItemsRequest.MonitoredItemIds) ;

		if (*pStatus == STATUS_OK)
			return
					new DeleteMonitoredItemsRequest(
							requestHeader,
							subscriptionId,
							monitoredItemIds
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (monitoredItemIds != NULL)
			monitoredItemIds->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_DeleteMonitoredItemsRequest& pDeleteMonitoredItemsRequest) const
	{
		requestHeader    ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId   ->fromCpptoC (pStatus, pDeleteMonitoredItemsRequest.SubscriptionId) ;
		monitoredItemIds ->fromCpptoC (pStatus, pDeleteMonitoredItemsRequest.NoOfMonitoredItemIds, pDeleteMonitoredItemsRequest.MonitoredItemIds) ;
	}

public:

	DeleteMonitoredItemsRequest(
			RequestHeader    * _requestHeader,
			IntegerId        * _subscriptionId,
			TableIntegerId   * _monitoredItemIds
			) : Structure() {
		(requestHeader    = _requestHeader)    ->take() ;
		(subscriptionId   = _subscriptionId)    ->take() ;
		(monitoredItemIds = _monitoredItemIds) ->take() ;
	}

	virtual ~DeleteMonitoredItemsRequest()
	{
		requestHeader    ->release() ;
		subscriptionId   ->release() ;
		monitoredItemIds ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline TableIntegerId   * getMonitoredItemIds()
	{
		return monitoredItemIds ;
	}

};
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_DELETEMONITOREDITEMSREQUEST_H_ */
