/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CREATEMONITOREDITEMSREQUEST_H_
#define OPCUA_CREATEMONITOREDITEMSREQUEST_H_

/*
 * Part 4, 5.12.2.2, Table 64, p. 67
 */

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemCreateRequest.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CreateMonitoredItemsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CreateMonitoredItemsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CreateMonitoredItemsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader                     * requestHeader ;
	IntegerId                         * subscriptionId ;
	TimestampsToReturn                * timestampsToReturn ;
	TableMonitoredItemCreateRequest   * itemsToCreate ;

public:

	static CreateMonitoredItemsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_CreateMonitoredItemsRequest const& pCreateMonitoredItemsRequest)
	{
		RequestHeader                     * requestHeader      = RequestHeader                  ::fromCtoCpp (pStatus, pRequestHeader) ;
		IntegerId                         * subscriptionId     = IntegerId                      ::fromCtoCpp (pStatus, pCreateMonitoredItemsRequest.SubscriptionId) ;
		TimestampsToReturn                * timestampsToReturn = TimestampsToReturn             ::fromCtoCpp (pStatus, pCreateMonitoredItemsRequest.TimestampsToReturn) ;
		TableMonitoredItemCreateRequest   * itemsToCreate      = TableMonitoredItemCreateRequest::fromCtoCpp (pStatus, pCreateMonitoredItemsRequest.NoOfItemsToCreate,pCreateMonitoredItemsRequest.ItemsToCreate) ;

		if (*pStatus == STATUS_OK)
			return
					new CreateMonitoredItemsRequest(
							requestHeader,
							subscriptionId,
							timestampsToReturn,
							itemsToCreate
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (timestampsToReturn != NULL)
			timestampsToReturn->checkRefCount() ;
		if (itemsToCreate != NULL)
			itemsToCreate->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_CreateMonitoredItemsRequest& pCreateMonitoredItemsRequest) const
	{
		requestHeader      ->fromCpptoC (pStatus, pRequestHeader) ;
		subscriptionId     ->fromCpptoC (pStatus, pCreateMonitoredItemsRequest.SubscriptionId) ;
		timestampsToReturn ->fromCpptoC (pStatus, pCreateMonitoredItemsRequest.TimestampsToReturn) ;
		itemsToCreate      ->fromCpptoC (pStatus, pCreateMonitoredItemsRequest.NoOfItemsToCreate, pCreateMonitoredItemsRequest.ItemsToCreate) ;
	}

public:

	CreateMonitoredItemsRequest(
			RequestHeader                     * _requestHeader,
			IntegerId                         * _subscriptionId,
			TimestampsToReturn                * _timestampsToReturn,
			TableMonitoredItemCreateRequest   * _itemsToCreate
			) : Structure() {
		(requestHeader      = _requestHeader)      ->take() ;
		(subscriptionId     = _subscriptionId)     ->take() ;
		(timestampsToReturn = _timestampsToReturn) ->take() ;
		(itemsToCreate      = _itemsToCreate)      ->take() ;
	}

	virtual ~CreateMonitoredItemsRequest()
	{
		requestHeader      ->release() ;
		subscriptionId     ->release() ;
		timestampsToReturn ->release() ;
		itemsToCreate      ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline TimestampsToReturn * getTimestampsToReturn()
	{
		return timestampsToReturn ;
	}

	inline TableMonitoredItemCreateRequest   * getItemsToCreate()
	{
		return itemsToCreate ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_CREATEMONITOREDITEMSREQUEST_H_ */
