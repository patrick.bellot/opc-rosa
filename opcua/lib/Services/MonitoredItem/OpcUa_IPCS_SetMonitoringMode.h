
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SETMONITORINGMODE_H_
#define OPCUA_SETMONITORINGMODE_H_

#include "../../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../OpcUa_BaseService.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_SetMonitoringMode(
		uint32_t					      secureChannelIdNum,
		OpcUa_RequestHeader             * pRequestHeader,
		OpcUa_SetMonitoringModeRequest  * pRequest,
		OpcUa_ResponseHeader            * pResponseHeader,
		OpcUa_SetMonitoringModeResponse * pResponse) ;

class MYDLL SetMonitoringMode
	: public BaseService
{
public:

	static void answer(
			SOPC_StatusCode                  * pStatus,
			uint32_t				           secureChannelIdNum,
		    class SetMonitoringModeRequest   * request,
		    class SetMonitoringModeResponse ** pResponse) ;

}; // class SetMonitoringMode

}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SETMONITORINGMODE_H_ */
