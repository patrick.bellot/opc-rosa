
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _QUERY_ALL_H_
#define _QUERY_ALL_H_

#include "OpcUa_IPCS_ReadRequest.h"
#include "OpcUa_IPCS_ReadResponse.h"
#include "OpcUa_IPCS_Read.h"

#include "OpcUa_IPCS_WriteRequest.h"
#include "OpcUa_IPCS_WriteResponse.h"
#include "OpcUa_IPCS_Write.h"

#endif /* _QUERY_ALL_H_ */
