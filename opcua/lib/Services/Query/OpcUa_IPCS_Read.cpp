/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_READ == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_ReadRequest.h"
#include "OpcUa_IPCS_ReadResponse.h"
#include "OpcUa_IPCS_Read.h"

#define DEBUG_READ (0)
// 0 no debug
// 1 debug call in and out
// 2 debug what is read eand result

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Read(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_ReadRequest          * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_ReadResponse         * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	ReadRequest * request  = NULL ;
	request = ReadRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	ReadResponse * response = NULL ;
	Read::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void Read::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class ReadRequest           * request,
	    class ReadResponse         ** pResponse)
{
	AddressSpace * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"Read","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;
	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"Read","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"Read","Request : getting parameters.") ;

	Duration           * maxAge             = request->getMaxAge() ;
	TimestampsToReturn * timestampsToReturn = request->getTimestampsToReturn() ;
	TableReadValueId   * nodesToRead        = request->getNodesToRead() ;

	debug(COM_DBG,"Read","Request : checking parameters.") ;

	StatusCode * statusCode = NULL ;

	if (maxAge->less(0.0))
		statusCode = StatusCode::Bad_MaxAgeInvalid ;
	else
	if (timestampsToReturn->isInvalid())
		statusCode = StatusCode::Bad_TimestampsToReturnInvalid ;

	debug(COM_DBG,"Read","Request : preparing results.") ;

	int32_t            length  = nodesToRead->getLength() ;
	TableDataValue   * results = new TableDataValue  (length) ;

	debug_i(COM_DBG,"Read","Request : getting %d results.",length) ;

	for (int i = 0 ; i < length ; i++) {

		debug_i(COM_DBG,"Read","Results begins (%d)",i) ;

		ReadValueId * nodeToRead = nodesToRead->get(i) ;
		NodeId      * nodeId     = nodeToRead->getNodeId() ;

		if (DEBUG_LEVEL & COM_DBG) {
			String * nodeIdString = nodeId->toString() ;
			nodeIdString->take() ;
			debug_sii(COM_DBG,"Read","NodeId \"%s\"->%d (%d)",nodeIdString->get(),nodeToRead->getAttributeId()->get(),i) ;
			nodeIdString->release() ;
		}

		debug_i(COM_DBG,"Read","Results get base (%d)",i) ;

		Base * base = addressSpace->get(nodeId) ;

		debug_p(COM_DBG,"Read","Results base (%p)",base) ;

		DataValue   * result ;

		if (base == NULL) {

			result = DataValue::Bad_NodeIdUnknown ;

			String * nodeIdString = nodeId->toString() ;
			debug_si(COM_ERR,"Read","Bad NodeId unknown \"%s\" (%d)",nodeIdString->get(),i) ;
			nodeIdString->checkRefCount() ;

		} else {

#if (DEBUG_READ & 2)
			{
				String * dbg = base->getNodeId()->toString() ;
				fprintf(stderr,"                    -> Reading: %s, attribute=%d\n",dbg->get(),nodeToRead->getAttributeId()->get()) ;
				dbg->checkRefCount() ;
			}
#endif

			debug(COM_DBG,"Read","Read begins") ;

			result
				= base->getAttribute(
						nodeToRead->getAttributeId(),
						nodeToRead->getIndexRange(),
						maxAge,
						timestampsToReturn
						) ;

#if (DEBUG_READ & 2)
			{
				Variant * variant = result->getVariantAttr() ; // special: because getVariant() is something else
				if (variant == NULL) {
					StatusCode * statusCode = result->getStatusCode() ;
					fprintf(stderr,"                    <- Result: no result (err=0x%08x)\n",statusCode->get()) ;
				} else {
					uint32_t typeId = variant->getValue()->getTypeId() ;
					fprintf(stderr,"                    <- Result: type=%d\n",typeId) ;
				}
			}
#endif
		}

		debug(COM_DBG,"Read","Setting results") ;

		results->set(i, result) ;

		debug_i(COM_DBG,"Read","Results ends (%d)",i) ;
	}

	debug(COM_DBG,"Read","Request succeeds : returning Response.") ;

	*pResponse = // Part 4, 5.10.2, p. 52
			new ReadResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							(statusCode != NULL) ? statusCode : ((length != 0) ? StatusCode::Good : StatusCode::Bad_NothingToDo),
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}

} /* namespace opcua */

#endif
