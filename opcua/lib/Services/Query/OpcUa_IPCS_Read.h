/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_READ_H_
#define OPCUA_READ_H_

#include "../../OpcUa.h"

#if (WITH_READ == 1)

#include "../OpcUa_BaseService.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_Read(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_ReadRequest          * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_ReadResponse         * pResponse) ;

class MYDLL Read
	: public BaseService
{
public:

	static void answer(
			SOPC_StatusCode             * pStatus,
			uint32_t					  secureChannelIdNum,
		    class ReadRequest           * request,
		    class ReadResponse         ** pResponse) ;

}; // class Read
} /* namespace opcua */
#endif
#endif /* READ_H_ */
