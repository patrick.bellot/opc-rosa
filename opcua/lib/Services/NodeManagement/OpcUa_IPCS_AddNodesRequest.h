
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDNODESREQUEST_H_
#define OPCUA_ADDNODESREQUEST_H_

/*
 * Part 4, 5.7.2.2, p. 32, table 18
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NodeManagement/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL AddNodesRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddNodesRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_AddNodesRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader       * requestHeader ;
	TableAddNodesItem   * nodesToAdd ;

public:

	static AddNodesRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_AddNodesRequest const& pAddNodesRequest)
	{
		RequestHeader       * requestHeader = RequestHeader  ::fromCtoCpp( pStatus, pRequestHeader) ;
		TableAddNodesItem   * nodesToAdd    = TableAddNodesItem::fromCtoCpp( pStatus, pAddNodesRequest.NoOfNodesToAdd, pAddNodesRequest.NodesToAdd) ;

		if (*pStatus == STATUS_OK)
			return
					new AddNodesRequest(
							requestHeader,
							nodesToAdd
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (nodesToAdd != NULL)
			nodesToAdd->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_AddNodesRequest& pAddNodesRequest) const
	{
		requestHeader ->fromCpptoC (pStatus, pRequestHeader) ;
		nodesToAdd    ->fromCpptoC (pStatus, pAddNodesRequest.NoOfNodesToAdd, pAddNodesRequest.NodesToAdd) ;
	}

public:

	AddNodesRequest(
			RequestHeader       * _requestHeader,
			TableAddNodesItem   * _nodesToAdd
			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(nodesToAdd    = _nodesToAdd)     ->take() ;
	}

	virtual ~AddNodesRequest()
	{
		requestHeader ->release() ;
		nodesToAdd    ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	virtual inline TableAddNodesItem   * getNodesToAdd()
	{
		return nodesToAdd ;
	}

};
} /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDNODESREQUEST_H_ */
