
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDREFERENCESREQUEST_H_
#define OPCUA_ADDREFERENCESREQUEST_H_

/*
 * Part 4, 5.7.3.2, p. 34, table 21
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NodeManagement/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL AddReferencesRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddReferencesRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_AddReferencesRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader            * requestHeader ;
	TableAddReferencesItem   * referencesToAdd ;

public:

	static AddReferencesRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_AddReferencesRequest const& pAddReferencesRequest)
	{
		RequestHeader            * requestHeader   = RequestHeader       ::fromCtoCpp ( pStatus, pRequestHeader) ;
		TableAddReferencesItem   * referencesToAdd = TableAddReferencesItem::fromCtoCpp ( pStatus, pAddReferencesRequest.NoOfReferencesToAdd, pAddReferencesRequest.ReferencesToAdd) ;

		if (*pStatus == STATUS_OK)
			return
					new AddReferencesRequest(
							requestHeader,
							referencesToAdd
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (referencesToAdd != NULL)
			referencesToAdd->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader & pRequestHeader, OpcUa_AddReferencesRequest& pAddReferencesRequest) const
	{
		requestHeader   ->fromCpptoC (pStatus, pRequestHeader) ;
		referencesToAdd ->fromCpptoC (pStatus, pAddReferencesRequest.NoOfReferencesToAdd, pAddReferencesRequest.ReferencesToAdd) ;
	}

public:

	AddReferencesRequest(
			RequestHeader            * _requestHeader,
			TableAddReferencesItem   * _referencesToAdd
			)
		: Structure()
	{
		(requestHeader   = _requestHeader)   ->take() ;
		(referencesToAdd = _referencesToAdd) ->take() ;
	}

	virtual ~AddReferencesRequest()
	{
		requestHeader   ->release() ;
		referencesToAdd ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	virtual inline TableAddReferencesItem   * getReferencesToAdd()
	{
		return referencesToAdd ;
	}

};
} /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDREFERENCESREQUEST_H_ */
