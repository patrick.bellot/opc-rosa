
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDREFERENCESRESPONSE_H_
#define OPCUA_ADDREFERENCESRESPONSE_H_

/*
 * Part 4, 5.7.3.2, p. 34, table 21
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../NodeManagement/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL AddReferencesResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddReferencesResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_AddReferencesResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	TableStatusCode       * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static AddReferencesResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_AddReferencesResponse const& pAddReferencesResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp   (pStatus, pResponseHeader) ;
		TableStatusCode       * results         = TableStatusCode::fromCtoCpp     (pStatus, pAddReferencesResponse.NoOfResults,         pAddReferencesResponse.Results) ;
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pAddReferencesResponse.NoOfDiagnosticInfos, pAddReferencesResponse.DiagnosticInfos) ;

		if (*pStatus == STATUS_OK)
			return
					new AddReferencesResponse(
							responseHeader,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader & pResponseHeader, OpcUa_AddReferencesResponse& pAddReferencesResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		results         ->fromCpptoC (pStatus, pAddReferencesResponse.NoOfResults,         pAddReferencesResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pAddReferencesResponse.NoOfDiagnosticInfos, pAddReferencesResponse.DiagnosticInfos) ;
	}

public:

	AddReferencesResponse(
			ResponseHeader        * _responseHeader,
			TableStatusCode       * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~AddReferencesResponse()
	{
		responseHeader  ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableStatusCode   * getResults()
	{
		return results ;
	}

	inline TableDiagnosticInfo   * getDiagnosticInfo()
	{
		return diagnosticInfos ;
	}

};
}      /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDREFERENCESRESPONSE_H_ */
