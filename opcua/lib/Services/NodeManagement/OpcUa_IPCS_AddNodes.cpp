/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_AddNodesRequest.h"
#include "OpcUa_IPCS_AddNodesResponse.h"
#include "OpcUa_IPCS_AddNodes.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_AddNodes(
		uint32_t					 secureChannelIdNum,
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_AddNodesRequest      * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_AddNodesResponse     * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	AddNodesRequest * request  = NULL ;
	request = AddNodesRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	AddNodesResponse * response = NULL ;
	AddNodes::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void AddNodes::answer(
		SOPC_StatusCode             * pStatus,
		uint32_t					  secureChannelIdNum,
	    class AddNodesRequest       * request,
	    class AddNodesResponse     ** pResponse)
{
	AddressSpace  * addressSpace  = AddressSpace::self ;

	debug(MAIN_LIFE_DBG,"AddNodes","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;

	Channel          		   * channel ;
	Session                    * session ;

	*pStatus = checkChannelAndSession(secureChannelIdNum,authenticationToken,&channel,&session) ;

	if (*pStatus != _Good) {
		debug_i(COM_ERR,"AddNodes","answer failed (2) with rc = 0x%08x",*pStatus) ;
		return ;
	}

	debug(COM_DBG,"AddNodes","Request : getting parameters.") ;

	TableAddNodesItem   * nodesToAdd = request->getNodesToAdd() ;

	debug(COM_DBG,"AddNodes","Request : preparing results.") ;

	int32_t length = nodesToAdd->getLength() ;

	TableAddNodesResult   * results         = new TableAddNodesResult  (length) ;

	debug_i(COM_DBG,"AddNodes","Request : getting %d results.",length) ;

	for (int i = 0 ; i < length ; i++) {

		debug_i(COM_DBG,"AddNodes","Results begins (%d)",i) ;

		AddNodesItem * addNodesItem = nodesToAdd->get(i) ;

		debug(COM_DBG,"AddNodes","Get parentNodeId") ;

		ExpandedNodeId * parentNodeId = addNodesItem->getParentNodeId() ;

		if (parentNodeId->getNamespaceUri()->isNotNullOrEmpty()) {
			debug(COM_ERR,"AddNodes","Not implemented parentNodeId with namespaceUri set") ;
			results         ->set(i, AddNodesResult::Bad_NotImplemented) ;
			continue ;
		}

		if (parentNodeId->getServerIndex()->isNotZero()) {
			debug(COM_ERR,"AddNodes","Not implemented parentNodeId with serverIndex not zero") ;
			results         ->set(i, AddNodesResult::Bad_NotImplemented) ;
			continue ;
		}

		NodeId * trueParentNodeId = parentNodeId->getNodeId() ;

		Base * parent = addressSpace->get(trueParentNodeId) ;

		if (parent == NULL) {
			debug(COM_ERR,"AddNodes","parentNodeId does not exist") ;
			results         ->set(i, AddNodesResult::Bad_ParentNodeIdInvalid) ;
			continue ;
		}

		/* parent ok ! */

		NodeId * referenceTypeId = addNodesItem->getReferenceTypeId() ;

		if (referenceTypeId->getNamespaceIndex()->isNotZero()) {
			debug(COM_ERR,"AddNodes","referenceTypeId not local (namespaceIndex != 0)") ;
			results         ->set(i, AddNodesResult::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		if (referenceTypeId->getIdentifierType()->get() != IdType_STRING_1) {
			debug(COM_ERR,"AddNodes","referenceTypeId not a string (identifierType != STRING_1)") ;
			results         ->set(i, AddNodesResult::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		String * referenceString = referenceTypeId->getString() ;

		const char * const reference_str = referenceString->get() ;

		uint32_t reference_code = ReferenceLink::getCode(reference_str) ;

		if (reference_code == 0) {
			debug(COM_ERR,"AddNodes","referenceTypeId not recognized (see ReferenceLink)") ;
			results         ->set(i, AddNodesResult::Bad_ReferenceTypeIdInvalid) ;
			continue ;
		}

		/* NOT CHECKED : Is the reference authorized by the data model ? */

		/* reference_code ok ! */

		ExpandedNodeId * requestedNewNodeId = addNodesItem->getRequestedNewNodeId() ;

		if (requestedNewNodeId->getNamespaceUri()->isNotNullOrEmpty()) {
			debug(COM_ERR,"AddNodes","Not implemented requestedNewNodeId with namespaceUri set") ;
			results->set(i, AddNodesResult::Bad_NotImplemented) ;
			continue ;
		}

		if (requestedNewNodeId->getServerIndex()->isNotZero()) {
			debug(COM_ERR,"AddNodes","Not implemented requestedNewNodeId with serverIndex not zero") ;
			results->set(i, AddNodesResult::Bad_NotImplemented) ;
			continue ;
		}

		NodeId * trueRequestedNewNodeId = requestedNewNodeId->getNodeId() ;

		Base * requested = addressSpace->get(trueRequestedNewNodeId) ;

		if (requested != NULL) {
			debug(COM_DBG,"AddNodes","requestedNewNodeId already exists") ;
			results->set(i, AddNodesResult::Bad_NodeIdExists) ;
			continue ;
		}

		/* trueRequestedNewNodeId ok ! */

		QualifiedName * browseName = addNodesItem->getBrowseName() ;

		/* NOT CHECKED : whether browseName is unique in its environment */

		/* browseName ok ! */

		NodeClass * nodeClass = addNodesItem->getNodeClass() ;

		int32_t _nodeClass = nodeClass->get() ;

		/* _nodeClass ok ! */

		ExpandedNodeId * typeDefinition = addNodesItem->getTypeDefinition() ;

		switch (_nodeClass) {
		case NodeClass_OBJECT_TYPE_8:
		case NodeClass_VARIABLE_TYPE_16:
			if (typeDefinition->isNullExpandedNodeId()) {
				debug(COM_ERR,"AddNodes","typeDefinition is null but nodeClass is object or variable") ;
				results->set(i, AddNodesResult::Bad_TypeDefinitionInvalid) ;
				continue ;
			} ;
			if (typeDefinition->getNamespaceUri()->isNotNullOrEmpty()) {
				debug(COM_ERR,"AddNodes","Not implemented typeDefinition with namespaceUri set") ;
				results->set(i, AddNodesResult::Bad_NotImplemented) ;
				continue ;
			}

			if (typeDefinition->getServerIndex()->isNotZero()) {
				debug(COM_ERR,"AddNodes","Not implemented typeDefinition with serverIndex not zero") ;
				results->set(i, AddNodesResult::Bad_NotImplemented) ;
				continue ;
			}
			break ;
		default:
			if (typeDefinition->isNotNullExpandedNodeId()) {
				debug(COM_ERR,"AddNodes","typeDefinition is not null but nodeClass is not object or variable") ;
				results->set(i, AddNodesResult::Bad_TypeDefinitionInvalid) ;
				continue ;
			} ;
			break ;
		}

		NodeId * trueTypeDefinition = typeDefinition->getNodeId() ;

		/* trueTypeDefinition is ok */

		NodeAttributes * nodeAttributes = addNodesItem->getNodeAttributes() ;

		NodeId * nodeAttributesNodeId = nodeAttributes->getNodeId()->getNodeId() ;

		if (nodeAttributesNodeId->getNamespaceIndex()->isNotZero()) {
			debug(COM_ERR,"AddNodes","nodeAttributes NodeId is not local") ;
			CHECK_INT(0) ;
		}

		if (nodeAttributesNodeId->getIdentifierType()->get() != IdType_NUMERIC_0) {
			debug(COM_ERR,"AddNodes","nodeAttributes NodeId is not a string") ;
			CHECK_INT(0) ;
		}

		int32_t nodeAttributesNodeId_uint32 = nodeAttributesNodeId->get() ;


		/* OK */

		Base * base = NULL ;

		switch (_nodeClass) {
		case NodeClass_OBJECT_1:

			if (nodeAttributesNodeId_uint32 != OpcUaId_ObjectAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","Object must be created without ObjectAttributes") ;
				results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				ObjectAttributes * objectAttributes = (ObjectAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = objectAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_Object_Attributes) != Mandatory_Object_Attributes) {
					debug(COM_ERR,"AddNodes","Object must be created but mandatory Object attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new Object(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							objectAttributes->getDisplayName(),   // M
							objectAttributes->getDescription(),   // O
							objectAttributes->getWriteMask(),     // O
							objectAttributes->getUserWriteMask(), // O
							objectAttributes->getEventNotifier()  // M
						) ;

			}
			break ;

		case NodeClass_VARIABLE_2:

			if (nodeAttributesNodeId_uint32 != OpcUaId_VariableAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","Variable must not be created without VariableAttributes") ;
				results         ->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				VariableAttributes * variableAttributes = (VariableAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = variableAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_Variable_Attributes) != Mandatory_Variable_Attributes) {
					debug(COM_ERR,"AddNodes","Variable must be created but mandatory Variable attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				String * aux = variableAttributes->getAux() ;

				if (aux == NULL || aux->isNullOrEmpty()) {

					base = new Variable(
								trueRequestedNewNodeId, // M
								browseName,             // M
								NULL, // SymbolicName

								variableAttributes->getDisplayName(),   // M
								variableAttributes->getDescription(),   // O
								variableAttributes->getWriteMask(),     // O
								variableAttributes->getUserWriteMask(), // O

								NULL, // ParentNodeId
								variableAttributes->getValue(),                   // M
								variableAttributes->getDataType(),                // M
								variableAttributes->getValueRank(),               // M
								variableAttributes->getArrayDimensions(),         // O
								variableAttributes->getAccessLevel(),             // M
								variableAttributes->getUserAccessLevel(),         // M
								variableAttributes->getMinimumSamplingInterval(), // O
								variableAttributes->getHistorizing()              // M
							) ;

				} else {

					base = new ExternalVariable(
								trueRequestedNewNodeId, // M
								browseName,             // M
								NULL, // SymbolicName

								variableAttributes->getDisplayName(),   // M
								variableAttributes->getDescription(),   // O
								variableAttributes->getWriteMask(),     // O
								variableAttributes->getUserWriteMask(), // O

								NULL, // ParentNodeId
								variableAttributes->getValue(),                   // M
								variableAttributes->getDataType(),                // M
								variableAttributes->getValueRank(),               // M
								variableAttributes->getArrayDimensions(),         // O
								variableAttributes->getAccessLevel(),             // M
								variableAttributes->getUserAccessLevel(),         // M
								variableAttributes->getMinimumSamplingInterval(), // O
								variableAttributes->getHistorizing(),             // M

								aux
							) ;

				}

			}
			break ;

		case NodeClass_METHOD_4:

			if (nodeAttributesNodeId_uint32 != OpcUaId_MethodAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","Method must not be created without MethodAttributes") ;
				results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				MethodAttributes * methodAttributes = (MethodAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = methodAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_Method_Attributes) != Mandatory_Method_Attributes) {
					debug(COM_ERR,"AddNodes","Method must be created but mandatory Method attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new Method(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							methodAttributes->getDisplayName(),   // M
							methodAttributes->getDescription(),   // O
							methodAttributes->getWriteMask(),     // O
							methodAttributes->getUserWriteMask(), // O

							methodAttributes->getExecutable(),      // M
							methodAttributes->getUserExecutable(),  // M

							NULL, // MethodDeclaractionId
							methodAttributes->getAux() // O : dllName
						) ;

			}
			break ;

		case NodeClass_OBJECT_TYPE_8:

			if (nodeAttributesNodeId_uint32 != OpcUaId_ObjectTypeAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","ObjectType must not be created without ObjectTypeAttributes") ;
				results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				ObjectTypeAttributes * objectTypeAttributes = (ObjectTypeAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = objectTypeAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_ObjectType_Attributes) != Mandatory_ObjectType_Attributes) {
					debug(COM_ERR,"AddNodes","ObjectType must be created but mandatory ObjectType attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new ObjectType(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							objectTypeAttributes->getDisplayName(),   // M
							objectTypeAttributes->getDescription(),   // O
							objectTypeAttributes->getWriteMask(),     // O
							objectTypeAttributes->getUserWriteMask(), // O

							objectTypeAttributes->getIsAbstract()     // M
						) ;

			}
			break ;

		case NodeClass_VARIABLE_TYPE_16:

			if (nodeAttributesNodeId_uint32 != OpcUaId_VariableTypeAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","VariableType must not be created without VariableTypeAttributes") ;
				results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				VariableTypeAttributes * variableTypeAttributes = (VariableTypeAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = variableTypeAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_VariableType_Attributes) != Mandatory_VariableType_Attributes) {
					debug(COM_ERR,"AddNodes","VariableType must be created but mandatory VariableType attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new VariableType(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							variableTypeAttributes->getDisplayName(),   // M
							variableTypeAttributes->getDescription(),   // O
							variableTypeAttributes->getWriteMask(),     // O
							variableTypeAttributes->getUserWriteMask(), // O

							variableTypeAttributes->getValue(),           // O
							variableTypeAttributes->getDataType(),        // M
							variableTypeAttributes->getValueRank(),       // M
							variableTypeAttributes->getArrayDimensions(), // O
							variableTypeAttributes->getIsAbstract()       // M
						) ;

			}
			break ;

		case NodeClass_REFERENCE_TYPE_32:

			if (nodeAttributesNodeId_uint32 != OpcUaId_ReferenceTypeAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","ReferenceType must not be created without ReferenceTypeAttributes") ;
				results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				ReferenceTypeAttributes * referenceTypeAttributes = (ReferenceTypeAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = referenceTypeAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_ReferenceType_Attributes) != Mandatory_ReferenceType_Attributes) {
					debug(COM_ERR,"AddNodes","ReferenceType must be created but mandatory ReferenceType attributes not present") ;
					results->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new ReferenceType(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							referenceTypeAttributes->getDisplayName(),   // M
							referenceTypeAttributes->getDescription(),   // O
							referenceTypeAttributes->getWriteMask(),     // O
							referenceTypeAttributes->getUserWriteMask(), // O

							referenceTypeAttributes->getIsAbstract(),    // M
							referenceTypeAttributes->getSymmetric(),     // M
							referenceTypeAttributes->getInverseName()   // O
						) ;

			}
			break ;

		case NodeClass_DATA_TYPE_64:

			if (nodeAttributesNodeId_uint32 != OpcUaId_DataTypeAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","DataType must not be created without DataTypeAttributes") ;
				results         ->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				DataTypeAttributes * dataTypeAttributes = (DataTypeAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = dataTypeAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_DataType_Attributes) != Mandatory_DataType_Attributes) {
					debug(COM_ERR,"AddNodes","DataType must be created but mandatory DataType attributes not present") ;
					results         ->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new DataType(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							dataTypeAttributes->getDisplayName(),   // M
							dataTypeAttributes->getDescription(),   // O
							dataTypeAttributes->getWriteMask(),     // O
							dataTypeAttributes->getUserWriteMask(), // O

							dataTypeAttributes->getIsAbstract()     // M
						) ;

			}
			break ;

		case NodeClass_VIEW_128:

			if (nodeAttributesNodeId_uint32 != OpcUaId_ViewAttributes_Encoding_DefaultBinary) {
				debug(COM_ERR,"AddNodes","View must be created without ViewAttributes") ;
				results         ->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
				continue ;
			} else {

				ViewAttributes * viewAttributes = (ViewAttributes *)nodeAttributes ;

				UInt32 *  specifiedAttributes = viewAttributes->getSpecifiedAttributes() ;
				uint32_t _specifiedAttributes = specifiedAttributes->get() ;

				if ((_specifiedAttributes & Mandatory_View_Attributes) != Mandatory_View_Attributes) {
					debug(COM_ERR,"AddNodes","View must be created but mandatory View attributes not present") ;
					results         ->set(i, AddNodesResult::Bad_NodeAttributesInvalid) ;
					continue ;
				}

				base = new View(
							trueRequestedNewNodeId, // M
							browseName,             // M
							NULL, // SymbolicName

							viewAttributes->getDisplayName(),   // M
							viewAttributes->getDescription(),   // O
							viewAttributes->getWriteMask(),     // O
							viewAttributes->getUserWriteMask(), // O

							viewAttributes->getContainsNoLoops(),  // M
							viewAttributes->getEventNotifier()     // M
						) ;

			}
			break ;

		default:
			debug(COM_ERR,"AddNodes","nodeClass unknown (impossible)") ;
			CHECK_INT(0) ;
		}


		addressSpace->put(base) ;


		new ReferenceLink(reference_code,parent,base) ;

		switch (_nodeClass) {
		case NodeClass_OBJECT_TYPE_8:
		case NodeClass_VARIABLE_TYPE_16:
			{
				Base * parentTypeDefinition = addressSpace->get(trueTypeDefinition) ;

				if (parentTypeDefinition == NULL) {
					debug(COM_ERR,"AddNodes","typeDefinition does not exist") ;
					results         ->set(i, AddNodesResult::Bad_TypeDefinitionInvalid) ;
					continue ;
				}

				new ReferenceLink(OpcUaRef_HasTypeDefinition,base,parentTypeDefinition) ;
			}
			break ;
		default:
			break ;
		}


		debug(COM_DBG,"AddNodes","Setting results") ;

		results ->set(i, new AddNodesResult(trueRequestedNewNodeId)) ;

		debug_i(COM_DBG,"AddNodes","Results ends (%d)",i) ;
	}

	debug(COM_DBG,"AddNodes","Request succeeds : returning Response.") ;

	*pResponse =
			new AddNodesResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					results,
					DiagnosticInfo::tableZero
			) ;
}

} /* namespace opcua */

#endif
