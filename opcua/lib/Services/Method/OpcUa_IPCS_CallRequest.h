/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_CALLREQUEST_H_
#define OPCUA_CALLREQUEST_H_

/*
 * Part 4, 5.11.2.2, p. 60
 */

#include "../../OpcUa.h"

#if (WITH_CALL == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL CallRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_CallRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_CallRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader            * requestHeader ;
	TableCallMethodRequest   * methodsToCall ;

public:

	static CallRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_CallRequest const& pCallRequest)
	{
		RequestHeader            * requestHeader = RequestHeader         ::fromCtoCpp (pStatus, pRequestHeader) ;
		TableCallMethodRequest   * methodsToCall = TableCallMethodRequest::fromCtoCpp (pStatus, pCallRequest.NoOfMethodsToCall,pCallRequest.MethodsToCall) ;

		if (*pStatus == STATUS_OK)
			return
					new CallRequest(
							requestHeader,
							methodsToCall
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (methodsToCall != NULL)
			methodsToCall->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader &pRequestHeader, OpcUa_CallRequest& pCallRequest) const
	{
		requestHeader ->fromCpptoC (pStatus, pRequestHeader) ;
		methodsToCall ->fromCpptoC (pStatus, pCallRequest.NoOfMethodsToCall,pCallRequest.MethodsToCall) ;
	}

public:

	CallRequest(
			RequestHeader            * _requestHeader,
			TableCallMethodRequest   * _methodsToCall
			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(methodsToCall = _methodsToCall) ->take() ;
	}

	virtual ~CallRequest()
	{
		requestHeader ->release() ;
		methodsToCall ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline TableCallMethodRequest   * getMethodsToCall()
	{
		return methodsToCall;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_CALLREQUEST_H_ */
