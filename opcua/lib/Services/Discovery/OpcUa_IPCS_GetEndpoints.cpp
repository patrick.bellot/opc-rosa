/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY== 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_GetEndpointsRequest.h"
#include "OpcUa_IPCS_GetEndpointsResponse.h"
#include "OpcUa_IPCS_GetEndpoints.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_GetEndpoints(
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_GetEndpointsRequest  * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_GetEndpointsResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	opcua::GetEndpointsRequest * request  = NULL ;
	request = opcua::GetEndpointsRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	opcua::GetEndpointsResponse * response = NULL ;
	opcua::GetEndpoints::answer(&status,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void GetEndpoints::answer(
		SOPC_StatusCode             * pStatus,
	    class GetEndpointsRequest   * request,
	    class GetEndpointsResponse ** pResponse)
{
	debug(MAIN_LIFE_DBG,"GetEndpoints","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;

	debug(DISC_DBG,"GetEndpoints","Request : checking parameters.") ;

#if (DEBUG_LEVEL & DISC_DBG)
	String * endpointUrl = request->getEndpointUrl() ;
	debug_s(DISC_DBG,"GetEndpoints","Request : checking endpointUrl=\"%s\"",endpointUrl->get()) ;
	TableLocaleId * localeIds = request->getLocaleIds() ;
	int32_t nbLocaleIds = localeIds->getLength() ;
	debug_i(DISC_DBG,"GetEndpoints","Request : number of LocaleId=%d",nbLocaleIds) ;
	TableString * profileUris = request->getProfileUris() ;
	int32_t nbProfileUris = profileUris->getLength() ;
	debug_i(DISC_DBG,"GetEndpoints","Request : number of ProfileUris=%d",nbProfileUris) ;
	for (int i = 0 ; i < nbProfileUris ; i++) {
		String * profileUri = profileUris->get(i) ;
		debug_i(DISC_DBG,"GetEndpoints","Request : ProfileUris number %d = \"%s\"",i,profileUri->get()) ;
	}
#endif

	debug(DISC_DBG,"GetEndpoints","Request : controlling result.") ;

	if (EndpointDescription::selfEndpoints == NULL) {
		debug_i(DISC_DBG,"GetEndpoints","Request : bad result, rc = 0x%08x",_Bad_NotImplemented) ;
		*pStatus = _Bad_NotImplemented ;
		return ;
	}

	debug(DISC_DBG,"GetEndpoints","Request : getting and checking parameters.") ;

	debug(DISC_DBG,"GetEndpoints","Request succeeds : returning Response.") ;

	*pResponse = // Part 4, 5.11.2.2, p. 60
			new GetEndpointsResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					EndpointDescription::selfEndpoints
			) ;
}

}      /* namespace opcua */
#endif
