/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY== 1)

#include "../../AddressSpace/All.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_FindServersRequest.h"
#include "OpcUa_IPCS_FindServersResponse.h"
#include "OpcUa_IPCS_FindServers.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_FindServers(
		OpcUa_RequestHeader        * pRequestHeader,
		OpcUa_FindServersRequest   * pRequest,
		OpcUa_ResponseHeader       * pResponseHeader,
		OpcUa_FindServersResponse  * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	opcua::FindServersRequest * request  = NULL ;
	request = opcua::FindServersRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	opcua::FindServersResponse * response = NULL ;
	opcua::FindServers::answer(&status,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}

MYDLL void FindServers::answer(
		SOPC_StatusCode             * pStatus,
	    class FindServersRequest    * request,
	    class FindServersResponse  ** pResponse)
{
	debug(MAIN_LIFE_DBG,"FindServers","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;

	debug(DISC_DBG,"FindServers","Request : checking parameters.") ;

	/* NO CHECK */

	debug(DISC_DBG,"FindServers","Request : computing result.") ;

	TableApplicationDescription * result = new TableApplicationDescription(1) ;
	result->set(0,EndpointDescription::selfEndpoints->get(0)->getServer()) ;

	debug(DISC_DBG,"FindServers","Request succeeds : returning Response.") ;

	*pResponse = // Part 4, 5.11.2.2, p. 60
			new FindServersResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					result
			) ;

	NOT_USED(pStatus) ;
}

}      /* namespace opcua */
#endif
