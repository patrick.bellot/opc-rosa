/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_GETENDPOINTSRESPONSE_H_
#define OPCUA_GETENDPOINTSRESPONSE_H_

/*
 * Part 4, 5.4.3.2
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL GetEndpointsResponse
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_GetEndpointsResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_GetEndpointsResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader           * responseHeader ;
	TableEndpointDescription * endpoints ;

public:

//	static GetEndpointsResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_GetEndpointsResponse const& pGetEndpointsResponse)
//	{
//		ResponseHeader           * responseHeader = ResponseHeader           ::fromCtoCpp (pStatus, pGetEndpointsResponse.ResponseHeader) ;
//		TableEndpointDescription * endpoints      = TableEndpointDescription ::fromCtoCpp (pStatus, pGetEndpointsResponse.NoOfEndpoints,  pGetEndpointsResponse.Endpoints) ;
//
//		if (*pStatus == STATUS_OK)
//			return
//					new GetEndpointsResponse(
//							responseHeader,
//							endpoints
//							) ;
//
//		if (responseHeader != NULL)
//			responseHeader->checkRefCount() ;
//		if (endpoints != NULL)
//			endpoints->checkRefCount() ;
//
//		return NULL ;
//	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_GetEndpointsResponse& pGetEndpointsResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		endpoints       ->fromCpptoC (pStatus, pGetEndpointsResponse.NoOfEndpoints,  pGetEndpointsResponse.Endpoints) ;
	}

public:

	GetEndpointsResponse(
			ResponseHeader           * _responseHeader,
			TableEndpointDescription * _endpoints
			)
		: Structure()
	{
		(responseHeader   = _responseHeader) ->take() ;
		(endpoints        = _endpoints)      ->take() ;
	}

	virtual ~GetEndpointsResponse()
	{
		responseHeader  ->release() ;
		endpoints       ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader()  /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline TableEndpointDescription * getEndpoints()
	{
		return endpoints;
	}

};
} /* namespace opcua */
#endif
#endif /* OPCUA_GETENDPOINTSRESPONSE_H_ */
