/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_GETENDPOINTSREQUEST_H_
#define OPCUA_GETENDPOINTSREQUEST_H_

/*
 * Part 4, 5.4.3.2
 */

#include "../../OpcUa.h"

#if (WITH_DISCOVERY == 1)

#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL GetEndpointsRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_GetEndpointsRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_GetEndpointsRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader * requestHeader ;
	String        * endpointUrl ;
	TableLocaleId * localeIds ;
	TableString   * profileUris ;

public:

	static GetEndpointsRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_GetEndpointsRequest const& pGetEndpointsRequest)
	{
		RequestHeader * requestHeader = RequestHeader ::fromCtoCpp (pStatus, pRequestHeader) ;
		String        * endpointUrl   = String        ::fromCtoCpp (pStatus, pGetEndpointsRequest.EndpointUrl) ;
		TableLocaleId * localeIds     = TableLocaleId ::fromCtoCpp (pStatus, pGetEndpointsRequest.NoOfLocaleIds,   pGetEndpointsRequest.LocaleIds) ;
		TableString   * profileUris   = TableString   ::fromCtoCpp (pStatus, pGetEndpointsRequest.NoOfProfileUris, pGetEndpointsRequest.ProfileUris) ;

		if (*pStatus == STATUS_OK)
			return
					new GetEndpointsRequest(
							requestHeader,
							endpointUrl,
							localeIds,
							profileUris
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (endpointUrl != NULL)
			endpointUrl->checkRefCount() ;
		if (localeIds != NULL)
			localeIds->checkRefCount() ;
		if (profileUris != NULL)
			profileUris->checkRefCount() ;

		return NULL ;
	}

//	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_GetEndpointsRequest& pGetEndpointsRequest) const
//	{
//		requestHeader ->fromCpptoC (pStatus, pGetEndpointsRequest.RequestHeader) ;
//		endpointUrl   ->fromCpptoC (pStatus, pGetEndpointsRequest.EndpointUrl) ;
//		localeIds     ->fromCpptoC (pStatus, pGetEndpointsRequest.NoOfLocaleIds,   pGetEndpointsRequest.LocaleIds) ;
//		profileUris   ->fromCpptoC (pStatus, pGetEndpointsRequest.NoOfProfileUris, pGetEndpointsRequest.ProfileUris) ;
//	}

public:

	GetEndpointsRequest(
			RequestHeader * _requestHeader,
			String        * _endpointUrl,
			TableLocaleId * _localeIds,
			TableString   * _profileUris

			)
		: Structure()
	{
		(requestHeader = _requestHeader) ->take() ;
		(endpointUrl   = _endpointUrl)   ->take() ;
		(localeIds     = _localeIds)     ->take() ;
		(profileUris   = _profileUris)   ->take() ;
	}

	virtual ~GetEndpointsRequest()
	{
		requestHeader ->release() ;
		endpointUrl   ->release() ;
		localeIds     ->release() ;
		profileUris   ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader()  /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	inline String * getEndpointUrl()
	{
		return endpointUrl ;
	}

	inline TableLocaleId * getLocaleIds()
	{
		return localeIds ;
	}

	inline TableString * getProfileUris()
	{
		return profileUris ;
	}


};
} /* namespace opcua */
#endif
#endif /* OPCUA_GETENDPOINTSREQUEST_H_ */
