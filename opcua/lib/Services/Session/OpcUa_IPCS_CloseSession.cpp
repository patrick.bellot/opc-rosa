
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"
#include "../../AddressSpace/All.h"

#include "OpcUa_IPCS_CloseSessionRequest.h"
#include "OpcUa_IPCS_CloseSessionResponse.h"
#include "OpcUa_IPCS_CloseSession.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_CloseSession(
		uint32_t					  secureChannelIdNum,
		OpcUa_RequestHeader         * pRequestHeader,
		OpcUa_CloseSessionRequest  * pRequest,
		OpcUa_ResponseHeader        * pResponseHeader,
		OpcUa_CloseSessionResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	CloseSessionRequest * request  = NULL ;
	request = CloseSessionRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	CloseSessionResponse * response = NULL ;
	CloseSession::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}


MYDLL void CloseSession::answer(
		SOPC_StatusCode              * pStatus,
		uint32_t					   secureChannelId,
	    class CloseSessionRequest   * request,
	    class CloseSessionResponse ** pResponse)
{
	ChannelsTable * channelsTable = ChannelsTable::self ;

	debug(MAIN_LIFE_DBG,"CloseSession","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;

	Channel * channel = channelsTable->get(secureChannelId) ;

	if (channel == NULL) {
		debug(COM_DBG,"CloseSession","Request failed : bad channel id.") ;
		* pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

	uint32_t sessionIndex = authenticationToken->get() ;

	Session * session = channel->getSession(sessionIndex) ;

	if (session == NULL) {
		debug_i(COM_DBG,"CloseSession","Request failed : bad session id (bad authenticationToken).",(int)(authenticationToken->get())) ;
		* pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

	if (session->getClosed()) {
		debug(COM_DBG,"CloseSession","Request failed : session is closed.") ;
		* pStatus = _Bad_SessionClosed ;
		return ;
	}

	debug(COM_DBG,"CloseSession","Request : closing session.") ;

	session->setClosed(true) ;

#if (WITH_SUBSCRIPTION == 1)
	if (request->getDeleteSubscriptions()->isTrue()) {
		debug(SUB_DBG,"CloseSession","Request : deleting subscriptions begins") ;
		session->deleteAllSubscriptions() ;
		debug(SUB_DBG,"CloseSession","Request : deleting subscriptions ends") ;
	}
#endif

	channel->deleteSession(sessionIndex) ;

	debug(COM_DBG,"CloseSession","Request succeeds : returning CloseSession Response.") ;

	*pResponse =
			new CloseSessionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					)
			) ;
}

} /* namespace opcua */
