
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ACTIVATESESSIONREQUEST_H_
#define OPCUA_ACTIVATESESSIONREQUEST_H_

/*
 * Part3, 5.6.3.2, p. 29
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ActivateSessionRequest
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ActivateSessionRequest ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ActivateSessionRequest_Encoding_DefaultBinary ;
	}

private:

	RequestHeader                    * requestHeader ;
	SignatureData                    * clientSignature ;
	TableSignedSoftwareCertificate   * clientSoftwareCertificate ;
	TableLocaleId                    * localeIds ;
	UserIdentityToken                * userIdentityToken ;
	SignatureData 					 * userTokenSignature ;

public:

	static ActivateSessionRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_RequestHeader const& pRequestHeader, OpcUa_ActivateSessionRequest const& pActivateSessionRequest)
	{
		RequestHeader                    * requestHeader             = RequestHeader     ::fromCtoCpp(pStatus, pRequestHeader) ;
		SignatureData                    * clientSignature           = SignatureData     ::fromCtoCpp(pStatus, pActivateSessionRequest.ClientSignature) ;
		TableSignedSoftwareCertificate   * clientSoftwareCertificate = TableSignedSoftwareCertificate::fromCtoCpp(pStatus, pActivateSessionRequest.NoOfClientSoftwareCertificates,pActivateSessionRequest.ClientSoftwareCertificates) ;
		TableLocaleId                    * localeIds                 = TableLocaleId::fromCtoCpp(pStatus, pActivateSessionRequest.NoOfLocaleIds,pActivateSessionRequest.LocaleIds) ;
		UserIdentityToken                * userIdentityToken         = UserIdentityToken ::fromCtoCpp(pStatus, pActivateSessionRequest.UserIdentityToken)  ;

		SignatureData 					 * userTokenSignature        = SignatureData     ::fromCtoCpp(pStatus, pActivateSessionRequest.UserTokenSignature)  ;

		if (*pStatus == STATUS_OK)
			return
					new ActivateSessionRequest(
							requestHeader,
							clientSignature,
							clientSoftwareCertificate,
							localeIds,
							userIdentityToken,

							userTokenSignature
							) ;

		if (requestHeader != NULL)
			requestHeader->checkRefCount() ;
		if (clientSignature != NULL)
			clientSignature->checkRefCount() ;
		if (clientSoftwareCertificate != NULL)
			clientSoftwareCertificate->checkRefCount() ;
		if (localeIds != NULL)
			localeIds->checkRefCount() ;
		if (userIdentityToken != NULL)
			userIdentityToken->checkRefCount() ;

		if (userTokenSignature != NULL)
			userTokenSignature->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_RequestHeader &pRequestHeader, OpcUa_ActivateSessionRequest& pActivateSessionRequest) const
	{
		requestHeader             ->fromCpptoC (pStatus, pRequestHeader) ;
		clientSignature           ->fromCpptoC (pStatus, pActivateSessionRequest.ClientSignature) ;
		clientSoftwareCertificate ->fromCpptoC (pStatus, pActivateSessionRequest.NoOfClientSoftwareCertificates,pActivateSessionRequest.ClientSoftwareCertificates) ;
		localeIds                 ->fromCpptoC (pStatus, pActivateSessionRequest.NoOfLocaleIds,pActivateSessionRequest.LocaleIds) ;
		userIdentityToken         ->fromCpptoC (pStatus, pActivateSessionRequest.UserIdentityToken) ;

		userTokenSignature        ->fromCpptoC (pStatus, pActivateSessionRequest.UserTokenSignature) ;
	}

public:

	ActivateSessionRequest(
			RequestHeader                    * _requestHeader,
			SignatureData                    * _clientSignature,
			TableSignedSoftwareCertificate   * _clientSoftwareCertificate,
			TableLocaleId                    * _localeIds,
			UserIdentityToken                * _userIdentityToken,
			SignatureData 					 * _userTokenSignature
			)
		: Structure()
	{
		(requestHeader             = _requestHeader)             ->take() ;
		(clientSignature           = _clientSignature)           ->take() ;
		(clientSoftwareCertificate = _clientSoftwareCertificate) ->take() ;
		(localeIds                 = _localeIds)                 ->take() ;
		(userIdentityToken         = _userIdentityToken)         ->take() ;

		(userTokenSignature        = _userTokenSignature)        ->take() ;
	}

	virtual ~ActivateSessionRequest()
	{
		requestHeader             ->release() ;
		clientSignature           ->release() ;
		clientSoftwareCertificate ->release() ;
		localeIds                 ->release() ;
		userIdentityToken         ->release() ;

		userTokenSignature        ->release() ;
	}

public:

	virtual inline RequestHeader * getRequestHeader() /* redefined from BaseDataType */
	{
		return requestHeader ;
	}

	virtual inline SignatureData * getClientSignature() /* redefined from BaseDataType */
	{
		return clientSignature ;
	}

};
} /* namespace opcua */
#endif /* ACTIVATESESSIONREQUEST_H_ */
