
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ACTIVATESESSIONRESPONSE_H_
#define OPCUA_ACTIVATESESSIONRESPONSE_H_

/*
 * Part3, 5.6.3.2, p. 29
 */

#include "../../OpcUa.h"
#include "../../StandardDataTypes/All.h"
#include "../../CommonParametersTypes/All.h"
#include "../../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL ActivateSessionResponse
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_ActivateSessionResponse ; }

	virtual uint32_t getBinaryEncodingId() const
	{
		return OpcUaId_ActivateSessionResponse_Encoding_DefaultBinary ;
	}

private:

	ResponseHeader        * responseHeader ;
	Nonce                 * serverNonce ;
	TableStatusCode       * results ;
	TableDiagnosticInfo   * diagnosticInfos ;

public:

	static ActivateSessionResponse * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader const& pResponseHeader, OpcUa_ActivateSessionResponse const& pActivateSessionResponse)
	{
		ResponseHeader        * responseHeader  = ResponseHeader ::fromCtoCpp(pStatus, pResponseHeader) ;
		Nonce                 * serverNonce     = Nonce          ::fromCtoCpp(pStatus, pActivateSessionResponse.ServerNonce) ;
		TableStatusCode       * results         = TableStatusCode::fromCtoCpp     (pStatus, pActivateSessionResponse.NoOfResults,pActivateSessionResponse.Results);
		TableDiagnosticInfo   * diagnosticInfos = TableDiagnosticInfo::fromCtoCpp (pStatus, pActivateSessionResponse.NoOfDiagnosticInfos,pActivateSessionResponse.DiagnosticInfos);

		if (*pStatus == STATUS_OK)
			return
					new ActivateSessionResponse(
							responseHeader,
							serverNonce,
							results,
							diagnosticInfos
							) ;

		if (responseHeader != NULL)
			responseHeader->checkRefCount() ;
		if (serverNonce != NULL)
			serverNonce->checkRefCount() ;
		if (results != NULL)
			results->checkRefCount() ;
		if (diagnosticInfos != NULL)
			diagnosticInfos->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_ResponseHeader& pResponseHeader, OpcUa_ActivateSessionResponse& pActivateSessionResponse) const
	{
		responseHeader  ->fromCpptoC (pStatus, pResponseHeader) ;
		serverNonce     ->fromCpptoC (pStatus, pActivateSessionResponse.ServerNonce) ;
		results         ->fromCpptoC (pStatus, pActivateSessionResponse.NoOfResults,pActivateSessionResponse.Results) ;
		diagnosticInfos ->fromCpptoC (pStatus, pActivateSessionResponse.NoOfDiagnosticInfos,pActivateSessionResponse.DiagnosticInfos) ;
	}

public:

	ActivateSessionResponse(
			ResponseHeader        * _responseHeader,
			Nonce                 * _serverNonce,
			TableStatusCode       * _results,
			TableDiagnosticInfo   * _diagnosticInfos
			)
		: Structure()
	{
		(responseHeader  = _responseHeader)  ->take() ;
		(serverNonce     = _serverNonce)     ->take() ;
		(results         = _results)         ->take() ;
		(diagnosticInfos = _diagnosticInfos) ->take() ;
	}

	virtual ~ActivateSessionResponse()
	{
		responseHeader  ->release() ;
		serverNonce     ->release() ;
		results         ->release() ;
		diagnosticInfos ->release() ;
	}

public:

	virtual inline ResponseHeader * getResponseHeader() /* redefined from BaseDataType */
	{
		return responseHeader ;
	}

	inline Nonce * getServerNonce()
	{
		return serverNonce ;
	}

};
} /* namespace opcua */
#endif /* ACTIVATESESSIONREQUEST_H_ */
