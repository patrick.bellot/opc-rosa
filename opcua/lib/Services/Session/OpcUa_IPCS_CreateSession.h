
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_CREATESESSION_H_
#define OPCUA_CREATESESSION_H_

#include "../../OpcUa.h"
#include "../OpcUa_BaseService.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_CreateSession(
		uint32_t					  secureChannelIdNum,
		OpcUa_RequestHeader         * pRequestHeader,
		OpcUa_CreateSessionRequest  * pRequest,
		OpcUa_ResponseHeader        * pResponseHeader,
		OpcUa_CreateSessionResponse * pResponse) ;

class MYDLL CreateSession
	: public BaseService
{
public:

	static void answer(
			SOPC_StatusCode              * pStatus,
			uint32_t					   secureChannelId,
		    class CreateSessionRequest   * request,
		    class CreateSessionResponse ** pResponse) ;

};     // class CreateSession
}      /* namespace opcua */
#endif /* CREATESESSION_H_ */
