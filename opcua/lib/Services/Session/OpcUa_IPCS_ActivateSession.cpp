
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"
#include "../../AddressSpace/All.h"

#include "OpcUa_IPCS_ActivateSessionRequest.h"
#include "OpcUa_IPCS_ActivateSessionResponse.h"
#include "OpcUa_IPCS_ActivateSession.h"

#include "../../Stacks/OpcUa_Certificates.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_ActivateSession(
		uint32_t					    secureChannelIdNum,
		OpcUa_RequestHeader           * pRequestHeader,
		OpcUa_ActivateSessionRequest  * pRequest,
		OpcUa_ResponseHeader          * pResponseHeader,
		OpcUa_ActivateSessionResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	ActivateSessionRequest * request  = NULL ;
	request = ActivateSessionRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	ActivateSessionResponse * response = NULL ;
	ActivateSession::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}


MYDLL void ActivateSession::answer(
		SOPC_StatusCode                * pStatus,
		uint32_t					     secureChannelId,
	    class ActivateSessionRequest   * request,
	    class ActivateSessionResponse ** pResponse)
{
	ChannelsTable * channelsTable = ChannelsTable::self ;

	debug(MAIN_LIFE_DBG,"ActivateSession","processing request") ;

	RequestHeader              * requestHeader       = request->getRequestHeader() ;
	IntegerId                  * requestHandle       = requestHeader->getRequestHandle() ;
	SessionAuthenticationToken * authenticationToken = requestHeader->getAuthenticationToken() ;

	Channel * channel = channelsTable->get(secureChannelId) ;

	if (channel == NULL) {
		debug(COM_DBG,"ActivateSession","Request failed : bad channel id.") ;
		* pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

	Session * session = channel->getSession(authenticationToken->get()) ;

	if (session == NULL) {
		debug_i(COM_DBG,"ActivateSession","Request failed : bad session id (bad authenticationToken).",(int)(authenticationToken->get())) ;
		* pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

#if (SECURITY != UANONE)

	if (request->getClientSignature     () == NULL ||
		Certificates::selfCertificate      == NULL ||
		session->getLocalNonce          () == NULL ||
		session->getDistantCertificate  () == NULL ) {

		debug (COM_ERR,"ActivateSession","channel parameter has not been found") ;
		* pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

#endif


	if (session->getClosed()) {
		debug(COM_DBG,"ActivateSession","Request failed : session is closed.") ;
		* pStatus = _Bad_SessionClosed ;
		return ;
	}

	debug(COM_DBG,"ActivateSession","Request : activating session.") ;

	session->setActivated(true) ;

	debug(COM_DBG,"ActivateSession","Request succeeds : returning ActivateSession Response.") ;

	// set nonce
	Nonce * serverNonce = NULL ;

#if (SECURITY == UANONE)
	serverNonce = Nonce::zero ;

	*pResponse =  // Part 4, 5.6.3.2, p. 29
			new ActivateSessionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					serverNonce,
					new TableStatusCode  (0),
					DiagnosticInfo::tableZero
			) ;
	return ;
#endif

#if (SECURITY != UANONE)
	if (Certificates::computeRandNonce(&serverNonce)) {
		debug (COM_ERR,"ActivateSession","computeRandNonce operation failed") ;
		* pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

	if (serverNonce == NULL) {
		debug (COM_ERR,"ActivateSession","serverNonce not found") ;
		* pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

	session->setLastCreateOrActivateSessionNonce(serverNonce) ;

	*pResponse = // Part 4, 5.6.3.2, p. 29
			new ActivateSessionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					serverNonce,
					new TableStatusCode  (0),
					DiagnosticInfo::tableZero
			) ;
#endif

}
} /* namespace opcua */
