
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../OpcUa.h"
#include "../../AddressSpace/All.h"
#include "../../Stacks/OpcUa_Certificates.h"
#include "../../Server/OpcUa_LinkDescription.h"
#include "../../Server/OpcUa_AddressSpace.h"

#include "OpcUa_IPCS_CreateSessionRequest.h"
#include "OpcUa_IPCS_CreateSessionResponse.h"
#include "OpcUa_IPCS_CreateSession.h"

namespace opcua {

MYDLL SOPC_StatusCode OpcUa_ServerApi_CreateSession(
		uint32_t					  secureChannelIdNum,
		OpcUa_RequestHeader         * pRequestHeader,
		OpcUa_CreateSessionRequest  * pRequest,
		OpcUa_ResponseHeader        * pResponseHeader,
		OpcUa_CreateSessionResponse * pResponse)
{
	SOPC_StatusCode status = STATUS_OK ;

	CreateSessionRequest * request  = NULL ;
	request = CreateSessionRequest::fromCtoCpp(&status,*pRequestHeader,*pRequest) ;
	if (status != STATUS_OK) {
		if (request != NULL)
			request->checkRefCount() ;
		return status ;
	}
	request->take() ;

	CreateSessionResponse * response = NULL ;
	CreateSession::answer(&status,secureChannelIdNum,request,&response) ;
	request->release() ;
	if (status != STATUS_OK) {
		if (response != NULL)
			response->checkRefCount() ;
		return status ;
	}

	response->fromCpptoC(&status,*pResponseHeader,*pResponse) ;
	response->checkRefCount() ;

	return status ;
}


MYDLL void CreateSession::answer(
		SOPC_StatusCode              * pStatus,
		uint32_t					   secureChannelId,
	    class CreateSessionRequest   * request,
	    class CreateSessionResponse ** pResponse)
{
	debug(MAIN_LIFE_DBG,"CreateSession","processing request") ;

	ChannelsTable * channelsTable = ChannelsTable::self ;

	IntegerId * requestHandle = request->getRequestHeader()->getRequestHandle() ;

	Channel * channel = channelsTable->get(secureChannelId) ;

	if (channel == NULL) {
		debug(COM_ERR,"CreateSession","Request fails : bad secure channel id invalid.") ;
		*pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

#if (SECURITY != UANONE)
	ApplicationInstanceCertificate * clientCertificate = request->getClientCertificate() ;

	if (clientCertificate == NULL) {
		debug_com(COM_ERR,"CreateSession","client certificate not found") ;
		*pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

	if (clientCertificate->isNotValid()) {
		debug_i(COM_ERR,"CreateSession", "clientCertificate not valid: error=%d",*pStatus) ;
		*pStatus = clientCertificate->getError() ;
		return ;
	}

	if (! clientCertificate->equals(channel->getDistantCertificate())) {
		debug_com(COM_ERR,"CreateSession","opens secure channel and create session certificates are not equal") ;
		*pStatus = _Bad_SecureChannelIdInvalid ;
		return ;
	}

  #if (VERIF_APPURI == VERIF_APPURI_ENABLED)
	String * certificateClientAppUri = NULL ;

	if (clientCertificate->getSubjectAltName(&certificateClientAppUri,6)) {
		debug_com(COM_ERR,"CreateSession","Get client application URI failed") ;
		*pStatus = _Bad_CertificateUriInvalid ;
		return ;
	}

	certificateClientAppUri->take() ;

	if(! (certificateClientAppUri->equals(request->getClientApplicationUri()))) {
		debug(COM_ERR,"CreateSession","The client certificate application URI, and the description client application URI are not equal") ;
		debug_ss(COM_ERR,"CreateSession","ie: \"%s\" != \"%s\"",certificateClientAppUri->get(),request->getClientApplicationUri()->get()) ;
		certificateClientAppUri->release() ;
		*pStatus = _Bad_CertificateUriInvalid ;
		return ;
	}

	certificateClientAppUri->release() ;
  #endif

#endif

	String * name = request->getSessionName() ;

	if (name->isNullOrEmpty())
		request->setSessionName(name = String::getRandomString()) ;

	Session * session = channel->getNewSession(name) ;

#if (SECURITY != UANONE)
	session->setDistantCertificate(clientCertificate) ;
	session->setDistantNonce(request->getClientNonce()) ;

	SignatureData * serverSignatureData = NULL ;
#endif

	Nonce * serverNonce	= NULL ;

#if (SECURITY == UANONE)
	serverNonce = Nonce::zero ;
#endif

#if (SECURITY != UANONE)
	if (Certificates::computeRandNonce(&serverNonce)) {
		debug (COM_ERR,"CreateSession","computeRandNonce operation failed") ;
		*pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

	if (serverNonce == NULL) {
		debug (COM_ERR,"CreateSession","serverNonce not found") ;
		*pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

	session->setLocalNonce(serverNonce) ;

	if (Certificates::computeSignatureData(&serverSignatureData, session->getDistantCertificate(), session->getDistantNonce())) {
		if (serverSignatureData != NULL)
			delete serverSignatureData ;
		*pStatus = _Bad_SessionIdInvalid ;
		return ;
	}

#endif

	session->setLastCreateOrActivateSessionNonce(serverNonce) ;

	debug(COM_DBG,"CreateSession","Request succeeds : returning CreateSession Response.") ;

	#if (SECURITY == UANONE)
	*pResponse =
			new CreateSessionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					session->getSessionId(),
					new SessionAuthenticationToken(session->getAuthenticationTokenNum()),
					Duration::maximum,
					serverNonce,
					Certificates::selfCertificate,
					EndpointDescription::selfEndpoints,
					new TableSignedSoftwareCertificate  (0),
					SignatureData::fakeSignatureData,
					UInt32::maximum
			) ;
#endif

#if (SECURITY != UANONE)
	*pResponse =
			new CreateSessionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					session->getSessionId(),
					new SessionAuthenticationToken(session->getAuthenticationTokenNum()),
					Duration::maximum,
					serverNonce,
					Certificates::selfCertificate,
					EndpointDescription::selfEndpoints,
					new TableSignedSoftwareCertificate  (0),
					serverSignatureData, // serverSignature
					UInt32::maximum
			) ;
#endif

	return ;
}

} /* namespace opcua */
