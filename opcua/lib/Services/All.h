/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALL_SERVICES_REQUEST_H_
#define ALL_SERVICES_REQUEST_H_

#include "Method/All.h"
#include "MonitoredItem/All.h"
#include "NodeManagement/All.h"
#include "Query/All.h"
#include "Session/All.h"
#include "Subscription/All.h"
#include "View/All.h"
#include "Discovery/All.h"

#endif /* ALL_H_ */
