
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_BASESERVICE_H_
#define OPCUA_BASESERVICE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Stacks/OpcUa_Channel.h"
#include "../Stacks/OpcUa_ChannelsTable.h"
#include "../Stacks/OpcUa_Session.h"
#include "../Stacks/OpcUa_SessionsTable.h"

namespace opcua {

class MYDLL BaseService
{

public:

	static BaseDataType * error(IntegerId * requestHandle, int32_t statusCodeNum)
	{
		return error(requestHandle,new StatusCode(statusCodeNum)) ;
	}

	static BaseDataType * error(IntegerId * requestHandle, StatusCode * statusCode)
	{
		debug_i(COM_ERR,"BaseService","Service Request failed : returning ServiceFault 0x%08x.",statusCode->get()) ;

		return
				new ServiceFault(
						new ResponseHeader(
								UtcTime::now(),
								requestHandle,
								statusCode,
								DiagnosticInfo::fakeDiagnosticInfo,
								String::tableZero
						)
				) ;
	}

public:

	static BaseDataType * checkChannelAndSession(
			uint32_t                      secureChannelIdNum,
			SessionAuthenticationToken  * authenticationToken,
			IntegerId                   * requestHandle,
			Channel                    ** pchannel,
			Session                    ** psession
			) ;

	static uint32_t checkChannelAndSession(
			uint32_t                      secureChannelIdNum,
			SessionAuthenticationToken  * authenticationToken,
			Channel                    ** pchannel,
			Session                    ** psession
			) ;

};

} /* namespace opcua */
#endif /* BASEREQUEST_H_ */
