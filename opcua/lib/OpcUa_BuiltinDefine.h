/*
Th * OpcUa_StatusCodeDefine.h
 *
 *  Created on: Aug 10, 2013
 *      Author: bellot
 */

#ifndef OPCUA_BUILTINDEFINE_H_
#define OPCUA_BUILTINDEFINE_H_

namespace opcua {

/* Part 6, Table 1, p. 6 */

#define Builtin_Boolean         ((uint32_t)   1)
#define Builtin_SByte           ((uint32_t)   2)
#define Builtin_Byte            ((uint32_t)   3)
#define Builtin_Int16           ((uint32_t)   4)
#define Builtin_UInt16          ((uint32_t)   5)
#define Builtin_Int32           ((uint32_t)   6)
#define Builtin_UInt32          ((uint32_t)   7)
#define Builtin_Int64           ((uint32_t)   8)
#define Builtin_UInt64          ((uint32_t)   9)
#define Builtin_Float           ((uint32_t)  10)
#define Builtin_Double          ((uint32_t)  11)
#define Builtin_String          ((uint32_t)  12)
#define Builtin_DateTime        ((uint32_t)  13)
#define Builtin_Guid            ((uint32_t)  14)
#define Builtin_ByteString      ((uint32_t)  15)
#define Builtin_XmlElement      ((uint32_t)  16)
#define Builtin_NodeId          ((uint32_t)  17)
#define Builtin_ExpandedNodeId  ((uint32_t)  18)
#define Builtin_StatusCode      ((uint32_t)  19)
#define Builtin_QualifiedName   ((uint32_t)  20)
#define Builtin_LocalizedText   ((uint32_t)  21)
#define Builtin_ExtensionObject ((uint32_t)  22)
#define Builtin_DataValue       ((uint32_t)  23)
#define Builtin_Variant         ((uint32_t)  24)
#define Builtin_DiagnosticInfo  ((uint32_t)  25)

} /* namespace opcua */
#endif /* OPCUA_BUILTINDEFINE_H_ */
