
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_LINKDESCRIPTIONS_H_
#define OPCUA_LINKDESCRIPTIONS_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "OpcUa_LinkDescription.h"

namespace opcua {

class MYDLL LinkDescriptions
	: public LinkedList<LinkDescription>
{
public:

	LinkDescriptions(LinkDescription * _car, LinkDescriptions * _cdr)
		: LinkedList<LinkDescription>(_car,_cdr)
	{}

	virtual ~LinkDescriptions()
	{
		delete getCar() ;
	}

public:

	inline LinkDescriptions  * getCdr()
	{
		return dynamic_cast<LinkDescriptions  *>(LinkedList<LinkDescription>::getCdr()) ;
	}

public:

	LinkDescriptions * implements(class NodesTable * nodesTable, bool withError)
	{
		LinkDescriptions * current  = this ;
		LinkDescriptions * result   = this ;
		LinkDescriptions * previous = NULL ;

		LinkDescription  * car ;
		LinkDescriptions * cdr ;

		do {
			cdr = current->getCdr() ;
			car = current->getCar()->implements(nodesTable,withError) ;

			if (car == NULL) {
				if (previous == NULL) {
					result = cdr ;
				} else {
					previous->setCdr(cdr) ;
				}
				delete current ;
			} else {
				previous = current ;
			}

			current = cdr ;
		} while (current != NULL) ;

		return result ;
	}

};

} /* namespace opcua */
#endif /* LINKDESCRIPTIONS_H_ */
