
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OPCUA_NODESTABLE_H_
#define OPCUA_NODESTABLE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_SyncHashTableHash.h"
#include "../Utils/OpcUa_StringUtils.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "../AddressSpace/All.h"
#include "OpcUa_Binary.h"
#include "OpcUa_NodesTable.h"
#include "OpcUa_LinkDescriptions.h"

extern "C" {
#   include "../libroxml/roxml.h"
#   include "../libroxml/roxml_tune.h"
#   include "../libroxml/roxml_types.h"
}

#include <stdio.h>
#include <errno.h>

#define DEBUG_PUT_GET 0
// 0 : No debug
// 1 : With debug 59

namespace opcua
{

class MYDLL NodesTable
	: public SyncHashTableHash<NodeId,Base>
{
public:

	/*
	 * The current XML namespace set in OpcUa_SelfInit.h
	 */

	static const char * const name_space ;

public:

	NodesTable(uint32_t _length)
		: SyncHashTableHash<NodeId,Base>(_length)
	{}

	virtual ~NodesTable()
	{
		while (removeOne() != 0) ;
	}

public:

	void put(Base * node)
	{
		node->take() ;

#if (DEBUG_PUT_GET == 1)
		NodeId * nodeId = node->getNodeId() ;
		if (nodeId->getIdentifierType()->get() == IdType_STRING_1)
			STOPIF(StringUtils::strcmpi(nodeId->getString()->get(),"InputArgumentsReadCoilxSensorY") == 0) ;
#endif

		if ((node = SyncHashTableHash<NodeId,Base>::replace(node->getNodeId(),node)) != NULL) {
			node->release() ;
		}
	}

	Base * get(QualifiedName * browseName)
	{
		NodeId * nodeId ;

		(nodeId = new NodeId(browseName->getNamespaceIndex(),browseName->getName()))->take() ;

		Base * base = get(nodeId) ;

		nodeId->release() ;

		return base ;
	}

	inline Base * get(NodeId * nodeId)
	{
#if (DEBUG_PUT_GET == 1)
		if (nodeId->getIdentifierType()->get() == IdType_STRING_1)
			STOPIF(StringUtils::strcmpi(nodeId->getString()->get(),"InputArgumentsReadCoilxSensorY") == 0) ;
#endif
		return SyncHashTableHash<NodeId,Base>::get(nodeId) ;
	}

public:

	char * get_string_element(
			const char * const why,
			const char * const file_str,
			node_t     * node)
	{
		int nb = roxml_get_chld_nb(node) ;

		if (nb != 0) {
			debug_ss(COM_ERR,"NodesTable","get string for  %s in file \"%s\", Error : element with sub-elements (%d)",why,file_str) ;
			debug_i (COM_ERR,"NodesTable","sub-elements count = %d",nb) ;
			NodesTable::print(file_str,node,1000) ;
			return NULL ;
		}

		int    len ;
		char * content = roxml_get_content(node,NULL,0,&len) ;

		if (len == 0) {
			debug_ss(COM_ERR,"NodesTable","get string for %s in file \"%s\", Error : element with zero-length string",why,file_str) ;
			NodesTable::print(file_str,node,1000) ;
			return NULL ;
		}

		debug_ss(XML_DBG,"NodesTable","get string for %s in file \"%s\"",why,file_str) ;
		debug_s (XML_DBG,"NodesTable","string is \"%s\"",content) ;

		char * result = new char[len + 1] ;
		strcpy(result,content) ;

		roxml_release(content) ;

		return result ;
	}

private:

	static void print_link_error(
			const char        * link_name,
			NodeId            * nodeId,
			const char        * const file_str,
			node_t            * node,
			const char        * err)
	{
		String * nodeIdStr = nodeId->toString() ;
		debug_ssss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : %s for link %s of \"%s\"",file_str,err,link_name,nodeIdStr->get()) ;
		NodesTable::print(file_str,node,1000) ;
		nodeIdStr->checkRefCount() ;
	}

public:

	static bool get_link_element(
			const char        * link_name,
			NodeId            * sourceNodeId,
			const char        * const file_str,
			node_t            * node,
			LinkDescriptions ** pLinkDescriptions)
	{
		String * _browseName     = NULL ;
		String * _nodeId         = NULL ;
		UInt16 * _namespaceIndex = NULL ;
		char   * _nodeClass      = NULL ;
		bool     _retro          = false ;

		int nba = roxml_get_attr_nb(node) ;

		for (int j = 0 ; j < nba ; j++) {
			node_t * attr = roxml_get_attr(node,NULL,j) ;
			char   * anam = roxml_get_name(attr,NULL,0) ;

			if (anam != NULL) {
				if (StringUtils::strcmpi(anam,"Retro") == 0) {
					char * content = roxml_get_content(attr,NULL,0,NULL) ;
					if (content != NULL) {
						if (StringUtils::strcmpi(content,"True") == 0) {
							_retro = true ;
						} else {
							_retro = false ;
						}
					} else {
						_retro = false ;
					}
					roxml_release(content) ;
				} else
				if (StringUtils::strcmpi(anam,"NodeClass") == 0) {
					if (_nodeClass != NULL) {
						print_link_error(link_name,sourceNodeId,file_str,node,"two NodeClass") ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL) {
							_nodeClass = new char[strlen(content) + 1] ;
							strcpy(_nodeClass,content) ;
						}
						roxml_release(content) ;
					}
				} else
				if (StringUtils::strcmpi(anam,"BrowseName") == 0) {
					if (_browseName != NULL) {
						print_link_error(link_name,sourceNodeId,file_str,node,"two BrowseName") ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL)
							(_browseName = new String(content))->take() ;
						roxml_release(content) ;
					}
				} else
				if (StringUtils::strcmpi(anam,"NodeId") == 0) {
					if (_nodeId != NULL) {
						print_link_error(link_name,sourceNodeId,file_str,node,"two NodeId") ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL)
							(_nodeId = new String(content))->take() ;
						roxml_release(content) ;
					}
				} else
				if (StringUtils::strcmpi(anam,"NamespaceIndex") == 0) {
					if (_namespaceIndex != NULL) {
						print_link_error(link_name,sourceNodeId,file_str,node,"two NamespaceIndex") ;
					} else {
						char * content = roxml_get_content(attr,NULL,0,NULL) ;
						if (content != NULL) {
							_namespaceIndex = UInt16::get_UInt16(file_str,content) ;
							if (_namespaceIndex != NULL)
								_namespaceIndex->take() ;
							roxml_release(content) ;
						}
						roxml_release(content) ;
					}
				} else {
					print_link_error(link_name,sourceNodeId,file_str,node,"unknown attribute") ;
				}
				roxml_release(anam) ;
			} else {
				print_link_error(link_name,sourceNodeId,file_str,node,"unknown attribute") ;
			}
		}

		NodeId * target = NULL ;

		if (_nodeClass == NULL) {
			print_link_error(link_name,sourceNodeId,file_str,node,"no NodeClass") ;
			goto error ;
		}

		if (_browseName == NULL && _nodeId == NULL) {
			print_link_error(link_name,sourceNodeId,file_str,node,"no BrowseName") ;
			goto error ;
		}

		if (_namespaceIndex == NULL)
			(_namespaceIndex = UInt16::zero)->take() ;

		if (_nodeId == NULL) {
			target = new NodeId(_namespaceIndex,_browseName) ;
		} else {
			const char * __nodeId = _nodeId->get() ;
			if (StringUtils::isNumeric(__nodeId)) {
				errno = 0 ;
				int64_t value = strtoll(__nodeId,(char **)NULL,0) ;
				if (errno == 0 && static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
					target = new NodeId(_namespaceIndex,new UInt32(static_cast<uint32_t>(value))) ;
				} else {
					target = new NodeId(_namespaceIndex,_nodeId) ;
				}
			} else {
				target = new NodeId(_namespaceIndex,_nodeId) ;
			}
		}
		target->take() ;

		_namespaceIndex->release() ;
		if (_browseName != NULL)
			_browseName->release() ;
		if (_nodeId != NULL)
			_nodeId->release() ;

		LinkDescription * linkDescription ;

		if (_retro) {
			linkDescription = new LinkDescription(link_name,_nodeClass,target,sourceNodeId) ;
		} else {
			linkDescription = new LinkDescription(link_name,_nodeClass,sourceNodeId,target) ;
		}

		target->release() ;

		*pLinkDescriptions = new LinkDescriptions(linkDescription, *pLinkDescriptions) ;

		delete [] _nodeClass ;

		return true ;

	error:

		if (_namespaceIndex != NULL)
			_namespaceIndex->release() ;
		if (_browseName != NULL)
			_browseName->release() ;
		if (_nodeId != NULL)
			_nodeId->release() ;

		return false ;
	}

private:

	void explore_BinaryFile_element(const char * const file_str, node_t * node)
	{
		char * file_name = NULL ;

		int nba = roxml_get_attr_nb(node) ;

		for (int j = 0 ; j < nba ; j++) {
			node_t * attr = roxml_get_attr(node,NULL,j) ;
			char   * anam = roxml_get_name(attr,NULL,0) ;
			if (StringUtils::strcmpi(anam,"Value") == 0) {
				char * content = roxml_get_content(attr,NULL,0,NULL) ;
				if (content != NULL) {
					file_name = new char[strlen(content) + 1] ;
					strcpy(file_name,content) ;
				}
			}
			roxml_release(anam) ;
		}

		if (file_name == NULL) {
			debug_si(COM_ERR,"NodesTable","Configuration binary file \"%s\", Error : BinaryFile element with no Value attribute (%d)",file_str,nba) ;
			NodesTable::print(file_str,node,1000) ;
			return ;
		}

		if (binary_load(file_str,file_name) != 0) {
			debug(COM_ERR,"NodesTable","Cannot load configuration binary file") ;
			EXIT(-1) ;
		}

		delete [] file_name ;
	}

private:

	void explore_File_element(const char * const file_str, node_t * node, LinkDescriptions ** pLinkDescriptions)
	{
		char * file_name = NULL ;

		int nba = roxml_get_attr_nb(node) ;

		for (int j = 0 ; j < nba ; j++) {
			node_t * attr = roxml_get_attr(node,NULL,j) ;
			char   * anam = roxml_get_name(attr,NULL,0) ;
			if (StringUtils::strcmpi(anam,"Value") == 0) {
				char * content = roxml_get_content(attr,NULL,0,NULL) ;
				if (content != NULL) {
					file_name = new char[strlen(content) + 1] ;
					strcpy(file_name,content) ;
				}
				roxml_release(content) ;
			}
			roxml_release(anam) ;
		}

		if (file_name == NULL) {
			debug_si(COM_ERR,"NodesTable","Configuration file \"%s\", Error : File element with no Value attribute (%d)",file_str,nba) ;
			NodesTable::print(file_str,node,1000) ;
			return ;
		}

		load(file_str,file_name,pLinkDescriptions) ;

		delete [] file_name ;
	}

public:

	static bool bad_ns(node_t * node,const char * const file_str)
	{
		node_t * ns  = roxml_get_ns(node) ;

		if (ns == NULL) {
			debug_s(COM_ERR,"NodesTable","No namespace (case 1)  in \"%s\"",file_str) ;
			NodesTable::print(file_str,node,1000) ;
			return false ;
		}

		char * web = roxml_get_content(ns,NULL,0,NULL) ;

		if (web == NULL) {
			debug_ss(COM_ERR,"NodesTable","No namespace (case 2)  in \"%s\", web is NULL != \"%s\"",file_str,name_space) ;
			NodesTable::print(file_str,node,1000) ;
			return false ;
		}

		bool res = (strcmp(web,name_space) != 0) ;

		if (res) {
			debug_sss(COM_ERR,"NodesTable","Bad namespace (case 3)  in \"%s\", web is \"%s\" != \"%s\"",file_str,web,name_space) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(web) ;

		return false ;
	}

private:

	void explore_nodes(const char * const file_str, node_t * nodes, LinkDescriptions ** pLinkDescriptions)
	{
		int nb = roxml_get_chld_nb(nodes) ;

		for (int i = 0 ; i < nb ; i++) {

			node_t * node = roxml_get_chld(nodes,NULL,i) ;

			if (bad_ns(node,file_str))
				continue ;

			char * name = roxml_get_name(node,NULL,0) ;

			if (StringUtils::strcmpi(name,"File") == 0) {

				explore_File_element(file_str,node,pLinkDescriptions) ;

			} else if (StringUtils::strcmpi(name,"BinaryFile") == 0) {

				explore_BinaryFile_element(file_str,node) ;

			} else if (StringUtils::strcmpi(name,"PartialLinks") == 0) {

				if (*pLinkDescriptions != NULL) {
					debug_i(XML_DBG,"NodesTable","Begin partial links operation (%d)",LinkDescriptions::getLength(*pLinkDescriptions)) ;
					*pLinkDescriptions = (*pLinkDescriptions)->implements(this,false) ;
					debug_i(XML_DBG,"NodesTable","After partial links operation (%d)",LinkDescriptions::getLength(*pLinkDescriptions)) ;
				}

			} else {

				String * _nodeId         = NULL ;
				String * _browseName     = NULL ;
				UInt16 * _namespaceIndex = NULL ;

				int nba = roxml_get_attr_nb(node) ;

				for (int j = 0 ; j < nba ; j++) {
					node_t * attr = roxml_get_attr(node,NULL,j) ;
					char   * anam = roxml_get_name(attr,NULL,0) ;

					if (StringUtils::strcmpi(anam,"NodeId") == 0) {
						if (_nodeId != NULL) {
							debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : two NodeId(s) for \"%s\"",file_str,name) ;
							NodesTable::print(file_str,node,1000) ;
						} else {
							char * content = roxml_get_content(attr,NULL,0,NULL) ;
#if (DEBUG_PUT_GET == 1)
							STOPIF(StringUtils::strcmpi(content,"InputArgumentsReadCoilxSensorY") == 0) ;
#endif
							if (content != NULL) {
								_nodeId = new String(content) ;
							}
							roxml_release(content) ;
						}
					} else
					if (StringUtils::strcmpi(anam,"BrowseName") == 0) {
						if (_browseName != NULL) {
							debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : two BrowseName(s) for \"%s\"",file_str,name) ;
							NodesTable::print(file_str,node,1000) ;
						} else {
							char * content = roxml_get_content(attr,NULL,0,NULL) ;
							if (content != NULL) {
								_browseName = new String(content) ;
							}
							roxml_release(content) ;
						}
					} else
					if (StringUtils::strcmpi(anam,"NamespaceIndex") == 0) {
						if (_namespaceIndex != NULL) {
							debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : two NamespaceIndex(es) for \"%s\"",file_str,name) ;
							NodesTable::print(file_str,node,1000) ;
						} else {
							char * content = roxml_get_content(attr,NULL,0,NULL) ;
							if (content != NULL) {
								_namespaceIndex = UInt16::get_UInt16(file_str,content) ;
							}
							roxml_release(content) ;
						}
					}

					roxml_release(anam) ;
				}

				if (_browseName == NULL && _nodeId == NULL) {

					debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : no BrowseName nor NodeId for \"%s\"",file_str,name) ;
					NodesTable::print(file_str,node,2) ;

				} else {

					if (_namespaceIndex == NULL)
						_namespaceIndex = UInt16::zero ;

					QualifiedName * browseName = new QualifiedName(_namespaceIndex,_browseName) ;
					browseName->take() ;

					NodeId * nodeId = NULL ;
					if (_nodeId != NULL) {
						const char * __nodeId = _nodeId->get() ;
						if (StringUtils::isNumeric(__nodeId)) {
							errno = 0 ;
							int64_t value = strtoll(__nodeId,(char **)NULL,0) ;
							if (errno == 0 && static_cast<int64_t>(0) <= value && value <= static_cast<int64_t>(UINT32_MAX)) {
								nodeId = new NodeId(_namespaceIndex,new UInt32(static_cast<uint32_t>(value))) ;
							} else {
								nodeId = new NodeId(_namespaceIndex,_nodeId) ;
							}
						} else {
							nodeId = new NodeId(_namespaceIndex,_nodeId) ;
						}
					} else {
						nodeId = new NodeId(_namespaceIndex,_browseName);
					}
					nodeId->take() ;

					Base * result ;

					if (StringUtils::strcmpi(name,"Reference") == 0) { // Just an XML tag
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", Reference element \"%s\"",file_str,_browseName->get()) ;
						Reference::explore_element(browseName,file_str,node,pLinkDescriptions) ;
						result = NULL ;
					} else
					if (StringUtils::strcmpi(name,"Variable") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", Variable element \"%s\"",file_str,_browseName->get()) ;
#if (DEBUG_PUT_GET == 1)
						if (nodeId != NULL) {
							if (nodeId->getIdentifierType()->get() == IdType_STRING_1)
								STOPIF(StringUtils::strcmpi(nodeId->getString()->get(),"InputArgumentsReadCoilxSensorY") == 0) ;
						}
#endif
						result = Variable::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
#if (VIEW_VARIABLES == 1)
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","Variable","Variables",nodeId),*pLinkDescriptions) ;
#endif
					} else
					if (StringUtils::strcmpi(name,"DataType") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", DataType element \"%s\"",file_str,_browseName->get()) ;
						result = DataType::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","DataType","90",nodeId),*pLinkDescriptions) ;
					} else
					if (StringUtils::strcmpi(name,"VariableType") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", VariableType element \"%s\"",file_str,_browseName->get()) ;
						result = VariableType::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","VariableType","89",nodeId),*pLinkDescriptions) ;
					} else
					if (StringUtils::strcmpi(name,"ObjectType") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", ObjectType element \"%s\"",file_str,_browseName->get()) ;
						result = ObjectType::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","ObjectType","88",nodeId),*pLinkDescriptions) ;
					} else
					if (StringUtils::strcmpi(name,"ReferenceType") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", ReferenceType element \"%s\"",file_str,_browseName->get()) ;
						result = ReferenceType::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","ReferenceType","91",nodeId),*pLinkDescriptions) ;
					} else
					if (StringUtils::strcmpi(name,"Method") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", Method element \"%s\"",file_str,_browseName->get()) ;
						result = Method::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
#if (VIEW_METHODS == 1)
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","Method","Methods",nodeId),*pLinkDescriptions) ;
#endif
					} else
					if (StringUtils::strcmpi(name,"Object") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", Object element \"%s\"",file_str,_browseName->get()) ;
						result = Object::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","Object","85",nodeId),*pLinkDescriptions) ;
					} else
					if (StringUtils::strcmpi(name,"View") == 0) {
						debug_ss(XML_DBG,"NodesTable","Configuration file \"%s\", View element \"%s\"",file_str,_browseName->get()) ;
						result = View::explore_element(browseName,nodeId,file_str,node,pLinkDescriptions) ;
						if (result != NULL)
							*pLinkDescriptions = new LinkDescriptions(new LinkDescription("Organizes","View","87",nodeId),*pLinkDescriptions) ;
					} else {
						debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\", Error : unknown node \"%s\"",file_str,name) ;
						NodesTable::print(file_str,node,1000) ;
						result = NULL ;
					}


#if (DEBUG_PUT_GET == 1)
					if (result != NULL) {
						NodeId * nodeId1 = result->getNodeId() ;
						if (nodeId1->getIdentifierType()->get() == IdType_STRING_1)
							STOPIF(StringUtils::strcmpi(nodeId1->getString()->get(),"InputArgumentsReadCoilxSensorY") == 0) ;
					}
#endif

					if (result != NULL)
						put(result) ;

					browseName->release() ;
					nodeId->release() ;
				}
			}

			roxml_release(name) ;
		}
	}

private:

	uint32_t binary_load(const char * const file_str)
	{
		debug_s(BIN_DBG,"NodesTable","Configuration binary file \"%s\"",file_str) ;

#ifdef _WIN32
		int fd = open(file_str,O_RDONLY | O_BINARY) ;
#else
		int fd = open(file_str,O_RDONLY) ;
#endif

		if (fd == -1) {
			debug_i(COM_ERR,"NodesTable","Cannot open binary file, errno=%d",errno) ;
			return 1 ;
		}

		uint32_t error = 0 ;

		if (binary_load(fd) != 0) {
			debug(COM_ERR,"NodesTable","Cannot load binary file") ;
			error = 1 ;
		}

		if (close(fd) == -1) {
			debug_i(COM_ERR,"NodesTable","Cannot close binary file, errno=%d",errno) ;
			error = 1 ;
		} else {
			debug_s(BIN_DBG,"NodesTable","Closed configuration binary file \"%s\"",file_str) ;
		}

		return error ;
	}

private:

	uint32_t binary_load(
			const char        * const old_file_str,
			const char        * const new_file_str)
	{
		char * file_str = fileName(old_file_str,new_file_str) ;
		uint32_t result = binary_load(file_str) ;
		delete[] file_str ;
		return result ;
	}

private:

	void load(const char * const file_str, LinkDescriptions ** pLinkDescriptions)
	{
		node_t * root = NULL ;
		node_t * xml ;
		char   * name = NULL ;
		int      nb ;
		int      nba ;
		node_t * nsdef ;
		char   * web = NULL ;

		debug_s(XML_DBG,"NodesTable","Configuration file \"%s\"",file_str) ;

		root = roxml_load_doc((char *)file_str) ;

		if (root == NULL) {
			debug_s(COM_ERR,"NodesTable","Configuration file \"%s\" does not exist",file_str) ;
			goto err ;
		}

		nb = roxml_get_chld_nb(root) ;

		if (nb != 1) {
			debug_si(COM_ERR,"NodesTable","Configuration file \"%s\" has %d root element",file_str,nb) ;
			NodesTable::print(file_str,root,2) ;
			goto err ;
		}

		xml  = roxml_get_chld(root,NULL,0) ;
		name = roxml_get_name(xml,NULL,0) ;

		if (StringUtils::strcmpi(name,"OpcRosa-Configuration") != 0) {
			debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\" has no OpcRosa-Configuration root element (%s)",file_str,name) ;
			NodesTable::print(file_str,root,2) ;
			goto err ;
		}

		nba = roxml_get_attr_nb(xml) ;

		if (nba != 1) {
			debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\" root element has single namespace element (%s)",file_str,name) ;
			NodesTable::print(file_str,root,2) ;
			goto err ;
		}

		nsdef = roxml_get_attr(xml,NULL,0) ;

		web = roxml_get_content(nsdef,NULL,0,NULL) ;

		if (strcmp(web,name_space) != 0) {
			debug_ss(COM_ERR,"NodesTable","Configuration file \"%s\" root element has bad namespace element (%s)",file_str,name) ;
			debug_s (COM_ERR,"NodesTable","Must be %s",name_space) ;
			NodesTable::print(file_str,root,2) ;
			goto err ;
		}

		explore_nodes(file_str,xml,pLinkDescriptions) ;

	err:

		if (name != NULL)
			roxml_release(name) ;

		if (web != NULL)
			roxml_release(web) ;

		roxml_close(root) ;
}

private:

	void load(
			const char        * const old_file_str,
			const char        * const new_file_str,
			LinkDescriptions ** pLinkDescriptions)
	{
		char * file_str = fileName(old_file_str,new_file_str) ;
		load(file_str,pLinkDescriptions) ;
		delete[] file_str ;
	}

public:

	void load(const char * const file_str)
	{
		debug_s(XML_DBG,"NodesTable","file_str \"%s\"",file_str) ;

		LinkDescriptions * linkDescriptions = NULL ;

		load(file_str,&linkDescriptions) ;

		if (linkDescriptions != NULL) {
			debug_i(XML_DBG,"NodesTable","Begin final links operation (%d)",LinkDescriptions::getLength(linkDescriptions)) ;
			linkDescriptions = linkDescriptions->implements(this,true) ;
			debug_i(XML_DBG,"NodesTable","After final links operation (%d)",LinkDescriptions::getLength(linkDescriptions)) ;
		}

		if (linkDescriptions != NULL) {
			debug_s(COM_ERR,"NodesTable","file_str \"%s\" : cannot implements all links",file_str) ;
			linkDescriptions->deleteAll() ;
		}
	}

public:

	static char * fileName(
			const char * const old_file_str,
			const char * const new_file_str
			)
	{
		debug_ss(XML_DBG,"NodesTable","old_file_str \"%s\", new_file_str \"%s\"",old_file_str,new_file_str) ;

		char * file_str = new char[strlen(old_file_str) + strlen(new_file_str) + 256] ;

		strcpy(file_str,old_file_str) ;

		char * end_ptr = file_str + strlen(file_str) ;

#ifdef _WIN32
		while (*end_ptr != '/' && *end_ptr != '\\' && end_ptr != file_str)
			end_ptr-- ;
#else
		while (*end_ptr != FILE_SEPARATOR && end_ptr != file_str)
			end_ptr-- ;
#endif

#ifdef _WIN32
		if (*end_ptr == '/' || *end_ptr == '\\')
			end_ptr++ ;
#else
		if (*end_ptr == FILE_SEPARATOR)
			end_ptr++ ;
#endif

		strcpy(end_ptr,new_file_str) ;

		debug_s(XML_DBG,"NodesTable","get file_str  \"%s\"",file_str) ;

		return file_str ;
	}

private: // for debugging only

	static int print_indent(char * my_array, int my_offset, int indent)
	{
		for (int i = 0 ; i < indent ; i++)
			my_array[my_offset++] = ' ' ;
		my_array[my_offset] = '\0' ;
		return my_offset ;
	}

public: // for debugging only

	static void print(const char * file_str, node_t * node, int rank)
	{
		unsigned long lbeg = 0 ;
		unsigned long lend = 0 ;

		which_lines(file_str,node->pos,node->end,&lbeg,&lend) ;
		{
			char * my_array = new char[strlen(file_str)+512] ;
			sprintf(my_array,"   In file %s,from line %lu to line %lu (approximatively):\n",file_str,lbeg,lend) ;
			debug(COM_ERR,"NodesTable",my_array) ;
			delete[] my_array ;
		}
		print_xml(node,rank,3) ;
	}

private:

	static void which_lines(
			const char    * file_str,
			unsigned long   beg,
			unsigned long   end,
			unsigned long * lbeg,
			unsigned long * lend)
	{
		FILE * file = fopen(file_str,"r") ;

		if (file == NULL) {
			debug_com_s(COM_ERR,"NodesTable","Cannot open \"%s\"",file_str) ;
			return ;
		}

		unsigned long curLine = 0 ;
		unsigned long curChar = 0 ;

		while (true) {

			int c = fgetc(file) ;

			if (c == EOF)
				break ;

			if (beg == curChar)
				*lbeg = curLine ;

			if (end == curChar)
				*lend = curLine ;

			curChar++ ;

			if ((char)c == '\n')
				curLine++ ;
		}

		fclose(file) ;
	}

public:  // for debugging only

	static void print(node_t * node, int rank, int indent)
	{
		print_xml(node,rank,indent) ;
	}

private:  // for debugging only

	static void print_xml(node_t * node, int rank, int indent)
	{
		char     my_array[10*1024] ;
		int      my_offset = 0 ;

		char   * name    = roxml_get_name(node,NULL,0) ;
		node_t * node_ns = roxml_get_ns(node) ;

		char   * ns = NULL ;
		if (node_ns != NULL)
			ns = roxml_get_name(node_ns,NULL,0) ;

		my_offset = print_indent(my_array,my_offset,indent) ;

		if (ns != NULL) {
			sprintf(my_array+my_offset,"<%s:%s",ns,name) ;
		} else {
			sprintf(my_array+my_offset,"<%s",name) ;
		}

		my_offset = strlen(my_array) ;

		int nb_attr = roxml_get_attr_nb(node) ;

		for (int i = 0 ; i < nb_attr ; i++) {

			debug(COM_ERR,"NodesTable",my_array) ;
			my_offset = 0 ;

			node_t * attr      = roxml_get_attr(node,NULL,i) ;

			char   * attr_name = roxml_get_name(attr,NULL,0) ;
			node_t * attr_ns   = roxml_get_ns(attr) ;

			char   * ans = NULL ;
			if (attr_ns != NULL)
				ans = roxml_get_name(attr_ns,NULL,0) ;

			my_offset = print_indent(my_array,my_offset,indent+6) ;

			if (ans != NULL) {
				sprintf(my_array+my_offset," %s:%s = ",ans,attr_name) ;
				roxml_release(ans) ;
			} else {
				sprintf(my_array+my_offset,"%s = ",attr_name) ;
			}

			my_offset = strlen(my_array) ;

			char * attr_value = roxml_get_content(attr,NULL,0,NULL) ;

			sprintf(my_array+my_offset,"\"%s\"",attr_value) ;
			my_offset = strlen(my_array) ;

			roxml_release(attr_value) ;
			roxml_release(attr_name) ;
		}

		int nb_chld = roxml_get_chld_nb(node) ;

		if (nb_chld == 0) {

			sprintf(my_array+my_offset,"/>") ;

			debug(COM_ERR,"NodesTable",my_array) ;
			my_offset = 0 ;


		} else {

			sprintf(my_array+my_offset,"/>") ;

			debug(COM_ERR,"NodesTable",my_array) ;
			my_offset = 0 ;

			if (rank > 0) {

				for (int i = 0 ; i < nb_chld ; i++) {
					node_t * chld = roxml_get_chld(node,NULL,i) ;
					print_xml(chld,rank-1,indent+3) ;
				}

			} else {

				my_offset = print_indent(my_array,my_offset,indent+3) ;

				sprintf(my_array+my_offset,"...") ;

				debug(COM_ERR,"NodesTable",my_array) ;
				my_offset = 0 ;

			}

			my_offset = print_indent(my_array,my_offset,indent) ;

			if (ns !=NULL) {
				sprintf(my_array+my_offset,"</%s:%s>",ns,name) ;
			} else {
				sprintf(my_array+my_offset,"</%s>",name) ;
			}

			debug(COM_ERR,"NodesTable",my_array) ;
			my_offset = 0 ;
		}

		roxml_release(ns) ;
		roxml_release(name) ;
	}

	// BINARY LOAD

private: // OK

	inline uint32_t loadObject(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","Beginning loadObject, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		uint32_t parentNodeId  ; // Part 6, F6
		uint8_t  eventNotifier ;

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		if (Binary::myread(fd,&parentNodeId) != 0) {
			debug(COM_ERR,"NodesTable","loadObject: cannot load ParentNodeId") ;
			return (uint32_t)3 ;
		}

		if (Binary::myread(fd,&eventNotifier) != 0) {
			debug(COM_ERR,"NodesTable","loadObject: cannot load EventNotifier") ;
			return (uint32_t)4 ;
		}

		Base * base =
				new Object(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						new Byte(eventNotifier)
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		debug_i(BIN_DBG_DETAIL,"NodesTable","Ending loadObject, rank=%i",i) ;
		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadVariable(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","loadVariable, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		uint32_t        parentNodeId ;            // Part6, F5
		BaseDataType  * value ;                   // M
		uint32_t        dataType ;                // M NodeId
		int32_t         valueRank ;               // M
		TableUInt32   * arrayDimensions ;         // O

		uint8_t         accessLevel     = 3;      // M
		uint8_t         userAccessLevel = 3;      // M

		uint32_t        minimumSamplingInterval ; // M

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		uint8_t exists ;

		if (Binary::myread(fd,&parentNodeId) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load ParentNodeId") ;
			return (uint32_t)3 ;
		}

		if (Binary::myread(fd,&value) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load Value") ;
			return (uint32_t)4 ;
		}

		if (Binary::myread(fd,&dataType) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load DataType") ;
			return (uint32_t)6 ;
		}

		if (Binary::myread(fd,&valueRank) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load ValueRank") ;
			return (uint32_t)7 ;
		}

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load ArrayDimensions existence") ;
			return (uint32_t)8 ;
		}

		if (exists == 0) {
			arrayDimensions = NULL ;
		} else {
			if (Binary::myread(fd,&arrayDimensions) != 0) {
				debug(COM_ERR,"NodesTable","loadVariable: cannot load ArrayDimensions") ;
				return (uint32_t)9 ;
			}
		}

		if (Binary::myread(fd,&accessLevel) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load AccessLevel existence") ;
			return (uint32_t)10 ;
		}

		if (Binary::myread(fd,&minimumSamplingInterval) != 0) {
			debug(COM_ERR,"NodesTable","loadVariable: cannot load MinimumSamplingInterval existence") ;
			return (uint32_t)11 ;
		}

		Base * base =
				new Variable(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						(parentNodeId == 0xffffffff) ? NULL : new NodeId(parentNodeId),
						value,
						(dataType == 0xffffffff)?new NodeId(1):new NodeId(dataType),
						new Int32(valueRank),
						arrayDimensions,
						new Byte(accessLevel),
						new Byte(userAccessLevel),
						(minimumSamplingInterval == 0xffffffff) ? NULL : new Duration(minimumSamplingInterval),
						Boolean::booleanFalse // historizing
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadMethod(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","loadMethod, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		uint32_t parentNodeId        ;
		uint32_t methodDeclarationId ;

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		if (Binary::myread(fd,&parentNodeId) != 0) {
			debug(COM_ERR,"NodesTable","loadMethod: cannot load ParentNodeId existence") ;
			return (uint32_t)3 ;
		}

		if (Binary::myread(fd,&methodDeclarationId) != 0) {
			debug(COM_ERR,"NodesTable","loadMethod: cannot load MethodDeclarationId existence") ;
			return (uint32_t)4 ;
		}

		Base * base =
				new Method(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						Boolean::booleanFalse, // executable
						Boolean::booleanFalse, // userExecutable
						(methodDeclarationId == 0xffffffff) ? NULL : new NodeId(methodDeclarationId),
						NULL
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadObjectType(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","Beginning loadObjectType, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		bool isAbstract ; // M

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		if (Binary::myread(fd,&isAbstract) != 0) {
			debug(COM_ERR,"NodesTable","loadObjectType: cannot load IsAbstract") ;
			return (uint32_t)3 ;
		}

		Base * base =
				new ObjectType(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						new Boolean(isAbstract)
						) ;

		put(base) ;


		deleteCommon(browseName,symbolicName) ;

		debug_i(BIN_DBG_DETAIL,"NodesTable","Ending loadObjectType, rank=%i",i) ;
		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadVariableType(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","loadVariableType, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		BaseDataType  * value ;                   // O
		uint32_t        dataType ;                // M NodeId
		int32_t         valueRank ;               // M
		TableUInt32   * arrayDimensions ;         // O
		bool            isAbstract ;              // M

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		uint8_t exists ;

		if (Binary::myread(fd,&value) != 0) {
			debug(COM_ERR,"NodesTable","loadVariableType: cannot load Value") ;
			return (uint32_t)3 ;
		}

		if (Binary::myread(fd,&dataType) != 0) {
			debug(COM_ERR,"NodesTable","loadVariableType: cannot load DataType") ;
			return (uint32_t)6 ;
		}

		if (Binary::myread(fd,&valueRank) != 0) {
			debug(COM_ERR,"NodesTable","loadVariableType: cannot load ValueRank") ;
			return (uint32_t)7 ;
		}

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"NodesTable","loadVariableType: cannot load ArrayDimensions existence") ;
			return (uint32_t)8 ;
		}

		if (exists == 0) {
			arrayDimensions = NULL ;
		} else {
			if (Binary::myread(fd,&arrayDimensions) != 0) {
				debug(COM_ERR,"NodesTable","loadVariableType: cannot load ArrayDimensions") ;
				return (uint32_t)9 ;
			}
		}

		if (Binary::myread(fd,&isAbstract) != 0) {
			debug(COM_ERR,"NodesTable","loadVariableType: cannot load IsAbstract existence") ;
			return (uint32_t)10 ;
		}

		Base * base =
				new VariableType(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						value,
						(dataType == 0xffffffff)?NULL:new NodeId(dataType),
						new Int32(valueRank),
						arrayDimensions,
						new Boolean(isAbstract)
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadReferenceType(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","loadReferenceType, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		bool            isAbstract  ; // M
		bool            symmetric   ; // M
		LocalizedText * inverseName ; // O

		uint8_t exists ;

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		if (Binary::myread(fd,&isAbstract) != 0) {
			debug(COM_ERR,"NodesTable","loadReferenceType: cannot load IsAbstract") ;
			return (uint32_t)3 ;
		}

		if (Binary::myread(fd,&symmetric) != 0) {
			debug(COM_ERR,"NodesTable","loadReferenceType: cannot load Symmetric") ;
			return (uint32_t)4 ;
		}

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"NodesTable","loadReferenceType: cannot load InverseName existence") ;
			return (uint32_t)5 ;
		}

		if (exists == 0) {
			inverseName = NULL ;
		} else {
			if (Binary::myread(fd,&inverseName) != 0) {
				debug(COM_ERR,"NodesTable","loadReferenceType: cannot load InverseName") ;
				return (uint32_t)6 ;
			}
		}

		Base * base =
				new ReferenceType(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						new Boolean(isAbstract),
						new Boolean(exists),
						inverseName
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadDataType(int fd,int i)
	{
		debug_i(BIN_DBG_DETAIL,"NodesTable","loadDataType, rank=%i",i) ;

		uint32_t        nodeId       ;  // M
		char          * browseName   ;  // M
		char          * symbolicName ;  // O
		LocalizedText * displayName  ;  // M
		LocalizedText * description  ;  // O

		bool isAbstract ; // M

		if (loadCommon(fd,&nodeId,&browseName,&symbolicName,&displayName,&description) != 0)
			return (uint32_t)2 ;

		if (Binary::myread(fd,&isAbstract) != 0) {
			debug(COM_ERR,"NodesTable","loadDataType: cannot load IsAbstract") ;
			return (uint32_t)3 ;
		}

		Base * base =
				new DataType(
						new NodeId(nodeId),
						new QualifiedName(UInt16::zero,new String(browseName)),
						(symbolicName == NULL) ? NULL : new String(symbolicName),
						displayName,
						description,
						NULL, // writeMask,
						NULL, // userWriteMask

						new Boolean(isAbstract)
						) ;

		put(base) ;

		deleteCommon(browseName,symbolicName) ;

		return (uint32_t)0;
	}

private: // OK

	inline uint32_t loadView(int fd,int i)
	{
		NOT_USED(fd) ;
		debug_i(COM_ERR,"NodesTable","loadView: cannot load base number %d",i) ;
		return (uint32_t)1;
	}

private: // OK

	void deleteCommon(
			char          * browseName,
			char          * symbolicName)
	{
		if (browseName != NULL)
			delete[] browseName ;
		if (symbolicName != NULL)
			delete[] symbolicName ;
	}

private: // OK

	uint32_t loadCommon(
			int              fd,
			uint32_t       * pNodeId,
			char          ** pBrowseName,
			char          ** pSymbolicName,
			LocalizedText ** pDisplayName,
			LocalizedText ** pDescription)
	{
		if (Binary::myread(fd,pNodeId) != 0) {
			debug(COM_ERR,"NodesTable","loadCommon: cannot load NodeId") ;
			return (uint32_t)1 ;
		}

		debug_i(BIN_DBG_DETAIL,"NodesTable","loadCommon: nodeId = %d",*pNodeId) ;

		if (Binary::myread(fd,pBrowseName) != 0) {
			debug(COM_ERR,"NodesTable","loadCommon: cannot load BrowseName") ;
			return (uint32_t)2 ;
		}

		uint8_t exists ;

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"NodesTable","loadCommon: cannot load SymbolicName existence") ;
			return (uint32_t)3 ;
		}

		if (exists == 0) {
			* pSymbolicName = NULL ;
		} else {
			if (Binary::myread(fd,pSymbolicName) != 0) {
				debug(COM_ERR,"NodesTable","loadCommon: cannot load SymbolicName") ;
				return (uint32_t)4 ;
			}
		}

		if (Binary::myread(fd,pDisplayName) != 0) {
			debug(COM_ERR,"NodesTable","loadCommon: cannot load Description") ;
			return (uint32_t)5 ;
		}

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"NodesTable","loadCommon: cannot load Description existence") ;
			return (uint32_t)6 ;
		}

		if (exists == 0) {
			* pDescription = NULL ;
		} else {
			if (Binary::myread(fd,pDescription) != 0) {
				debug(COM_ERR,"NodesTable","loadCommon: cannot load Description") ;
				return (uint32_t)7 ;
			}
		}

		return (uint32_t)0 ;
	}


private: // OK

	uint32_t loadBase(int fd,int i)
	{

		uint8_t nodeClass ;

		if (Binary::myread(fd,&nodeClass) != 0) {
			debug_i(COM_ERR,"NodesTable","loadBase: cannot read nodeClass of base number %d",i) ;
			return (uint32_t)1 ;
		}

		debug_i(BIN_DBG_DETAIL,"NodesTable","loadBase: nodeClass = %d",nodeClass) ;

		switch (nodeClass) {
		case (uint8_t)  1: return loadObject(fd,i) ;
		case (uint8_t)  2: return loadVariable(fd,i) ;
		case (uint8_t)  4: return loadMethod(fd,i) ;
		case (uint8_t)  8: return loadObjectType(fd,i) ;
		case (uint8_t) 16: return loadVariableType(fd,i) ;
		case (uint8_t) 32: return loadReferenceType(fd,i) ;
		case (uint8_t) 64: return loadDataType(fd,i) ;
		case (uint8_t)128: return loadDataType(fd,i) ;
		default:  break ;
		}

		debug_ii(COM_ERR,"NodesTable","loadBase: cannot load base number %d with unknown nodeClass=%d",i,nodeClass) ;
		return (uint32_t)2;
	}

private:

	uint32_t loadReference(int fd,int i)
	{
		uint32_t type ;
		uint32_t source ;
		uint32_t target ;

		if (Binary::myread(fd,&type) != 0) {
			debug_i(COM_ERR,"NodesTable","loadReference: cannot read type of reference number %d",i) ;
			return (uint32_t)1 ;
		}

		if (Binary::myread(fd,&source) != 0) {
			debug_i(COM_ERR,"NodesTable","loadReference: cannot read source of reference number %d",i) ;
			return (uint32_t)2 ;
		}

		if (Binary::myread(fd,&target) != 0) {
			debug_i(COM_ERR,"NodesTable","loadReference: cannot read target of reference number %d",i) ;
			return (uint32_t)3 ;
		}

		NodeId * nodeIdSource = new NodeId(source) ;
		Base   * baseSource   = SyncHashTableHash<NodeId,Base>::get(nodeIdSource) ;
		nodeIdSource->checkRefCount() ;

		if (baseSource == NULL) {
			debug_ii(COM_ERR,"NodesTable","loadReference: cannot compute source base (%d) of reference number %d",source,i) ;
			return (uint32_t)4 ;
		}

		NodeId * nodeIdTarget = new NodeId(target) ;
		Base   * baseTarget   = SyncHashTableHash<NodeId,Base>::get(nodeIdTarget) ;
		nodeIdTarget->checkRefCount() ;

		if (baseTarget == NULL) {
			debug_ii(COM_ERR,"NodesTable","loadReference: cannot compute target base (%d) of reference number %d",source,i) ;
			return (uint32_t)5 ;
		}

		new ReferenceLink(type,baseSource,baseTarget) ;

		return (uint32_t)0;
	}

private: // OK

	uint32_t binary_load(int fd)
	{
		int32_t count ;

		if (Binary::myread(fd,&count) == 0) {
			debug_i(BIN_DBG,"NodesTable","load: loading %d bases",count) ;
		} else {
			debug(COM_ERR,"NodesTable","load: cannot read count of bases") ;
			return (uint32_t)1 ;
		}

		for (int i = 0 ; i < count ; i++) {
			debug_i(BIN_DBG_DETAIL,"NodesTable","load: loading base number %i",i) ;
			if (loadBase(fd,i) != 0)
				return (uint32_t)2 ;
		}

		if (Binary::myread(fd,&count) == 0) {
			debug_i(BIN_DBG,"NodesTable","load: loading %d references",count) ;
		} else {
			debug(COM_ERR,"NodesTable","load: cannot read count of references") ;
			return (uint32_t)3 ;
		}

		for (int i = 0 ; i < count ; i++) {
			debug_i(BIN_DBG_DETAIL,"NodesTable","load: loading reference number %i",i) ;
			if (loadReference(fd,i) != 0)
				return (uint32_t)4 ;
		}

		return (uint32_t)0 ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_HASHTABLEInt_H_ */
