
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDRESSSPACE_H_
#define OPCUA_ADDRESSSPACE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

#include "OpcUa_NodesTable.h"

namespace opcua {

class MYDLL AddressSpace
{
public:

	static AddressSpace * self ;

private:

	int32_t       nameSpaceCount ;
	String     ** nameSpace ;
	NodesTable  * nodesTable ;

public:

	AddressSpace()
	{
		nameSpaceCount = 2 ;

		nameSpace = new String * [nameSpaceCount] ;

		(nameSpace[0] = new String("http://opcfoundation.org/UA/"))->take() ;
		(nameSpace[1] = new String("http://cluster-connexion.org/opc-rosa"))->take() ;

		nodesTable = new NodesTable(NODES_TABLE_COUNT) ;

		self = this ;
	}

	virtual ~AddressSpace()
	{
		self = NULL ;

		nameSpace[0]->release() ;
		nameSpace[1]->release() ;

		delete nodesTable ;
	}

public:

	inline NodesTable * getNodesTable()
	{
		return nodesTable ;
	}

public:

	inline void load(const char * config_file)
	{
		nodesTable->load(config_file) ;
	}

public:

	inline void put(Base * node)
	{
		nodesTable->put(node) ;
	}

	inline Base * get(NodeId * nodeId)
	{
		return nodesTable->get(nodeId) ;
	}

	inline Base * get(uint32_t _nodeId)
	{
		NodeId * nodeId = new NodeId(UInt16::zero, new UInt32(_nodeId)) ;
		Base * result =  nodesTable->get(nodeId) ;
		nodeId->checkRefCount() ;
		return result ;
	}

};

}      /* namespace opcua */
#endif /* OPCUA_ADDRESSSPACE_H_ */
