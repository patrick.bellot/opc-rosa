
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BINARY_H_
#define BINARY_H_

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
#include <io.h>
#endif
#include <stdio.h>

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_Bits.h"
#include "../Error/OpcUa_Debug.h"

namespace opcua {

class Binary
{
public:

	static uint32_t readCount ;

	static int MYREAD(void * pVal, size_t size, size_t count, int fd)
	{
		size_t orig = count * size ;
		size_t now  ;
		do {
			now = read(fd,pVal,orig) ;
			if (now <= 0)
				break ;
			orig -= now ;
			if (orig == 0)
				return 0 ;
			pVal = (void *)((char *)pVal + now) ;
		} while (true) ;

		if (now == 0) {
			debug_i(COM_ERR,"Binary","End of binary file at char %u",readCount) ;
		} else {
			debug_ii(COM_ERR,"Binary","Error reading binary file, errno=%d at char %u",errno,readCount) ;
		}

		return 1 ;
	}

	static uint32_t myread(int fd, bool * pVal)
	{
		if (MYREAD(pVal,sizeof(int8_t),1,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read bool in binary file, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)1 ;
		}
		readCount += 1 ;
		debug_i(BIN_DBG_DETAIL,"Binary","read bool %d",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, int8_t * pVal)
	{
		if (MYREAD(pVal,1,1,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read int8_t in binary file, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)1 ;
		}
		readCount += 1 ;
		debug_i(BIN_DBG_DETAIL,"Binary","read int8 %d",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, uint8_t * pVal)
	{
		if (MYREAD(pVal,1,1,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read uint8_t in binary file, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)1 ;
		}
		readCount += 1 ;
		debug_i(BIN_DBG_DETAIL,"Binary","read uint8 %u",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, uint32_t * pVal)
	{
		if (MYREAD(pVal,4,1,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read uint32_t in binary file, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)1 ;
		}
		readCount += 4 ;
		* pVal = Bits::from_little_endian_uint32(* pVal) ;
		debug_i(BIN_DBG_DETAIL,"Binary","read uint32 %d",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, int32_t * pVal)
	{
		if (MYREAD(pVal,4,1,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read int32_t in binary file, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)1 ;
		}
		readCount += 4 ;
		* pVal = Bits::from_little_endian_int32(* pVal) ;
		debug_i(BIN_DBG_DETAIL,"Binary","read int32 %u",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, char ** pVal)
	{
		int32_t length ;

		if (myread(fd,&length) != 0) {
			debug(COM_ERR,"Binary","Cannot read (char *) in binary file, case 1") ;
			return (uint32_t)1 ;
		}

		if (length <= 0) {
			* pVal = NULL ;
			debug(BIN_DBG_DETAIL,"Binary","read (char *) NULL") ;
			return (uint32_t)0 ;
		}

		if (length > 64*1024)
			debug_i(COM_ERR,"Binary","Read (char *) length is %d. Is it normal ?????",length) ;

		* pVal = new char[length + 1] ;

		if (MYREAD(* pVal,sizeof(char),length,fd) != 0) {
			debug_ii(COM_ERR,"Binary","Cannot read (char *) in binary file, case 2, errno=%d at char %u",errno,readCount) ;
			return (uint32_t)2 ;
		}

		(* pVal)[length] = '\0' ;

		readCount += length ;
		debug_s(BIN_DBG_DETAIL,"Binary","read (char *) \"%s\"",*pVal) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, LocalizedText ** pVal)
	{
		bool   exists ;
		char * text   = NULL ;
		char * locale = NULL ;

		if (myread(fd,&exists) != 0) {
			debug(COM_ERR,"Binary","Cannot read (LocalizedText *) in binary file, case 1") ;
			return (uint32_t)1 ;
		}

		if (exists) {
			if (myread(fd,&text) != 0) {
				debug(COM_ERR,"Binary","Cannot read (LocalizedText *) in binary file, case 2") ;
				return (uint32_t)2 ;
			}
		}

		if (myread(fd,&exists) != 0) {
			debug(COM_ERR,"Binary","Cannot read (LocalizedText *) in binary file, case 3") ;
			if (text)
				delete[] text ;
			return (uint32_t)3 ;
		}

		if (exists) {
			if (myread(fd,&locale) != 0) {
				debug(COM_ERR,"Binary","Cannot read (LocalizedText *) in binary file, case 4") ;
				if (text)
					delete[] text ;
				return (uint32_t)4 ;
			}
		}

		* pVal = new LocalizedText(new String(text,1),new LocaleId(locale,1)) ;
		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, TableUInt32 ** pVal)
	{
		int32_t length ;

		if (myread(fd,&length) != 0) {
			debug(COM_ERR,"Binary","Cannot read (TableOfUint32 *) in binary file, case 1") ;
			return (uint32_t)1 ;
		}

		debug_i(BIN_DBG_DETAIL,"Binary","read (TableOfUint32 *) in binary file, length = %d",length) ;

		* pVal = new TableUInt32(length) ;

		for (int32_t i = 0 ; i < length ; i++) {
			uint32_t val ;
			if (myread(fd,&val) != 0) {
				debug_ii(COM_ERR,"Binary","Cannot read (TableOfUint32 *) in binary file at rank %d on %d, case 2",i,length) ;
				delete * pVal ;
				return (uint32_t)2 ;
			}
			(* pVal)->set(i, new UInt32(val)) ;
		}

		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, BaseDataType ** pVal)
	{
		uint8_t exists ;

		if (Binary::myread(fd,&exists) != 0) {
			debug(COM_ERR,"Binary","myread: cannot load BaseDataType existence") ;
			return (uint32_t)1 ;
		}

		if (exists == 0) {
			*pVal = Boolean::booleanFalse ;
		} else {
			switch (exists) {

			case 1: // String
			{
				char * val = NULL ;
				if (Binary::myread(fd,&val) != 0) {
					debug(COM_ERR,"Binary","myread: cannot load Value: (char *) as a String") ;
					if (val != NULL)
						delete [] val ;
					return 101 ;
				}
				*pVal = new String(val,1) ;
			} ;
			break ;

			case 2: // ByteString
			{
				uint32_t   length = 0 ;
				uint8_t  * array  = NULL ;
				if (Binary::myread(fd,&length) != 0) {
					debug(COM_ERR,"Binary","myread: cannot load Value: length for a ByteString") ;
					return 102 ;
				}
				if (Binary::myread(fd,&array,length) != 0) {
					debug(COM_ERR,"Binary","myread: cannot load Value: array as a Byte String") ;
					if (array != NULL)
						delete [] array ;
					return 103 ;
				}
				*pVal = new ByteString(length,array,1) ;
			} ;
			break ;

			case 3: // Int32
			{
				int32_t val = (int32_t)0 ;
				if (Binary::myread(fd,&val) != 0) {
					debug(COM_ERR,"Binary","myread: cannot load Value: (int32_t) as a String") ;
					return 105 ;
				}
				*pVal = new Int32(val) ;
			} ;
			break ;

			case 4: // UInt32
			{
				uint32_t val = (uint32_t)0 ;
				if (Binary::myread(fd,&val) != 0) {
					debug(COM_ERR,"Binary","myread: cannot load Value: (uint32_t) as a String") ;
					return 106 ;
				}
				*pVal = new UInt32(val) ;
			} ;
			break ;

			default:
				debug_i(COM_ERR,"Binary","myread: cannot load Value with code %d",exists) ;
				return (uint32_t)2 ;
			} ;
		}

		return (uint32_t)0 ;
	}

	static uint32_t myread(int fd, uint8_t ** pVal, int32_t length)
	{
		if (length > 0) {
			* pVal = new uint8_t[length] ;
			for (int32_t i = 0 ; i < length ; i++)
				if (myread(fd,(*pVal)+i) != 0) {
					debug_i(COM_ERR,"Binary","myread: cannot (uint8_t *) array at rank i=%d",i) ;
					return 1 ;
				}
		}
		return (uint32_t)0 ;
	}

};     /* class Binary */
}      /* namespace    */
#endif /* BINARY_H_    */
