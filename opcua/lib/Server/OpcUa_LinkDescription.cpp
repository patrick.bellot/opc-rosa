
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "OpcUa_LinkDescription.h"
#include "OpcUa_NodesTable.h"

#include <string.h>

namespace opcua {

LinkDescription * LinkDescription::implements(NodesTable * nodesTable, bool withError)
{
		uint32_t   type_flag ;
		Base     * source_node ;
		Base     * target_node ;
		int        rc ;

		source_node = nodesTable->get(source) ;

		if (source_node == NULL) {
			rc = 1 ;
			goto error ;
		}

		target_node = nodesTable->get(target) ;

		if (target_node == NULL) {
			rc = 2 ;
			goto error ;
		}

		type_flag = ReferenceLink::getCode(type) ;

		if (type_flag == (uint32_t)0) {
			rc = 3 ;
	    	goto error ;
	    }

		debug_s (XML_DBG,"LinkDescription","Link, type=\"%s\"",type) ;

		new ReferenceLink(type_flag,source_node,target_node) ;

		return NULL ;

error:

		if (withError) {
			const char * error = NULL ;

			switch (rc) {
			case 1  : error = "Can't find source node !" ; break ;
			case 2  : error = "Can't find target node !" ; break ;
			case 3  : error = "Unimplemented reference"  ; break ;
			default : error = "Unknown error"            ; break ;
			}

			debug_s(COM_ERR,"LinkDescription","Cannot implements links, error=%s",error) ;

			String * sourceStr = source->toString() ;
			String * targetStr = target->toString() ;

			debug_ssss(COM_ERR,"LinkDescription","Type=%s NodeClass=%s Source=%s Target=%s",type,nodeClass,sourceStr->get(),targetStr->get()) ;

			sourceStr->checkRefCount() ;
			targetStr->checkRefCount() ;
		}

		return this ;
}

}

