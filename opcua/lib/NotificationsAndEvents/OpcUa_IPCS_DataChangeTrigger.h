
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATACHANGETRIGGER_H_
#define OPCUA_DATACHANGETRIGGER_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 7.16.2, p. 124
 * IdType = 717
 */

enum _DataChangeTrigger {
	DataChangeTrigger_STATUS_0 = 0,
	DataChangeTrigger_STATUS_VALUE_1 = 1,
	DataChangeTrigger_STATUS_VALUE_TIMESTAMP_2 = 2
} ;

class MYDLL DataChangeTrigger
		: public Enumeration
{
public:

	static DataChangeTrigger * status_0 ;
	static DataChangeTrigger * status_value_1 ;
	static DataChangeTrigger * status_value_timestamp_2 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_DataChangeTrigger ; }

public:

	static DataChangeTrigger * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_DataChangeTrigger const& value)
	{
		uint32_t _value = (uint32_t)value ;
		if ((int)value <= (int)DataChangeTrigger_STATUS_VALUE_TIMESTAMP_2)
			return new DataChangeTrigger((_DataChangeTrigger)_value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	inline void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_DataChangeTrigger& pValue) const
	{
		int32_t value = get() ;
		if ((int)value <= (int)DataChangeTrigger_STATUS_VALUE_TIMESTAMP_2)
			pValue = (OpcUa_DataChangeTrigger)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

public:

	DataChangeTrigger(_DataChangeTrigger _value)
		: Enumeration(_value)
	{}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_DATACHANGETRIGGER_H_ */
