
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITORINGMODE_H_
#define OPCUA_MONITORINGMODE_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/OpcUa_Enumeration.h"

namespace opcua {

/*
 * OPC-UA Part 4, 7.17, p. 128
 * IdType = 716
 */

enum _MonitoringMode {
	MonitoringMode_DISABLED_0  = 0,
	MonitoringMode_SAMPLING_1  = 1,
	MonitoringMode_REPORTING_2 = 2
} ;

class MYDLL MonitoringMode
		: public Enumeration
{
public:

	static MonitoringMode * disabled_0 ;
	static MonitoringMode * sampling_1 ;
	static MonitoringMode * reporting_2 ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoringMode ; }

public:

	static MonitoringMode * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoringMode const& value)
	{
		uint32_t _value = (uint32_t)value ;
		if ((int)value <= (int)MonitoringMode_REPORTING_2)
			return new MonitoringMode((_MonitoringMode)_value) ;
		*pStatus = _Bad_DecodingError ;
		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoringMode& pValue) const
	{
		int32_t value = get() ;
		if ((int)value <= (int)MonitoringMode_REPORTING_2)
			pValue = (OpcUa_MonitoringMode)value ;
		else
			*pStatus = _Bad_EncodingError ;
	}

private:

	MonitoringMode(_MonitoringMode _value)
		: Enumeration(_value)
	{}

public:

	bool isDisabled()
	{
		return get() == MonitoringMode_DISABLED_0 ;
	}

	bool isSampling()
	{
		return get() == MonitoringMode_SAMPLING_1 ;
	}

	bool isReporting()
	{
		return get() == MonitoringMode_REPORTING_2 ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITORINGMODE_H_ */
