/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITOREDITEMMODIFYREQUEST_H_
#define OPCUA_MONITOREDITEMMODIFYREQUEST_H_

/*
 * OPC-UA Part 4, 5.12.3.2, p. 69, Table 67Ò (inside the table)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringMode.h"
#include "OpcUa_IPCS_MonitoringParameters.h"

namespace opcua {

class MYDLL MonitoredItemModifyRequest
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemModifyRequest; }

private:

	IntegerId            * monitoredItemId ;
	MonitoringParameters * requestedParameters ;

public:

	static MonitoredItemModifyRequest * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemModifyRequest const& pMonitoredItemModifyRequest)
	{
		IntegerId            * monitoredItemId     = IntegerId            ::fromCtoCpp (pStatus, pMonitoredItemModifyRequest.MonitoredItemId) ;
		MonitoringParameters * requestedParameters = MonitoringParameters ::fromCtoCpp (pStatus, pMonitoredItemModifyRequest.RequestedParameters) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoredItemModifyRequest(
							monitoredItemId,
							requestedParameters
							) ;

		if (monitoredItemId != NULL)
			monitoredItemId->checkRefCount() ;
		if (requestedParameters != NULL)
			requestedParameters->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemModifyRequest& pMonitoredItemModifyRequest) const
	{
		monitoredItemId     ->fromCpptoC(pStatus, pMonitoredItemModifyRequest.MonitoredItemId) ;
		requestedParameters ->fromCpptoC(pStatus, pMonitoredItemModifyRequest.RequestedParameters) ;
	}

public:

	MonitoredItemModifyRequest(
			IntegerId            * _monitoredItemId,
			MonitoringParameters * _requestedParameters)
		: Structure()
	{
		(monitoredItemId     = _monitoredItemId)     ->take() ;
		(requestedParameters = _requestedParameters) ->take() ;
	}

	virtual ~MonitoredItemModifyRequest()
	{
		monitoredItemId     ->release() ;
		requestedParameters ->release() ;
	}

public:

	inline IntegerId * getMonitoredItemId()
	{
		return monitoredItemId ;
	}

	inline MonitoringParameters * getRequestedParameters()
	{
		return requestedParameters ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITOREDITEMMODIFYREQUEST_H_ */
