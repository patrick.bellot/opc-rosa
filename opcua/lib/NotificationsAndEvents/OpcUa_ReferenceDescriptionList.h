
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_REFERENCEDESCRIPTIONLIST_H_
#define OPCUA_REFERENCEDESCRIPTIONLIST_H_

#include "../OpcUa.h"

#if (WITH_BROWSE == 1)

#include "../CommonParametersTypes/OpcUa_IPCS_ReferenceDescription.h"
#include "../Utils/OpcUa_LinkedList.h"


namespace opcua {


class MYDLL ReferenceDescriptionList
	: public LinkedList<ReferenceDescription>
{
public:

	ReferenceDescriptionList(ReferenceDescription * _car, ReferenceDescriptionList * _cdr)
		: LinkedList<ReferenceDescription>(_car,_cdr)
    {
		getCar()->take() ;
    }

	virtual ~ReferenceDescriptionList()
	{
		getCar()->release() ;
	}

public:

	inline ReferenceDescriptionList * getCdr() { return (ReferenceDescriptionList *)LinkedList<ReferenceDescription>::getCdr() ; }

};


} /* namespace opcua */

#endif /* WITH_BROWSE */
#endif /* OPCUA_REFERENCEDESCRIPTIONLIST_H_ */
