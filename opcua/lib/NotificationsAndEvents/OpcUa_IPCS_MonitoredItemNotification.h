
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_MONITOREDITEMNOTIFICATION_H_
#define OPCUA_MONITOREDITEMNOTIFICATION_H_

/*
 * OPC-UA Part 4, 7.19.2, p. 132, inside Table 148
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_NotificationData.h"

namespace opcua {

class MYDLL MonitoredItemNotification
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemNotification ; }

private:

	IntegerId * clientHandle ;
	DataValue * value ;

public:

	static MonitoredItemNotification * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemNotification const& pMonitoredItemNotification)
	{
		IntegerId * clientHandle = IntegerId ::fromCtoCpp( pStatus, pMonitoredItemNotification.ClientHandle) ;
		DataValue * value        = DataValue ::fromCtoCpp( pStatus, pMonitoredItemNotification.Value) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoredItemNotification(
							clientHandle,
							value
							) ;

		if (clientHandle != NULL)
			clientHandle->checkRefCount() ;
		if (value != NULL)
			value->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemNotification& pMonitoredItemNotification) const
	{
		clientHandle ->fromCpptoC (pStatus, pMonitoredItemNotification.ClientHandle) ;
		value        ->fromCpptoC (pStatus, pMonitoredItemNotification.Value) ;
	}

public:

	MonitoredItemNotification(
			IntegerId * _clientHandle,
			DataValue * _value
			)
		: Structure()
	{
		(clientHandle = _clientHandle) ->take() ;
		(value        = _value)        ->take() ;
	}

	virtual ~MonitoredItemNotification()
	{
		clientHandle ->release() ;
		value        ->release() ;
	}

public:

	inline IntegerId * getClientHandle()
	{
		return clientHandle ;
	}

	inline DataValue * getValue()
	{
		return value ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITOREDITEMNOTIFICATION_H_ */
