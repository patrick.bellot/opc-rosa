
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_DATACHANGEFILTER_H_
#define OPCUA_DATACHANGEFILTER_H_

/*
 * OPC-UA Part 4, 7.16.2, p. 124
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include <math.h>

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_DataChangeTrigger.h"
#include "OpcUa_IPCS_MonitoringFilter.h"
#include "OpcUa_IPCS_MonitoredItemNotification.h"
#include "OpcUa_IPCS_MonitoringNullFilterResult.h"

namespace opcua {

// For deadbandType value
#define NONE_0     ((uint32_t)0)
#define ABSOLUTE_1 ((uint32_t)1)
#define PERCENT_2  ((uint32_t)2)

class MYDLL DataChangeFilter
	: public MonitoringFilter
{
public:

	static DataChangeFilter * nullFilter ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_DataChangeFilter ; }

private:

	DataChangeTrigger * trigger ;
	UInt32            * deadbandType ;
	Double            * deadbandValue ;

	int32_t            vTrigger ;
	uint32_t           vDeadbandType ;
	double             vDeadbandValue ;

public:

	static DataChangeFilter * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_DataChangeFilter const& pDataChangeFilter)
	{
		DataChangeTrigger * trigger       = DataChangeTrigger ::fromCtoCpp (pStatus, pDataChangeFilter.Trigger) ;
		UInt32            * deadbandType  = UInt32            ::fromCtoCpp (pStatus, pDataChangeFilter.DeadbandType) ;
		Double            * deadbandValue = Double            ::fromCtoCpp (pStatus, pDataChangeFilter.DeadbandValue) ;

		if (*pStatus == STATUS_OK)
			return
					new DataChangeFilter(
							trigger,
							deadbandType,
							deadbandValue
							) ;

		if (trigger != NULL)
			trigger->checkRefCount() ;
		if (deadbandType != NULL)
			deadbandType->checkRefCount() ;
		if (deadbandValue != NULL)
			deadbandValue->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pDataChangeFilter) const
	{
		expandedNodeId->fromCpptoC(pStatus,pDataChangeFilter.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pDataChangeFilter.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pDataChangeFilter.Body.Object.ObjType = &OpcUa_DataChangeFilter_EncodeableType ;
			pDataChangeFilter.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_DataChangeFilter)) ;
			OpcUa_DataChangeFilter_Initialize(pDataChangeFilter.Body.Object.Value) ;
			DataChangeFilter::fromCpptoC(pStatus,*static_cast<_OpcUa_DataChangeFilter *>(pDataChangeFilter.Body.Object.Value)) ;
			pDataChangeFilter.Length = sizeof(_OpcUa_DataChangeFilter) ;
		}
	}

private:

	inline void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_DataChangeFilter& pDataChangeFilter) const
	{
		trigger       ->fromCpptoC (pStatus, pDataChangeFilter.Trigger) ;
		deadbandType  ->fromCpptoC (pStatus, pDataChangeFilter.DeadbandType) ;
		deadbandValue ->fromCpptoC (pStatus, pDataChangeFilter.DeadbandValue) ;
	}

public:

	DataChangeFilter(
			DataChangeTrigger * _trigger,
			UInt32            * _deadbandType,
			Double            * _deadbandValue
			)
		: MonitoringFilter()
	{
		(trigger       = _trigger)       ->take() ;
		(deadbandType  = _deadbandType)  ->take() ;
		(deadbandValue = _deadbandValue) ->take() ;

		vTrigger       = trigger       ->get() ;
		vDeadbandType  = deadbandType  ->get() ;
		vDeadbandValue = deadbandValue ->get() ;

		cachedDataValue = NULL ;
	}

	virtual ~DataChangeFilter()
	{
		trigger       ->release() ;
		deadbandType  ->release() ;
		deadbandValue ->release() ;

		if (cachedDataValue != NULL)
			cachedDataValue->release() ;
	}

public :

	virtual MonitoringFilterResult * getMonitoringFilterResult()
	{
		return MonitoringNullFilterResult::nullFilterResult ;
	}

private:

	DataValue * cachedDataValue ;

	void setCachedDataValue(DataValue * _cachedDataValue)
	{
		DataValue * oldDataValue = cachedDataValue ;
		(cachedDataValue = _cachedDataValue)->take() ;
		if (oldDataValue != NULL)
			oldDataValue->release() ;
	}

public:

	bool isOk(DataValue * dataValue,Base * variable)
	{
		if (vDeadbandType == NONE_0)
			return true ;

		if (cachedDataValue == NULL) {
			setCachedDataValue(dataValue) ;
			return true ;
		}

		Variant * variant = dataValue->getVariant() ;

		if (variant == NULL)
			return true ;

		if (! variant->isNumeric())
			return true ;

		if (variant->getArrayLength() != -1)
			return true ;

		Variant * cachedVariant = cachedDataValue->getVariant() ;

		if (cachedVariant == NULL)
			return true ;

		if (! cachedVariant->isNumeric())
			return true ;

		if (cachedVariant->getArrayLength() != -1)
			return true ;

		bool result ;

		if (vDeadbandType == ABSOLUTE_1) {
			result = (fabs(variant->getDouble() - cachedVariant->getDouble()) > vDeadbandValue) ;
		} else { /* PERCENT_2 */
			debug(COM_ERR,"DataChangeFilter","isOk() with DeadbandType=PERCENT_2, unimplemented") ;
			NOT_USED(variable) ;
			// TBD
			result = false ;
		}

		setCachedDataValue(dataValue) ;
		return result ;
	}

public:

	inline DataChangeTrigger * getTrigger()
	{
		return trigger ;
	}

	inline UInt32 * getDeadbandType()
	{
		return deadbandType ;
	}

	inline Double * getDeadbandValue()
	{
		return deadbandValue ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_DATACHANGEFILTER_H_ */
