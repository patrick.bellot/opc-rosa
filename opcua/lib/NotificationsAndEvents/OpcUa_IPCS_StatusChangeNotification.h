
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_STATUSCHANGENOTIFICATION_H_
#define OPCUA_STATUSCHANGENOTIFICATION_H_

/*
 * OPC-UA Part 4, 7.19.4, p. 133
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_NotificationData.h"

namespace opcua {

class MYDLL StatusChangeNotification
	: public NotificationData
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_StatusChangeNotification ; }

private:

	StatusCode     * status ;
	DiagnosticInfo * diagnosticInfo ;

public:

	static StatusChangeNotification * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_StatusChangeNotification const& pStatusChangeNotification)
	{
		StatusCode     * status         = StatusCode     ::fromCtoCpp (pStatus, pStatusChangeNotification.Status) ;
		DiagnosticInfo * diagnosticInfo = DiagnosticInfo ::fromCtoCpp (pStatus, pStatusChangeNotification.DiagnosticInfo) ;

		if (*pStatus == STATUS_OK)
			return
					new StatusChangeNotification(
							status,
							diagnosticInfo
							) ;

		if (status != NULL)
			status->checkRefCount() ;
		if (diagnosticInfo != NULL)
			diagnosticInfo->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pStatusChangeNotification) const
	{
		expandedNodeId->fromCpptoC(pStatus,pStatusChangeNotification.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pStatusChangeNotification.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pStatusChangeNotification.Body.Object.ObjType = &OpcUa_StatusChangeNotification_EncodeableType ;
			pStatusChangeNotification.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_StatusChangeNotification)) ;
			OpcUa_StatusChangeNotification_Initialize(pStatusChangeNotification.Body.Object.Value) ;
			StatusChangeNotification::fromCpptoC(pStatus,*static_cast<_OpcUa_StatusChangeNotification *>(pStatusChangeNotification.Body.Object.Value)) ;
			pStatusChangeNotification.Length = sizeof(_OpcUa_StatusChangeNotification) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_StatusChangeNotification& pStatusChangeNotification) const
	{
		status         ->fromCpptoC (pStatus, pStatusChangeNotification.Status) ;
		diagnosticInfo ->fromCpptoC (pStatus, pStatusChangeNotification.DiagnosticInfo) ;
	}

public:

	StatusChangeNotification(
			StatusCode     * _status,
			DiagnosticInfo * _diagnosticInfo
			)
		: NotificationData()
	{
		(status         = _status)         ->take() ;
		(diagnosticInfo = _diagnosticInfo) ->take() ;
	}

	virtual ~StatusChangeNotification()
	{
		status         ->release() ;
		diagnosticInfo ->release() ;
	}

public:

	inline StatusCode * getStatus()
	{
		return status ;
	}

	inline DiagnosticInfo * getDiagnosticInfo()
	{
		return diagnosticInfo ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_STATUSCHANGENOTIFICATION_H_ */
