
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_MONITOREDITEM_H_
#define OPCUA_MONITOREDITEM_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_SyncQueue.h"

#include "OpcUa_IPCS_MonitoredItemCreateResult.h"
#include "OpcUa_IPCS_NotificationData.h"
#include "OpcUa_IPCS_MonitoringMode.h"
#include "OpcUa_IPCS_MonitoringParameters.h"
#include "OpcUa_IPCS_MonitoredItemModifyResult.h"

namespace opcua {

class MYDLL MonitoredItem
{
private:

	static int32_t _nextIntegerId ;

private:

	static IntegerId *  getNextIntegerId()
	{
		return new IntegerId(++_nextIntegerId) ;
	}

private:

	int32_t                       monitoringMode ;

	IntegerId                   * monitoredItemId ;
	double                        samplingInterval ;
	Duration                    * revisedSamplingInterval ;
	uint64_t                      operationTime ;
	bool			              discardOldest ;
	int                           queueSize ;
	SyncQueue<NotificationData> * notificationsQueue ;

public:

	MonitoredItem(
			MonitoringMode * _monitoringMode,
			Duration       * _samplingInterval,
			Counter        * _queueSize,
			Boolean        * _discardOldest
			)
	{
		debug(SUB_DBG,"MonitoredItem","MonitoredItem creation begins") ;

		setMonitoringMode(_monitoringMode) ;

		(monitoredItemId = MonitoredItem::getNextIntegerId())->take() ;

		debug_i(SUB_DBG,"MonitoredItem","MonitoredItem monitoredItemId=0x%04x",monitoredItemId->get()) ;

		double iSamplingInterval = _samplingInterval->get() ;

		if (fabs(iSamplingInterval) < 10e-7) {
			samplingInterval = (double)128 ;
		} else if (iSamplingInterval < 0) {
			samplingInterval = (double)512 ;
		} else {
			samplingInterval = iSamplingInterval ;
		}

		debug_i(SUB_DBG,"MonitoredItem","MonitoredItem samplingInterval = %d ms",samplingInterval) ;

		(revisedSamplingInterval = new Duration(samplingInterval))->take() ;

		operationTime = (uint64_t)0 ;

		queueSize = _queueSize->get() ;
		if (queueSize == 0)
			queueSize = 1 ;

		debug_i(SUB_DBG,"MonitoredItem","MonitoredItem queueSize = %d",queueSize) ;

		// Je me demande bien ce que ça vient faire là !
		// Je le laisse pour m'en souvenir.
		//		NotificationData * notificationData ;
		//		while ((notificationData = nextNotificationData()) != NULL)
		//			delete notificationData ;
		//		delete notificationsQueue ;

		notificationsQueue = new SyncQueue<NotificationData>(queueSize) ;

		discardOldest = _discardOldest->get() ;

		debug(SUB_DBG,"MonitoredItem","MonitoredItem creation ends") ;
	}

	virtual ~MonitoredItem()
	{
		debug(SUB_DBG,"MonitoredItem","MonitoredItem destruction begins") ;

		monitoredItemId->release() ;
		revisedSamplingInterval->release() ;
		delete notificationsQueue ;

		debug(SUB_DBG,"MonitoredItem","MonitoredItem destruction ends") ;
	}

public:

	inline void setMonitoringMode(MonitoringMode * _monitoringMode)
	{
		monitoringMode = _monitoringMode->get() ;
	}

	inline int32_t getMonitoringMode()
	{
		return monitoringMode ;
	}

	inline IntegerId * getMonitoredItemId()
	{
		return monitoredItemId ;
	}

	inline Duration * getRevisedSamplingInterval()
	{
		return revisedSamplingInterval ;
	}

	inline double getSamplingInterval()
	{
		return samplingInterval ;
	}

	inline Counter * getRevisedQueueSize()
	{
		return new Counter(static_cast<uint32_t>(queueSize)) ;
	}

public:

	MonitoredItemModifyResult * ModifyMonitoredItem(MonitoringParameters * requestedParameters)
	{
		debug_i(SUB_DBG,"MonitoredItem","ModifyMonitoredItem monitoredItemId=0x%04x",getMonitoredItemId()->get()) ;

		Duration * _samplingInterval = requestedParameters->getSamplingInterval() ;

		int32_t iSamplingInterval = static_cast<int32_t>(_samplingInterval->get()) ;

		if (iSamplingInterval == 0) {
			samplingInterval = (uint32_t)128 ;
		} else if (iSamplingInterval < 0) {
			samplingInterval = (uint32_t)512 ;
		} else {
			samplingInterval = static_cast<uint32_t>(iSamplingInterval) ;
		}

		debug_i(SUB_DBG,"MonitoredItem","ModifyMonitoredItem samplingInterval = %d ms",samplingInterval) ;

		revisedSamplingInterval->release() ;

		(revisedSamplingInterval = new Duration(samplingInterval))->take() ;

		operationTime = (uint64_t)0 ;

		Counter * _queueSize = requestedParameters->getQueueSize() ;

		queueSize = _queueSize->get() ;
		if (queueSize == 0)
			queueSize = 1 ;

		SyncQueue<NotificationData> * _notificationsQueue = new SyncQueue<NotificationData>(queueSize) ;

		NotificationData * notificationData ;
		while ((notificationData = nextNotificationData()) != NULL) {
			if (! _notificationsQueue->tryPush(notificationData))
				break ;
		}

		delete notificationsQueue ;

		notificationsQueue = _notificationsQueue ;

		debug_i(SUB_DBG,"MonitoredItem","ModifyMonitoredItem queueSize = %d",queueSize) ;

		Boolean * _discardOldest = requestedParameters->getDiscardOldest() ;

		discardOldest = _discardOldest->get() ;

		debug(SUB_DBG,"MonitoredItem","ModifyMonitoredItem filter") ;

		setFilter(requestedParameters->getFilter()) ;

		debug(SUB_DBG,"MonitoredItem","ModifyMonitoredItem creation ends") ;

		return getMonitoredItemModifyResult() ;
	}

public:

	virtual bool setTimestampToReturn(TimestampsToReturn * timestampToReturn)
	{
		NOT_USED(timestampToReturn) ;
		return false ;
	}

	virtual void setFilter(MonitoringFilter * filter) = 0 ;

	virtual MonitoredItemModifyResult * getMonitoredItemModifyResult() = 0 ;

protected:

	bool isOperationTime(uint64_t now)
	{
		debug_i(SUB_DBG,"MonitoredItem","isOperationTime(%ud) begins",now) ;

		if (now - operationTime > static_cast<uint64_t>(samplingInterval)) {
			operationTime = now ;
			debug(SUB_DBG,"MonitoredItem","isOperationTime returns true") ;
			return true ;
		}
		debug(SUB_DBG,"MonitoredItem","isOperationTime returns false") ;
		return false ;
	}

public:

	inline bool HasNotificationsAvailable()
	{
		return monitoringMode == MonitoringMode_REPORTING_2  && isNotificationsQueueNotEmpty() ;
	}

public:

	inline bool isNotificationsQueueNotEmpty()
	{
#if ((SUB_DBG) & DEBUG_LEVEL)
		bool result =  notificationsQueue->isNotEmpty() ;
		if (result) {
			debug(SUB_DBG,"MonitoredItem","bool isNotificationsQueueNotEmpty() return true") ;
		} else {
			debug(SUB_DBG,"MonitoredItem","bool isNotificationsQueueNotEmpty() return false") ;
		}
		return result ;
#else
		return notificationsQueue->isNotEmpty() ;
#endif
	}

public:

	NotificationData * nextNotificationData()
	{
		if (notificationsQueue->isEmpty()) {
			debug(SUB_DBG,"MonitoredItem","NotificationData * nextNotificationData() returning NULL") ;
			return NULL ;
		}
		debug(SUB_DBG,"MonitoredItem","NotificationData * nextNotificationData() returning non NULL") ;
		return notificationsQueue-> pop() ;
	}

protected:

	bool pushNotificationData(NotificationData * notificationData)
	{
		debug(SUB_DBG,"MonitoredItem","bool pushNotificationData(NotificationData * notificationData) begins") ;

		if (notificationsQueue->isNotFull()) {
			notificationsQueue->push(notificationData) ;
			debug(SUB_DBG,"MonitoredItem","bool pushNotificationData(NotificationData * notificationData) returns true (1)") ;
			return true ;
		}

		if (discardOldest) {
			delete notificationsQueue->pop() ;
			notificationsQueue->push(notificationData) ;
			debug(SUB_DBG,"MonitoredItem","bool pushNotificationData(NotificationData * notificationData) returns false") ;
			return false ;
		}

		delete notificationData ;
		debug(SUB_DBG,"MonitoredItem","bool pushNotificationData(NotificationData * notificationData) returns true (2)") ;
		return true ;
	}

public:

	virtual MonitoredItemCreateResult * getMonitoredItemCreateResult() = 0 ;

public:

	virtual void go(uint64_t now, class SamplingTimer * samplingTimer) = 0 ; /* called for sampling time */

};


} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_MONITOREDITEM_H_ */
