/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SUBSCRIPTION_H_
#define OPCUA_SUBSCRIPTION_H_

/*
 * OPC-UA Part 5.13, p. 73
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../Utils/OpcUa_Timer.h"
#include "../Services/Subscription/OpcUa_IPCS_CreateSubscriptionRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_CreateSubscriptionResponse.h"
#include "../Services/Subscription/OpcUa_IPCS_PublishRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_PublishResponse.h"
#include "../Services/Subscription/OpcUa_IPCS_RepublishRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_ModifySubscriptionRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_SetPublishingModeRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_TransferSubscriptionsRequest.h"
#include "../Services/Subscription/OpcUa_IPCS_DeleteSubscriptionsRequest.h"
#include "../Threads/OpcUa_Thread.h"
#include "../Stacks/OpcUa_Session.h"
#include "../Server/OpcUa_AddressSpace.h"
#include "../Services/OpcUa_BaseService.h"
#include "OpcUa_IPCS_NotificationMessage.h"
#include "OpcUa_NotificationMessagesList.h"
#include "OpcUa_PublishingReqQueue.h"
#include "OpcUa_SubscriptionsTable.h"
#include "OpcUa_MonitoredItemsList.h"
#include "OpcUa_NotificationDataList.h"
#include "OpcUa_IPCS_StatusChangeNotification.h"
#include "OpcUa_IPCS_MonitoredItemCreateRequest.h"
#include "OpcUa_IPCS_MonitoredItemCreateResult.h"
#include "OpcUa_IPCS_MonitoringNullFilterResult.h"
#include "OpcUa_VariableValueMonitoredItem.h"
#include "OpcUa_SamplingTimer.h"

MYDLL void ServerService_SendResponse(
    uint32_t               scConnectionId,
	uint32_t			   requestId,
	SOPC_EncodeableType  * encodeable,
    OpcUa_ResponseHeader * responseHeader,
    void                 * response) ;

namespace opcua {

MYDLL void ServerService_SendError(
	uint32_t               scConnectionId,
	uint32_t			   requestId,
	uint32_t               requestHandle,
	SOPC_StatusCode        status) ;

}

namespace opcua {

class MYDLL Subscription
	: public TimerInterface
{
private:

	uint32_t subscriptionId ;
	uint32_t publishingIntervalMs ;
	uint32_t lifetimeCount ;
	uint32_t maxKeepAliveCount ;
	uint32_t maxNotificationsPerPublish ;
	bool     publishingEnabled ;
	uint8_t  priority ;

	NotificationMessagesList * notificationMessagesList ;
	MonitoredItemsList       * monitoredItemsList ;
	MonitoredItemsList       * currMonitoredItemsList ;

	uint32_t nextNotificationSequenceNumber ;

private:

	int32_t    CurrentState ;
#define        SUBSCRIPTION_CLOSED                  ((int32_t)0)
#define		   SUBSCRIPTION_CREATING                ((int32_t)1)
#define		   SUBSCRIPTION_NORMAL                  ((int32_t)2)
#define		   SUBSCRIPTION_LATE                    ((int32_t)3)
#define		   SUBSCRIPTION_KEEPALIVE               ((int32_t)4)

	bool            PublishRateChange ;
	uint32_t        LifetimeCounter ;
	bool            MessageSent ;
	bool            PublishingEnabled ;
	uint32_t        SeqNum ;
	Timer         * PublishingTimer ;
	uint32_t        KeepAliveCounter ;
	Session       * session ;
	SamplingTimer * samplingTimer ;

public:

	Subscription(CreateSubscriptionRequest * request)
	{
		debug(SUB_DBG,"Subscription","Subscription(..) begins") ;

		CurrentState = SUBSCRIPTION_CREATING ;

		subscriptionId = 0 ;

		publishingIntervalMs       = (int32_t)(request->getRequestedPublishingInterval()->get()) ;
		lifetimeCount              = request->getRequestedLifetimeCount()->get() ;
		maxKeepAliveCount          = request->getRequestedMaxKeepAliveCount()->get() ;
		maxNotificationsPerPublish = request->getMaxNotificationsPerPublish()->get() ;
		publishingEnabled          = request->getPublishingEnabled()->get() ;
		priority                   = request->getPriority()->get() ;

		ResetKeepAliveCounter() ;
		PublishingTimer = new Timer(this) ;
		samplingTimer   = new SamplingTimer() ;
		session = NULL ;

		notificationMessagesList = NULL ;
		monitoredItemsList       = NULL ;
		currMonitoredItemsList   = NULL ;

		nextNotificationSequenceNumber = 1 ;

		debug(SUB_DBG,"Subscription","Subscription(..) ends") ;
	}

	virtual ~Subscription()
	{
		debug(SUB_DBG,"Subscription","~Subscription() begins") ;

		delete PublishingTimer ;
		delete samplingTimer ;

		SubscriptionsTable::subscriptionsLock() ;

		if (notificationMessagesList != NULL) {
			notificationMessagesList->deleteAll() ;
			notificationMessagesList = NULL ;		}

		if (monitoredItemsList != NULL) {
			monitoredItemsList->deleteAll() ;
			currMonitoredItemsList = NULL ;
		}

		debug(SUB_DBG,"Subscription","~Subscription() ends") ;

		SubscriptionsTable::subscriptionsUnlock() ;
}

private:

	bool NotificationsAvailable()
	{
		return samplingTimer->NotificationsAvailable() ;
	}

private:

#if ((SUB_DBG) & DEBUG_LEVEL)
		void printCurrentState()
		{
			switch(CurrentState) {
			case SUBSCRIPTION_CLOSED:
				debug(SUB_DBG,"Subscription","CurrentState = SUBSCRIPTION_CLOSED") ;
				break ;
			case SUBSCRIPTION_CREATING:
				debug(SUB_DBG,"Subscription","CurrentState = SUBSCRIPTION_CREATING") ;
				break ;
			case SUBSCRIPTION_NORMAL:
				debug(SUB_DBG,"Subscription","CurrentState = SUBSCRIPTION_NORMAL") ;
				break ;
			case SUBSCRIPTION_LATE:
				debug(SUB_DBG,"Subscription","CurrentState = SUBSCRIPTION_LATE") ;
				break ;
			case SUBSCRIPTION_KEEPALIVE:
				debug(SUB_DBG,"Subscription","CurrentState = SUBSCRIPTION_KEEPALIVE") ;
				break ;
			default:
				debug(SUB_DBG,"Subscription","CurrentState = unknown") ;
				break ;
			}
		}
#endif

public:

	inline void updateMonitoredItemList()
	{
		samplingTimer->setMonitoredItemsList(monitoredItemsList) ;
	}

private:

	void registerMonitoredItem(MonitoredItem * monitoredItem)
	{
		monitoredItemsList = new MonitoredItemsList(monitoredItem,monitoredItemsList) ;
		updateMonitoredItemList() ;
	}

public:

	MonitoredItemCreateResult * createMonitoredItem(
			AddressSpace                * addressSpace,
			TimestampsToReturn          * timestampsToReturn,
			MonitoredItemCreateRequest  * createRequest) ;


public:

	BaseDataType * onReceiveCreateSubscriptionRequest(CreateSubscriptionRequest * request, Session * session)
	{

		debug(SUB_DBG,"Subscription","onReceiveCreateSubscriptionRequest begins") ;

		InitializeSubscription(session) ;

		MessageSent = false ;

		RequestHeader * requestHeader = request->getRequestHeader() ;
		IntegerId     * requestHandle = requestHeader->getRequestHandle() ;

		IntegerId * pSubscriptionId            = new IntegerId(subscriptionId) ;
		Duration  * pRevisedPublishingInterval = new Duration((double)publishingIntervalMs) ;
		Counter   * pRevisedLifetimeCount      = new Counter(lifetimeCount) ;
		Counter   * pRevisedMaxKeppAliveCount  = new Counter(maxKeepAliveCount) ;

		BaseDataType * result =
				new CreateSubscriptionResponse(
					new ResponseHeader(
							UtcTime::now(),
							requestHandle,
							StatusCode::Good,
							DiagnosticInfo::fakeDiagnosticInfo,
							String::tableZero
					),
					pSubscriptionId,
					pRevisedPublishingInterval,
					pRevisedLifetimeCount,
					pRevisedMaxKeppAliveCount
				) ;

		CurrentState = SUBSCRIPTION_NORMAL ;

		debug(SUB_DBG,"Subscription","onReceiveCreateSubscriptionRequest succeeds : returning Response (NORMAL).") ;

		return result ;
	}

private:

	void onLifetimeTimerExpires()
	{
		debug(SUB_DBG,"Subscription","onLifetimeTimerExpires begins") ;
#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE : // 27
			DeleteMonitoredItems() ;
			if (PublishingReqQueued()) {
				PublishingReq * publishingReq = DequeuePublishReq() ;
				IssueStatusChangeNotification(publishingReq,StatusCode::Bad_Timeout) ;
				delete publishingReq ;
			}
			CloseSubscription() ;
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif
		debug(SUB_DBG,"Subscription","onLifetimeTimerExpires ends") ;
	}

public:

	void onTimerExpires() //onPublishingTimerExpires()
	{
		SubscriptionsTable::subscriptionsLock() ;

		debug(SUB_DBG,"Subscription","onTimerExpires (onPublishingTimerExpires) begins") ;
#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
			break ;
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
			if (PublishingReqQueued()) {
				if (PublishingEnabled) {
					if (NotificationsAvailable()) { // 6
						StartPublishingTimer() ;
						PublishingReq * publishingReq = DequeuePublishReq() ;
						ReturnNotifications(publishingReq) ;
						delete publishingReq ;
						MessageSent = true ;
					} else {
						if (MessageSent) { // 9
							StartPublishingTimer() ;
							ResetKeepAliveCounter() ;
							CurrentState = SUBSCRIPTION_KEEPALIVE ;
						} else { // 7
							StartPublishingTimer() ;
							PublishingReq * publishingReq = DequeuePublishReq() ;
							ReturnKeepAlive(publishingReq) ;
							delete publishingReq ;
							MessageSent = true ;
						}
					}
				} else {
					if (MessageSent) { // 9
						StartPublishingTimer() ;
						ResetKeepAliveCounter() ;
						CurrentState = SUBSCRIPTION_KEEPALIVE ;
					} else { // 7
						StartPublishingTimer() ;
						PublishingReq * publishingReq = DequeuePublishReq() ;
						ReturnKeepAlive(publishingReq) ;
						delete publishingReq ;
						MessageSent = true ;
					}
				}
			} else {
				if (PublishingEnabled) {
					if (NotificationsAvailable()) { // 8
						StartPublishingTimer() ;
						CurrentState = SUBSCRIPTION_LATE ;
					} else {
						if (MessageSent) { // 9
							StartPublishingTimer() ;
							ResetKeepAliveCounter() ;
							CurrentState = SUBSCRIPTION_KEEPALIVE ;
						} else { // 8
							StartPublishingTimer() ;
							CurrentState = SUBSCRIPTION_LATE ;
						}
					}
				} else {
					if (MessageSent) { // 9
						StartPublishingTimer() ;
						ResetKeepAliveCounter() ;
						CurrentState = SUBSCRIPTION_KEEPALIVE ;
					} else { // 8
						StartPublishingTimer() ;
						CurrentState = SUBSCRIPTION_LATE ;
					}
				}
			}
			break ;
		case SUBSCRIPTION_LATE : // 12
			StartPublishingTimer() ;
			break ;
		case SUBSCRIPTION_KEEPALIVE :
			if (PublishingEnabled) {
				if (NotificationsAvailable()) {
					if (PublishingReqQueued()) { // 14
						StartPublishingTimer() ;
						PublishingReq * publishingReq = DequeuePublishReq() ;
						ReturnNotifications(publishingReq) ;
						delete publishingReq ;
						MessageSent = true ;
						CurrentState = SUBSCRIPTION_NORMAL ;
					} else {
						if (KeepAliveCounter == 1) { // 17
							StartPublishingTimer() ;
							CurrentState = SUBSCRIPTION_LATE ;
						} else { // 17
							StartPublishingTimer() ;
							CurrentState = SUBSCRIPTION_LATE ;
						}
					}
				} else {
					if (PublishingReqQueued()) {
						if (KeepAliveCounter == 1) { // 15
							StartPublishingTimer() ;
							PublishingReq * publishingReq = DequeuePublishReq() ;
							ReturnKeepAlive(publishingReq) ;
							delete publishingReq ;
							ResetKeepAliveCounter() ;
						} else { // 16
							StartPublishingTimer() ;
							KeepAliveCounter -- ;
						}
					} else {
						if (KeepAliveCounter == 1) { // 17
							StartPublishingTimer() ;
							CurrentState = SUBSCRIPTION_LATE ;
						} else { // 16
							StartPublishingTimer() ;
							KeepAliveCounter -- ;
						}
					}
				}
			} else {
				if (PublishingReqQueued()) {
					if (KeepAliveCounter == 1) { // 15
						StartPublishingTimer() ;
						PublishingReq * publishingReq = DequeuePublishReq() ;
						ReturnKeepAlive(publishingReq) ;
						delete publishingReq ;
						ResetKeepAliveCounter() ;
					} else { // 16
						StartPublishingTimer() ;
						KeepAliveCounter -- ;
					}
				} else {
					if (KeepAliveCounter == 1) { // 17
						StartPublishingTimer() ;
						CurrentState = SUBSCRIPTION_LATE ;
					} else { // 16
						StartPublishingTimer() ;
						KeepAliveCounter -- ;
					}
				}
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif
		debug(SUB_DBG,"Subscription","onTimerExpires (onPublishingTimerExpires) ends") ;

		SubscriptionsTable::subscriptionsUnlock() ;
	}

public:

	BaseDataType * onReceivePublishRequest(
			SOPC_StatusCode            * pStatus,
			uint32_t					 scConnectionId,
		    uint32_t                     requestId,
			PublishRequest             * request)
	{
		debug(SUB_DBG,"Subscription","onReceivePublishRequest begins") ;

#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif

		BaseDataType      * result  = NULL ;
		TableStatusCode   * results = NULL ;

 		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
			if (PublishingEnabled) {
				if (MoreNotifications()) { // 5
					ResetLifetimeCounter() ;
					results = DeleteAckedNotificationMsgs(request) ;
					ReturnNotifications(pStatus, scConnectionId, requestId, request, results) ;
					MessageSent = true ;
				} else { // 4
					ResetLifetimeCounter() ;
					results = DeleteAckedNotificationMsgs(request) ;
					result = EnqueuePublishingReq(pStatus, scConnectionId, requestId, request, results) ;
				}
			} else { // 4
				ResetLifetimeCounter() ;
				results = DeleteAckedNotificationMsgs(request) ;
				result = EnqueuePublishingReq(pStatus, scConnectionId, requestId, request, results) ;
			}
			break ;
		case SUBSCRIPTION_LATE :
			if (PublishingEnabled) {
				if (NotificationsAvailable()) { // 10
					ResetLifetimeCounter() ;
					results = DeleteAckedNotificationMsgs(request) ;
					ReturnNotifications(pStatus, scConnectionId, requestId, request, results) ;
					MessageSent = true ;
					CurrentState = SUBSCRIPTION_NORMAL ;
				} else {
					if (MoreNotifications()) { // 10
						ResetLifetimeCounter() ;
						results = DeleteAckedNotificationMsgs(request) ;
						ReturnNotifications(pStatus, scConnectionId, requestId, request, results) ;
						MessageSent = true ;
						CurrentState = SUBSCRIPTION_NORMAL ;
					} else { // 11
						ResetLifetimeCounter() ;
						results = DeleteAckedNotificationMsgs(request) ;
						ReturnKeepAlive(pStatus, scConnectionId, requestId, request, results) ;
						MessageSent = true ;
						CurrentState = SUBSCRIPTION_KEEPALIVE ;
					}
				}
			} else { // 11
				ResetLifetimeCounter() ;
				results = DeleteAckedNotificationMsgs(request) ;
				ReturnKeepAlive(pStatus, scConnectionId, requestId, request, results) ;
				MessageSent = true ;
				CurrentState = SUBSCRIPTION_KEEPALIVE ;
			}
			break ;
		case SUBSCRIPTION_KEEPALIVE : // 13
			ResetLifetimeCounter() ;
			results = DeleteAckedNotificationMsgs(request) ;
			result = EnqueuePublishingReq(pStatus, scConnectionId, requestId, request, results) ;
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

#if ((SUB_DBG) & DEBUG_LEVEL)
		printCurrentState() ;
#endif
		debug(SUB_DBG,"Subscription","onReceivePublishRequest ends") ;

		return result ;
	}

public:

	NotificationMessage * onReceiveRepublishRequest(uint32_t retransmitSequenceNumber)
	{
		debug(SUB_DBG,"Subscription","onReceiveRepublishRequest begins") ;

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE :
			{
				NotificationMessage * notificationMessage = RequestedMessageFound(retransmitSequenceNumber) ;
				ResetLifetimeCounter() ;
				if (notificationMessage != NULL) { // 20
					debug(SUB_DBG,"Subscription","onReceiveRepublishRequest succeeds (message found)") ;
					return notificationMessage ;
				} else { // 21
					debug(SUB_DBG,"Subscription","onReceiveRepublishRequest failed (message not found)") ;
					return NULL ;
				}
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

		debug(SUB_DBG,"Subscription","onReceiveRepublishRequest fails ?") ;

   		return NULL ; // for compiler not to complain
	}

public:

	void onReceiveModifySubscriptionRequest(
			double   * _publishingInterval,
			uint32_t * _lifetimeCount,
			uint32_t * _maxKeepAliveCount,
			uint32_t   _maxNotificationsPerPublish,
			uint8_t    _priority
		)
	{
		debug(SUB_DBG,"Subscription","onReceiveModifySubscriptionRequest begins") ;

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE :
			{   // 18
				ResetLifetimeCounter() ;
				UpdateSubscriptionParams(
						_publishingInterval,
						_lifetimeCount,
						_maxKeepAliveCount,
						_maxNotificationsPerPublish,
						_priority
						) ;
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

		debug(SUB_DBG,"Subscription","onReceiveModifySubscriptionRequest ends") ;
	}

public:

	StatusCode * onReceiveTransferSubscriptionRequest(
						uint32_t                        secureChannelIdNum,
						Session                       * session,
						TransferSubscriptionsRequest  * request,
						bool                            sendInitialValue,
						TableCounter                 ** availableSequenceNumber)
	{
		NOT_USED(sendInitialValue) ; // TBD
		NOT_USED(request) ; // After S2OPC

		debug(SUB_DBG,"Subscription","onReceiveTransferSubscriptionRequest begins") ;

		StatusCode * result = NULL ;

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE :
			if (SessionChanged(session)) {
				if (ClientValidated(session)) { // 23
					BindSession(secureChannelIdNum,session) ;
					ResetLifetimeCounter() ;
					//DeleteAckedNotificationMsgs(request) ; // cited in specs but how to you want to delete from TransferSubscriptionsRequest ?
					result = StatusCode::Good ;
					if (PublishingReqQueued()) {
						PublishingReq * publishingReq = DequeuePublishReq() ;
						IssueStatusChangeNotification(publishingReq,StatusCode::Good_SubscriptionTransferred) ;
						delete publishingReq ;
					}
					if (notificationMessagesList == NULL) {
						* availableSequenceNumber = Counter::tableZero ;
					} else {
						* availableSequenceNumber = notificationMessagesList->getAvailableSequenceNumbers() ;
					}
				} else { // 24
					result = StatusCode::Bad_UserAccessDenied ;
					* availableSequenceNumber = Counter::tableZero ;
				}
			} else { // 22
				ResetLifetimeCounter() ;
				result = StatusCode::Bad_NothingToDo ;
				* availableSequenceNumber = Counter::tableZero ;
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

		debug(SUB_DBG,"Subscription","onReceiveTransferSubscriptionRequest ends (returning result)") ;

   		return result ;
	}

public:

	StatusCode * onReceiveDeleteSubscriptionRequest(Session * session)
	{
		debug(SUB_DBG,"Subscription","onReceiveDeleteSubscriptionRequest begins") ;

		StatusCode * result = NULL ;

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE :
			if (SubscriptionAssignedToClient(session)) { // 25
				DeleteMonitoredItems() ;
				DeleteClientPublReqQueue() ;
				CloseSubscription() ;
				result = StatusCode::Good ;
			} else { // 26
				ResetLifetimeCounter() ;
				result = StatusCode::Bad_UserAccessDenied ;
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

		debug(SUB_DBG,"Subscription","onReceiveDeleteSubscriptionRequest ends (returning result)") ;

		return result ;
	}

public:

	StatusCode * onReceiveSetPublishingModeRequest(bool PublishingEnabled)
	{
		debug(SUB_DBG,"Subscription","onReceiveSetPublishingModeRequest begins") ;

		StatusCode * result = NULL ;

		switch (CurrentState) {
		case SUBSCRIPTION_CLOSED :
		case SUBSCRIPTION_CREATING :
			CHECK_INT(0) ;
			break ;
		case SUBSCRIPTION_NORMAL:
		case SUBSCRIPTION_LATE :
		case SUBSCRIPTION_KEEPALIVE :
			{   // 19
				ResetLifetimeCounter() ;
				result = SetPublishingEnabled(PublishingEnabled) ;
				//MoreNotifications = false ;
			}
			break ;
		default:
			CHECK_INT(0) ;
			break ;
		}

		debug(SUB_DBG,"Subscription","onReceiveSetPublishingModeRequest ends (returning result)") ;

		return result ;
	}

private:

	bool MoreNotifications()
	{
		MonitoredItemsList * tmp = monitoredItemsList ;

		while (tmp != NULL) {
			if (tmp->getCar()->isNotificationsQueueNotEmpty()) {
				debug(SUB_DBG,"Subscription","MoreNotifications() returns TRUE") ;
				return true ;
			}
			tmp = tmp->getCdr() ;
		}

		debug(SUB_DBG,"Subscription","MoreNotifications() returns FALSE") ;
		return false ;
	}

private:

	void CloseSubscription()
	{
		debug(SUB_DBG,"Subscription","CloseSubscription() begins") ;
		PublishingTimer->stop() ;
		samplingTimer->stop() ;
		CurrentState = SUBSCRIPTION_CLOSED ;
		debug(SUB_DBG,"Subscription","CloseSubscription() ends") ;
	}

public:

	inline bool isClosed()
	{
		if (CurrentState == SUBSCRIPTION_CLOSED) {
			debug(SUB_DBG,"Subscription","isClosed() returns TRUE") ;
		} else {
			debug(SUB_DBG,"Subscription","isClosed() returns FALSE") ;
		}
		return (CurrentState == SUBSCRIPTION_CLOSED) ;
	}

private:

	void SetSession(Session * _session)
	{
		debug(SUB_DBG,"Subscription","SetSession(..)") ;
		(session = _session)->addSubscription(this) ;
	}

public:

	inline void UnsetSession()
	{
		debug(SUB_DBG,"Subscription","UnsetSession()") ;
		session->deleteSubscription(this) ;
		session = NULL ;
	}

private:

	void BindSession(uint32_t secureChannelIdNum, Session * _session)
	{
		debug(SUB_DBG,"Subscription","BindSession(..) begins") ;

		if (session->onlyOneSubscription()) {

			PublishingReqQueue * publishingReqQueue = session->getPublishingReqQueue() ;

			while (publishingReqQueue->isNotEmpty()) {
				PublishingReq * publishingReq = publishingReqQueue->pop() ;
				/* send the response */
				uint32_t requestId = publishingReq->getRequestId() ;
				uint32_t requestHandle = publishingReq->getRequestHeader()->getRequestHandle()->get() ;

				ServerService_SendError(secureChannelIdNum,requestId,requestHandle,_Bad_NoSubscription) ;

				delete publishingReq ;
			}
		}

		UnsetSession() ;
		SetSession(_session) ;

		debug(SUB_DBG,"Subscription","BindSession(..) ends gracefully") ;
	}

private:

	NotificationMessage * RequestedMessageFound(uint32_t retransmitSequenceNumber)
	{
		debug_i(SUB_DBG,"Subscription","RequestedMessageFound(%u) begins",retransmitSequenceNumber) ;

		if (notificationMessagesList == NULL) {
			debug_i(SUB_DBG,"Subscription","RequestedMessageFound(%u) failed (notificationMessagesList NULL)",retransmitSequenceNumber) ;
			return NULL ;
		}

		NotificationMessage * result = notificationMessagesList->searchFor(retransmitSequenceNumber) ;

		if (result == NULL) {
			debug_i(SUB_DBG,"Subscription","RequestedMessageFound(%u) failed (no such message)",retransmitSequenceNumber) ;
		} else {
			debug_i(SUB_DBG,"Subscription","RequestedMessageFound(%u) succeeds (there is such message)",retransmitSequenceNumber) ;
		}

		return result ;
	}

private:

	bool DeleteOneAckedNotificationMsg(uint32_t sequenceNumber)
	{
		debug_i(SUB_DBG,"Subscription","DeleteOneAckedNotificationMsg(%u) begins",sequenceNumber) ;

		NotificationMessage * notificationMessage = RequestedMessageFound(sequenceNumber) ;

		if (notificationMessage == NULL) {
			debug_i(SUB_DBG,"Subscription","DeleteOneAckedNotificationMsg(%u) failed (no such message)",sequenceNumber) ;
			return false ;
		}

		notificationMessagesList = (NotificationMessagesList *)NotificationMessagesList::deleteElement(notificationMessagesList,notificationMessage) ;
		debug_i(SUB_DBG,"Subscription","DeleteOneAckedNotificationMsg(%u) succeeds",sequenceNumber) ;

		return true ;
	}

public:

	TableStatusCode   * DeleteAckedNotificationMsgs(PublishRequest * publishRequest)
	{
		debug(SUB_DBG,"Subscription","DeleteAckedNotificationMsgs(PublishRequest *) begins") ;

		TableSubscriptionAcknowledgement   * subscriptionAcknowledgements = publishRequest->getSubscriptionAcknowledgements() ;
		int32_t                              length                       = subscriptionAcknowledgements->getLength() ;

		TableStatusCode   * results = new TableStatusCode  (length) ;

		for (int i = 0 ; i < length ; i++) {

			SubscriptionAcknowledgement * subscriptionAcknowledgement = subscriptionAcknowledgements->get(i) ;

			if (subscriptionAcknowledgement->getSubscriptionId()->equals(subscriptionId)) {
				if (DeleteOneAckedNotificationMsg(subscriptionAcknowledgement->getSequenceNumber()->get())) {
					results->set(i,StatusCode::Good) ;
				} else {
					results->set(i,StatusCode::Bad_SequenceNumberUnknown) ;
				}
			} else {
				results->set(i,StatusCode::Bad_SubscriptionIdInvalid) ;
			}
		}

		debug(SUB_DBG,"Subscription","DeleteAckedNotificationMsgs(PublishRequest *) ends") ;

		return results ;
	}


private:

	void ReturnOneNotification(
			SOPC_StatusCode            * pStatus,
			uint32_t                     scConnectionId,
			uint32_t                     requestId,
			RequestHeader              * requestHeader,
			NotificationMessage        * notificationMessage,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","ReturnOneNotification(..) begins") ;

		PublishResponse * result
		     = new PublishResponse(
		    		 new ResponseHeader(
		    				 UtcTime::now(),
		    				 requestHeader->getRequestHandle(),
		    				 StatusCode::Good,
		    				 DiagnosticInfo::fakeDiagnosticInfo,
		    				 String::tableZero
		    		 ),
		    		 new IntegerId(subscriptionId),
		    		 (notificationMessagesList == NULL)?(Counter::tableZero):(notificationMessagesList->getAvailableSequenceNumbers()),
		    		 new Boolean(MoreNotifications()),
		    		 notificationMessage,
					 results,
		    		 DiagnosticInfo::tableZero
		     ) ;

		debug(SUB_DBG,"Subscription","ReturnOneNotification(..) adding message in old message list") ;

		notificationMessagesList = new NotificationMessagesList(notificationMessage,notificationMessagesList) ;

		// Transferring the message to C

		OpcUa_ResponseHeader responseHeader ;
		OpcUa_ResponseHeader_Initialize(&responseHeader) ;

		OpcUa_PublishResponse response ;
		OpcUa_PublishResponse_Initialize(&response) ;

		SOPC_StatusCode status = SOPC_STATUS_OK ;

		result->fromCpptoC(&status,responseHeader,response) ;
		result->checkRefCount() ;

		if (status != SOPC_STATUS_OK) {
			debug(COM_ERR,"Subscription","ReturnOneNotificatio, cannot transfer PublishRequest t C") ;
			goto error ;
		}

		ServerService_SendResponse(
		    scConnectionId,
			requestId,
			&OpcUa_PublishResponse_EncodeableType,
		    &responseHeader,
		    (void *)&response) ;


		debug(SUB_DBG,"Subscription","ReturnOneNotification(..) after SOPC_Endpoint_SendResponse") ;

error:
		if (*pStatus != _Good)
			debug_i(SUB_DBG,"Subscription","ReturnOneNotification(..) SOPC_Endpoint_SendResponse ends with rc=0x%08x",*pStatus) ;

		/* ?? */

		OpcUa_ResponseHeader_Clear(&responseHeader) ;
	    OpcUa_PublishResponse_Clear(&response);

		debug(SUB_DBG,"Subscription","ReturnOneNotification(..) ends gracefully") ;
	}

private:

	void ReturnNotifications(
			SOPC_StatusCode            * pStatus,
			uint32_t					 scConnectionId,
			uint32_t                     requestId,
			RequestHeader              * requestHeader,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","ReturnNotifications a (..) begins") ;

		NotificationMessage * notificationMessage = CreateNotificationMsg() ;

		if (notificationMessage == NULL) {
			debug(SUB_DBG,"Subscription","ReturnNotifications a (..) ends (no message [1])") ;
			return ;
		}

		debug(SUB_DBG,"Subscription","ReturnNotifications a (..) one message [1]") ;
		ReturnOneNotification(
				pStatus,
				scConnectionId,
				requestId,
				requestHeader,
				notificationMessage,
				results) ;

		while (MoreNotifications() && PublishingReqQueued()) {

			PublishingReq * publishingReq = DequeuePublishReq() ;

			notificationMessage = CreateNotificationMsg() ;

			if (notificationMessage == NULL) {
				debug(SUB_DBG,"Subscription","ReturnNotifications a (..) ends (no message [2])") ;
				return ;
			}

			debug(SUB_DBG,"Subscription","ReturnNotifications a (..) one message [2]") ;
			ReturnOneNotification(
					pStatus,
					scConnectionId,
					requestId,
					publishingReq->getRequestHeader(),
					notificationMessage,
					StatusCode::tableZero) ;
		}

		debug(SUB_DBG,"Subscription","ReturnNotifications a (..) ends") ;
	}

private:

	void ReturnNotifications(
			SOPC_StatusCode            * pStatus,
			uint32_t                     scConnectionId,
			uint32_t                     requestId,
			PublishRequest             * publishRequest,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","ReturnNotifications b (SOPC_Endpoint *, SOPC_RequestContext *, PublishRequest *) transferring") ;
		ReturnNotifications(
				pStatus,
				scConnectionId,
				requestId,
				publishRequest->getRequestHeader(),
				results) ;
	}

private:

	void ReturnNotifications(PublishingReq * publishingReq)
	{
		debug(SUB_DBG,"Subscription","ReturnNotifications c (PublishingReq *) transferring") ;
		SOPC_StatusCode status   = STATUS_OK ;

		ReturnNotifications(
				&status,
				publishingReq->getScConnectionId(),
				publishingReq->getRequestId(),
				publishingReq->getRequestHeader(),
				publishingReq->getResults()) ;
	}

private:

	void ReturnKeepAlive(
			SOPC_StatusCode            * pStatus,
			uint32_t				     scConnectionId,
			uint32_t                     requestId,
			RequestHeader              * requestHeader,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","ReturnKeepAlive(...) begins") ;
		ReturnOneNotification(
				pStatus,
				scConnectionId,
				requestId,
				requestHeader,
				CreateEmptyNotificationMsg(),
				results) ;
		debug(SUB_DBG,"Subscription","ReturnKeepAlive(...) ends") ;
	}

private:

	void ReturnKeepAlive(
			SOPC_StatusCode            * pStatus,
			uint32_t				     scConnectionId,
			uint32_t                     requestId,
			PublishRequest             * publishRequest,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","ReturnKeepAlive(SOPC_Endpoint *, SOPC_RequestContext *, PublishRequest *) transferring") ;

		ReturnKeepAlive(
				pStatus,
				scConnectionId,
				requestId,
				publishRequest->getRequestHeader(),
				results) ;
	}

private:

	void ReturnKeepAlive(PublishingReq * publishingReq)
	{
		debug(SUB_DBG,"Subscription","ReturnKeepAlive(PublishingReq *) transferring") ;
		SOPC_StatusCode status   = STATUS_OK ;
		ReturnKeepAlive(
				&status,
				publishingReq->getScConnectionId(),
				publishingReq->getRequestId(),
				publishingReq->getRequestHeader(),
				publishingReq->getResults()) ;
	}

private:

	NotificationMessage * CreateEmptyNotificationMsg()
	{
		debug(SUB_DBG,"Subscription","CreateEmptyNotificationMsg() returning") ;

		uint32_t next = nextNotificationSequenceNumber ;
		nextNotificationSequenceNumber = (nextNotificationSequenceNumber == UINT32_MAX)?1:(nextNotificationSequenceNumber + 1) ;

		return
				new NotificationMessage(
						new Counter(next),
						UtcTime::now(),
						new TableNotificationData  (0)
						) ;
	}


private:

	NotificationMessage * CreateNotificationMsg()
	{
		debug(SUB_DBG,"Subscription","CreateNotificationMsg() begins") ;

		NotificationDataList * notificationDataList  = NULL ;
		int32_t                cntNotificationData   = 0 ;

		if (monitoredItemsList != NULL) {
			debug(SUB_DBG,"Subscription","CreateNotificationMsg() monitoredItemsList != NULL") ;

			uint32_t maxNotifications = (maxNotificationsPerPublish == 0)?UINT32_MAX:maxNotificationsPerPublish ;
			debug_i(SUB_DBG,"Subscription","CreateNotificationMsg() maxNotifications = %u",maxNotifications) ;

			MonitoredItemsList * tmpMonitoredItemsList = currMonitoredItemsList ;

			int sndRun = 0 ;

			while (maxNotifications != 0) {

				if (tmpMonitoredItemsList == NULL) {
					sndRun++ ;
					if (sndRun == 2)
						break ;
					tmpMonitoredItemsList = monitoredItemsList ;
				}

				MonitoredItem * monitoredItem = tmpMonitoredItemsList->getCar() ;

				while (true) {

					NotificationData * notificationData = monitoredItem->nextNotificationData() ;

					if (notificationData == NULL)
						break ;

					notificationDataList = new NotificationDataList(notificationData,notificationDataList) ;
					cntNotificationData++ ;

					if (--maxNotifications == 0)
						break ;
				}

				tmpMonitoredItemsList = tmpMonitoredItemsList->getCdr() ;

				if (tmpMonitoredItemsList == currMonitoredItemsList)
					break ;
			}

			samplingTimer->setNotifications() ;
		}

		debug_i(SUB_DBG,"Subscription","CreateNotificationMsg() cntNotificationData=%u",cntNotificationData) ;

		TableNotificationData   * result = new TableNotificationData  (cntNotificationData) ;

		while (--cntNotificationData >= 0) {

			result->set(cntNotificationData,notificationDataList->getCar()) ;

			NotificationDataList * tmp = notificationDataList->getCdr() ;
			delete notificationDataList ;
			notificationDataList = tmp ;
		}

		uint32_t next = nextNotificationSequenceNumber ;
		nextNotificationSequenceNumber = (nextNotificationSequenceNumber == UINT32_MAX)?1:(nextNotificationSequenceNumber + 1) ;

		debug_i(SUB_DBG,"Subscription","CreateNotificationMsg() returning %u",next) ;

		return
				new NotificationMessage(
						new Counter(next),
						UtcTime::now(),
						result
						) ;
	}

private:

	void IssueStatusChangeNotification(PublishingReq * publishingReq, StatusCode * statusCode)
	{
		debug(SUB_DBG,"Subscription","IssueStatusChangeNotification() begins") ;

		TableNotificationData   * notificationData = new TableNotificationData  (1) ;
		notificationData->set(0,new StatusChangeNotification(statusCode,DiagnosticInfo::fakeDiagnosticInfo)) ;

		uint32_t next = nextNotificationSequenceNumber ;
		nextNotificationSequenceNumber = (nextNotificationSequenceNumber == UINT32_MAX)?1:(nextNotificationSequenceNumber + 1) ;

		NotificationMessage * notificationMessage
			= new NotificationMessage(
					new Counter(next),
					UtcTime::now(),
					notificationData
					) ;

		SOPC_StatusCode status = STATUS_OK ;

		ReturnOneNotification(
				&status,
				publishingReq->getScConnectionId(),
				publishingReq->getRequestId(),
				publishingReq->getRequestHeader(),
				notificationMessage,
				publishingReq->getResults()) ;

		debug(SUB_DBG,"Subscription","IssueStatusChangeNotification() ends") ;
	}

private:

	void DeleteMonitoredItems()
	{
		debug(SUB_DBG,"Subscription","DeleteMonitoredItems() begins") ;

		while (monitoredItemsList != NULL) {
			DeleteMonitoredItem(monitoredItemsList->getCar()) ;
		}

		debug(SUB_DBG,"Subscription","DeleteMonitoredItems() ends") ;
	}

private:

	void DeleteMonitoredItem(MonitoredItem * monitoredItem)
	{
		debug(SUB_DBG,"Subscription","DeleteMonitoredItem(MonitoredItem * monitoredItem)") ;

		monitoredItemsList = MonitoredItemsList::deleteElement(monitoredItemsList,monitoredItem) ;

		updateMonitoredItemList() ;

		delete monitoredItem ;

		currMonitoredItemsList = NULL ;
	}

public:

	StatusCode * DeleteMonitoredItem(IntegerId * monitoredItemId)
	{
		debug(SUB_DBG,"Subscription","DeleteMonitoredItem(IntegerId * monitoredItemId) begins") ;

		if (monitoredItemsList == NULL) {
			debug(COM_ERR,"Subscription","DeleteMonitoredItem(IntegerId * monitoredItemId) : invalid monitoredItemId (1)") ;
			return StatusCode::Bad_MonitoredItemIdInvalid ;
		}

		MonitoredItem * monitoredItem = monitoredItemsList->searchById(monitoredItemId) ;

		if (monitoredItem == NULL) {
			debug(COM_ERR,"Subscription","DeleteMonitoredItem(IntegerId * monitoredItemId) : invalid monitoredItemId (2)") ;
			return StatusCode::Bad_MonitoredItemIdInvalid ;
		}

		DeleteMonitoredItem(monitoredItem) ;
		return StatusCode::Good ;
	}



public:

	MonitoredItemModifyResult * ModifyMonitoredItem(IntegerId            * monitoredItemId,
													TimestampsToReturn   * timestampToReturn,
													MonitoringParameters * requestedParameters)
	{
		debug(SUB_DBG,"Subscription","ModifyMonitoredItem(..) begins") ;

		if (monitoredItemsList == NULL) {
			debug(COM_ERR,"Subscription","ModifyMonitoredItem(..) : invalid monitoredItemId (1)") ;
			return new MonitoredItemModifyResult(StatusCode::Bad_MonitoredItemIdInvalid) ;
		}

		MonitoredItem * monitoredItem = monitoredItemsList->searchById(monitoredItemId) ;

		if (monitoredItem == NULL) {
			debug(COM_ERR,"Subscription","ModifyMonitoredItem(..) : invalid monitoredItemId (2)") ;
			return new MonitoredItemModifyResult(StatusCode::Bad_MonitoredItemIdInvalid) ;
		}

		if (! monitoredItem->setTimestampToReturn(timestampToReturn)) {
			debug(COM_ERR,"Subscription","ModifyMonitoredItem(..) : cannot set TimestampToReturn") ;
			return new MonitoredItemModifyResult(StatusCode::Bad_TimestampsToReturnInvalid) ;
		}

		return monitoredItem->ModifyMonitoredItem(requestedParameters) ;
	}



public:

	StatusCode * SetMonitoringMode(IntegerId * monitoredItemId, MonitoringMode * monitoringMode)
	{
		debug(SUB_DBG,"Subscription","SetMonitoringMode(..) begins") ;

		if (monitoredItemsList == NULL) {
			debug(COM_ERR,"Subscription","SetMonitoringMode(..) : invalid monitoredItemId (1)") ;
			return StatusCode::Bad_MonitoredItemIdInvalid ;
		}

		MonitoredItem * monitoredItem = monitoredItemsList->searchById(monitoredItemId) ;

		if (monitoredItem == NULL) {
			debug(COM_ERR,"Subscription","SetMonitoringMode(..) : invalid monitoredItemId (2)") ;
			return StatusCode::Bad_MonitoredItemIdInvalid ;
		}

		monitoredItem->setMonitoringMode(monitoringMode) ;
		return StatusCode::Good ;
	}



public:

	void UpdateSubscriptionParams(
			double   * _publishingInterval,
			uint32_t * _lifetimeCount,
			uint32_t * _maxKeepAliveCount,
			uint32_t   _maxNotificationsPerPublish,
			uint8_t    _priority)
	{
		debug(SUB_DBG,"Subscription","UpdateSubscriptionParams(..) begins") ;

		if (* _publishingInterval == 0) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : publishingIntervalMs == 0 (invalid argument)") ;
			* _publishingInterval = publishingIntervalMs ;
		} else {
			publishingIntervalMs = * _publishingInterval ;
		}
		bool error_lifetimeCount     = false ;
		if (* _lifetimeCount <= 3) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : lifetimeCount <= 3 (invalid argument)") ;
			* _lifetimeCount = lifetimeCount ;
			error_lifetimeCount = true ;
		}
		bool error_maxKeepAliveCount = false ;
		if (* _maxKeepAliveCount != 0) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : maxKeepAliveCount == 0 (invalid argument)") ;
			* _maxKeepAliveCount = maxKeepAliveCount ;
			error_maxKeepAliveCount = true ;
		}
		if (lifetimeCount <= 3*maxKeepAliveCount) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : lifetimeCount <= 3*maxKeepAliveCount (invalid argument)") ;
			* _lifetimeCount = lifetimeCount ;
			* _maxKeepAliveCount = maxKeepAliveCount ;
			error_lifetimeCount = true ;
			error_maxKeepAliveCount = true ;
		}
		if (! error_lifetimeCount) {
			lifetimeCount     = * _lifetimeCount     ;
		}
		if (! error_maxKeepAliveCount) {
			maxKeepAliveCount = * _maxKeepAliveCount ;
		}
		if (_maxNotificationsPerPublish != 0) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : maxNotificationsPerPublish != 0 (not implemented)") ;
		}
		if (_priority != 0) {
			debug(COM_ERR,"Subscription","UpdateSubscriptionParams() : priority != 0 (not implemented)") ;
		}

		debug(SUB_DBG,"Subscription","UpdateSubscriptionParams(..) ends") ;
	}

private:

	inline bool SubscriptionAssignedToClient(Session * _session)
	{
		bool result = session->isEqual(_session) ;
		debug_s(SUB_DBG,"Subscription","SubscriptionAssignedToClient(..) returns %s",(result)?"TRUE":"FALSE") ;
		return result ;
	}

private:

	bool ClientValidated(Session * session)
	{
		NOT_USED(session) ;
		debug(SUB_DBG,"Subscription","ClientValidated(..) NOT IMPLEMENTED : returns TRUE") ;
		// TBD : how to know that the Client is operating on behalf the same User ?
		return true ;
	}

private:

	StatusCode * SetPublishingEnabled(bool _PublishingEnabled)
	{
		PublishingEnabled = _PublishingEnabled ;
		debug(SUB_DBG,"Subscription","SetPublishingEnabled(..) : returns StatusCode::Good") ;
		return StatusCode::Good ;
	}

private:

	inline bool SessionChanged(Session * _session)
	{
		bool result = session->isNotEqual(_session) ;
		debug_s(SUB_DBG,"Subscription","SessionChanged(..) returns %s",(result)?"TRUE":"FALSE") ;
		return result ;
	}

private:

	PublishingReq *  DequeuePublishReq()
	{
		debug(SUB_DBG,"Subscription","DequeuePublishReq() begins") ;
		PublishingReq * result = session->getPublishingReqQueue()->pop() ;
		debug(SUB_DBG,"Subscription","DequeuePublishReq(..) returning result") ;
		return result ;
	}

private:

	void DeleteClientPublReqQueue()
	{
		debug(SUB_DBG,"Subscription","DeleteClientPublReqQueue() begins") ;
		if (session->onlyOneSubscription()) {
			PublishingReqQueue * publishingReqQueue = session->getPublishingReqQueue() ;
			while (publishingReqQueue->isNotEmpty()) {
				delete publishingReqQueue->pop() ;
				debug(SUB_DBG,"Subscription","DeleteClientPublReqQueue() one item deleted") ;
			}
		} else {
			debug(SUB_DBG,"Subscription","DeleteClientPublReqQueue() more than one subscription (nothing to do)") ;
		}
		debug(SUB_DBG,"Subscription","DeleteClientPublReqQueue() ends") ;
	}

private:

	inline BaseDataType * EnqueuePublishingReq(
			SOPC_StatusCode            * pStatus,
			uint32_t                     scConnectionId,
			uint32_t                     requestId,
			PublishRequest             * publishRequest,
			TableStatusCode            * results)
	{
		debug(SUB_DBG,"Subscription","EnqueuePublishingReq(SOPC_Endpoint *, SOPC_RequestContext *, PublishRequest *) transferring with queue") ;
		return EnqueuePublishingReq(
				pStatus,
				scConnectionId,
				requestId,
				publishRequest,
				results,
				session->getPublishingReqQueue()) ;
	}

public:

	static BaseDataType * EnqueuePublishingReq(
			SOPC_StatusCode            * pStatus,
			uint32_t                     scConnectionId,
			uint32_t                     requestId,
			PublishRequest             * publishRequest,
			TableStatusCode            * results,
			PublishingReqQueue         * publishingReqQueue)
	{
		debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) begins") ;

		RequestHeader * requestHeader   = publishRequest->getRequestHeader() ;

		PublishingReq * publishingReq
			= new PublishingReq(
					scConnectionId,
					requestId,
					requestHeader,
					results
					) ;

		BaseDataType * result = NULL ;
		debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) setting result to NULL") ;

		while (true) {
			if (publishingReqQueue->tryPush(publishingReq)) {
				debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) pushes and returns result") ;
				return result ;
			} else {
				PublishingReq * publishingReq2 = publishingReqQueue->tryPop() ;
				debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) pops first") ;
				if (publishingReq2 != NULL)
					delete publishingReq2 ;
				if (result == NULL) {
					debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) setting result to StatusCode::Bad_TooManyPublishRequests") ;
					*pStatus = _Bad_TooManyPublishRequests ;
				}
			}
		}

		debug(SUB_DBG,"Subscription","EnqueuePublishingReq(..) returning NULL") ;
		return NULL ;
	}

private:

	inline bool PublishingReqQueued()
	{
		PublishingReqQueue * publishingReqQueue = session->getPublishingReqQueue() ;
		bool result = publishingReqQueue->isNotEmpty() ;
		debug_s(SUB_DBG,"Subscription","PublishingReqQueued() returns %s",(result)?"TRUE":"FALSE") ;
		return result ;
	}

private:

	void StartPublishingTimer()
	{
		debug(SUB_DBG,"Subscription","StartPublishingTimer() begins") ;
		PublishingTimer->start(publishingIntervalMs) ;
		if (--LifetimeCounter == 0) {
			debug(SUB_DBG,"Subscription","StartPublishingTimer() --LifetimeCounter == 0") ;
			onLifetimeTimerExpires() ;
		}
		debug(SUB_DBG,"Subscription","StartPublishingTimer() ends") ;
	}

private:

	inline void ResetKeepAliveCounter ()
	{
		debug(SUB_DBG,"Subscription","ResetKeepAliveCounter() calls") ;
		KeepAliveCounter = maxKeepAliveCount ;
	}

private:

	inline void ResetLifetimeCounter()
	{
		debug(SUB_DBG,"Subscription","ResetLifetimeCounter() calls") ;
		LifetimeCounter = lifetimeCount ;
	}

private:

	inline void InitializeSubscription(Session * session)
	{
		debug(SUB_DBG,"Subscription","InitializeSubscription(..) begins") ;

		subscriptionId = SubscriptionsTable::subscriptionsTable->newSubscriptionId() ;

		ResetLifetimeCounter() ;
		PublishRateChange = false ;
		PublishingEnabled = publishingEnabled ;
		SeqNum = (uint32_t)0 ;
		SetSession(session) ;
		StartPublishingTimer() ;

		debug(SUB_DBG,"Subscription","InitializeSubscription(..) ends") ;
	}

public:

	inline int32_t getSubscriptionId()
	{
		return       subscriptionId ;
	}

public:

	int32_t isOk()
	{
		debug(SUB_DBG,"Subscription","isOk() begins") ;

		if (publishingIntervalMs == 0) {
			debug(COM_ERR,"Subscription","isOk() : publishingIntervalMs == 0 (invalid argument)") ;
			return _Bad_InvalidArgument ;
		}
		if (lifetimeCount <= 3) {
			debug(COM_ERR,"Subscription","isOk() : lifetimeCount <= 3 (invalid argument)") ;
			return _Bad_InvalidArgument ;
		}
		if (maxKeepAliveCount == 0) {
			debug(COM_ERR,"Subscription","isOk() : maxKeepAliveCount == 0 (invalid argument)") ;
			return _Bad_InvalidArgument ;
		}
		if (lifetimeCount <= 3*maxKeepAliveCount) {
			debug(COM_ERR,"Subscription","isOk() : lifetimeCount <= 3*maxKeepAliveCount (invalid argument)") ;
			return _Bad_InvalidArgument ;
		}
		if (maxNotificationsPerPublish != 0) {
			debug(COM_ERR,"Subscription","isOk() : maxNotificationsPerPublish != 0 (not implemented)") ;
			return _Bad_NotImplemented ;
		}
		if (priority != 0) {
			debug(COM_ERR,"Subscription","isOk() : priority != 0 (not implemented)") ;
			return _Bad_NotImplemented ;
		}

		debug(SUB_DBG,"Subscription","isOk() returning Good") ;
		return _Good ;
	}

};

}      /* namespace */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCU_SUBSCRIPTION_H_ */
