
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_PUBLISHINGREQQUEUE_H_
#define OPCUA_PUBLISHINGREQQUEUE_H_

/*
 * OPC-UA Part 4
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_SyncQueue.h"
#include "OpcUa_IPCS_PublishingReq.h"

namespace opcua {

class MYDLL PublishingReqQueue
	: public SyncQueue<PublishingReq>
{
public:

	PublishingReqQueue()
		: SyncQueue<PublishingReq>(EXPECTED_PUBLISH_REQ_QUEUE_LENGTH)
	{}

	virtual ~PublishingReqQueue()
	{}

};

} /* namespace opcua */

#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_PUBLISHINGREQQUEUE_H_ */
