
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_MONITOREDITEMSLIST_H_
#define OPCUA_MONITOREDITEMSLIST_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "OpcUa_MonitoredItem.h"



// A field of Subscription

namespace opcua {

class MYDLL MonitoredItemsList
	: public LinkedList<MonitoredItem>
{
public:

	MonitoredItemsList(MonitoredItem * _car, MonitoredItemsList * _cdr)
		: LinkedList<MonitoredItem>(_car,_cdr)
    {}

	virtual ~MonitoredItemsList()
	{}

public:

	inline MonitoredItemsList * getCdr()
	{
		return dynamic_cast<MonitoredItemsList *>(LinkedList<MonitoredItem>::getCdr()) ;
	}

public:

	inline static MonitoredItemsList * deleteElement(MonitoredItemsList * list, MonitoredItem * element)
	{
		return dynamic_cast<MonitoredItemsList *>(LinkedList<MonitoredItem>::deleteElement(list,element)) ;
	}

public:

	MonitoredItem * searchById(IntegerId * monitoredItemId)
	{
		MonitoredItemsList * monitoredItemsList = this ;
		do {
			MonitoredItem * monitoredItem = monitoredItemsList->getCar() ;
			if (monitoredItem->getMonitoredItemId()->equals(monitoredItemId))
				return monitoredItem ;
			monitoredItemsList = monitoredItemsList->getCdr() ;
		} while (monitoredItemsList != NULL) ;
		return NULL ;
	}

};


} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_MONITOREDITEMSLIST_H_ */
