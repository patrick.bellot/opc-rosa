/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SUBSCRIPTIONACKNOWLEDGEMENT_H_
#define OPCUA_SUBSCRIPTIONACKNOWLEDGEMENT_H_

/*
 * Part 4, 5.13.5.2, p. 86
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL SubscriptionAcknowledgement
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_SubscriptionAcknowledgement ; }

private:

	IntegerId * subscriptionId ;
	Counter   * sequenceNumber ;

public:

	static SubscriptionAcknowledgement * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_SubscriptionAcknowledgement const& pSubscriptionAcknowledgement)
	{
		IntegerId * subscriptionId = IntegerId ::fromCtoCpp( pStatus, pSubscriptionAcknowledgement.SubscriptionId) ;
		Counter   * sequenceNumber = Counter   ::fromCtoCpp( pStatus, pSubscriptionAcknowledgement.SequenceNumber) ;

		if (*pStatus == STATUS_OK)
			return
					new SubscriptionAcknowledgement(
							subscriptionId,
							sequenceNumber
							) ;

		if (subscriptionId != NULL)
			subscriptionId->checkRefCount() ;
		if (sequenceNumber != NULL)
			sequenceNumber->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_SubscriptionAcknowledgement& pSubscriptionAcknowledgement) const
	{
		subscriptionId ->fromCpptoC (pStatus, pSubscriptionAcknowledgement.SubscriptionId) ;
		sequenceNumber ->fromCpptoC (pStatus, pSubscriptionAcknowledgement.SequenceNumber) ;
	}

public:

	SubscriptionAcknowledgement(
			IntegerId * _subscriptionId,
			Counter   * _sequenceNumber
			)
		: Structure()
	{
		(subscriptionId = _subscriptionId) ->take() ;
		(sequenceNumber = _sequenceNumber) ->take() ;
	}

	virtual ~SubscriptionAcknowledgement()
	{
		subscriptionId ->release() ;
		sequenceNumber ->release() ;
	}

public:

	inline IntegerId * getSubscriptionId()
	{
		return subscriptionId ;
	}

	inline Counter * getSequenceNumber()
	{
		return sequenceNumber ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_SUBSCRIPTIONACKNOWLEDGEMENTS_H_ */



