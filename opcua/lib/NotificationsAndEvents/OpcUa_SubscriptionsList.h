
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_SUBSCRIPTIONSLIST_H_
#define OPCUA_SUBSCRIPTIONSLIST_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "OpcUa_IPCS_Subscription.h"

namespace opcua {

class MYDLL SubscriptionsList
	: public LinkedList<Subscription>
{
public:

	SubscriptionsList(Subscription * _car, SubscriptionsList * _cdr)
		: LinkedList<Subscription>(_car,_cdr)
    {}

	virtual ~SubscriptionsList()
	{}

public:

	inline SubscriptionsList * getCdr() { return (SubscriptionsList *)LinkedList<Subscription>::getCdr() ; }

public:

	inline static SubscriptionsList * deleteElement(SubscriptionsList * list, Subscription * elt)
	{
		return (SubscriptionsList *)(LinkedList<Subscription>::deleteElement(list,elt)) ;
	}

};


} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_SUBSCRIPTIONLIST_H_ */
