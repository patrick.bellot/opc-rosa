
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_TRANSFERRESULT_H_
#define OPCUA_TRANSFERRESULT_H_

/*
 * OPC-UA Part 4, 5.13.7.2, p. 88, inside Table 95
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

namespace opcua {

class MYDLL TransferResult
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_TransferResult ; }

private:

	StatusCode     * statusCode ;
	TableCounter   * availableSequenceNumbers ;

public:

	static TransferResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_TransferResult const& pTransferResult)
	{
		StatusCode     * statusCode               = StatusCode ::fromCtoCpp (pStatus, pTransferResult.StatusCode) ;
		TableCounter   * availableSequenceNumbers = TableCounter::fromCtoCpp  (pStatus, pTransferResult.NoOfAvailableSequenceNumbers, pTransferResult.AvailableSequenceNumbers) ;

		if (*pStatus == STATUS_OK)
			return
					new TransferResult(
							statusCode,
							availableSequenceNumbers
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (availableSequenceNumbers != NULL)
			availableSequenceNumbers->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_TransferResult& pTransferResult) const
	{
		statusCode               ->fromCpptoC (pStatus, pTransferResult.StatusCode) ;
		availableSequenceNumbers ->fromCpptoC (pStatus, pTransferResult.NoOfAvailableSequenceNumbers, pTransferResult.AvailableSequenceNumbers) ;
	}

public:

	TransferResult(
			StatusCode     * _statusCode,
			TableCounter   * _availableSequenceNumbers
			)
		: Structure()
	{
		(statusCode               = _statusCode)                ->take() ;
		(availableSequenceNumbers = _availableSequenceNumbers)  ->take() ;
	}

	virtual ~TransferResult()
	{
		statusCode               ->release() ;
		availableSequenceNumbers ->release() ;
	}

public:

	inline StatusCode * getEventsField()
	{
		return statusCode ;
	}

	inline TableCounter   * getAvailableSequenceNumbers()
	{
		return availableSequenceNumbers ;
	}

};

} /* namespace opcua */

#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_TRANSFERRESULT_H_ */
