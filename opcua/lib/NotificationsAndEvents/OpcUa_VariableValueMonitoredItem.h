
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_VARIABLEVALUEMONITOREDITEM_H_
#define OPCUA_VARIABLEVALUEMONITOREDITEM_H_

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "OpcUa_MonitoredItem.h"
#include "OpcUa_IPCS_DataChangeNotification.h"
#include "OpcUa_SamplingTimer.h"

namespace opcua {

class MYDLL VariableValueMonitoredItem
	: public MonitoredItem
{
private:

	Base               * variable ;
	NumericRange       * numericRange ;
	TimestampsToReturn * timestampsToReturn ;
	IntegerId          * clientHandle ;
	MonitoringFilter   * filter ;

public:

	VariableValueMonitoredItem(
			Base                 * _variable,
			NumericRange         * _numericRange,
			TimestampsToReturn   * _timestampsToReturn,
			MonitoringMode       * _monitoringMode,
			MonitoringParameters * _requestedParameters)
		: MonitoredItem(
				_monitoringMode,
				_requestedParameters->getSamplingInterval(),
				_requestedParameters->getQueueSize(),
				_requestedParameters->getDiscardOldest()
				)
	{
		debug(SUB_DBG,"VariableValueMonitoredItem","VariableValueMonitoredItem creation begins") ;

		(variable           = _variable)                               -> take() ;
		(numericRange       = _numericRange)                           -> take() ;
		(timestampsToReturn = _timestampsToReturn)                     -> take() ;
		(clientHandle       = _requestedParameters->getClientHandle()) -> take() ;
		(filter             = _requestedParameters->getFilter())       -> take() ;

		debug(SUB_DBG,"VariableValueMonitoredItem","VariableValueMonitoredItem destruction begins") ;
	}

	virtual ~VariableValueMonitoredItem()
	{
		debug(SUB_DBG,"VariableValueMonitoredItem","VariableValueMonitoredItem destruction begins") ;

		variable           -> release() ;
		numericRange       -> release() ;
		timestampsToReturn -> release() ;
		clientHandle       -> release() ;
		filter             -> release() ;

		debug(SUB_DBG,"VariableValueMonitoredItem","VariableValueMonitoredItem destruction ends") ;
	}

public:

	virtual void setFilter(MonitoringFilter * _filter)
	{
		MonitoringFilter * tmp = filter ;
		(filter = _filter)->take() ;
		tmp->release() ;
	}

public:

	virtual bool setTimestampToReturn(TimestampsToReturn * _timestampsToReturn)
	{
		TimestampsToReturn * tmp = timestampsToReturn ;
		(timestampsToReturn = _timestampsToReturn)->take() ;
		tmp->release() ;
		return true ;
	}

public:

	virtual void go(uint64_t now, SamplingTimer * samplingTimer)
	{
		debug_i(SUB_DBG,"VariableValueMonitoredItem","void go(%llu,samplingTimer)",now) ;

		if (getMonitoringMode() == MonitoringMode_DISABLED_0) {
			debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) with monitoringMode == DISABLED_0, returning") ;
			return ;
		}

		if (isOperationTime(now)) {

			debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) : isOperationTime(now) has returned true") ;

			DataValue * value
				= variable->getAttribute(
							IntegerId::AttributeId_Value_as_IntegerId,
							numericRange,
							getRevisedSamplingInterval(),
							timestampsToReturn
					) ;

			value->take() ;

			debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) : value of variable got") ;

			if (filter->isOk(value,variable)) {

				debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) : filter returns true") ;

				MonitoredItemNotification * monitoredItemNotification
					= new MonitoredItemNotification(
							clientHandle,
							value) ;

				TableMonitoredItemNotification   * monitoredItems
					= new TableMonitoredItemNotification  (1) ;

				monitoredItems->set(0,monitoredItemNotification) ;

				DataChangeNotification * dataChangeNotification
					= new DataChangeNotification(
							monitoredItems,
							DiagnosticInfo::tableZero) ;

				if (! pushNotificationData(dataChangeNotification))
					value->set_Overflow_StatusCode() ;

				if (getMonitoringMode() == MonitoringMode_REPORTING_2) {
					samplingTimer->setNotificationsAvailable(true) ;
				}

				value->release() ;
				return ;
			}

			debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) : filter returns false") ;

			value->release() ;
			return ;
		}

		debug(SUB_DBG,"VariableValueMonitoredItem","void go(uint32_t now, SamplingTimer * samplingTimer) : isOperationTime(now) has returned false") ;
	}

public:

	virtual MonitoredItemCreateResult * getMonitoredItemCreateResult()
	{
		debug(SUB_DBG,"VariableValueMonitoredItem","MonitoredItemCreateResult * getMonitoredItemCreateResult() call") ;

		return
				new MonitoredItemCreateResult(
						StatusCode::Good,
						getMonitoredItemId(),
						getRevisedSamplingInterval(),
						getRevisedQueueSize(),
						filter->getMonitoringFilterResult()
				) ;
	}

public:

	virtual MonitoredItemModifyResult * getMonitoredItemModifyResult()
	{
		debug(SUB_DBG,"VariableValueMonitoredItem","MonitoredItemModifyResult * getMonitoredItemModifyResult() call") ;

		return
				new MonitoredItemModifyResult(
						StatusCode::Good,
						getRevisedSamplingInterval(),
						getRevisedQueueSize(),
						filter->getMonitoringFilterResult()
				) ;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* OPCUA_MONITOREDITEM_H_ */
