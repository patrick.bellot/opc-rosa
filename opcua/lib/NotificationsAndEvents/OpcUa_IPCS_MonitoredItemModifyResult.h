/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_MONITOREDITEMMODIFYRESULT_H_
#define OPCUA_MONITOREDITEMMODIFYRESULT_H_

/*
 * OPC-UA Part 4, 5.13.3.2, p. 69, Table 67 (inside the table)
 */

#include "../OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"

#include "OpcUa_IPCS_MonitoringNullFilterResult.h"

namespace opcua {

class MYDLL MonitoredItemModifyResult
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_MonitoredItemModifyResult; }

private:

	StatusCode             * statusCode ;
	Duration               * revisedSamplingInterval ;
	Counter                * revisedQueueSize ;
	MonitoringFilterResult * filterResult ;

public:

	static MonitoredItemModifyResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemModifyResult const& pMonitoredItemModifyResult)
	{
		StatusCode             * statusCode              = StatusCode             ::fromCtoCpp (pStatus, pMonitoredItemModifyResult.StatusCode) ;
		Duration               * revisedSamplingInterval = Duration               ::fromCtoCpp (pStatus, pMonitoredItemModifyResult.RevisedSamplingInterval) ;
		Counter                * revisedQueueSize        = Counter                ::fromCtoCpp (pStatus, pMonitoredItemModifyResult.RevisedQueueSize) ;
		MonitoringFilterResult * filterResult            = MonitoringFilterResult ::fromCtoCpp (pStatus, pMonitoredItemModifyResult.FilterResult) ;

		if (*pStatus == STATUS_OK)
			return
					new MonitoredItemModifyResult(
							statusCode,
							revisedSamplingInterval,
							revisedQueueSize,
							filterResult
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (revisedSamplingInterval != NULL)
			revisedSamplingInterval->checkRefCount() ;
		if (revisedQueueSize != NULL)
			revisedQueueSize->checkRefCount() ;
		if (filterResult != NULL)
			filterResult->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_MonitoredItemModifyResult& pMonitoredItemModifyResult) const
	{
		statusCode              ->fromCpptoC(pStatus, pMonitoredItemModifyResult.StatusCode) ;
		revisedSamplingInterval ->fromCpptoC(pStatus, pMonitoredItemModifyResult.RevisedSamplingInterval) ;
		revisedQueueSize        ->fromCpptoC(pStatus, pMonitoredItemModifyResult.RevisedQueueSize) ;
		filterResult            ->fromCpptoC(pStatus, pMonitoredItemModifyResult.FilterResult) ;
	}

public:

	MonitoredItemModifyResult(
			StatusCode             * _statusCode,
			Duration               * _revisedSamplingInterval,
			Counter                * _revisedQueueSize,
			MonitoringFilterResult * _filterResult
			)
		: Structure()
	{
		(statusCode              = _statusCode)              ->take() ;
		(revisedSamplingInterval = _revisedSamplingInterval) ->take() ;
		(revisedQueueSize        = _revisedQueueSize)        ->take() ;
		(filterResult            = _filterResult)            ->take() ;
	}

	MonitoredItemModifyResult(StatusCode * _statusCode)
		: Structure()
	{
		(statusCode              = _statusCode)                                  ->take();
		(revisedSamplingInterval = Duration::zero)                               ->take();
		(revisedQueueSize        = Counter::zero)                                ->take();
		(filterResult            = MonitoringNullFilterResult::nullFilterResult) ->take();
	}


	virtual ~MonitoredItemModifyResult()
	{
		statusCode              ->release();
		revisedSamplingInterval ->release();
		revisedQueueSize        ->release();
		filterResult            ->release();
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode;
	}

	inline Duration * getRevisedSamplingInterval()
	{
		return revisedSamplingInterval;
	}

	inline Counter * getRevisedQueueSize()
	{
		return revisedQueueSize;
	}

	inline MonitoringFilterResult * getFilterResult()
	{
		return filterResult;
	}

};
} /* namespace opcua */
#endif /* WITH_SUBSCRIPTION */
#endif /* OPCUA_MONITOREDITEMMODIFYRESULT_H_ */
