
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_EUINFORMATION_H_
#define OPCUA_EUINFORMATION_H_

/*
 * OPC-UA Part 8, 5.5.3, p. 10
 * Engineering Units
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua {

class EUInformation
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_EUInformation ; }

private:

	String        * namespaceUri ;
	Int32         * unitId ;
	LocalizedText * displayName ;
	LocalizedText * description ;

public:

	static EUInformation * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_EUInformation const& pEUInformation)
	{
		String        * namespaceUri = String        ::fromCtoCpp(pStatus, pEUInformation.NamespaceUri) ;
		Int32         * unitId       = Int32         ::fromCtoCpp(pStatus, pEUInformation.UnitId) ;
		LocalizedText * displayName  = LocalizedText ::fromCtoCpp(pStatus, pEUInformation.DisplayName) ;
		LocalizedText * description  = LocalizedText ::fromCtoCpp(pStatus, pEUInformation.Description) ;

		if (*pStatus == STATUS_OK)
			return
					new EUInformation(
							namespaceUri,
							unitId,
							displayName,
							description
							) ;

		if (namespaceUri != NULL)
			namespaceUri->checkRefCount() ;
		if (unitId != NULL)
			unitId->checkRefCount() ;
		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_EUInformation& pEUInformation) const
	{
		namespaceUri ->fromCpptoC(pStatus, pEUInformation.NamespaceUri) ;
		unitId       ->fromCpptoC(pStatus, pEUInformation.UnitId) ;
		displayName  ->fromCpptoC(pStatus, pEUInformation.DisplayName) ;
		description  ->fromCpptoC(pStatus, pEUInformation.Description) ;
	}

public:

	EUInformation(
			String        * _namespaceUri,
			Int32         * _unitId,
			LocalizedText * _displayName,
			LocalizedText * _description
			)
	{
		(namespaceUri = _namespaceUri) ->take() ;
		(unitId       = _unitId)       ->take() ;
		(displayName  = _displayName)  ->take() ;
		(description  = _description)  ->take() ;
	}

	virtual ~EUInformation()
	{
		namespaceUri -> release() ;
		unitId       -> release() ;
		displayName  -> release() ;
		description  -> release() ;
	}

public:

	String * getNamespaceUri()
	{
		return namespaceUri ;
	}

	Int32 * getUnitId()
	{
		return unitId ;
	}

	LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	LocalizedText * getDescription()
	{
		return description ;
	}

public:

	static EUInformation * get_EUInformation_element(const char * const file_str, node_t * node) ;

};
} /* namespace opcua */
#endif /* OPCUA_EUINFORMATION_H_ */
