
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "OpcUa_IPCS_Range.h"
#include "../StandardDataTypes/OpcUa_IPCS_Double.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

Range * Range::get_Range_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	Double  * low  = NULL ;
	Double  * high = NULL ;

	int nb = roxml_get_chld_nb(node) ;

	for (int i = 0 ; i < nb ; i++) {

		node_t * chld = roxml_get_chld(node,NULL,i) ;
		char *   name = roxml_get_name(chld,NULL,0) ;

		debug_ss(XML_DBG,"Range","Configuration file \"%s\", Range element = \"%s\"",file_str,name) ;

		if (StringUtils::strcmpi(name,"Low") == 0 && low == NULL) {
			if ((low = Double::get_Double_element(file_str,chld)))
				low->take() ;
		} else
		if (StringUtils::strcmpi(name,"High") == 0 && high == NULL) {
			if ((high = Double::get_Double_element(file_str,chld)))
				high->take() ;
		} else {
			debug_ss(COM_ERR,"Range","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"Range\"",file_str,name) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(name) ;
	}

	if (low == NULL)
		(low = Double::zero)->take() ;

	if (high == NULL)
		(high = Double::zero)->take() ;

	Range * result = new Range(
			low,
			high
			) ;

	low  ->release() ;
	high ->release() ;

	return result ;
}

} /* namespace opcua */
