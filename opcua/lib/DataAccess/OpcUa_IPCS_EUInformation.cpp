
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "OpcUa_IPCS_EUInformation.h"
#include "../StandardDataTypes/OpcUa_IPCS_Double.h"
#include "../Server/OpcUa_NodesTable.h"

namespace opcua {

EUInformation * EUInformation::get_EUInformation_element(const char * const file_str, node_t * node)
{
	if (NodesTable::bad_ns(node,file_str))
		return NULL ;

	String        * namespaceUri = NULL ;
	Int32         * unitld       = NULL ;
	LocalizedText * displayName  = NULL ;
	LocalizedText * description  = NULL ;

	int nb = roxml_get_chld_nb(node) ;

	for (int i = 0 ; i < nb ; i++) {

		node_t * chld = roxml_get_chld(node,NULL,i) ;
		char *   name = roxml_get_name(chld,NULL,0) ;

		debug_ss(XML_DBG,"EUInformation","Configuration file \"%s\", EUInformation element = \"%s\"",file_str,name) ;

		if (StringUtils::strcmpi(name,"NamespaceUri") == 0 && namespaceUri == NULL) {
			if ((namespaceUri = String::get_String_element(file_str,chld)))
				namespaceUri->take() ;
		} else
		if (StringUtils::strcmpi(name,"Unitld") == 0 && unitld == NULL) {
			if ((unitld = Int32::get_Int32_element(file_str,chld)))
				unitld->take() ;
		} else
		if (StringUtils::strcmpi(name,"DisplayName") == 0 && displayName == NULL) {
			if ((displayName = LocalizedText::get_LocalizedText_element(file_str,chld)))
				displayName->take() ;
		} else
		if (StringUtils::strcmpi(name,"Description") == 0 && description == NULL) {
			if ((description = LocalizedText::get_LocalizedText_element(file_str,chld)))
				description->take() ;
		} else {
			debug_ss(COM_ERR,"EUInformation","Configuration file \"%s\", Error : unknown or duplicate element \"%s\" in \"Range\"",file_str,name) ;
			NodesTable::print(file_str,node,1000) ;
		}

		roxml_release(name) ;
	}

	EUInformation * result ;

	if (namespaceUri == NULL) {
		debug_s(COM_ERR,"EUInformation","Configuration file \"%s\", EUInformation element = \"NamespaceUri\" is not present",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}
	if (unitld == NULL) {
		debug_s(COM_ERR,"EUInformation","Configuration file \"%s\", EUInformation element = \"Unitld\" is not present",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}
	if (displayName == NULL) {
		debug_s(COM_ERR,"EUInformation","Configuration file \"%s\", EUInformation element = \"DisplayName\" is not present",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}
	if (description == NULL) {
		debug_s(COM_ERR,"EUInformation","Configuration file \"%s\", EUInformation element = \"Description\" is not present",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}
	if (unitld->get() == -1) {
		debug_s(COM_ERR,"EUInformation","Configuration file \"%s\", EUInformation element = \"Unitld\" must not be -1",file_str) ;
		NodesTable::print(file_str,node,1000) ;
		result = NULL ;
		goto error ;
	}

	result = new EUInformation(
			namespaceUri,
			unitld,
			displayName,
			description
			) ;

error:

	if (namespaceUri != NULL)
		namespaceUri->release() ;
	if (unitld != NULL)
		unitld->release() ;
	if (displayName != NULL)
		displayName->release() ;
	if (description != NULL)
		description->release() ;

	return result ;
}

} /* namespace opcua */
