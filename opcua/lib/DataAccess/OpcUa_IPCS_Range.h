
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_RANGE_H_
#define OPCUA_RANGE_H_

/*
 * OPC-UA Part 8, 5.5.2, p. 9
 */

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"

namespace opcua {

class Range
	: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_Range ; }

private:

	Double * low ;
	Double * high ;

public:

	static Range * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_Range const& pRange)
	{
		Double * low  = Double ::fromCtoCpp(pStatus, pRange.Low) ;
		Double * high = Double ::fromCtoCpp(pStatus, pRange.High) ;

		if (*pStatus == STATUS_OK)
			return
					new Range(
							low,
							high
							) ;

		if (low != NULL)
			low->checkRefCount() ;
		if (high != NULL)
			high->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_Range& pRange) const
	{
		low  ->fromCpptoC(pStatus, pRange.Low) ;
		high ->fromCpptoC(pStatus, pRange.High) ;
	}

public:

	Range(
			Double * _low,
			Double * _high
			)
	{
		(low  = _low)  ->take() ;
		(high = _high) ->take() ;
	}

	virtual ~Range()
	{
		low  -> release() ;
		high -> release() ;
	}

public:

	static Range * get_Range_element(const char * const file_str, node_t * node) ;

};
} /* namespace opcua */
#endif /* OPCUA_RANGE_H_ */
