
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_HASHTABLEINT_H_
#define OPCUA_HASHTABLEINT_H_

#include "../OpcUa.h"

namespace opcua
{

template <typename OBJ> class MYDLL HashTableIntEntry
{
private:

	uint32_t                 key ;
	OBJ                    * value ;
	HashTableIntEntry<OBJ> * next ;

public:

	HashTableIntEntry(
			uint32_t                 _key,
			OBJ                    * _value,
			HashTableIntEntry<OBJ> * _next
			)
		: key(_key),
		  value(_value),
		  next(_next)
	{}

	virtual ~HashTableIntEntry()
	{}

public:

	inline uint32_t                 getKey()   { return key ; }
	inline OBJ                    * getValue() { return value ; }
	inline HashTableIntEntry<OBJ> * getNext()  { return next ; }

public:

	inline void setValue(OBJ * _value)
	{
		value = _value ;
	}

	inline void setNext(HashTableIntEntry<OBJ> * _next)
	{
		next = _next ;
	}

};


template <typename OBJ> class HashTableInt
{
private:

	uint32_t                  length ;
	HashTableIntEntry<OBJ> ** table ;

public:

	inline uint32_t getLength()
	{
		return length ;
	}

	inline HashTableIntEntry<OBJ> ** getTable()
	{
		return table ;
	}

public:

	HashTableInt(uint32_t _length)
	{
		length = _length ;
		table  = new HashTableIntEntry<OBJ> *[length] ;
		for (int i = 0 ; i < (int)length ; i++)
			table[i] = NULL ;
	}

	virtual ~HashTableInt()
	{
		delete[] table ;
	}

public:

#if (DEBUG_LEVEL & HASH_DBG)

	int count()
	{
		int res = 0 ;
		for (int i = 0 ; i < (int)length ; i++) {
			HashTableIntEntry<OBJ> * entry = table[i] ;
			while (entry != NULL) {
				res++ ;
				entry = entry->getNext() ;
			}
		}
		return res ;
	}

#endif

public:

	void put(uint32_t key, OBJ * value)
	{
		uint32_t idx = (key % length) ;
		table[idx] = new HashTableIntEntry<OBJ>(key,value,table[idx]) ;
	}

	OBJ * replace(uint32_t key, OBJ * value)
	{
		uint32_t idx = (key % length) ;
		HashTableIntEntry<OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (entry->getKey() == key) {
				OBJ * obj = entry->getValue() ;
				entry->setValue(value) ;
				return obj ;
			}
			entry = entry->getNext() ;
		}
		table[idx] = new HashTableIntEntry<OBJ>(key,value,table[idx]) ;
		return NULL ;
	}

	OBJ * get(uint32_t key)
	{
		uint32_t idx = (key % length) ;
		HashTableIntEntry<OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (entry->getKey() == key)
				return entry->getValue() ;
			entry = entry->getNext() ;
		}
		return NULL ;
	}

	bool exists(uint32_t key)
	{
		uint32_t idx = (key % length) ;
		HashTableIntEntry<OBJ> * entry = table[idx] ;
		while (entry != NULL) {
			if (entry->getKey() == key)
				return true ;
			entry = entry->getNext() ;
		}
		return false ;
	}

	bool remove(uint32_t key, bool doDelete)
	{
		uint32_t idx = (key % length) ;
		HashTableIntEntry<OBJ> * curr = table[idx] ;
		HashTableIntEntry<OBJ> * prev = NULL ;
		while (curr != NULL) {
			if (curr->getKey() == key) {
				if (doDelete)
					delete curr->getValue() ;
				if (prev == NULL)
					table[idx] = curr->getNext() ;
				else
					prev->setNext(curr->getNext()) ;
				delete curr ;
				return true ;
			}
			prev = curr ;
			curr = curr->getNext() ;
		}
		return false ;
	}

	OBJ * getAndRemove(uint32_t key)
	{
		uint32_t idx = (key % length) ;
		HashTableIntEntry<OBJ> * curr = table[idx] ;
		HashTableIntEntry<OBJ> * prev = NULL ;
		while (curr != NULL) {
			if (curr->getKey() == key) {
				OBJ * result = curr->getValue() ;
				if (prev == NULL)
					table[idx] = curr->getNext() ;
				else
					prev->setNext(curr->getNext()) ;
				delete curr ;
				return result ;
			}
			prev = curr ;
			curr = curr->getNext() ;
		}
		return NULL ;
	}

public :

	OBJ * removeOne()
	{
		HashTableIntEntry<OBJ> * entry ;
		uint32_t i = 0 ;
		while (i < length) {
			if ((entry = table[i]) != NULL) {
				OBJ * result = entry->getValue() ;
				table[i] = table[i]->getNext() ;
				delete entry ;
				return result ;
			} else {
				i++ ;
			}
		}
		return NULL ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_HASHTABLEInt_H_ */
