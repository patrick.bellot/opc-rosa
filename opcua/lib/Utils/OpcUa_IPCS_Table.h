
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_TABLE_H_
#define OPCUA_TABLE_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/OpcUa_BaseDataType.h"
#include "../Utils/OpcUa_HashCode.h"

#include <stdlib.h>

namespace opcua {

class MYDLL Table : public BaseDataType
{
public:

	virtual uint32_t getTypeId() const {
		CHECK_INT(0) ;
		return (uint32_t)0 ;
	}

	virtual Table * getTable(int) const {
		CHECK_INT(0) ;
		return NULL ;
	}

protected:

	BaseDataType ** tab ;
	int32_t         len ;

public:

	Table(int32_t _len)
		: len(_len)
	{
		if (len <= 0) {
			tab = NULL ;
		} else {
			tab = static_cast<BaseDataType **>(calloc(len,sizeof(BaseDataType *))) ;
		}
	}


	virtual ~Table()
	{
		if (tab != NULL) {
			for (int32_t i = 0 ; i < len ; i++) {
				if (tab[i] != NULL)
					tab[i]->release() ;
			}
			free(tab) ;
		}
	}

public:

	virtual class Variant * getVariant() ;

public:

	virtual uint32_t hashCode(uint32_t hash) const
	{
		for (int32_t i = 0 ; i < len ; i++)
			hash = tab[i]->hashCode(hash) ;
		return hash ;
	}

public:

	void set(int32_t i, BaseDataType * element)
	{
		BaseDataType * tmp = tab[i] ;
		if ((tab[i] = element))
			element->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	virtual bool isArray() const // redefined from BaseDataType where it gives false
	{
		return true ;
	}

	inline int32_t getLength()  const // redefined from BaseDataType where it gives -1
	{
		if (len < 0)
			return 0 ;
		return len ;
	}

	inline BaseDataType * getArrayElt(int32_t i)  // redefined from BaseDataType where it crashes
	{
		return tab[i] ;
	}

public:

# ifdef _WIN32
	virtual BaseDataType * takeArray(
			class NumericRange  * indexRange,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions) ;
# else
	virtual BaseDataType * takeArray(
			class NumericRange * indexRange,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions) ;
# endif

private:

	void _takeArray(
			Table       * result,
			int32_t     * pcurResult,
			const char  * indexRangeStr,
			const char  * indexRangeEnd,
			int32_t       somDim,
			int32_t       curDim,
			TableUInt32 * arrayDimensions,
			int           curValue
			) ;

public:

# ifdef _WIN32
	virtual void setArray(
			class NumericRange  * indexRange,
			class BaseDataType  * newValue,
			class Int32         * valueRank,
			class BaseDataType  * _arrayDimensions) ;
# else
	virtual void setArray(
			class NumericRange * indexRange,
			class BaseDataType * newValue,
			class Int32        * valueRank,
			class TableUInt32  * arrayDimensions) ;
# endif

private:

	void _setArray(
			BaseDataType * newValue,
			int32_t      * pcurResult,
			const char   * indexRangeStr,
			const char   * indexRangeEnd,
			int32_t        somDim,
			int32_t        curDim,
			TableUInt32  * arrayDimensions,
			int             curValue
			) ;

}; // class Table
}      /* namespace opcua */

#include "OpcUa_GENR_Tables.h"

#endif /* OPCUA_TABLE_H_ */
