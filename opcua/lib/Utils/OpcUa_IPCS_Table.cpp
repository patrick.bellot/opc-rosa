/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpcUa_IPCS_Table.h"

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"
#include "../DataAccess/All.h"
#include "../NotificationsAndEvents/All.h"
#include "../NodeManagement/All.h"
#include "../NotificationsAndEvents/OpcUa_IPCS_MonitoredItemModifyRequest.h"
#include <string.h>
#include "OpcUa_GENR_Tables.h"

namespace opcua {


MYDLL Variant * Table::getVariant()
{
	uint32_t typeId = getTypeId() ;
	if (typeId <= 25)
		return new Variant(typeId,this) ;
	debug_i(COM_ERR,"Table","getVariant() failed with typeId = %d",typeId) ;
	CHECK_INT(0) ;
	return NULL ;
}


#ifdef _WIN32
MYDLL BaseDataType * Table::takeArray(
		NumericRange  * indexRange,
		Int32         * valueRank,
		BaseDataType  * _arrayDimensions)
#else
MYDLL BaseDataType * Table::takeArray(
		NumericRange  * indexRange,
		Int32         * valueRank,
		TableUInt32   * arrayDimensions)
#endif
{
#ifdef _WIN32
		TableUInt32   * arrayDimensions = dynamic_cast<TableUInt32   *>(_arrayDimensions) ;
#else
#endif

		const char * indexRangeStr  = indexRange->get() ;
		const char * indexRangeEnd  = indexRangeStr + strlen(indexRangeStr) ;

		int32_t      valueRankInt32 = valueRank->get() ;

		int count = 1 ;

		debug(ARRAY_DBG,"Table","takeArray: step 1 : counting count") ;

		const char * tmpStr = indexRangeStr ;

		while (tmpStr < indexRangeEnd) {
			const char * comma = strchr(tmpStr,',') ;
			if (comma == NULL)
				comma = indexRangeEnd ;
			const char * colon = strchr(tmpStr,':') ;
			if (colon == NULL)
				colon = comma ;
			if (colon < comma) {
				long idx1 = strtol(tmpStr,NULL,0) ;
				long idx2 = strtol(colon+1,NULL,0) ;
				count *= (idx2 + 1 - idx1) ;
			}
			tmpStr = comma+1 ;
		}

		debug_i(ARRAY_DBG,"Table","takeArray: step 2 : count %d",count) ;

		/* count is the size of the result */

		int32_t somDim = 1 ;

		for (int i = 0 ; i < valueRankInt32 ; i ++)
			somDim *= arrayDimensions->get(i)->get() ;

		debug_i(ARRAY_DBG,"Table","takeArray: step 2 : somDim %d",somDim) ;

		/* somDim is the product of the dimension */

		int     curResult = 0 ;
		Table * result    = getTable(count) ;

		debug(ARRAY_DBG,"Table","takeArray: step 3 : looping takeArray") ;

		_takeArray(
				result,
				&curResult,
				indexRangeStr,
				indexRangeEnd,
				somDim,
				0,
				arrayDimensions,
				0
			) ;

		debug(ARRAY_DBG,"Table","takeArray: step 4 : returning result") ;

		return result ;
}



MYDLL void Table::_takeArray(
			Table         * result,
			int32_t       * pcurResult,
			const char    * indexRangeStr,
			const char    * indexRangeEnd,
			int32_t         somDim,
			int32_t         curDim,
			TableUInt32   * arrayDimensions,
			int             curValue
			)
{

	debug(ARRAY_DBG,"Table","takeArray Loop: step 1 : begins") ;

	if (indexRangeStr >= indexRangeEnd) {
		debug_ii(ARRAY_DBG,"Table","takeArray Loop: step 1.1 : begins result[%d] = value[%d]",*pcurResult,curValue) ;
		result->set(*pcurResult,tab[curValue]) ;
		(*pcurResult)++ ;
		debug(ARRAY_DBG,"Table","takeArray Loop: step 1.2 : ends") ;
		return ;
	}

	debug(ARRAY_DBG,"Table","takeArray Loop: step 2 : comma") ;

	const char * comma = strchr(indexRangeStr,',') ;

	if (comma == NULL) {
		debug(ARRAY_DBG,"Table","takeArray Loop: step 2.1 : comma = indexRangeEnd") ;
		comma = indexRangeEnd ;
	}

	debug(ARRAY_DBG,"Table","takeArray Loop: step 3 : colon") ;

	const char * colon = strchr(indexRangeStr,':') ;

	if (colon == NULL) {
		debug(ARRAY_DBG,"Table","takeArray Loop: step 3.1 : colon = comma") ;
		colon = comma ;
	}

	if (colon >= comma) {
		debug(ARRAY_DBG,"Table","takeArray Loop: step 4 : colon >= comma") ;

		int32_t idx    = (int32_t)strtol(indexRangeStr,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","takeArray Loop: step 4.1 : idx = %d",idx) ;

		int32_t setDim = somDim / static_cast<int32_t>(arrayDimensions->get(curDim)->get()) ;
		debug_i(ARRAY_DBG,"Table","takeArray Loop: step 4.2 : setDim = %d",setDim) ;

		debug(ARRAY_DBG,"Table","takeArray Loop: step 4.3 : before") ;
		_takeArray(
				result,
				pcurResult,
				comma + 1,
				indexRangeEnd,
				setDim,
				curDim + 1,
				arrayDimensions,
				curValue + (idx*setDim)
			) ;
		debug(ARRAY_DBG,"Table","takeArray Loop: step 4.4 : after") ;

	} else {
		debug(ARRAY_DBG,"Table","takeArray Loop: step 5 : colon < comma") ;

		int32_t idx1   = (int32_t)strtol(indexRangeStr,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","takeArray Loop: step 5.1 : idx1 = %d",idx1) ;

		int32_t idx2   = (int32_t)strtol(colon+1,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","takeArray Loop: step 5.2 : idx2 = %d",idx2) ;

		int32_t setDim = somDim / static_cast<int32_t>(arrayDimensions->get(curDim)->get()) ;
		debug_ii(ARRAY_DBG,"Table","takeArray Loop: step 5.3.a : curDim = %d (arrayDimensions[curDim] = %d)",curDim,static_cast<int32_t>(arrayDimensions->get(curDim)->get())) ;
		debug_ii(ARRAY_DBG,"Table","takeArray Loop: step 5.3.a : setDim = %d (somDim = %d)",setDim,somDim) ;

		for (int i = idx1 ; i <= idx2 ; i++) {
			debug(ARRAY_DBG,"Table","takeArray Loop: step 5.4 : before") ;
			_takeArray(
					result,
					pcurResult,
					comma + 1,
					indexRangeEnd,
					setDim,
					curDim + 1,
					arrayDimensions,
					curValue + (i*setDim)
			) ;
			debug(ARRAY_DBG,"Table","takeArray Loop: step 5.5 : after") ;
		}
	}
}



# ifdef _WIN32
MYDLL void Table::setArray(
		NumericRange  * indexRange,
		BaseDataType  * newValue,
		Int32         * valueRank,
		BaseDataType  * _arrayDimensions)
# else
MYDLL void Table::setArray(
		NumericRange  * indexRange,
		BaseDataType  * newValue,
		Int32         * valueRank,
		TableUInt32   * arrayDimensions)
# endif
{
# ifdef _WIN32
		TableUInt32   * arrayDimensions = dynamic_cast<TableUInt32   *>(_arrayDimensions) ;
# else
# endif

		const char * indexRangeStr  = indexRange->get() ;
		const char * indexRangeEnd  = indexRangeStr + strlen(indexRangeStr) ;

		int32_t      valueRankInt32 = valueRank->get() ;

		int32_t somDim = 1 ;
		for (int i = 0 ; i < valueRankInt32 ; i ++)
			somDim *= arrayDimensions->get(i)->get() ;

		debug_i(ARRAY_DBG,"Table","setArray: step 1 : somDim %d",somDim) ;

		/* somDim is the product of the dimension */

		int        curResult = 0 ;

		debug(ARRAY_DBG,"Table","setArray: step 2 : looping setArray") ;

		_setArray(
				newValue,
				&curResult,
				indexRangeStr,
				indexRangeEnd,
				somDim,
				0,
				arrayDimensions,
				0
			) ;

		debug(ARRAY_DBG,"Table","setArray: step 3 : returning") ;
}




MYDLL void Table::_setArray(
			BaseDataType  * newValue,
			int32_t       * pcurResult,
			const char    * indexRangeStr,
			const char    * indexRangeEnd,
			int32_t         somDim,
			int32_t         curDim,
			TableUInt32   * arrayDimensions,
			int             curValue
			)
{

	debug(ARRAY_DBG,"Table","setArray Loop: step 1 : begins") ;

	if (indexRangeStr >= indexRangeEnd) {
		debug_ii(ARRAY_DBG,"Table","setArray Loop: step 1.1 : begins value[%d] = result[%d]",curValue,*pcurResult) ;
		if (curValue >= getLength())
			return ;
		if (*pcurResult >= newValue->getLength())
			return ;
		set(curValue,newValue->getArrayElt(*pcurResult)) ;
		(*pcurResult)++ ;
		debug(ARRAY_DBG,"Table","setArray Loop: step 1.2 : ends") ;
		return ;
	}

	debug(ARRAY_DBG,"Table","setArray Loop: step 2 : comma") ;

	const char * comma = strchr(indexRangeStr,',') ;

	if (comma == NULL) {
		debug(ARRAY_DBG,"Table","setArray Loop: step 2.1 : comma = indexRangeEnd") ;
		comma = indexRangeEnd ;
	}

	debug(ARRAY_DBG,"Table","setArray Loop: step 3 : colon") ;

	const char * colon = strchr(indexRangeStr,':') ;

	if (colon == NULL) {
		debug(ARRAY_DBG,"Table","setArray Loop: step 3.1 : colon = comma") ;
		colon = comma ;
	}

	if (colon >= comma) {
		debug(ARRAY_DBG,"Table","setArray Loop: step 4 : colon >= comma") ;

		int32_t idx    = (int32_t)strtol(indexRangeStr,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","setArray Loop: step 4.1 : idx = %d",idx) ;

		int32_t setDim = somDim / static_cast<int32_t>(arrayDimensions->get(curDim)->get()) ;
		debug_i(ARRAY_DBG,"Table","setArray Loop: step 4.2 : setDim = %d",setDim) ;

		debug(ARRAY_DBG,"Table","setArray Loop: step 4.3 : before") ;
		_setArray(
				newValue,
				pcurResult,
				comma + 1,
				indexRangeEnd,
				setDim,
				curDim + 1,
				arrayDimensions,
				curValue + (idx*setDim)
			) ;
		debug(ARRAY_DBG,"Table","setArray Loop: step 4.4 : after") ;

	} else {
		debug(ARRAY_DBG,"Table","setArray Loop: step 5 : colon < comma") ;

		int32_t idx1   = (int32_t)strtol(indexRangeStr,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","setArray Loop: step 5.1 : idx1 = %d",idx1) ;

		int32_t idx2   = (int32_t)strtol(colon+1,NULL,0) ;
		debug_i(ARRAY_DBG,"Table","setArray Loop: step 5.2 : idx2 = %d",idx2) ;

		int32_t setDim = somDim / static_cast<int32_t>(arrayDimensions->get(curDim)->get()) ;
		debug_ii(ARRAY_DBG,"Table","setArray Loop: step 5.3.a : curDim = %d (arrayDimensions[curDim] = %d)",curDim,static_cast<int32_t>(arrayDimensions->get(curDim)->get())) ;
		debug_ii(ARRAY_DBG,"Table","setArray Loop: step 5.3.a : setDim = %d (somDim = %d)",setDim,somDim) ;

		for (int i = idx1 ; i <= idx2 ; i++) {
			debug(ARRAY_DBG,"Table","setArray Loop: step 5.4 : before") ;
			_setArray(
					newValue,
					pcurResult,
					comma + 1,
					indexRangeEnd,
					setDim,
					curDim + 1,
					arrayDimensions,
					curValue + (i*setDim)
			) ;
			debug(ARRAY_DBG,"Table","setArray Loop: step 5.5 : after") ;
		}
	}
}

} /* namespace */




