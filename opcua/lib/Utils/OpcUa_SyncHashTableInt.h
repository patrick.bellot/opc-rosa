/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SYNCHASHTABLEINT_H_
#define OPCUA_SYNCHASHTABLEINT_H_

#include "../OpcUa.h"
#include "../Threads/OpcUa_Mutex.h"
#include "../Utils/OpcUa_HashTableInt.h"

namespace opcua
{

template <typename OBJ>
class MYDLL SyncHashTableInt
	: public HashTableInt<OBJ>
{
private:

	Mutex * mutex ;

public:

	SyncHashTableInt(uint32_t _length)
		: HashTableInt<OBJ>(_length)
	{
		mutex  = new Mutex() ;
	}

	virtual ~SyncHashTableInt()
	{
		delete mutex ;
	}

public:

	void setEmptyDelete()
	{
		OBJ * obj ;
		while ((obj = SyncHashTableInt<OBJ>::removeOne()) != NULL)
			delete obj ;
	}

#if (DEBUG_LEVEL & HASH_DBG)

	int count()
	{
		mutex->lock() ;
		int res = HashTableInt<OBJ>::count() ;
		mutex->unlock() ;
		return res ;
	}

#endif

public:

	inline Mutex * getMutex()
	{
		return mutex ;
	}

public:

	void put(uint32_t key, OBJ * value)
	{
		mutex->lock() ;
		HashTableInt<OBJ>::put(key,value) ;
		mutex->unlock() ;
	}

	OBJ * replace(uint32_t key, OBJ * value)
	{
		mutex->lock() ;
		OBJ * obj = HashTableInt<OBJ>::replace(key,value) ;
		mutex->unlock() ;
		return obj ;
	}

	OBJ * get(uint32_t key)
	{
		mutex->lock() ;
		OBJ * result = HashTableInt<OBJ>::get(key) ;
		mutex->unlock() ;
		return result ;
	}


	bool exists(uint32_t key)
	{
		mutex->lock() ;
		bool result = HashTableInt<OBJ>::exists(key) ;
		mutex->unlock() ;
		return result ;
	}


	bool remove(uint32_t key, bool doDelete)
	{
		mutex->lock() ;
		bool result = HashTableInt<OBJ>::remove(key,doDelete) ;
		mutex->unlock() ;
		return result ;
	}

	OBJ * getAndRemove(uint32_t key)
	{
		mutex->lock() ;
		OBJ * result = HashTableInt<OBJ>::getAndRemove(key) ;
		mutex->unlock() ;
		return result ;
	}

public:

	OBJ * removeOne()
	{
		mutex->lock() ;
		OBJ * result = HashTableInt<OBJ>::removeOne() ;
		mutex->unlock() ;
		return result ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_HASHTABLEInt_H_ */
