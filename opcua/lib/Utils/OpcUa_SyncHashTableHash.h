
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SYNCHASHTABLEHASH_H_
#define OPCUA_SYNCHASHTABLEHASH_H_

#include "../OpcUa.h"
#include "../Threads/OpcUa_Mutex.h"
#include "../Utils/OpcUa_HashTableHash.h"

namespace opcua
{

template <typename KEY,typename OBJ>
class MYDLL SyncHashTableHash
	: public HashTableHash<KEY,OBJ>
{
private:

	Mutex * mutex ;

public:

	SyncHashTableHash(uint32_t _length)
		: HashTableHash<KEY,OBJ>(_length)
	{
		mutex  = new Mutex() ;
	}

	virtual ~SyncHashTableHash()
	{
		delete mutex ;
	}

public:

	void put(KEY * key, OBJ * value)
	{
		mutex->lock() ;
		HashTableHash<KEY,OBJ>::put(key,value) ;
		mutex->unlock() ;
	}

	OBJ * replace(KEY * key, OBJ * value)
	{
		mutex->lock() ;
		OBJ * obj = HashTableHash<KEY,OBJ>::replace(key,value) ;
		mutex->unlock() ;
		return obj ;
	}

	OBJ * get(KEY * key)
	{
		mutex->lock() ;
		OBJ * result = HashTableHash<KEY,OBJ>::get(key) ;
		mutex->unlock() ;
		return result ;
	}


	bool exists(KEY * key)
	{
		mutex->lock() ;
		bool result = HashTableHash<KEY,OBJ>::exists(key) ;
		mutex->unlock() ;
		return result ;
	}


public:

	bool removeOne()
	{
		mutex->lock() ;
		bool result = HashTableHash<KEY,OBJ>::removeOne() ;
		mutex->unlock() ;
		return result ;
	}

};

} /* namespace opcua */
#endif /* OPCUA_HASHTABLEInt_H_ */
