
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ALEA_H_
#define OPCUA_ALEA_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_Clock.h"
#include "../Utils/OpcUa_HashCode.h"

namespace opcua
{

class MYDLL Alea
{
public:

	static Alea * alea ;

public:

	Alea()
	{
		srand(Clock::get_microsecondssince1601()) ;
	}

	virtual ~Alea()
	{}

public:

	uint32_t random_uint32()
	{
		uint32_t tmp = ((uint32_t)rand()) + (((uint32_t)rand()) << 16) ;
		return HashCode::hashCode((int32_t)0, (const uint8_t *)(&tmp), (int32_t)sizeof(int32_t)) ;
	}

	uint64_t random_uint64()
	{
		uint64_t tmp = ((uint64_t)rand()) + (((uint64_t)rand()) << 32) ;
		return HashCode::hashCode64((uint64_t)0, (const uint8_t *)(&tmp), (int32_t)sizeof(uint64_t)) ;
	}
};

} /* namespace opcua */
#endif /* OPCUA_ALEA_H_ */
