
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_STRINGUTILS_H_
#define OPCUA_STRINGUTILS_H_

#include "../OpcUa.h"

#include <string.h>
#include <ctype.h>

namespace opcua
{

class MYDLL StringUtils
{
public:

	static int strcmpi (const char *s1, const char *s2)
	{
	   if (s1 == NULL)
		   return s2 == NULL ? 0 : -(*s2);

	   if (s2 == NULL)
		   return *s1;

	   int c1, c2;
	   while ((c1 = tolower (*s1)) == (c2 = tolower (*s2))) {
	     if (c1 == 0)
	    	 return 0 ;
	     ++s1;
	     ++s2;
	   }

	   return c1 - c2;
	}

public:

	static bool isNumeric(const char *s)
	{
		if (s == NULL || *s == '\0')
			return false ;
		do {
			if (*s < '0' || '9' < *s)
				return false ;
		} while (*++s != '\0') ;
		return true ;
	}

public:

	static char * strdup(const char *s)
	{
		if (s == NULL)
			return NULL ;
		char * res = new char[strlen(s) + 1] ;
		strcpy(res,s) ;
		return res ;
	}

};     /* class StringUtils */
}      /* namespace opcua */
#endif /* OPCUA_ALEA_H_ */
