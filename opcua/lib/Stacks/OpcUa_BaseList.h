
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OPCUA_BASELIST_H_
#define OPCUA_BASELIST_H_

#include "../OpcUa.h"
#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_LinkedList.h"
#include "../AddressSpace/OpcUa_Base.h"

namespace opcua {

// Uniquely used for registered nodes in Session

class MYDLL BaseList
{
private:

	Base     * car ;
	BaseList * cdr ;

public:

	BaseList(Base * _car, BaseList * _cdr)
	{
		(car = _car)->take() ;
		cdr = _cdr ;
    }

	virtual ~BaseList()
	{
		car->release() ;
	}

public:

	inline Base * getCar()
	{
		return car ;
	}

	inline BaseList * getCdr()
	{
		return cdr ;
	}

	inline void setCdr(BaseList * _cdr)
	{
		cdr = _cdr ;
	}

public:

	static int32_t getLength(BaseList * baseList)
	{
		int32_t length = 0 ;
		while (baseList != NULL) {
			length++ ;
			baseList = baseList->getCdr() ;
		}
		return length ;
	}

public:

	static void deleteAll(BaseList * basesList)
	{
		while (basesList != NULL) {
			BaseList * cdr = basesList->getCdr() ;
			delete basesList ;
			basesList = cdr ;
		}
	}

public:

	Base * getBase(NodeId * nodeId)
	{
		Base * car = getCar() ;
		if (car->getNodeId()->equals(nodeId))
			return car ;
		BaseList * cdr = getCdr() ;
		if (cdr == NULL)
			return NULL ;
		return cdr->getBase(nodeId) ;
	}

public:

	static BaseList * deleteBase(BaseList * list, Base * element)
	{
		if (list == NULL)
			return list ;

		if (list->getCar() == element)  {
			BaseList * res = list->getCdr() ;
			// Do not delete car
			delete list ;
			return res ;
		}

		BaseList * prev = list ;
		BaseList * next ;

		while (prev != NULL) {
			next = prev->getCdr() ;
			if (next == NULL)
				break ;
			if (next->getCar() == element) {
				prev->setCdr(next->getCdr()) ;
				// Do not delete car
				delete next ;
				break ;
			}
			prev = next ;
		}

		return list ;
	}
};


} /* namespace opcua */
#endif /* BASELIST_H_ */
