
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"
#include "OpcUa_Session.h"
#include "../NotificationsAndEvents//OpcUa_SubscriptionsTable.h"
#include "../NotificationsAndEvents//OpcUa_SubscriptionsList.h"

namespace opcua {

#if (WITH_SUBSCRIPTION == 1)

	void Session::deleteAllSubscriptions()
	{
		if (subscriptionsList != NULL) {
			subscriptionsList->deleteAllWithCar() ;
			subscriptionsList = NULL ;
		}
	}

	void Session::addSubscription(Subscription * subscription)
	{
		subscriptionsCount++ ;
		subscriptionsList = new SubscriptionsList(subscription,subscriptionsList) ;
	}

	void Session::deleteSubscription(Subscription * subscription)
	{
		subscriptionsCount-- ;
		subscriptionsList = SubscriptionsList::deleteElement(subscriptionsList,subscription) ;
	}

	Subscription * Session::getCurrentSubscription()
	{
		if (subscriptionsCount == 0)
			return NULL ;
		int32_t n = (Alea::alea->random_uint32() % subscriptionsCount) ;
		if (n == 0) {
			return subscriptionsList->getCar() ;
		}
		SubscriptionsList * tmp = subscriptionsList ;
		while (n-- != 0)
			tmp = tmp->getCdr() ;
		return tmp->getCar() ;
	}

#endif /* WITH_SUBSCRIPTION */
}      /* namespace opcua */
