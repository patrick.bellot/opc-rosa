/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.


 * SecurityHeader.h
 *
 *  Created on: 18 mars 2015
 *      Author: mohamed
 */


#ifndef LIB_STACKS_SECURITY_OPCUA_CERTIFICATES_H_
#define LIB_STACKS_SECURITY_OPCUA_CERTIFICATES_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_RefCount.h"
#include "../CommonParametersTypes/OpcUa_SecurityProfileUri.h"
#include "../CommonParametersTypes/OpcUa_IPCS_ApplicationInstanceCertificate.h"
#include "../CommonParametersTypes/OpcUa_IPCS_SignatureData.h"

#include <string.h>


namespace opcua {

	class MYDLL Certificates
	{
	public:

		static const char * dir_str  	 			;						/* directory of all certificates and keys (fileName)*/

	#if (SECURITY != UANONE)
		static const char * rootCertificate_str		;						/* self signed certificate certificate (fileName)*/
		static const char * signingCertificate_str  ;
	#endif

		static const char * self_str     			;						/* user certificate (fileName)*/

	#if (SECURITY != UANONE)
		static const char * crl_str ;								 		/* certificate revocation list*/
	#endif

		static ApplicationInstanceCertificate * selfCertificate ;    		/* user certificate (ApplicationInstanceCertificate)*/

	#if (SECURITY != UANONE)
		static const char * privKey_str   ; 								/* user privKey name*/
	#endif

	#if (SECURITY != UANONE)
		static mbedtls_x509_crt   mbedtlsCert ;
		static mbedtls_pk_context mbedtlsPrivKey ;
	#endif

	public:

		static void init(const char * _dir_str)
		{
			dir_str = _dir_str ;

			self_str = new char [strlen(dir_str) + strlen("/certs/self.der") + 1] ;
			strcpy((char *)self_str, dir_str) ;
			strcat((char *)self_str,"/certs/self.der") ;

			selfCertificate = ApplicationInstanceCertificate::getApplicationInstanceCertificate(self_str) ;

			if (selfCertificate == NULL) {
				debug(COM_ERR,"Certificates","file not found") ;
				EXIT(-1) ;
			}

			selfCertificate->take() ;

			if (selfCertificate->isNotValid()) {
				debug_s(COM_ERR,"Certificates","Certificate not valid \"%s\"",self_str) ;
				selfCertificate->release() ;
				EXIT(-1) ;
			}

	#if (SECURITY != UANONE)
			privKey_str = new char [strlen(dir_str)+strlen("/keys/self.der") +1] ;
			strcpy((char *)privKey_str, dir_str) ;
			strcat((char *)privKey_str,"/keys/self.der") ;
	#endif

	#if (SECURITY != UANONE)
			mbedtlsCert = selfCertificate->getCertificate() ;

			if (ApplicationInstanceCertificate::extractPrivateKey(&Certificates::mbedtlsPrivKey, (const char *) privKey_str)) {
				debug_s(COM_ERR,"Certificates","extractPrivateKey operation failed \"%s\"",self_str) ;
				selfCertificate->release() ;
				EXIT(-1) ;
			}
	#endif
		}

	public:

		static int32_t computeRandNonce (Nonce ** nonce)
		{
			uint8_t * randNonce = new uint8_t [NonceLength];
			int32_t   rc;

			/*A random number generation*/

			mbedtls_entropy_context entropy;
			char perso [30] = "ensure the uniqueness" ;
			mbedtls_ctr_drbg_context context ;

			mbedtls_entropy_init (&entropy) ;
			mbedtls_ctr_drbg_init(&context) ;

			if((rc = mbedtls_ctr_drbg_seed(&context, mbedtls_entropy_func, &entropy, (unsigned char *)perso, strlen(perso))) != 0) {

				debug_i (COM_ERR, "Certificates", "nonce computing failed (mbedtls_ctr_drbg_seed), %d", rc) ;
				if (randNonce != NULL)
					delete [] randNonce ;
				return (int32_t) 1 ;
			}

			if((rc = mbedtls_ctr_drbg_random(&context, randNonce, NonceLength)) != 0) {

				debug_i (COM_ERR, "Certificates", "nonce computing failed (mbedtls_ctr_drbg_random), %d", rc) ;
				if (randNonce != NULL)
					delete [] randNonce ;
				return (int32_t) 1 ;
			}

			mbedtls_ctr_drbg_free(&context) ;
			mbedtls_entropy_free (&entropy) ;

			*nonce = new Nonce (NonceLength, randNonce) ;

			delete [] randNonce ;

			return (int32_t) 0 ;
		}

public:

	#if (SECURITY != UANONE)
		static int32_t computeSignatureData(
				SignatureData                  ** signatureData,
				ApplicationInstanceCertificate  * distantCertificate,
				Nonce                           * distantNonce)
		{
			int32_t      rc	= 0 ;

			ByteString * distantCertificateContent    = distantCertificate->getContent() ;

			int32_t      distantCertificateContentLen = distantCertificateContent->getLen() ;
			int32_t      distantNonceLen              = distantNonce->getLen() ;

			uint32_t     plainBufferTmpLength = distantCertificateContentLen + distantNonceLen ;
			uint8_t    * plainBufferTmp 	  = new uint8_t[plainBufferTmpLength] ;

			uint32_t     signatureTmpLength	    = 0 ;
			uint8_t    * signatureTmp			= NULL ;

			if (distantCertificate->getSignatureAlgorithm() == NULL) {
				debug (COM_ERR,"Certificates","SignatureData : certificate signature algorithm not found") ;
				rc = (int32_t) 1 ;
				goto error ;
			}


			memcpy (plainBufferTmp,                                distantCertificateContent->getArray(), distantCertificateContentLen) ;
			memcpy (plainBufferTmp + distantCertificateContentLen, distantNonce->getArray(),              distantNonceLen) ;

			if ((ApplicationInstanceCertificate::asymmetricSignature(
																	  &signatureTmp,
																	  &signatureTmpLength,
																	  plainBufferTmp,
																	  plainBufferTmpLength,
																	  Certificates::mbedtlsPrivKey))) {

				debug(COM_ERR,"Certificates","AsymetricSignature failed") ;
				rc = (int32_t) 1 ;
				goto error ;
			}

			if (signatureTmpLength != SignatureLength) {
				debug_i(COM_ERR,"Certificates","signatureLength != %u",SignatureLength) ;
				rc = (int32_t) 1 ;
				goto error ;
			}

			debug(SEC_DBG,"Certificates","SignatureData computation finished with success") ;
			//print_uint8_array("siGNature: ", (int32_t) signatureTmpLength, signatureTmp) ;

			* signatureData = new SignatureData(distantCertificate->getSignatureAlgorithm(),new ByteString(signatureTmpLength,signatureTmp)) ;

	error :

			delete [] plainBufferTmp ;
			if (signatureTmp != NULL) {
				delete [] signatureTmp ;
				signatureTmp = NULL    ;
			}

			return rc ;

		}
	#endif

	};

} /* namespace */
#endif /* LIB_STACKS_SECURITY_OPCUA_CERTIFICATES_H_ */
