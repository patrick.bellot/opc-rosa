
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SESSIONSTABLE_H_
#define SESSIONSTABLE_H_

#include "../OpcUa.h"
#include "../Utils/OpcUa_SyncHashTableInt.h"
#include "../Utils/OpcUa_Alea.h"
#include "../Stacks/OpcUa_Session.h"

namespace opcua {

class MYDLL SessionsTable
	: public SyncHashTableInt<Session>
{
public:

	SessionsTable(uint32_t size)
		: SyncHashTableInt<Session>(size)
	{}

	virtual ~SessionsTable()
	{
		setEmptyDelete() ;
	}


public:

	Session * getNewSession(String * sessionName)
	{
		getMutex()->lock() ;

		uint32_t authenticationToken  ;
		while (HashTableInt<Session>::exists(authenticationToken = Alea::alea->random_uint32())) ;

		Session * session = new Session(authenticationToken,sessionName) ;
		HashTableInt<Session>::put(authenticationToken,session) ;

		getMutex()->unlock() ;

#if (DEBUG_LEVEL & HASH_DBG)
		debug_i(HASH_DBG,"SessionsTable","Session count in hash table: %d",count()) ;
#endif

		return session ;
	}

#if (_CHECK_FOR_SESSIONS_BUG_ == 1)

public:

	bool isBugged()
	{
		debug(BUG_DBG,"SessionsRable","isBugged start") ;
		getMutex()->lock() ;

		uint32_t                      length = getLength() ;
		HashTableIntEntry<Session> ** table  = getTable() ;

		for (int i = 0 ; i < (int)length ; i++) {
			HashTableIntEntry<Session> * entry = table[i] ;
			while (entry != NULL) {
				Session * session = entry->getValue() ;
				debug_p(BUG_DBG,"SessionsRable","isBugged session=%p",session) ;
				debug_i(BUG_DBG,"SessionsRable","         authentificationToken=%ud",session->getAuthenticationTokenNum()) ;
				if (0x200000000 < (uint64_t)session ->getPublishingReqQueue()) {
					debug_i(BUG_DBG,"SessionsRable","It is Bugged : lld !",(uint64_t)session ->getPublishingReqQueue()) ;
					getMutex()->unlock() ;
					debug(BUG_DBG,"SessionsRable","isBugged stop wit true") ;
					return true ;
				}
				entry = entry->getNext() ;
			}
		}

		getMutex()->unlock() ;
		debug(BUG_DBG,"SessionsRable","isBugged stop wit false") ;
		return false ;
	}

#endif
};

} /* namespace opcua */
#endif /* SESSIONSTABLE_H_ */
