
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"

#include "OpcUa_Channel.h"

namespace opcua {

	MYDLL Channel::Channel(uint32_t _channelIdNum)
	{
		mutex = new Mutex();

		(token = new ChannelSecurityToken(_channelIdNum,0,UtcTime::now(),3600000))->take() ;
		previousToken = NULL ;
		sessionsTable = new SessionsTable(4) ;

		(sequence = new Sequence()) ->take();

		lastDisconnect = 0L;

		scConfig = NULL ;

#if (SECURITY != UA_NONE)
		distantCertificate	   = NULL ;
#endif
		localNonce		 	   = NULL ;
		distantNonce	  	   = NULL ;

	#if (SECURITY != UA_NONE)
		symmetricSignKey   	   = NULL ;
		symmetricUnsignKey	   = NULL ;
	#endif

	#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		symmetricEncryptionIv  = NULL ;
		symmetricDecryptionIv  = NULL ;
		symmetricEncryptionKey = NULL ;
		symmetricDecryptionKey = NULL ;
	#endif
	}

	MYDLL Channel::~Channel()
	{
		delete mutex;

		token->release() ;
		if (previousToken != NULL)
			previousToken->release() ;
		sessionsTable->setEmptyDelete() ;
		delete sessionsTable ;

		sequence->release();

#if (SECURITY != UA_NONE)
		if (distantCertificate != NULL)
			distantCertificate->release() ;
#endif
		if (localNonce != NULL)
			localNonce->release() ;
		if (distantNonce != NULL)
			distantNonce->release() ;

	#if (SECURITY != UA_NONE)
		if (symmetricSignKey != NULL)
			symmetricSignKey->release() ;
		if (symmetricUnsignKey != NULL)
			symmetricUnsignKey->release() ;
	#endif

	#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		if (symmetricEncryptionIv != NULL)
			symmetricEncryptionIv->release() ;
		if (symmetricDecryptionIv != NULL)
			symmetricDecryptionIv->release() ;
		if (symmetricEncryptionKey != NULL)
			symmetricEncryptionKey->release() ;
		if (symmetricDecryptionKey != NULL)
			symmetricDecryptionKey->release() ;
	#endif
	}

	MYDLL void Channel::setChannelInfo( // called only by client
			uint32_t 	  _channelIdNum,
			uint32_t      _tokenIdNum,
			UtcTime     * _createdAt,
			int32_t       _revisedLifetime)
	{
		ChannelSecurityToken * tmp = previousToken;
		previousToken = token;
		token = new ChannelSecurityToken(
				_channelIdNum,
				_tokenIdNum,
				_createdAt,
				_revisedLifetime) ;
		token->take();
		sessionsTable->setEmptyDelete();
		if (tmp != NULL)
			tmp->release();
	}

	MYDLL void Channel::renewToken() // called only by server
	{
		ChannelSecurityToken * tmp = previousToken;
		previousToken = token;
		token = new ChannelSecurityToken(
				getChannelIdNum(),
				0,
				UtcTime::now(),
				3600000) ;
		token->take();
		if (tmp != NULL)
			tmp->release();
		lastDisconnect = 0L;
	}

} /* namespace opcua */

