
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OPCUA_SERVERCHANNEL_H_
#define _OPCUA_SERVERCHANNEL_H_

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"

#include "../Utils/OpcUa_Sequence.h"

#include "OpcUa_SessionsTable.h"

namespace opcua {


class MYDLL Channel
{

private:

	Mutex 				           * mutex;

	ChannelSecurityToken           * token ;
	ChannelSecurityToken           * previousToken ;
	SessionsTable 		           * sessionsTable ;

	Sequence			           * sequence;

	uint64_t			             lastDisconnect;

	SOPC_SecureChannel_Config      * scConfig ;
	uint32_t						 epConfigIdx ;

#if (SECURITY != UANONE)
	ApplicationInstanceCertificate * distantCertificate   ;
#endif
	Nonce 						   * localNonce           ;
	Nonce 						   * distantNonce         ;

#if (SECURITY != UA_NONE)
	ByteString					   * symmetricSignKey     ;
	ByteString					   * symmetricUnsignKey   ;
#endif

#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
	ByteString					   * symmetricEncryptionIv ;
	ByteString					   * symmetricDecryptionIv ;
	ByteString					   * symmetricEncryptionKey ;
	ByteString					   * symmetricDecryptionKey ;
#endif

public:

	Channel(uint32_t _channelIdNum) ;

	virtual ~Channel() ;

public:

	inline SOPC_SecureChannel_Config * getScConfig()
	{
		return scConfig ;
	}

	inline void setScConfig(SOPC_SecureChannel_Config * _scConfig)
	{
		scConfig = _scConfig ;
	}

	inline uint32_t getEpConfigIdx()
	{
		return epConfigIdx;
	}

	inline void setEpConfigIdx(uint32_t _epConfigIdx)
	{
		epConfigIdx = _epConfigIdx ;
	}

	inline uint32_t getChannelIdNum()
	{
		return token->getChannelId();
	}

	inline void setChannelIdNum(uint32_t _channelIdNum)
	{
		token->setChannelId(_channelIdNum);
	}

	inline ChannelSecurityToken * getToken()
	{
		return token ;
	}

	inline ChannelSecurityToken * getPreviousToken()
	{
		return previousToken ;
	}

	inline Session * getSession(uint32_t sessionIndex)
	{
		return sessionsTable->get(sessionIndex) ;
	}

	inline SessionsTable * getSessionsTable()
	{
		return sessionsTable ;
	}

	void deleteSession(uint32_t sessionIndex)
	{
		sessionsTable->remove(sessionIndex,true) ;
	}

public:

	void setChannelInfo(uint32_t      _channelIdNum,
						uint32_t      _tokenIdNum,
						UtcTime     * _createdAt,
						int32_t       _revisedLifetime
	);

	void renewToken();

public:

	inline Session * getNewSession(String * sessionName)
	{
		return sessionsTable->getNewSession(sessionName) ;
	}

	inline void pushNewSession(uint32_t key, Session * _session)
	{
	    sessionsTable->put(key, _session);
	}

public:

	uint32_t getSequenceNumber(bool isFinal)
	{
		uint32_t tmp = sequence->next();
		if (isFinal)
			sequence->tryReset();
		return tmp;
	}

public:

	void setLastDisconnect(bool isDisconnected)
	{
		lastDisconnect = isDisconnected ? Clock::get_millisecondssince1601() : (uint64_t)0 ;
	}

	inline uint64_t getLastDisconnect()
	{
		return lastDisconnect;
	}

public:

#if (SECURITY != UANONE)
	inline ApplicationInstanceCertificate * getDistantCertificate()
	{
		return distantCertificate ;
	}

	void setDistantCertificate(ApplicationInstanceCertificate * _distantCertificate)
	{
		ApplicationInstanceCertificate * tmp = distantCertificate ;
		if ((distantCertificate = _distantCertificate))
			_distantCertificate->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}
#endif

	inline Nonce * getLocalNonce()
	{
		return localNonce ;
	}

	void setLocalNonce (Nonce * _localNonce)
	{
		Nonce * tmp = localNonce ;
		(localNonce = _localNonce)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline Nonce * getDistantNonce()
	{
		return distantNonce ;
	}

	void setDistantNonce (Nonce * _distantNonce)
	{
		Nonce * tmp = distantNonce ;
		(distantNonce = _distantNonce)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

public:

#if (SECURITY != UA_NONE)

	inline ByteString * getSymmetricSignKey()
	{
		return symmetricSignKey ;
	}

	void setSymmetricSignKey(ByteString * _symmetricSignKey)
	{
		ByteString * tmp = symmetricSignKey ;
		(symmetricSignKey = _symmetricSignKey)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline ByteString * getSymmetricUnsignKey()
	{
		return symmetricUnsignKey;
	}

	void setSymmetricUnsignKey(ByteString * _symmetricUnsignKey)
	{
		ByteString * tmp = symmetricUnsignKey ;
		(symmetricUnsignKey = _symmetricUnsignKey)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

#endif

#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)

public:

	inline ByteString * getSymmetricEncryptionIv()
	{
		return symmetricEncryptionIv ;
	}

	void setSymmetricEncryptionIv(ByteString * _iv)
	{
		ByteString * tmp = symmetricEncryptionIv ;
		(symmetricEncryptionIv = _iv)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline ByteString * getSymmetricDecryptionIv()
	{
		return symmetricDecryptionIv ;
	}

	void setSymmetricDecryptionIv(ByteString * _iv)
	{
		ByteString * tmp = symmetricDecryptionIv ;
		(symmetricDecryptionIv = _iv)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline ByteString * getSymmetricEncryptionKey()
	{
		return symmetricEncryptionKey ;
	}

	void setSymmetricEncryptionKey(ByteString * _symmetricEncryptionKey)
	{
		ByteString * tmp = symmetricEncryptionKey ;
		(symmetricEncryptionKey = _symmetricEncryptionKey)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

	inline ByteString * getSymmetricDecryptionKey()
	{
		return symmetricDecryptionKey ;
	}

	void setSymmetricDecryptionKey(ByteString * _symmetricDecryptionKey)
	{
		ByteString * tmp = symmetricDecryptionKey ;
		(symmetricDecryptionKey = _symmetricDecryptionKey)->take() ;
		if (tmp != NULL)
			tmp->release() ;
	}

#endif

#if (SECURITY != UA_NONE)

public:

	void symmetricSignKeyClean()
	{
		if (symmetricSignKey != NULL) {
			symmetricSignKey->release() ;
			symmetricSignKey = NULL ;
		}
	}

	void symmetricUnsignKeyClean()
	{
		if (symmetricUnsignKey != NULL) {
			symmetricUnsignKey->release() ;
			symmetricUnsignKey = NULL ;
		}
	}

#endif

#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)

public:

	void ivClean()
	{
		if (symmetricEncryptionIv != NULL) {
			symmetricEncryptionIv->release() ;
			symmetricEncryptionIv = NULL ;
		}

		if (symmetricDecryptionIv != NULL) {
			symmetricDecryptionIv->release() ;
			symmetricDecryptionIv = NULL ;
		}
	}

	void symmetricEncryptionKeyClean()
	{
		if (symmetricEncryptionKey != NULL) {
			symmetricEncryptionKey->release() ;
			symmetricEncryptionKey = NULL ;
		}
	}

	void symmetricDecryptionKeyClean()
	{
		if (symmetricDecryptionKey != NULL) {
			symmetricDecryptionKey->release() ;
			symmetricDecryptionKey = NULL ;
		}
	}

#endif // (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)

#if (SECURITY != UA_NONE)

public:

	void symmetricKeyClean()
	{
		symmetricSignKeyClean() ;
		symmetricUnsignKeyClean() ;

#if (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
		ivClean() ;
		symmetricEncryptionKeyClean() ;
		symmetricDecryptionKeyClean() ;
#endif // (SECURITY == UASIGNANDENCRYPT_Basic256Sha256)
	}

#endif // (SECURITY != UA_NONE)

};     /* class */
}      /* namespace opcua */
#endif /* CHANNEL_H_ */
