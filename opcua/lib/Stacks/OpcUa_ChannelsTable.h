
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_SERVERCHANNELSTABLE_H_
#define OPCUA_SERVERCHANNELSTABLE_H_

#include "../OpcUa.h"
#include "../Threads/All.h"
#include "../Utils/OpcUa_SyncHashTableInt.h"
#include "../Stacks/OpcUa_Channel.h"

namespace opcua {

class MYDLL ChannelsTable
	: public SyncHashTableInt<Channel>
{
public:

	static ChannelsTable * self ;

private:

	uint32_t   channelIdTableLength;
	uint32_t * channelIdTable;

public:

	ChannelsTable(uint32_t size)
		: SyncHashTableInt<Channel>(size)
	{
		channelIdTableLength = size;
		channelIdTable       = new uint32_t[channelIdTableLength];
		for (uint32_t i=0; i<channelIdTableLength; i++)
			channelIdTable[i] = (uint32_t)0;
	}

	virtual ~ChannelsTable()
	{
		delete [] channelIdTable;
		setEmptyDelete() ;
	}

public: // For server

	Channel * createNewChannel(uint32_t secureChannelId, SOPC_SecureChannel_Config *scConfig, uint32_t epConfigIdx)
	{
		getMutex()->lock() ;

		Channel * channel = HashTableInt<Channel>::get(secureChannelId) ;

		if (channel == NULL) {
			channel = new Channel(secureChannelId) ;
			HashTableInt<Channel>::put(secureChannelId,channel) ;
		}

		channel->setScConfig(scConfig) ;
		channel->setEpConfigIdx(epConfigIdx) ;

		getMutex()->unlock() ;
		return channel ;
	}

#if (SECURITY != UANONE)
	Channel * createNewChannel(uint32_t secureChannelId, ApplicationInstanceCertificate * clientCertificate, SOPC_SecureChannel_Config *scConfig, uint32_t epConfigIdx)
	{
		getMutex()->lock() ;

		Channel * channel = HashTableInt<Channel>::get(secureChannelId) ;

		if (channel == NULL) {
			channel = new Channel(secureChannelId) ;
			HashTableInt<Channel>::put(secureChannelId,channel) ;
		}

		channel->setDistantCertificate(clientCertificate) ;
		channel->setScConfig(scConfig) ;
		channel->setEpConfigIdx(epConfigIdx) ;

		getMutex()->unlock() ;
		return channel ;
	}
#endif

public:

	bool remove(uint32_t secureChannelId, bool doDelete)
	{
		return SyncHashTableInt<Channel>::remove(secureChannelId,doDelete) ;
	}

	void removeByEpConfigIdx(uint32_t epConfigIdx)
	{
		NOT_USED(epConfigIdx) ;
		// TBDS2OPC
	}



};     /* class */
}      /* namespace opcua */
#endif /* CHANNELSTABLE_H_ */
