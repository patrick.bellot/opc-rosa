
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STACKS_ALL_H_
#define STACKS_ALL_H_

#include "OpcUa_BaseList.h"
#include "OpcUa_Certificates.h"
#include "OpcUa_Channel.h"
#include "OpcUa_ChannelsTable.h"
#include "OpcUa_Session.h"
#include "OpcUa_SessionsTable.h"

#endif /* STACK_ALL_H_ */
