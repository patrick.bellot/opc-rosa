
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpcUa.h"
#include "StandardDataTypes/All.h"
#include "CommonParametersTypes/All.h"
#include "NodeManagement/All.h"
#include "NotificationsAndEvents/All.h"
#include "Services/All.h"

#include "Server/OpcUa_NodesTable.h"

#include "Utils/OpcUa_IPCS_Table.h"

#include "Stacks/OpcUa_Channel.h"
#include "Stacks/OpcUa_Certificates.h"

#include "Error/All.h"

/*
 * These are global initializations of global variables that must be done
 * in a given order.
 */

#include <float.h>

#include "Threads/OpcUa_Mutex.h"

/****************************************************************************/
/* Mutex below must be the first...                                         */
MYDLL opcua::Mutex * opcua::RefCount::mutex = new opcua::Mutex() ;        /**/
/* Mutex above must be the first...                                         */
/****************************************************************************/


namespace opcua {

int letsprintprint(const char *s)
{
	debug(STATIC_DBG,"",s) ;
	return 1064 ;
}

int xxx = letsprintprint("Beginning of static initialization") ;

#if (WITH_BROWSE == 1)
MYDLL TableReferenceDescription   * ReferenceDescription::tableZero
	= (TableReferenceDescription   *)((new TableReferenceDescription  (0))->takeAndReturn()) ;
#endif

MYDLL TableUInt32   * UInt32::tableZero
	= (TableUInt32   *)((new TableUInt32  (0))->takeAndReturn()) ;
MYDLL TableString   * String::tableZero
	= (TableString   *)((new TableString  (0))->takeAndReturn()) ;
MYDLL TableCounter   * Counter::tableZero
	= (TableCounter   *)((new TableCounter  (0))->takeAndReturn()) ;
MYDLL TableDiagnosticInfo   * DiagnosticInfo::tableZero
	= (TableDiagnosticInfo   *)((new TableDiagnosticInfo  (0))->takeAndReturn()) ;



MYDLL Mutex * Base::mutex
	= new Mutex() ;


MYDLL const char * const NodesTable::name_space
	= "http://www.cluster-connexion.fr/opc-rosa" ;


#if (MEM_DBG & DEBUG_LEVEL)

MYDLL int32_t RefCount::refTotal  = 0 ;
MYDLL int32_t RefCount::refAlive  = 0 ;

#endif


MYDLL Counter * Counter::zero
	= (Counter *)((new Counter(0))->takeAndReturn()) ;


MYDLL	const char *	 Certificates::dir_str 			      = NULL ;

#if (SECURITY != UANONE && (CERT_VERIF == CERT_VERIF_SELF || CERT_VERIF == CERT_VERIF_PKI))
MYDLL   const char *     Certificates::rootCertificate_str	  = NULL ;
MYDLL   const char *     Certificates::signingCertificate_str = NULL ;
#endif

#if (SECURITY != UANONE && CERT_VERIF == CERT_VERIF_PKI)
MYDLL	const char *     Certificates::crl_str					   = NULL ;
#endif

MYDLL	const char *	 Certificates::self_str 		      = NULL ;

MYDLL   ApplicationInstanceCertificate * Certificates::selfCertificate   = NULL ;

#if (SECURITY != UANONE)
MYDLL	const char * 	 Certificates::privKey_str   	= NULL ;
#endif
#if (SECURITY != UANONE)
MYDLL mbedtls_x509_crt    Certificates::mbedtlsCert    ;
MYDLL mbedtls_pk_context  Certificates::mbedtlsPrivKey ;
#endif

MYDLL String * ApplicationInstanceCertificate::fake_name_tranfer_from_C_to_Cpp
	= (String *)((new String("Transfered certificate fromCtoCpp"))->takeAndReturn()) ;

MYDLL IntegerId * IntegerId::AttributeId_Value_as_IntegerId
	= (IntegerId *)((new IntegerId((int32_t)AttributeId_Value))->takeAndReturn()) ;




MYDLL Byte * Byte::zero
	= (Byte *)((new Byte((uint8_t)0))->takeAndReturn()) ;

MYDLL Byte * Byte::un
	= (Byte *)((new Byte((uint8_t)1))->takeAndReturn()) ;



MYDLL SByte * SByte::zero
	= (SByte *)((new SByte((int8_t)0))->takeAndReturn()) ;



MYDLL Int16 * Int16::zero
	= (Int16 *)((new Int16((int16_t)0))->takeAndReturn()) ;



MYDLL Int64 * Int64::zero
	= (Int64 *)((new Int64((int64_t)0))->takeAndReturn()) ;


MYDLL UInt64 * UInt64::zero
	= (UInt64 *)((new UInt64((uint64_t)0))->takeAndReturn()) ;


MYDLL Float * Float::zero
	= (Float *)((new Float((float)0.0))->takeAndReturn()) ;


MYDLL Double * Double::zero
	= (Double *)((new Double((double)0.0))->takeAndReturn()) ;

MYDLL Double * Double::maximum
	= (Double *)((new Double((double)INT64_MAX)) ->takeAndReturn()) ;



MYDLL ApplicationType * ApplicationType::server_0
	= (ApplicationType *)((new ApplicationType(ApplicationType_SERVER_0))->takeAndReturn()) ;

MYDLL ApplicationType * ApplicationType::client_1
	= (ApplicationType *)((new ApplicationType(ApplicationType_CLIENT_1))->takeAndReturn()) ;

MYDLL ApplicationType * ApplicationType::clientandserver_2
	= (ApplicationType *)((new ApplicationType(ApplicationType_CLIENTANDSERVER_2))->takeAndReturn()) ;

MYDLL ApplicationType * ApplicationType::discoveryserver_3
	= (ApplicationType *)((new ApplicationType(ApplicationType_DISCOVERYSERVER_3))->takeAndReturn()) ;



MYDLL StatusCode * const StatusCode::Good
	= (StatusCode *)((new StatusCode(_Good))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_RequestTypeInvalid
	= (StatusCode *)((new StatusCode(_Bad_RequestTypeInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SecureChannelIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_SecureChannelIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SessionIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_SessionIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_CertificateUriInvalid
	= (StatusCode *)((new StatusCode(_Bad_CertificateUriInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SessionNotActivated
	= (StatusCode *)((new StatusCode(_Bad_SessionNotActivated))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SessionClosed
	= (StatusCode *)((new StatusCode(_Bad_SessionClosed))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NotImplemented
	= (StatusCode *)((new StatusCode(_Bad_NotImplemented))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_IndexRangeInvalid
	= (StatusCode *)((new StatusCode(_Bad_IndexRangeInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_AttributeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_AttributeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_TypeMismatch
	= (StatusCode *)((new StatusCode(_Bad_TypeMismatch))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NotReadable
	= (StatusCode *)((new StatusCode(_Bad_NotReadable))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NotWritable
	= (StatusCode *)((new StatusCode(_Bad_NotWritable))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_UserAccessDenied
	= (StatusCode *)((new StatusCode(_Bad_UserAccessDenied))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NodeIdUnknown
	= (StatusCode *)((new StatusCode(_Bad_NodeIdUnknown))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NotSupported
	= (StatusCode *)((new StatusCode(_Bad_NotSupported))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_TooManyPublishRequests
	= (StatusCode *)((new StatusCode(_Bad_TooManyPublishRequests))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SubscriptionIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_SubscriptionIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NothingToDo
	= (StatusCode *)((new StatusCode(_Bad_NothingToDo))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_MessageNotAvailable
	= (StatusCode *)((new StatusCode(_Bad_MessageNotAvailable))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NoSubscription
	= (StatusCode *)((new StatusCode(_Bad_NoSubscription))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Good_SubscriptionTransferred
	= (StatusCode *)((new StatusCode(_Good_SubscriptionTransferred))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_Timeout
	= (StatusCode *)((new StatusCode(_Bad_Timeout))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_DataEncodingUnsupported
	= (StatusCode *)((new StatusCode(_Bad_DataEncodingUnsupported))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_DataEncodingInvalid
	= (StatusCode *)((new StatusCode(_Bad_DataEncodingInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NodeClassInvalid
	= (StatusCode *)((new StatusCode(_Bad_NodeClassInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_MonitoredItemFilterUnsupported
	= (StatusCode *)((new StatusCode(_Bad_MonitoredItemFilterUnsupported))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_MonitoredItemIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_MonitoredItemIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_TimestampsToReturnInvalid
	= (StatusCode *)((new StatusCode(_Bad_TimestampsToReturnInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NodeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_NodeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_ParentNodeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_ParentNodeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_ReferenceTypeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_ReferenceTypeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NodeIdExists
	= (StatusCode *)((new StatusCode(_Bad_NodeIdExists))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_TypeDefinitionInvalid
	= (StatusCode *)((new StatusCode(_Bad_TypeDefinitionInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NodeAttributesInvalid
	= (StatusCode *)((new StatusCode(_Bad_NodeAttributesInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SourceNodeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_SourceNodeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_TargetNodeIdInvalid
	= (StatusCode *)((new StatusCode(_Bad_TargetNodeIdInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_MaxAgeInvalid
	= (StatusCode *)((new StatusCode(_Bad_MaxAgeInvalid))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_OutOfRange
	= (StatusCode *)((new StatusCode(_Bad_OutOfRange))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_IndexRangeNoData
	= (StatusCode *)((new StatusCode(_Bad_IndexRangeNoData))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_NoMatch
	= (StatusCode *)((new StatusCode(_Bad_NoMatch))->takeAndReturn()) ;

MYDLL StatusCode * const StatusCode::Bad_SequenceNumberUnknown
	= (StatusCode *)((new StatusCode(_Bad_SequenceNumberUnknown))->takeAndReturn()) ;





MYDLL TableStatusCode   * const StatusCode::tableZero
	= (TableStatusCode   *)((new TableStatusCode  (0))->takeAndReturn()) ;




MYDLL DataValue * const DataValue::Bad_AttributeIdInvalid
	= (DataValue *)((new DataValue(StatusCode::Bad_AttributeIdInvalid))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_IndexRangeInvalid
	= (DataValue *)((new DataValue(StatusCode::Bad_IndexRangeInvalid))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_NotImplemented
	= (DataValue *)((new DataValue(StatusCode::Bad_NotImplemented))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_NodeIdUnknown
	= (DataValue *)((new DataValue(StatusCode::Bad_NodeIdUnknown))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_NotReadable
	= (DataValue *)((new DataValue(StatusCode::Bad_NotReadable))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_UserAccessDenied
	= (DataValue *)((new DataValue(StatusCode::Bad_UserAccessDenied))->takeAndReturn()) ;

MYDLL DataValue * const DataValue::Bad_TimestampsToReturnInvalid
	= (DataValue *)((new DataValue(StatusCode::Bad_TimestampsToReturnInvalid))->takeAndReturn()) ;




MYDLL String * String::empty
	= (String *)((new String(NULL))->takeAndReturn()) ;



MYDLL Int32 * Int32::minusTwo
	= (Int32 *)((new Int32((int32_t)-2))->takeAndReturn()) ;

MYDLL Int32 * Int32::minusOne
	= (Int32 *)((new Int32((int32_t)-1))->takeAndReturn()) ;

MYDLL Int32 * Int32::zero
	= (Int32 *)((new Int32((int32_t)0))->takeAndReturn()) ;

MYDLL Int32 * Int32::maximum
	= (Int32 *)((new Int32(INT32_MAX))->takeAndReturn()) ;




MYDLL UInt16 * UInt16::zero
	= (UInt16 *)((new UInt16((uint16_t)0)) ->takeAndReturn()) ;



MYDLL UInt32 * UInt32::maximum
	= (UInt32 *)((new UInt32(UINT32_MAX))->takeAndReturn()) ;

MYDLL UInt32 * UInt32::zero
	= (UInt32 *)((new UInt32((uint32_t)0)) ->takeAndReturn()) ;




MYDLL LocaleId * LocaleId::empty
	= (LocaleId *)((new LocaleId(NULL))->takeAndReturn()) ;

MYDLL LocaleId * LocaleId::en
	= (LocaleId *)((new LocaleId("en"))->takeAndReturn()) ;

MYDLL LocaleId * LocaleId::fr
	= (LocaleId *)((new LocaleId("fr"))->takeAndReturn()) ;



MYDLL NodeClass * NodeClass::object_1
	= (NodeClass *)((new NodeClass(NodeClass_OBJECT_1))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::variable_2
	= (NodeClass *)((new NodeClass(NodeClass_VARIABLE_2))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::method_4
	= (NodeClass *)((new NodeClass(NodeClass_METHOD_4))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::object_type_8
	= (NodeClass *)((new NodeClass(NodeClass_OBJECT_TYPE_8))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::variable_type_16
	= (NodeClass *)((new NodeClass(NodeClass_VARIABLE_TYPE_16))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::reference_type_32
	= (NodeClass *)((new NodeClass(NodeClass_REFERENCE_TYPE_32))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::data_type_64
	= (NodeClass *)((new NodeClass(NodeClass_DATA_TYPE_64))->takeAndReturn()) ;

MYDLL NodeClass * NodeClass::view_128
	= (NodeClass *)((new NodeClass(NodeClass_VIEW_128))->takeAndReturn()) ;



MYDLL NamingRuleType * NamingRuleType::mandatory_1
	= (NamingRuleType *)((new NamingRuleType(NamingRuleType_MANDATORY_1))->takeAndReturn()) ;

MYDLL NamingRuleType * NamingRuleType::optional_2
	= (NamingRuleType *)((new NamingRuleType(NamingRuleType_OPTIONAL_2))->takeAndReturn()) ;

MYDLL NamingRuleType * NamingRuleType::constraint_3
	= (NamingRuleType *)((new NamingRuleType(NamingRuleType_CONSTRAINT_3))->takeAndReturn()) ;



MYDLL IdType * IdType::numeric_0
	= (IdType *)((new IdType(IdType_NUMERIC_0))->takeAndReturn()) ;

MYDLL IdType * IdType::string_1
	= (IdType *)((new IdType(IdType_STRING_1))->takeAndReturn()) ;

MYDLL IdType * IdType::guid_2
	= (IdType *)((new IdType(IdType_GUID_2))->takeAndReturn()) ;

MYDLL IdType * IdType::opaque_3
	= (IdType *)((new IdType(IdType_OPAQUE_3))->takeAndReturn()) ;


#if WITH_READ == 1
MYDLL TimestampsToReturn * TimestampsToReturn::source_0
	= (TimestampsToReturn *)((new TimestampsToReturn(TimestampsToReturn_SOURCE_0))->takeAndReturn()) ;

MYDLL TimestampsToReturn * TimestampsToReturn::server_1
	= (TimestampsToReturn *)((new TimestampsToReturn(TimestampsToReturn_SERVER_1))->takeAndReturn()) ;

MYDLL TimestampsToReturn * TimestampsToReturn::both_2
	= (TimestampsToReturn *)((new TimestampsToReturn(TimestampsToReturn_BOTH_2))->takeAndReturn()) ;

MYDLL TimestampsToReturn * TimestampsToReturn::neither_3
	= (TimestampsToReturn *)((new TimestampsToReturn(TimestampsToReturn_NEITHER_3))->takeAndReturn()) ;
#endif




/* Before ContinuationPoint */
MYDLL ByteString * ByteString::empty
	= (ByteString *)((new ByteString((int32_t)0,(uint8_t *)NULL))->takeAndReturn()) ;



#if (WITH_BROWSE == 1)
/* After ContinuationPoint, Before BrowseResult */
MYDLL ContinuationPoint * ContinuationPoint::empty
	= (ContinuationPoint *)((new ContinuationPoint((int32_t)0,NULL))->takeAndReturn()) ;
#endif


MYDLL Boolean * Boolean::booleanFalse
	= (Boolean *)((new Boolean(0)) ->takeAndReturn()) ;

MYDLL Boolean * Boolean::booleanTrue
	= (Boolean *)((new Boolean(1)) ->takeAndReturn()) ;



MYDLL String * SecurityProfileUri::SecurityPolicy_None
	= (String *)(new String("http://opcfoundation.org/UA/SecurityPolicy#None"))->takeAndReturn() ;

MYDLL String * SecurityProfileUri::SecurityPolicy_Basic128Rsa15
	= (String *)(new String("http://opcfoundation.org/UA/SecurityPolicy#Basic128Rsa15"))->takeAndReturn() ;

MYDLL String * SecurityProfileUri::SecurityPolicy_256
	= (String *)(new String("http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256"))->takeAndReturn() ;



MYDLL SecurityTokenRequestType * SecurityTokenRequestType::issue_0
	= (SecurityTokenRequestType *)((new SecurityTokenRequestType(SecurityTokenRequestType_ISSUE_0))->takeAndReturn()) ;

MYDLL SecurityTokenRequestType * SecurityTokenRequestType::renew_1
	= (SecurityTokenRequestType *)((new SecurityTokenRequestType(SecurityTokenRequestType_RENEW_1))->takeAndReturn()) ;



MYDLL UserIdentityTokenType * UserIdentityTokenType::anonymous_0
	= (UserIdentityTokenType *)((new UserIdentityTokenType(UserIdentityTokenType_ANONYMOUS_0))->takeAndReturn()) ;

MYDLL UserIdentityTokenType * UserIdentityTokenType::username_1
	= (UserIdentityTokenType *)((new UserIdentityTokenType(UserIdentityTokenType_USERNAME_1))->takeAndReturn()) ;

MYDLL UserIdentityTokenType * UserIdentityTokenType::certificate_2
	= (UserIdentityTokenType *)((new UserIdentityTokenType(UserIdentityTokenType_CERTIFICATE_2))->takeAndReturn()) ;

MYDLL UserIdentityTokenType * UserIdentityTokenType::issuedtoken_3
	= (UserIdentityTokenType *)((new UserIdentityTokenType(UserIdentityTokenType_ISSUEDTOKEN_3))->takeAndReturn()) ;



#if (WITH_SUBSCRIPTION == 1)

MYDLL MonitoringMode * MonitoringMode::disabled_0
	= (MonitoringMode *)((new MonitoringMode(MonitoringMode_DISABLED_0))->takeAndReturn()) ;

MYDLL MonitoringMode * MonitoringMode::sampling_1
	= (MonitoringMode *)((new MonitoringMode(MonitoringMode_SAMPLING_1))->takeAndReturn()) ;

MYDLL MonitoringMode * MonitoringMode::reporting_2
	= (MonitoringMode *)((new MonitoringMode(MonitoringMode_REPORTING_2))->takeAndReturn()) ;

#endif



MYDLL MessageSecurityMode * MessageSecurityMode::invalid_0
	= (MessageSecurityMode *)((new MessageSecurityMode(MessageSecurityMode_INVALID_0))->takeAndReturn()) ;

MYDLL MessageSecurityMode * MessageSecurityMode::none_1
	= (MessageSecurityMode *)((new MessageSecurityMode(MessageSecurityMode_NONE_1))->takeAndReturn()) ;

MYDLL MessageSecurityMode * MessageSecurityMode::sign_2
	= (MessageSecurityMode *)((new MessageSecurityMode(MessageSecurityMode_SIGN_2))->takeAndReturn()) ;

MYDLL MessageSecurityMode * MessageSecurityMode::signandencrypt_3
	= (MessageSecurityMode *)((new MessageSecurityMode(MessageSecurityMode_SIGNANDENCRYPT_3))->takeAndReturn()) ;



MYDLL DiagnosticInfo * DiagnosticInfo::fakeDiagnosticInfo
	= (DiagnosticInfo *)((new DiagnosticInfo(NULL,NULL,NULL,NULL,NULL,NULL,NULL))->takeAndReturn()) ;



MYDLL SignatureData * SignatureData::fakeSignatureData
	= (SignatureData *)((new SignatureData(String::empty,ByteString::empty))->takeAndReturn()) ;


MYDLL SignedSoftwareCertificate * const SignedSoftwareCertificate::fakeSignedSoftwareCertificate
	= (SignedSoftwareCertificate *)((new SignedSoftwareCertificate(ByteString::empty,ByteString::empty))->takeAndReturn()) ;

MYDLL String * TransportProfileUri::Transport_TcpMessage_UaNone_UaBinary
	= (String *)(new String("http://www.cluster-connexion.fr/UA-Profiles/Transport/tcpmessage-uanone-uabinary"))->takeAndReturn() ;




MYDLL ExpandedNodeId * AnonymousIdentityToken::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_AnonymousIdentityToken_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * UserNameIdentityToken::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_UserNameIdentityToken_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * X509IdentityToken::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_X509IdentityToken_Encoding_DefaultXml))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * IssuedIdentityToken::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_IssuedIdentityToken_Encoding_DefaultXml))))->takeAndReturn()) ;




MYDLL NumericRange * NumericRange::null
	= (NumericRange *)((new NumericRange(NULL))->takeAndReturn()) ;



MYDLL QualifiedName * QualifiedName::null
	= (QualifiedName *)((new QualifiedName(UInt16::zero,String::empty))->takeAndReturn()) ;

MYDLL QualifiedName * QualifiedName::defaultBinary
	= (QualifiedName *)((new QualifiedName(UInt16::zero,new String("DefaultBinary")))->takeAndReturn()) ;

MYDLL QualifiedName * QualifiedName::defaultXML
	= (QualifiedName *)((new QualifiedName(UInt16::zero,new String("DefaultXML")))->takeAndReturn()) ;




MYDLL Alea * Alea::alea
	= new Alea() ;



#if (WITH_SUBSCRIPTION == 1)

MYDLL ExpandedNodeId * DataChangeNotification::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_DataChangeNotification_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * EventNotificationList::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_EventNotificationList_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * StatusChangeNotification::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_StatusChangeNotification_Encoding_DefaultBinary))))->takeAndReturn()) ;

#endif


#if (WITH_SUBSCRIPTION == 1)

MYDLL ExpandedNodeId * DataChangeFilter::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_DataChangeFilter_Encoding_DefaultBinary))))->takeAndReturn()) ;

//MYDLL ExpandedNodeId * EventFilter::expandedNodeId
//	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_EventFilter_Encoding_DefaultBinary))))->takeAndReturn()) ;

//MYDLL ExpandedNodeId * AggregateFilter::expandedNodeId
//	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_AggregateFilter_Encoding_DefaultBinary))))->takeAndReturn()) ;

#endif



#if (WITH_SUBSCRIPTION == 1)

MYDLL DataChangeTrigger * DataChangeTrigger::status_0
	= (DataChangeTrigger *)((new DataChangeTrigger(DataChangeTrigger_STATUS_0))->takeAndReturn()) ;

MYDLL DataChangeTrigger * DataChangeTrigger::status_value_1
	= (DataChangeTrigger *)((new DataChangeTrigger(DataChangeTrigger_STATUS_VALUE_1))->takeAndReturn()) ;

MYDLL DataChangeTrigger * DataChangeTrigger::status_value_timestamp_2
	= (DataChangeTrigger *)((new DataChangeTrigger(DataChangeTrigger_STATUS_VALUE_TIMESTAMP_2))->takeAndReturn()) ;

#endif




#if (WITH_SUBSCRIPTION == 1)

MYDLL DataChangeFilter * DataChangeFilter::nullFilter
	= (DataChangeFilter *)((new DataChangeFilter(DataChangeTrigger::status_value_1, new UInt32(NONE_0), Double::zero))->takeAndReturn()) ;

#endif




#if (WITH_SUBSCRIPTION == 1)

MYDLL MonitoringNullFilterResult * MonitoringNullFilterResult::nullFilterResult
	= (MonitoringNullFilterResult *)((new MonitoringNullFilterResult())->takeAndReturn()) ;

#endif



#if (WITH_SUBSCRIPTION == 1)

MYDLL ExpandedNodeId * MonitoringNullFilterResult::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_MonitoringFilterResult_Encoding_DefaultBinary))))->takeAndReturn()) ;

//MYDLL ExpandedNodeId * EventFilterResult::expandedNodeId
//	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new String(OpcUaId_EventFilterResult_Encoding_DefaultBinary))))->takeAndReturn()) ;
//
//MYDLL ExpandedNodeId * AggregateFilterResult::expandedNodeId
//	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new String(OpcUaId_AggregateFilterResult_Encoding_DefaultBinary))))->takeAndReturn()) ;

#endif



MYDLL NodeId * Boolean::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Boolean")))->takeAndReturn()) ;

MYDLL NodeId * Byte::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Byte")))->takeAndReturn()) ;

MYDLL NodeId * Int32::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Int32")))->takeAndReturn()) ;

MYDLL NodeId * Int64::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Int64")))->takeAndReturn()) ;

MYDLL NodeId * Float::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Float")))->takeAndReturn()) ;

MYDLL NodeId * Double::nodeId
	= (NodeId *)((new NodeId(UInt16::zero,new String("Double")))->takeAndReturn()) ;



#if (WITH_NODEMNGT == 1)

MYDLL ExpandedNodeId * ObjectAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_ObjectAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * VariableAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_VariableAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * MethodAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_MethodAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * ObjectTypeAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_ObjectTypeAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * VariableTypeAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_VariableTypeAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * ReferenceTypeAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_ReferenceTypeAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * DataTypeAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_DataTypeAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * ViewAttributes::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_ViewAttributes_Encoding_DefaultBinary))))->takeAndReturn()) ;

#endif



#if (WITH_QUERY == 1)

MYDLL FilterOperator * FilterOperator::Equals_0
	= (FilterOperator *)((new FilterOperator(FilterOperator_EQUALS_0))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::IsNull_1
	= (FilterOperator *)((new FilterOperator(FilterOperator_ISNULL_1))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::GreaterThan_2
	= (FilterOperator *)((new FilterOperator(FilterOperator_GREATERTHAN_2))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::LessThan_3
	= (FilterOperator *)((new FilterOperator(FilterOperator_LESSTHAN_3))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::GreaterThanOrEqual_4
	= (FilterOperator *)((new FilterOperator(FilterOperator_GREATERTHANOREQUAL_4))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::LessThanOrEqual_5
	= (FilterOperator *)((new FilterOperator(FilterOperator_LESSTHANOREQUAL_5))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::Like_6
	= (FilterOperator *)((new FilterOperator(FilterOperator_LIKE_6))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::Not_7
	= (FilterOperator *)((new FilterOperator(FilterOperator_NOT_7))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::Between_8
	= (FilterOperator *)((new FilterOperator(FilterOperator_BETWEEN_8))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::InList_9
	= (FilterOperator *)((new FilterOperator(FilterOperator_INLIST_9))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::And_10
	= (FilterOperator *)((new FilterOperator(FilterOperator_AND_10))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::Or_11
	= (FilterOperator *)((new FilterOperator(FilterOperator_OR_11))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::Cast_12
	= (FilterOperator *)((new FilterOperator(FilterOperator_CAST_12))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::InView_13
	= (FilterOperator *)((new FilterOperator(FilterOperator_INVIEW_13))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::OfType_14
	= (FilterOperator *)((new FilterOperator(FilterOperator_OFTYPE_14))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::RelatedTo_15
	= (FilterOperator *)((new FilterOperator(FilterOperator_RELATEDTO_15))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::BitwiseAnd_16
	= (FilterOperator *)((new FilterOperator(FilterOperator_BITWISEAND_16))->takeAndReturn()) ;

MYDLL FilterOperator * FilterOperator::BitwiseOr_17
	= (FilterOperator *)((new FilterOperator(FilterOperator_BITWISEOR_17))->takeAndReturn()) ;

#endif



#if (WITH_QUERY == 1)

MYDLL ExpandedNodeId * ElementOperand::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_ElementOperand_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * LiteralOperand::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_LiteralOperand_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * AttributeOperand::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_AttributeOperand_Encoding_DefaultBinary))))->takeAndReturn()) ;

MYDLL ExpandedNodeId * SimpleAttributeOperand::expandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(new NodeId(UInt16::zero,new UInt32(OpcUaId_SimpleAttributeOperand_Encoding_DefaultBinary))))->takeAndReturn()) ;

#endif



#if (WITH_BROWSE == 1)

MYDLL BrowseDirection * BrowseDirection::forward_0
	= (BrowseDirection *)((new BrowseDirection(BrowseDirection_FORWARD_0))->takeAndReturn()) ;

MYDLL BrowseDirection * BrowseDirection::inverse_1
	= (BrowseDirection *)((new BrowseDirection(BrowseDirection_INVERSE_1))->takeAndReturn()) ;

MYDLL BrowseDirection * BrowseDirection::both_2
	= (BrowseDirection *)((new BrowseDirection(BrowseDirection_BOTH_2))->takeAndReturn()) ;

#endif



/* After ContinuationTime */
#if (WITH_BROWSE == 1)

MYDLL BrowseResult * BrowseResult::Bad_NodeIdInvalid
	= (BrowseResult *)((new BrowseResult(StatusCode::Bad_NodeIdInvalid))->takeAndReturn()) ;

MYDLL BrowseResult * BrowseResult::Bad_ReferenceTypeIdInvalid
	= (BrowseResult *)((new BrowseResult(StatusCode::Bad_ReferenceTypeIdInvalid))->takeAndReturn()) ;

#endif



// Before ExpandedNodeId::nullExpandedNodeId
MYDLL NodeId * NodeId::nullNodeId
	= (NodeId *)((new NodeId(UInt16::zero,UInt32::zero))->takeAndReturn()) ;



// After NodeId::nullNodeId
MYDLL ExpandedNodeId * const ExpandedNodeId::nullExpandedNodeId
	= (ExpandedNodeId *)((new ExpandedNodeId(NodeId::nullNodeId))->takeAndReturn()) ;



MYDLL LocalizedText * LocalizedText::null
	= (LocalizedText *)((new LocalizedText(String::empty,LocaleId::empty))->takeAndReturn()) ;



MYDLL UtcTime * UtcTime::null
	= (UtcTime *)((new UtcTime(0))->takeAndReturn()) ;


#if (WITH_BROWSE == 1)
MYDLL ViewDescription * ViewDescription::nullView
	= (ViewDescription *)((new ViewDescription(NodeId::nullNodeId,UtcTime::null,UInt32::zero))->takeAndReturn()) ;
#endif


#if (WITH_NODEMNGT == 1)

MYDLL AddNodesResult * const AddNodesResult::Bad_ParentNodeIdInvalid
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_ParentNodeIdInvalid))->takeAndReturn()) ;

MYDLL AddNodesResult * const AddNodesResult::Bad_NotImplemented
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_NotImplemented))->takeAndReturn()) ;

MYDLL AddNodesResult * const AddNodesResult::Bad_ReferenceTypeIdInvalid
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_ReferenceTypeIdInvalid))->takeAndReturn()) ;

MYDLL AddNodesResult * const AddNodesResult::Bad_NodeIdExists
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_NodeIdExists))->takeAndReturn()) ;

MYDLL AddNodesResult * const AddNodesResult::Bad_TypeDefinitionInvalid
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_TypeDefinitionInvalid))->takeAndReturn()) ;

MYDLL AddNodesResult * const AddNodesResult::Bad_NodeAttributesInvalid
	= (AddNodesResult *)((new AddNodesResult(StatusCode::Bad_NodeAttributesInvalid))->takeAndReturn()) ;

#endif





MYDLL ExtensionObject * ExtensionObject::null
	= (ExtensionObject *)((new ExtensionObject(ExpandedNodeId::nullExpandedNodeId,Byte::zero,NULL))->takeAndReturn()) ;




MYDLL Nonce * Nonce::zero
	= (Nonce *)((new Nonce((int32_t)0,(uint8_t *)NULL))->takeAndReturn()) ;




MYDLL TableEndpointDescription   * EndpointDescription::selfEndpoints
	= NULL ;



MYDLL ChannelsTable * ChannelsTable::self
	= NULL ;




/*
 * To stop the debugger : call stop(0)
 */

void stop_for_debug(int n)
{
	*(int *)(&n) /= n ;
}

/*
 * To stop the debugger : put a break point
 */

void stop_for_debug()
{}

/*
 * Printing an array of uint8_t
 */

void print_uint8_array(const char * msg, int32_t length, uint8_t * array)
{
	char my_array[3*1024] ;

	sprintf(my_array,"%s (%d):",msg,length) ;
	debug(SELF_DBG,"print_uint8_array",my_array) ;

	sprintf(my_array,"   %3d : ",0) ;
	for (int i = 0 ; i < length ; i++) {
		sprintf(my_array+strlen(my_array),"0x%02x ",array[i]) ;
		if (i % 25 == 0 && i != length)  {
			debug(SELF_DBG,"print_uint8_array",my_array) ;
			sprintf(my_array,"   %3d : ",i) ;
		}
	}
	debug(SELF_DBG,"print_uint8_array",my_array) ;
}

MYDLL void reexit(int n)
{
	exit(n) ;
}


int yyy = letsprintprint("End of static initialization") ;

} /* namespace opcua */
