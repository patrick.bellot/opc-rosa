
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NODEMANAGEMENT_ALL_H_
#define NODEMANAGEMENT_ALL_H_

#include "OpcUa_IPCS_AddNodesItem.h"
#include "OpcUa_IPCS_AddNodesResult.h"
#include "OpcUa_IPCS_AddReferencesItem.h"
#include "OpcUa_IPCS_DataTypeAttributes.h"
#include "OpcUa_IPCS_MethodAttributes.h"
#include "OpcUa_IPCS_NodeAttributes.h"
#include "OpcUa_IPCS_ObjectAttributes.h"
#include "OpcUa_IPCS_ObjectTypeAttributes.h"
#include "OpcUa_IPCS_ReferenceTypeAttributes.h"
#include "OpcUa_IPCS_VariableAttributes.h"
#include "OpcUa_IPCS_VariableTypeAttributes.h"
#include "OpcUa_IPCS_ViewAttributes.h"

#endif /* NODEMANAGEMENT_ALL_H_ */
