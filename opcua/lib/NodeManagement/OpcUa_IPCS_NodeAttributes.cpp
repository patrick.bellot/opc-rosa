
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../NodeManagement/All.h"

namespace opcua {

	MYDLL NodeAttributes * NodeAttributes::fromCtoCpp(SOPC_StatusCode * pStatus, SOPC_ExtensionObject const& pNodeAttributes)
	{
		ExpandedNodeId * expandedNodeId   = ExpandedNodeId::fromCtoCpp(pStatus,pNodeAttributes.TypeId) ;

		if (*pStatus != STATUS_OK)
			return NULL ;

		NodeId * parameterTypeId = expandedNodeId->getNodeId() ;

		Byte * encoding = NULL ;

		if (parameterTypeId->getNamespaceIndex()->get() == 0 && parameterTypeId->getIdentifierType()->get() == IdType_NUMERIC_0) {

			encoding = Byte::fromCtoCpp(pStatus,pNodeAttributes.Encoding) ;

			if (*pStatus != STATUS_OK) {
				*pStatus = _Bad_DecodingError ;
				expandedNodeId->checkRefCount() ;
				return NULL ;
			}

			if (encoding->equals((uint8_t)0x03)) {
				switch (pNodeAttributes.Body.Object.ObjType->TypeId) {
		    	case OpcUaId_ObjectAttributes:
				{
					_OpcUa_ObjectAttributes * tokenObjectAttributes = static_cast<_OpcUa_ObjectAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return ObjectAttributes::fromCtoCpp(pStatus,*tokenObjectAttributes) ;
				}
		    	case OpcUaId_VariableAttributes:
				{
					_OpcUa_VariableAttributes * tokenVariableAttributes = static_cast<_OpcUa_VariableAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return VariableAttributes::fromCtoCpp(pStatus,*tokenVariableAttributes) ;
				}
		    	case OpcUaId_MethodAttributes:
				{
					_OpcUa_MethodAttributes * tokenMethodAttributes = static_cast<_OpcUa_MethodAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return MethodAttributes::fromCtoCpp(pStatus,*tokenMethodAttributes) ;
				}
		    	case OpcUaId_ObjectTypeAttributes:
				{
					_OpcUa_ObjectTypeAttributes * tokenObjectTypeAttributes = static_cast<_OpcUa_ObjectTypeAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return ObjectTypeAttributes::fromCtoCpp(pStatus,*tokenObjectTypeAttributes) ;
				}
		    	case OpcUaId_VariableTypeAttributes:
				{
					_OpcUa_VariableTypeAttributes * tokenVariableTypeAttributes = static_cast<_OpcUa_VariableTypeAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return VariableTypeAttributes::fromCtoCpp(pStatus,*tokenVariableTypeAttributes) ;
				}

		    	case OpcUaId_ReferenceTypeAttributes:
				{
					_OpcUa_ReferenceTypeAttributes * tokenReferenceTypeAttributes = static_cast<_OpcUa_ReferenceTypeAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return ReferenceTypeAttributes::fromCtoCpp(pStatus,*tokenReferenceTypeAttributes) ;
				}
		       	case OpcUaId_DataTypeAttributes:
				{
					_OpcUa_DataTypeAttributes * tokenDataTypeAttributes = static_cast<_OpcUa_DataTypeAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return DataTypeAttributes::fromCtoCpp(pStatus,*tokenDataTypeAttributes) ;
				}
		       	case OpcUaId_ViewAttributes:
				{
					_OpcUa_ViewAttributes * tokenViewAttributes = static_cast<_OpcUa_ViewAttributes *>(pNodeAttributes.Body.Object.Value) ;
					encoding->checkRefCount() ;
					expandedNodeId->checkRefCount() ;
					return ViewAttributes::fromCtoCpp(pStatus,*tokenViewAttributes) ;
				}

		    	default:
					break ;
				}
			} else {
				debug_i(COM_ERR,"NodeAttributes","fromCtoCpp: Encoding is not 0x01, it is 0x%02x",encoding->get()) ;
			}
		}

		*pStatus = _Bad_DecodingError ;

		if (expandedNodeId != NULL)
			expandedNodeId->checkRefCount() ;
		if (encoding != NULL)
			encoding->checkRefCount() ;

		return NULL ;
	}

}
#endif // NODEMNGT == 1



