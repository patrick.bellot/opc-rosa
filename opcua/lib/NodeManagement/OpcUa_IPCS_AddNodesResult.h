
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDNODESRESULT_H_
#define OPCUA_ADDNODESRESULT_H_

/*
 * OPC-UA, Part 4, 5.7.2.2, p. 32, inside Table 18
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../CommonParametersTypes/All.h"

namespace opcua {

class MYDLL AddNodesResult
		: public Structure
{
public:

	static AddNodesResult * const Bad_NotImplemented ;
	static AddNodesResult * const Bad_ParentNodeIdInvalid ;
	static AddNodesResult * const Bad_ReferenceTypeIdInvalid ;
	static AddNodesResult * const Bad_NodeIdExists ;
	static AddNodesResult * const Bad_TypeDefinitionInvalid ;
	static AddNodesResult * const Bad_NodeAttributesInvalid ;

public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddNodesResult ; }

private:

	StatusCode * statusCode ;
	NodeId     * addedNodeId ;

public:

	static AddNodesResult * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_AddNodesResult const& pAddNodesResult)
	{
		StatusCode * statusCode  = StatusCode ::fromCtoCpp( pStatus, pAddNodesResult.StatusCode) ;
		NodeId     * addedNodeId = NodeId     ::fromCtoCpp( pStatus, pAddNodesResult.AddedNodeId) ;

		if (*pStatus == STATUS_OK)
			return
					new AddNodesResult(
							statusCode,
							addedNodeId
							) ;

		if (statusCode != NULL)
			statusCode->checkRefCount() ;
		if (addedNodeId != NULL)
			addedNodeId->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_AddNodesResult& pAddNodesResult) const
	{
		statusCode  ->fromCpptoC (pStatus, pAddNodesResult.StatusCode) ;
		addedNodeId ->fromCpptoC (pStatus, pAddNodesResult.AddedNodeId) ;
}

private:

	AddNodesResult(
			StatusCode * _statusCode,
			NodeId     * _addedNodeId
			)
		: Structure()
	{
		(statusCode  = _statusCode)  ->take() ;
		(addedNodeId = _addedNodeId) ->take() ;
	}

public:

	AddNodesResult(
			StatusCode * _statusCode
			)
		: Structure()
	{
		(statusCode  = _statusCode)        ->take() ;
		(addedNodeId = NodeId::nullNodeId) -> take() ;
	}

	AddNodesResult(
			NodeId     * _addedNodeId
			)
		: Structure()
	{
		(statusCode  = StatusCode::Good) ->take() ;
		(addedNodeId = _addedNodeId)     ->take() ;
	}

	virtual ~AddNodesResult()
	{
		statusCode  ->release() ;
		addedNodeId ->release() ;
	}

public:

	inline StatusCode * getStatusCode()
	{
		return statusCode ;
	}

	inline NodeId * getAddedNodeId()
	{
		return addedNodeId ;
	}

};
} /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDNODESRESULT_H_ */
