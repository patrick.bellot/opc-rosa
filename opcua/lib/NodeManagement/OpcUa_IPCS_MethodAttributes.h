
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_METHOD_ATTRIBUTES_H_
#define OPCUA_METHOD_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.4, p. 130
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL MethodAttributes
	: public NodeAttributes
{
public:
	// This function is just to keep compatibility with OPC-UA/ROSA: there was a field
	// named String * aux which contains the DLL of a Method.

	inline String * getAux() { return NULL ; }

public:

	virtual uint32_t getTypeId() const { return OpcUaId_MethodAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Boolean       * executable ;
	Boolean       * userExecutable ;

public:

	static MethodAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_MethodAttributes const& pMethodAttributes)
	{
		uint32_t specifiedAttributes = pMethodAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pMethodAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pMethodAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pMethodAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pMethodAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Boolean * executable ;
		if (specifiedAttributes & NodeAttributeId_Executable)
			executable = Boolean ::fromCtoCpp(pStatus,pMethodAttributes.Executable) ;
		else
			executable = NULL ;

		Boolean * userExecutable ;
		if (specifiedAttributes & NodeAttributeId_UserExecutable)
			userExecutable = Boolean ::fromCtoCpp(pStatus,pMethodAttributes.UserExecutable) ;
		else
			userExecutable = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new MethodAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							executable,
							userExecutable
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (executable != NULL)
			executable->checkRefCount() ;
		if (userExecutable != NULL)
			userExecutable->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pMethodAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pMethodAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pMethodAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pMethodAttributes.Body.Object.ObjType = &OpcUa_MethodAttributes_EncodeableType ;
			pMethodAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_MethodAttributes)) ;
			OpcUa_MethodAttributes_Initialize(pMethodAttributes.Body.Object.Value) ;
			MethodAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_MethodAttributes *>(pMethodAttributes.Body.Object.Value)) ;
			pMethodAttributes.Length = sizeof(_OpcUa_MethodAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_MethodAttributes& pMethodAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pMethodAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pMethodAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pMethodAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pMethodAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pMethodAttributes.UserWriteMask) ;

		if (executable != NULL)
			executable ->fromCpptoC(pStatus,pMethodAttributes.Executable) ;
		if (userExecutable != NULL)
			userExecutable ->fromCpptoC(pStatus,pMethodAttributes.UserExecutable) ;
	}

public:

	MethodAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Boolean       * _executable,
			Boolean       * _userExecutable
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;

		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((executable = _executable)) {
			_specifiedAttributes |= NodeAttributeId_Executable ;
		} else {
			executable = Boolean::booleanFalse ;
		}
		executable->take() ;

		if ((userExecutable = _userExecutable)) {
			_specifiedAttributes |= NodeAttributeId_UserExecutable ;
		} else {
			userExecutable = Boolean::booleanFalse ;
		}
		userExecutable->take() ;

		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~MethodAttributes()
	{
		specifiedAttributes ->release() ;

		displayName         ->release() ;
		description         ->release() ;

		writeMask           ->release() ;
		userWriteMask       ->release() ;

		executable          ->release() ;
		userExecutable      ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}

	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}


	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}


	inline Boolean * getExecutable()
	{
		return executable ;
	}

	inline Boolean * getUserExecutable()
	{
		return userExecutable ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_METHOD_ATTRIBUTES_H_ */
