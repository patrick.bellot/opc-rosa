
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_ADDNODESITEM_H_
#define OPCUA_ADDNODESITEM_H_

/*
 * OPC-UA, Part 4, 5.7.2.2, p. 32, inside Table 18
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL AddNodesItem
		: public Structure
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_AddNodesItem ; }

private:

	ExpandedNodeId * parentNodeId ;
	NodeId         * referenceTypeId ;
	ExpandedNodeId * requestedNewNodeId ;
	QualifiedName  * browseName ;
	NodeClass      * nodeClass ;

	NodeAttributes * nodeAttributes ;
	ExpandedNodeId * typeDefinition ;

public:

	static AddNodesItem * fromCtoCpp(SOPC_StatusCode * pStatus, OpcUa_AddNodesItem const& pAddNodesItem)
	{
		ExpandedNodeId * parentNodeId       = ExpandedNodeId ::fromCtoCpp( pStatus, pAddNodesItem.ParentNodeId) ;
		NodeId         * referenceTypeId    = NodeId         ::fromCtoCpp( pStatus, pAddNodesItem.ReferenceTypeId) ;
		ExpandedNodeId * requestedNewNodeId = ExpandedNodeId ::fromCtoCpp( pStatus, pAddNodesItem.RequestedNewNodeId) ;
		QualifiedName  * browseName         = QualifiedName  ::fromCtoCpp( pStatus, pAddNodesItem.BrowseName) ;
		NodeClass      * nodeClass          = NodeClass      ::fromCtoCpp( pStatus, pAddNodesItem.NodeClass) ;

		NodeAttributes * nodeAttributes     = NodeAttributes ::fromCtoCpp( pStatus, pAddNodesItem.NodeAttributes) ;
		ExpandedNodeId * typeDefinition     = ExpandedNodeId ::fromCtoCpp( pStatus, pAddNodesItem.TypeDefinition) ;

		if (*pStatus == STATUS_OK)
			return
					new AddNodesItem(
							parentNodeId,
							referenceTypeId,
							requestedNewNodeId,
							browseName,
							nodeClass,

							nodeAttributes,
							typeDefinition
							) ;

		if (parentNodeId != NULL)
			parentNodeId->checkRefCount() ;
		if (referenceTypeId != NULL)
			referenceTypeId->checkRefCount() ;
		if (requestedNewNodeId != NULL)
			requestedNewNodeId->checkRefCount() ;
		if (browseName != NULL)
			browseName->checkRefCount() ;
		if (nodeClass != NULL)
			nodeClass->checkRefCount() ;

		if (nodeAttributes != NULL)
			nodeAttributes->checkRefCount() ;
		if (typeDefinition != NULL)
			typeDefinition->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, OpcUa_AddNodesItem& pAddNodesItem) const
	{
		parentNodeId       ->fromCpptoC (pStatus, pAddNodesItem.ParentNodeId) ;
		referenceTypeId    ->fromCpptoC (pStatus, pAddNodesItem.ReferenceTypeId) ;
		requestedNewNodeId ->fromCpptoC (pStatus, pAddNodesItem.RequestedNewNodeId) ;
		browseName         ->fromCpptoC (pStatus, pAddNodesItem.BrowseName) ;
		nodeClass          ->fromCpptoC (pStatus, pAddNodesItem.NodeClass) ;

		nodeAttributes     ->fromCpptoC (pStatus, pAddNodesItem.NodeAttributes) ;
		typeDefinition     ->fromCpptoC (pStatus, pAddNodesItem.TypeDefinition) ;
}

public:

	AddNodesItem(
			ExpandedNodeId * _parentNodeId,
			NodeId         * _referenceTypeId,
			ExpandedNodeId * _requestedNewNodeId,
			QualifiedName  * _browseName,
			NodeClass      * _nodeClass,
			NodeAttributes * _nodeAttributes,
			ExpandedNodeId * _typeDefinition
			)
		: Structure()
	{
		(parentNodeId       = _parentNodeId)       ->take() ;
		(referenceTypeId    = _referenceTypeId)    ->take() ;
		(requestedNewNodeId = _requestedNewNodeId) ->take() ;
		(browseName         = _browseName)         ->take() ;
		(nodeClass          = _nodeClass)          ->take() ;
		(nodeAttributes     = _nodeAttributes)     ->take() ;
		(typeDefinition     = _typeDefinition)     ->take() ;
	}

	virtual ~AddNodesItem()
	{
		parentNodeId       ->release() ;
		referenceTypeId    ->release() ;
		requestedNewNodeId ->release() ;
		browseName         ->release() ;
		nodeClass          ->release() ;
		nodeAttributes     ->release() ;
		typeDefinition     ->release() ;
	}

public:

	inline ExpandedNodeId * getParentNodeId()
	{
		return parentNodeId ;
	}

	inline NodeId * getReferenceTypeId()
	{
		return referenceTypeId ;
	}

	inline ExpandedNodeId * getRequestedNewNodeId()
	{
		return requestedNewNodeId ;
	}

	inline QualifiedName * getBrowseName()
	{
		return browseName ;
	}

	inline NodeClass * getNodeClass()
	{
		return nodeClass ;
	}

	inline NodeAttributes * getNodeAttributes()
	{
		return nodeAttributes ;
	}

	inline ExpandedNodeId * getTypeDefinition()
	{
		return typeDefinition ;
	}


};
} /* namespace opcua */
#endif /* WITH_NODEMNGT */
#endif /* OPCUA_ADDNODESITEM_H_ */
