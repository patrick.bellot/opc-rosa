
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPCUA_VARIABLETYPE_ATTRIBUTES_H_
#define OPCUA_VARIABLETYPE_ATTRIBUTES_H_

/*
 * OPC-UA Part 4, 7.18.6, p. 130
 */

#include "../OpcUa.h"

#if (WITH_NODEMNGT == 1)

#include "../StandardDataTypes/All.h"
#include "../Utils/OpcUa_IPCS_Table.h"
#include "OpcUa_NodeAttributesId.h"
#include "OpcUa_IPCS_NodeAttributes.h"

namespace opcua {

class MYDLL VariableTypeAttributes
	: public NodeAttributes
{
public:

	virtual uint32_t getTypeId() const { return OpcUaId_VariableTypeAttributes ; }

private:

	UInt32        * specifiedAttributes ;

	LocalizedText * displayName ;
	LocalizedText * description ;

	UInt32        * writeMask ;
	UInt32        * userWriteMask ;

	Variant       * value ;
	NodeId		  * dataType ;
	Int32         * valueRank ;

	TableUInt32   * arrayDimensions ;

	Boolean       * isAbstract ;

public:

	static VariableTypeAttributes * fromCtoCpp(SOPC_StatusCode * pStatus, _OpcUa_VariableTypeAttributes const& pVariableTypeAttributes)
	{
		uint32_t specifiedAttributes = pVariableTypeAttributes.SpecifiedAttributes ;

		LocalizedText * displayName ;
		if (specifiedAttributes & NodeAttributeId_DisplayName)
			displayName = LocalizedText ::fromCtoCpp(pStatus,pVariableTypeAttributes.DisplayName) ;
		else
			displayName = NULL ;

		LocalizedText * description ;
		if (specifiedAttributes & NodeAttributeId_Description)
			description = LocalizedText ::fromCtoCpp(pStatus,pVariableTypeAttributes.Description) ;
		else
			description = NULL ;


		UInt32 * writeMask ;
		if (specifiedAttributes & NodeAttributeId_WriteMask)
			writeMask = UInt32 ::fromCtoCpp(pStatus,pVariableTypeAttributes.WriteMask) ;
		else
			writeMask = NULL ;

		UInt32 * userWriteMask ;
		if (specifiedAttributes & NodeAttributeId_UserWriteMask)
			userWriteMask = UInt32 ::fromCtoCpp(pStatus,pVariableTypeAttributes.UserWriteMask) ;
		else
			userWriteMask = NULL ;


		Variant * value ;
		if (specifiedAttributes & NodeAttributeId_Value)
			value = Variant ::fromCtoCpp(pStatus,pVariableTypeAttributes.Value) ;
		else
			value = NULL ;

		NodeId * dataType ;
		if (specifiedAttributes & NodeAttributeId_DataType)
			dataType = NodeId ::fromCtoCpp(pStatus,pVariableTypeAttributes.DataType) ;
		else
			dataType = NULL ;

		Int32 * valueRank ;
		if (specifiedAttributes & NodeAttributeId_ValueRank)
			valueRank = Int32 ::fromCtoCpp(pStatus,pVariableTypeAttributes.ValueRank) ;
		else
			valueRank = NULL ;


		TableUInt32   * arrayDimensions ;
		if (specifiedAttributes & NodeAttributeId_ArrayDimensions)
			arrayDimensions = TableUInt32::fromCtoCpp(pStatus,pVariableTypeAttributes.NoOfArrayDimensions,pVariableTypeAttributes.ArrayDimensions) ;
		else
			arrayDimensions = NULL ;


		Boolean * isAbstract ;
		if (specifiedAttributes & NodeAttributeId_AccessLevel)
			isAbstract = Boolean ::fromCtoCpp(pStatus,pVariableTypeAttributes.IsAbstract) ;
		else
			isAbstract = NULL ;


		if (*pStatus == STATUS_OK)
			return
					new VariableTypeAttributes(
							displayName,
							description,

							writeMask,
							userWriteMask,

							value,
							dataType,
							valueRank,

							arrayDimensions,

							isAbstract
							) ;

		if (displayName != NULL)
			displayName->checkRefCount() ;
		if (description != NULL)
			description->checkRefCount() ;

		if (writeMask != NULL)
			writeMask->checkRefCount() ;
		if (userWriteMask != NULL)
			userWriteMask->checkRefCount() ;

		if (value != NULL)
			value->checkRefCount() ;
		if (dataType != NULL)
			dataType->checkRefCount() ;
		if (valueRank != NULL)
			valueRank->checkRefCount() ;

		if (arrayDimensions != NULL)
			arrayDimensions->checkRefCount() ;

		if (isAbstract != NULL)
			isAbstract->checkRefCount() ;

		return NULL ;
	}

	void fromCpptoC(SOPC_StatusCode * pStatus, SOPC_ExtensionObject& pVariableTypeAttributes) const
	{
		expandedNodeId->fromCpptoC(pStatus,pVariableTypeAttributes.TypeId) ;

		if (*pStatus == STATUS_OK) {
			pVariableTypeAttributes.Encoding = SOPC_ExtObjBodyEncoding_Object ;
			pVariableTypeAttributes.Body.Object.ObjType = &OpcUa_VariableTypeAttributes_EncodeableType ;
			pVariableTypeAttributes.Body.Object.Value = (void *)malloc(sizeof(_OpcUa_VariableTypeAttributes)) ;
			OpcUa_VariableTypeAttributes_Initialize(pVariableTypeAttributes.Body.Object.Value) ;
			VariableTypeAttributes::fromCpptoC(pStatus,*static_cast<_OpcUa_VariableTypeAttributes *>(pVariableTypeAttributes.Body.Object.Value)) ;
			pVariableTypeAttributes.Length = sizeof(_OpcUa_VariableTypeAttributes) ;
		}
	}

private:

	void fromCpptoC(SOPC_StatusCode * pStatus, _OpcUa_VariableTypeAttributes& pVariableTypeAttributes) const
	{
		specifiedAttributes->fromCpptoC(pStatus,pVariableTypeAttributes.SpecifiedAttributes) ;

		if (displayName != NULL)
			displayName   ->fromCpptoC(pStatus,pVariableTypeAttributes.DisplayName) ;
		if (description != NULL)
			description   ->fromCpptoC(pStatus,pVariableTypeAttributes.Description) ;

		if (writeMask != NULL)
			writeMask     ->fromCpptoC(pStatus,pVariableTypeAttributes.WriteMask) ;
		if (userWriteMask != NULL)
			userWriteMask ->fromCpptoC(pStatus,pVariableTypeAttributes.UserWriteMask) ;

		if (value != NULL)
			value     ->fromCpptoC(pStatus,pVariableTypeAttributes.Value) ;
		if (dataType != NULL)
			dataType  ->fromCpptoC(pStatus,pVariableTypeAttributes.DataType) ;
		if (valueRank != NULL)
			valueRank ->fromCpptoC(pStatus,pVariableTypeAttributes.ValueRank) ;

		if (arrayDimensions != NULL)
			arrayDimensions ->fromCpptoC (pStatus,pVariableTypeAttributes.NoOfArrayDimensions,pVariableTypeAttributes.ArrayDimensions) ;

		if (isAbstract != NULL)
			isAbstract  ->fromCpptoC(pStatus,pVariableTypeAttributes.IsAbstract) ;
	}

public:

	VariableTypeAttributes(
			LocalizedText * _displayName,
			LocalizedText * _description,

			UInt32        * _writeMask,
			UInt32        * _userWriteMask,

			Variant       * _value,
			NodeId		  * _dataType,
			Int32         * _valueRank,

			TableUInt32   * _arrayDimensions,

			Boolean       * _isAbstract
			)
		: NodeAttributes()
	{
		uint32_t _specifiedAttributes = 0 ;



		if ((displayName = _displayName)) {
			_specifiedAttributes |= NodeAttributeId_DisplayName ;
		} else {
			displayName = LocalizedText::null ;
		}
		displayName->take() ;

		if ((description = _description)) {
			_specifiedAttributes |= NodeAttributeId_Description ;
		} else {
			description = LocalizedText::null ;
		}
		description->take() ;


		if ((writeMask = _writeMask)) {
			_specifiedAttributes |= NodeAttributeId_WriteMask ;
		} else {
			writeMask = UInt32::zero ;
		}
		writeMask->take() ;

		if ((userWriteMask = _userWriteMask)) {
			_specifiedAttributes |= NodeAttributeId_UserWriteMask ;
		} else {
			userWriteMask = UInt32::zero ;
		}
		userWriteMask->take() ;


		if ((value = _value)) {
			_specifiedAttributes |= NodeAttributeId_Value ;
		} else {
			value = new Variant(Builtin_Byte,Byte::zero) ;
		}
		value->take() ;

		if ((dataType = _dataType)) {
			_specifiedAttributes |= NodeAttributeId_DataType ;
		} else {
			dataType = NodeId::nullNodeId ;
		}
		dataType->take() ;

		if ((valueRank = _valueRank)) {
			_specifiedAttributes |= NodeAttributeId_ValueRank ;
		} else {
			valueRank = Int32::zero ;
		}
		valueRank->take() ;


		if ((arrayDimensions = _arrayDimensions)) {
			_specifiedAttributes |= NodeAttributeId_ArrayDimensions ;
		} else {
			arrayDimensions = UInt32::tableZero ;
		}
		arrayDimensions->take() ;


		if ((isAbstract = _isAbstract)) {
			_specifiedAttributes |= NodeAttributeId_IsAbstract ;
		} else {
			isAbstract = Boolean::booleanFalse ;
		}
		isAbstract->take() ;



		(specifiedAttributes = new UInt32(_specifiedAttributes))->take() ;
	}

	virtual ~VariableTypeAttributes()
	{
		specifiedAttributes     ->release() ;

		displayName             ->release() ;
		description             ->release() ;

		writeMask               ->release() ;
		userWriteMask           ->release() ;

		value                   ->release() ;
		dataType                ->release() ;
		valueRank               ->release() ;

		arrayDimensions         ->release() ;

		isAbstract              ->release() ;
	}

public:

	inline UInt32 * getSpecifiedAttributes()
	{
		return specifiedAttributes ;
	}

	inline LocalizedText * getDisplayName()
	{
		return displayName ;
	}

	inline LocalizedText * getDescription()
	{
		return description ;
	}

	inline UInt32 * getWriteMask()
	{
		return writeMask ;
	}

	inline UInt32 * getUserWriteMask()
	{
		return userWriteMask ;
	}


	inline Variant * getValue()
	{
		return value ;
	}

	inline NodeId * getDataType()
	{
		return dataType ;
	}

	inline Int32 * getValueRank()
	{
		return valueRank ;
	}


	inline TableUInt32   * getArrayDimensions()
	{
		return arrayDimensions ;
	}


	inline Boolean * getIsAbstract()
	{
		return isAbstract ;
	}

public:

	static  ExpandedNodeId * expandedNodeId ;

	virtual ExpandedNodeId * getNodeId() const
	{
		return expandedNodeId ;
	}

};
}      /* namespace opcua */
#endif /* NODE_MNGT */
#endif /* OPCUA_VARIABLETYPE_ATTRIBUTES_H_ */
