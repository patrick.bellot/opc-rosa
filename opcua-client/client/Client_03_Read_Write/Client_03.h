
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_03_H_
#define CLIENT_03_H_

#include "lib/OpcUa.h"

#if (WITH_WRITE == 1 && WITH_READ == 1)

#include "lib/Stacks/All.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

#include "../OpcUa_BaseClient.h"

namespace opcua {

class Client_03
	: public BaseClient
{
public:

	void run()
	{
		int nbExt = 1000 ; // Number of total loops from CreateSession to CloseSession
		int nbInt = 10000 ; // Number of internal loops (after ActivateSession and before CloseSession)

		SOPC_StatusCode status = STATUS_OK ;

		// EXTERNAL LOOP
		while (--nbExt >= 0) {

			// CREATE SESSION
			debug(MAIN_LIFE_DBG,"Client_03","Create Session begin") ;
			status = createSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_03","Cannot Create Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_03","Create Session succeeds") ;

			// ACTIVATE SESSION
			debug(MAIN_LIFE_DBG,"Client_03","Activate Session begin") ;
			status = activateSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_03","Cannot Activate Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_03","Activate Session succeeds") ;

			// INTERNAL LOOP
			int _nbInt = nbInt ;
			while (--_nbInt >= 0) {

				// ReadFour
				debug(MAIN_LIFE_DBG,"Client_03","ReadFour begin") ;
				status = doReadFour() ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_03","Cannot ReadFour: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_03","ReadFour succeeds") ;

				// WriteRead
				debug(MAIN_LIFE_DBG,"Client_03","WriteRead begin") ;
				status = doWriteRead(_nbInt) ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_03","Cannot WriteRead: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_03","WriteRead succeeds") ;

				// WriteReadArray
				debug(MAIN_LIFE_DBG,"Client_03","WriteReadArray begin") ;
				status = doWriteReadArray() ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_03","Cannot WriteReadArray: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_03","WriteRead succeeds") ;

				// WriteReadArrayDll
				debug(MAIN_LIFE_DBG,"Client_03","WriteReadArrayDll begin") ;
				status = doWriteReadArray() ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_03","Cannot WriteReadArrayDll: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_03","WriteReadDll succeeds") ;

			} // nbInt

			// CLOSE SESSION
			debug(MAIN_LIFE_DBG,"Client_03","Close Session begin") ;
			status = closeSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_03","Cannot Close Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_03","Close Session succeeds") ;
		} // nbExt

		debug(MAIN_LIFE_DBG,"Client_03","Ending thread") ;
	} // run()

public:

	Client_03(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx,String * endpointUrl)
		: BaseClient(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~Client_03()
	{}

public:

	inline SOPC_StatusCode createSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (Certificates::computeRandNonce(&clientNonce)) {
			debug(COM_ERR, "Client_03", "computeRandNonce(clientNonce) operation failed");
			if (clientNonce != NULL)
				clientNonce->checkRefCount() ;
			return 0x00000001 ;
		}

		clientNonce->take() ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSessionRequest * createSessionRequest =
		// OPC UA, Part 4, 5.5.2.2, table 11, p. 26		=
				new CreateSessionRequest(
						// OPC_UA, Part 4, 7.26, table 157, p. 138
						new RequestHeader(
								SessionAuthenticationToken::nullNodeId,
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
								),
						// OPC_UA, Part 4, 7.1, p. 106
						new ApplicationDescription(
								new String("opc-tcp://192.168.1.150:10000"),
								new String("opc-rosa"),
								new LocalizedText(new String("Client_03"),LocaleId::en),
								ApplicationType::client_1,
								String::empty,
								String::empty,
								new TableString  (0)),
						String::empty,
						endpointUrl,
						new String("Session Client_03"),
						clientNonce,
						Certificates::selfCertificate,
						Duration::maximum,
						UInt32::maximum);

		CreateSessionResponse * createSessionResponse = NULL ;

		status = Connexion_CreateSession(createSessionRequest,&createSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot Create Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_03","Create Session: processing parameters from response");

		sessionId              = createSessionResponse->getSessionId();
		authenticationTokenNum = createSessionResponse->getAuthenticationToken()->get() ;
	    serverNonce            = createSessionResponse->getServerNonce() ;

#if (SECURITY != UANONE)
		if (createSessionResponse->getServerCertificate() == NULL ||
			createSessionResponse->getServerSignature  () == NULL ||
			Certificates::selfCertificate                 == NULL ||
			clientNonce 								  == NULL) {

			debug (COM_ERR,"Client_03","Create Session: channel parameter has not been found") ;
			status = 0x00000002 ;
			goto error ;
		}
#endif

		session = new Session(authenticationTokenNum, sessionId);
		session->setLastCreateOrActivateSessionNonce(serverNonce) ;

#if (SECURITY != UANONE)
		session->setDistantNonce(serverNonce) ;
		session->setLocalNonce(clientNonce) ;
		session->setDistantCertificate(createSessionResponse->getServerCertificate()) ;
#endif

		clientChannel->pushNewSession(authenticationTokenNum, session);

	    createSessionRequest->checkRefCount() ;
	    createSessionResponse->checkRefCount() ;

		clientNonce->release() ;
		clientNonce = NULL ;

		return STATUS_OK ;

error:
		clientNonce->release() ;
		clientNonce = NULL ;

		createSessionRequest->checkRefCount() ;

		if (createSessionResponse != NULL)
			createSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

public:

	inline SOPC_StatusCode activateSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Nonce  * serverNonce = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_03", "activateSession(): clientChannel null");
			return 0x00000001 ;
		}

		Session * session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_03", "activateSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

#if (SECURITY == UANONE)
		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
				// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						new UInt32(5000) // TimeoutHint
				),
				SignatureData::fakeSignatureData,
				new TableSignedSoftwareCertificate  (0),
				tableLocaleId,
				new AnonymousIdentityToken(new String("Anonymous")),
				SignatureData::fakeSignatureData);
#endif

#if (SECURITY != UANONE)
		Nonce * distantNonce = session->getDistantNonce() ;
		if (distantNonce == NULL) {
			debug(COM_ERR, "Client_03", "activateSession(): distantNonce == NULL");
			return 0x00000003 ;
		}

		ApplicationInstanceCertificate * distantCertificate = session->getDistantCertificate() ;
		if (distantCertificate == NULL) {
			debug(COM_ERR, "Client_03", "activateSession(): distantCertificate == NULL");
			return 0x00000003 ;
		}

		SignatureData * clientSignatureData = NULL ;
		if ((status = Certificates::computeSignatureData(&clientSignatureData, session->getDistantCertificate(), session->getDistantNonce()))) {
			debug_i(COM_ERR, "Client_03", "activateSession(): Certificates::computeSignatureData(..) returns %d",status);
			return status ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							new UInt32(5000) // TimeoutHint
					),
					clientSignatureData, // clientSignatureData
					new TableSignedSoftwareCertificate  (0),
					tableLocaleId,
					new AnonymousIdentityToken(new String("Anonymous")),
					SignatureData::fakeSignatureData); // fakeSignature
#endif

		ActivateSessionResponse * activateSessionResponse = NULL ;

		status = Connexion_ActivateSession(activateSessionRequest,&activateSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot Activate Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_03","Activate Session: processing parameters from response");

		serverNonce = activateSessionResponse->getServerNonce();

		session->setLastCreateOrActivateSessionNonce(serverNonce);

	    activateSessionRequest  ->checkRefCount() ;
	    activateSessionResponse ->checkRefCount() ;

		return STATUS_OK ;

error:

		activateSessionRequest->checkRefCount() ;

		if (activateSessionResponse != NULL)
			activateSessionResponse->checkRefCount() ;

		return status ;
	}

public:

	inline SOPC_StatusCode closeSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_03", "closeSession(): clientChannel null");
			return 0x00000001 ;
		}

		session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_03", "closeSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		// OPC-UA, Part3, 5.6.2, p. 26
		CloseSessionRequest * closeSessionRequest = new CloseSessionRequest(
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
				),
				Boolean::booleanFalse);

		CloseSessionResponse * closeSessionResponse = NULL ;

		status = Connexion_CloseSession(closeSessionRequest,&closeSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot Close Session: status=0x%08x",status) ;
			goto error ;
		}

	    clientChannel->deleteSession(authenticationTokenNum) ;

error:

		closeSessionRequest->checkRefCount() ;

		if (closeSessionResponse != NULL)
			closeSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

private:

	SOPC_StatusCode doReadFour()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableDataValue   * results = NULL ;
		int32_t            length  = 0 ;

		char tmp[1024] ; // To output string

		debug(MAIN_LIFE_DBG,"Client_03","Testing ReadFour") ;

		NodeId * nodeToRead0 = new NodeId(UInt16::zero,new String("XMouse")) ;
		NodeId * nodeToRead1 = new NodeId(UInt16::zero,new String("YMouse")) ;

		TableReadValueId   * nodesToRead = new TableReadValueId  (4) ;

		nodesToRead->set(0,
				new ReadValueId(nodeToRead0,
						new IntegerId(AttributeId_BrowseName),
						NumericRange::null, QualifiedName::defaultBinary));

		nodesToRead->set(1,
				new ReadValueId(nodeToRead0,
						new IntegerId(AttributeId_Value),
						NumericRange::null, QualifiedName::defaultBinary));

		nodesToRead->set(2,
				new ReadValueId(nodeToRead1,
						new IntegerId(AttributeId_BrowseName),
						NumericRange::null, QualifiedName::defaultBinary));

		nodesToRead->set(3,
				new ReadValueId(nodeToRead1,
						new IntegerId(AttributeId_Value),
						NumericRange::null, QualifiedName::defaultBinary));

		uint32_t sequence = requestSequence->allocate(1) ;

		ReadRequest * readRequest
		// OPC UA, Part 4, 5.10.2, p. 51
			= new ReadRequest(
			// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
				Duration::zero, // maxAge,
				TimestampsToReturn::neither_3, // timestamp to return
				nodesToRead);

		ReadResponse * readResponse = NULL ;

		status = Connexion_Read(readRequest,&readResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot ReadFour: status=0x%08x",status) ;
			goto error ;
		}

		results = readResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       ReadFour: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {

			DataValue     * result  = results->get(i);
			Variant       * variant = result->getVariantAttr();
			StatusCode    * status  = result->getStatusCode();

			if (variant != NULL) {
				switch (variant->getBuiltinTypeId()) {
				case Builtin_QualifiedName: {
					QualifiedName * qualifiedName = dynamic_cast<QualifiedName *>(variant->getValue());
					sprintf(tmp,"QualifiedName : %d:%s (0x%08x)",qualifiedName->getNamespaceIndex()->get(),qualifiedName->getName()->get(),status->get());
				}	break;
				case Builtin_UInt32: {
					UInt32 * uInt32 = dynamic_cast<UInt32 *>(variant->getValue()) ;
					sprintf(tmp,"UInt32        : %d (0x%08x)",uInt32->get(),status->get());
				}	break;
				default: {
					uint32_t uint32 = (uint32_t) (variant->getBuiltinTypeId()) ;
					sprintf(tmp,"Unknown       : typeid=%d (0x%08x)",uint32,status->get());
				}	break;
				}
			} else {
				if (status) {
					sprintf(tmp,"Error         : 0x%08x",status->get());
				} else {
					sprintf(tmp,"Error         : unknown");
				}
			}
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

	    readRequest  ->checkRefCount() ;
	    readResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","ReadFour OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","ReadFour NOT OK") ;

		readRequest->checkRefCount() ;

		if (readResponse != NULL)
			readResponse->checkRefCount() ;

		return status ;
	}


private:

	SOPC_StatusCode doWriteRead(int n)
	{
		SOPC_StatusCode status = doWrite(n)  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Write failed, status=0x08x",status) ;
			return status ;
		}

		status = doRead()  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Read failed, status=0x08x",status) ;
			return status ;
		}

		return STATUS_OK ;
	}

	SOPC_StatusCode doWrite(int n)
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableStatusCode   * results = NULL ;
		int32_t             length  = 0 ;

		char tmp[1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing Write") ;

		NodeId * node0 = new NodeId(UInt16::zero,new String("WriteReadVariable")) ;
		NodeId * node1 = new NodeId(UInt16::zero,new String("MemoryVariable")) ;

		TableWriteValue   * nodesToWriteRead = new TableWriteValue  (2);

		nodesToWriteRead->set(
				0,
				new WriteValue(
						node0,
						new IntegerId(AttributeId_Value),
						NumericRange::null,
						new DataValue(new Variant(Builtin_Int16,new Int16((int16_t) (n + 5000))))));

		nodesToWriteRead->set(
				1,
				new WriteValue(
						node1,
						new IntegerId(AttributeId_Value),
						NumericRange::null,
						new DataValue(new Variant(Builtin_Byte,new Byte((uint8_t) (n + 5000))))));

		uint32_t sequence = requestSequence->allocate(1) ;

		WriteRequest * writeRequest
		// OPC UA, Part 4, 5.10.2, p. 51
		= new WriteRequest(
		// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero), // TimeoutHint
				nodesToWriteRead);

		WriteResponse * writeResponse = NULL ;

		status = Connexion_Write(writeRequest,&writeResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot Write: status=0x%08x",status) ;
			goto error ;
		}

		results = writeResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       Write: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {
			StatusCode * statusCode = results->get(i);
			sprintf(tmp,"status = 0x%08x",statusCode->get()) ;
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

		writeRequest  ->checkRefCount() ;
		writeResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","Write OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","Write NOT OK") ;

		writeRequest->checkRefCount() ;

		if (writeResponse != NULL)
			writeResponse->checkRefCount() ;

		return status ;
	}

	SOPC_StatusCode doRead()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableDataValue   * results = NULL ;
		int32_t            length  = 0 ;

		char tmp[1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing Read") ;

		NodeId * node0 = new NodeId(UInt16::zero,new String("WriteReadVariable")) ;
		NodeId * node1 = new NodeId(UInt16::zero,new String("MemoryVariable")) ;

		TableReadValueId   * nodesToRead = new TableReadValueId  (2) ;

		nodesToRead->set(0,
				new ReadValueId(node0,
						new IntegerId(AttributeId_Value),
						NumericRange::null, QualifiedName::defaultBinary));

		nodesToRead->set(1,
				new ReadValueId(node1,
						new IntegerId(AttributeId_Value),
						NumericRange::null, QualifiedName::defaultBinary));

		uint32_t sequence = requestSequence->allocate(1) ;

		ReadRequest * readRequest
		// OPC UA, Part 4, 5.10.2, p. 51
			= new ReadRequest(
			// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
				Duration::zero, // maxAge,
				TimestampsToReturn::neither_3, // timestamp to return
				nodesToRead);

		ReadResponse * readResponse = NULL ;

		status = Connexion_Read(readRequest,&readResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot Read: status=0x%08x",status) ;
			goto error ;
		}

		results = readResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       Read: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {

			DataValue     * result  = results->get(i);
			Variant       * variant = result->getVariantAttr();
			StatusCode    * status  = result->getStatusCode();

			if (variant != NULL) {
				switch (variant->getBuiltinTypeId()) {
				case Builtin_Byte: {
					Byte * byte = dynamic_cast<Byte *>(variant->getValue()) ;
					sprintf(tmp,"Int16         : %d (0x%08x)",byte->get(),status->get());
				}	break;
				case Builtin_Int16: {
					Int16 * int16 = dynamic_cast<Int16 *>(variant->getValue()) ;
					sprintf(tmp,"Int16         : %d (0x%08x)",int16->get(),status->get());
				}	break;
				default: {
					uint32_t uint32 = (uint32_t) (variant->getBuiltinTypeId()) ;
					sprintf(tmp,"Unknown       : typeid=%d (0x%08x)",uint32,status->get());
				}	break;
				}
			} else {
				if (status) {
					sprintf(tmp,"Error         : 0x%08x",status->get());
				} else {
					sprintf(tmp,"Error         : unknown");
				}
			}
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

	    readRequest  ->checkRefCount() ;
	    readResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","Read OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","Read NOT OK") ;

		readRequest->checkRefCount() ;

		if (readResponse != NULL)
			readResponse->checkRefCount() ;

		return status ;
	}


private:

	SOPC_StatusCode doWriteReadArray()
	{
		SOPC_StatusCode status = STATUS_OK ;

		status = doWriteArray()  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","WriteArray failed, status=0x08x",status) ;
			return status ;
		}

		status = doReadArray()  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","ReadArray failed, status=0x08x",status) ;
			return status ;
		}

		return STATUS_OK ;
	}

	SOPC_StatusCode doWriteArray()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableStatusCode   * results = NULL ;
		int32_t             length  = 0 ;

		char tmp[1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing WriteArray") ;

		NodeId * node0 = new NodeId(UInt16::zero, new String("MouseXY")) ;
		NodeId * node1 = new NodeId(UInt16::zero, new String("MyArray")) ;

		TableWriteValue   * nodesToWriteRead = new TableWriteValue  (5);

		TableUInt32   * writeArgs = new TableUInt32  (2);
		writeArgs->set(0, new UInt32(Alea::alea->random_uint32()));
		writeArgs->set(1, new UInt32(Alea::alea->random_uint32()));

		nodesToWriteRead->set(
				0,
				new WriteValue(
						node0,
						new IntegerId(AttributeId_Value),
						NumericRange::null,
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (4);
		writeArgs->set(0, new UInt32(1));
		writeArgs->set(1, new UInt32(2));
		writeArgs->set(2, new UInt32(3));
		writeArgs->set(3, new UInt32(4));

		nodesToWriteRead->set(
				1,
				new WriteValue(
						node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("3:4,6:7"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (2);
		writeArgs->set(0, new UInt32(5));
		writeArgs->set(1, new UInt32(6));

		nodesToWriteRead->set(
				2,
				new WriteValue(
						node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("4,8:9"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (4);
		writeArgs->set(0, new UInt32(7));
		writeArgs->set(1, new UInt32(8));
		writeArgs->set(2, new UInt32(9));
		writeArgs->set(3, new UInt32(10));

		nodesToWriteRead->set(
				3,
				new WriteValue(
						node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("6:9,9"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (1);
		writeArgs->set(0, new UInt32(11));

		nodesToWriteRead->set(
				4,
				new WriteValue(
						node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("2,5"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		uint32_t sequence = requestSequence->allocate(1) ;

		WriteRequest * writeRequest
		// OPC UA, Part 4, 5.10.2, p. 51
		= new WriteRequest(
		// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero), // TimeoutHint
				nodesToWriteRead);

		WriteResponse * writeResponse = NULL ;

		status = Connexion_Write(writeRequest,&writeResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot WriteArray: status=0x%08x",status) ;
			goto error ;
		}

		results = writeResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       WriteArray: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {
			StatusCode * statusCode = results->get(i);
			sprintf(tmp,"status = 0x%08x",statusCode->get()) ;
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

		writeRequest  ->checkRefCount() ;
		writeResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","WriteArray OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","WriteArray NOT OK") ;

		writeRequest->checkRefCount() ;

		if (writeResponse != NULL)
			writeResponse->checkRefCount() ;

		return status ;
	}

	SOPC_StatusCode doReadArray()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableDataValue   * results = NULL ;
		int32_t            length  = 0 ;

		char tmp[10 * 1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing ReadArray") ;

		NodeId * node0 = new NodeId(UInt16::zero, new String("MouseXY")) ;
		NodeId * node1 = new NodeId(UInt16::zero, new String("MyArray")) ;

		TableReadValueId   * nodesToRead = new TableReadValueId  (5) ;

		nodesToRead->set(0,
				new ReadValueId(node0,
						new IntegerId(AttributeId_Value),
						NumericRange::null,
						QualifiedName::defaultBinary));

		nodesToRead->set(1,
				new ReadValueId(node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("3:4,6:7"),
						QualifiedName::defaultBinary));

		nodesToRead->set(2,
				new ReadValueId(node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("4,8:9"),
						QualifiedName::defaultBinary));

		nodesToRead->set(3,
				new ReadValueId(node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("6:9,9"),
						QualifiedName::defaultBinary));

		nodesToRead->set(4,
				new ReadValueId(node1,
						new IntegerId(AttributeId_Value),
						new NumericRange("2,5"),
						QualifiedName::defaultBinary));

		uint32_t sequence = requestSequence->allocate(1) ;

		ReadRequest * readRequest
		// OPC UA, Part 4, 5.10.2, p. 51
			= new ReadRequest(
			// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
				Duration::zero, // maxAge,
				TimestampsToReturn::neither_3, // timestamp to return
				nodesToRead);

		ReadResponse * readResponse = NULL ;

		status = Connexion_Read(readRequest,&readResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot ReadArray: status=0x%08x",status) ;
			goto error ;
		}

		results = readResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       ReadArray: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {

			DataValue     * result  = results->get(i);
			Variant       * variant = result->getVariantAttr();
			StatusCode    * status  = result->getStatusCode();

			if (variant != NULL) {
				switch (variant->getBuiltinTypeId()) {
				case Builtin_UInt32: {
					int32_t arrayLength = variant->getArrayLength() ;
					if (arrayLength != -1) {
						TableUInt32   * tableRead = dynamic_cast<TableUInt32   *>(variant->getValue());
						if (tableRead) {
							sprintf(tmp,"Array UInt32[%d]: ",arrayLength) ;
							for (int j = 0; j < arrayLength; j++)
								sprintf(tmp+strlen(tmp)," %d",tableRead->get(j)->get()) ;
						} else {
							sprintf(tmp,"Not a UINT32 array ?");
						}
					} else {
						sprintf(tmp,"ArrayLengthObj (UInt32) NULL");
					}
				}	break;
				default: {
					uint32_t uint32 = (uint32_t) (variant->getBuiltinTypeId()) ;
					sprintf(tmp,"Unknown       : typeid=%d (0x%08x)",uint32,status->get());
				}	break;
				}
			} else {
				if (status) {
					sprintf(tmp,"Error         : 0x%08x",status->get());
				} else {
					sprintf(tmp,"Error         : unknown");
				}
			}
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

	    readRequest  ->checkRefCount() ;
	    readResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","ReadArray OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","ReadArray NOT OK") ;

		readRequest->checkRefCount() ;

		if (readResponse != NULL)
			readResponse->checkRefCount() ;

		return status ;
	}


private:

	SOPC_StatusCode doWriteReadArrayDll()
	{
		SOPC_StatusCode status = STATUS_OK ;

		status = doWriteArrayDll()  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","WriteArrayDll failed, status=0x08x",status) ;
			return status ;
		}

		status = doReadArrayDll()  ;

		if (status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","ReadArrayDll failed, status=0x08x",status) ;
			return status ;
		}

		return STATUS_OK ;
	}

	SOPC_StatusCode doWriteArrayDll()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableStatusCode   * results = NULL ;
		int32_t             length  = 0 ;

		char tmp[1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing WriteArrayDll") ;

		NodeId * node = new NodeId(UInt16::zero, new String("MyArrayDll")) ;

		TableWriteValue   * nodesToWriteRead = new TableWriteValue  (4);

		TableUInt32   * writeArgs = new TableUInt32  (4);
		writeArgs->set(0, new UInt32(1));
		writeArgs->set(1, new UInt32(2));
		writeArgs->set(2, new UInt32(3));
		writeArgs->set(3, new UInt32(4));

		nodesToWriteRead->set(
				0,
				new WriteValue(
						node,
						new IntegerId(AttributeId_Value),
						new NumericRange("3:4,6:7"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (2);
		writeArgs->set(0, new UInt32(5));
		writeArgs->set(1, new UInt32(6));

		nodesToWriteRead->set(
				1,
				new WriteValue(
						node,
						new IntegerId(AttributeId_Value),
						new NumericRange("4,8:9"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (4);
		writeArgs->set(0, new UInt32(7));
		writeArgs->set(1, new UInt32(8));
		writeArgs->set(2, new UInt32(9));
		writeArgs->set(3, new UInt32(10));

		nodesToWriteRead->set(
				2,
				new WriteValue(
						node,
						new IntegerId(AttributeId_Value),
						new NumericRange("6:9,9"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		writeArgs = new TableUInt32  (1);
		writeArgs->set(0, new UInt32(11));

		nodesToWriteRead->set(
				3,
				new WriteValue(
						node,
						new IntegerId(AttributeId_Value),
						new NumericRange("2,5"),
						new DataValue(new Variant(Builtin_UInt32,writeArgs))));

		uint32_t sequence = requestSequence->allocate(1) ;

		WriteRequest * writeRequest
		// OPC UA, Part 4, 5.10.2, p. 51
		= new WriteRequest(
		// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero), // TimeoutHint
				nodesToWriteRead);

		WriteResponse * writeResponse = NULL ;

		status = Connexion_Write(writeRequest,&writeResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot WriteArrayDll: status=0x%08x",status) ;
			goto error ;
		}

		results = writeResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       WriteArrayDll: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {
			StatusCode * statusCode = results->get(i);
			sprintf(tmp,"status = 0x%08x",statusCode->get()) ;
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

		writeRequest  ->checkRefCount() ;
		writeResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","WriteArrayDll OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","WriteArrayDll NOT OK") ;

		writeRequest->checkRefCount() ;

		if (writeResponse != NULL)
			writeResponse->checkRefCount() ;

		return status ;
	}

	SOPC_StatusCode doReadArrayDll()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableDataValue   * results = NULL ;
		int32_t            length  = 0 ;

		char tmp[10 * 1024] ;

		debug(MAIN_LIFE_DBG,"Client_03","Testing ReadArrayDll") ;

		NodeId * node = new NodeId(UInt16::zero, new String("MyArrayDll")) ;

		TableReadValueId   * nodesToRead = new TableReadValueId  (4) ;

		nodesToRead->set(0,
				new ReadValueId(node,
						new IntegerId(AttributeId_Value),
						new NumericRange("3:4,6:7"),
						QualifiedName::defaultBinary));

		nodesToRead->set(1,
				new ReadValueId(node,
						new IntegerId(AttributeId_Value),
						new NumericRange("4,8:9"),
						QualifiedName::defaultBinary));

		nodesToRead->set(2,
				new ReadValueId(node,
						new IntegerId(AttributeId_Value),
						new NumericRange("6:9,9"),
						QualifiedName::defaultBinary));

		nodesToRead->set(3,
				new ReadValueId(node,
						new IntegerId(AttributeId_Value),
						new NumericRange("2,5"),
						QualifiedName::defaultBinary));

		uint32_t sequence = requestSequence->allocate(1) ;

		ReadRequest * readRequest
		// OPC UA, Part 4, 5.10.2, p. 51
			= new ReadRequest(
			// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
				Duration::zero, // maxAge,
				TimestampsToReturn::neither_3, // timestamp to return
				nodesToRead);

		ReadResponse * readResponse = NULL ;

		status = Connexion_Read(readRequest,&readResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_03","Cannot ReadArrayDll: status=0x%08x",status) ;
			goto error ;
		}

		results = readResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_03","       ReadArrayDll: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {

			DataValue     * result  = results->get(i);
			Variant       * variant = result->getVariantAttr();
			StatusCode    * status  = result->getStatusCode();

			if (variant != NULL) {
				switch (variant->getBuiltinTypeId()) {
				case Builtin_UInt32: {
					int32_t arrayLength = variant->getArrayLength() ;
					if (arrayLength != -1) {
						TableUInt32   * tableRead = dynamic_cast<TableUInt32   *>(variant->getValue());
						if (tableRead) {
							sprintf(tmp,"Array UInt32[%d]: ",arrayLength) ;
							for (int j = 0; j < arrayLength; j++)
								sprintf(tmp+strlen(tmp)," %d",tableRead->get(j)->get()) ;
						} else {
							sprintf(tmp,"Not a UINT32 array ?");
						}
					} else {
						sprintf(tmp,"ArrayLengthObj (UInt32) NULL");
					}
				}	break;
				default: {
					uint32_t uint32 = (uint32_t) (variant->getBuiltinTypeId()) ;
					sprintf(tmp,"Unknown       : typeid=%d (0x%08x)",uint32,status->get());
				}	break;
				}
			} else {
				if (status) {
					sprintf(tmp,"Error         : 0x%08x",status->get());
				} else {
					sprintf(tmp,"Error         : unknown");
				}
			}
			debug_is(COM_ERR,"Client_03","          Result number %d : %s",i,tmp) ;
		}

	    readRequest  ->checkRefCount() ;
	    readResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_03","ReadArrayDll OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_03","ReadArrayDll NOT OK") ;

		readRequest->checkRefCount() ;

		if (readResponse != NULL)
			readResponse->checkRefCount() ;

		return status ;
	}


};     /* class */
}      /* namespace opcua */
#endif // (WITH_WRITE == 1 && WITH_READ == 1)
#endif /* CLIENT03_H_ */
