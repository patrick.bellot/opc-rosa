
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASESESSION_H_
#define CLIENT_OPCUA_BASESESSION_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseSubscription.h"

namespace opcua {

class BaseSession
	: public BaseSubscription
{
public:

	BaseSession(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseSubscription(num, pScConfig, channel_config_idx, endpointUrl)
	{}

	virtual ~BaseSession() {}

protected: // INTERNAL CREATE SESSION

	SOPC_StatusCode Connexion_CreateSession(
			CreateSessionRequest   * rrequest,
			CreateSessionResponse ** pRResponse
			)
	{
		debug_i(MAIN_LIFE_DBG,"BaseSession","Connexion_CreateSession begin, client=%d",num) ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_CreateSessionRequest request ;
	    OpcUa_CreateSessionRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseSession","Connexion_CreateSession failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseSession","Connexion_CreateSession: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_CreateSessionRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseSession","Connexion_CreateSession: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseSession","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseSession","Connexion_CreateSession: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseSession","Connexion_CreateSession: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_CreateSessionResponse) {
	    	debug_iii(COM_ERR,"BaseSession","Connexion_CreateSession: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseSession","Connexion_CreateSession: answer is CreateSession") ;
	    *pRResponse = (CreateSessionResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_CreateSessionRequest_Clear(&request) ;
		return status ;
	}

protected: // INTERNAL ACTIVATE SESSION

	SOPC_StatusCode Connexion_ActivateSession(
			ActivateSessionRequest   * rrequest,
			ActivateSessionResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseSession","Connexion_ActivateSession begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_ActivateSessionRequest request ;
	    OpcUa_ActivateSessionRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseSession","Connexion_ActivateSession failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseSession","Connexion_ActivateSession: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_ActivateSessionRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseSession","Connexion_ActivateSession: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseSession","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseSession","Connexion_ActivateSession: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseSession","Connexion_ActivateSession: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_ActivateSessionResponse) {
	    	debug_iii(COM_ERR,"BaseSession","Connexion_ActivateSession: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseSession","Connexion_ActivateSession: answer is ActivateSession") ;
	    *pRResponse = (ActivateSessionResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_ActivateSessionRequest_Clear(&request) ;
		return status ;
	}


	//protected: // INTERNAL CLOSE SESSION


	SOPC_StatusCode Connexion_CloseSession(
			CloseSessionRequest   * rrequest,
			CloseSessionResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseSession","Connexion_CloseSession begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_CloseSessionRequest request ;
	    OpcUa_CloseSessionRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseSession","Connexion_CloseSession failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseSession","Connexion_CloseSession: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_CloseSessionRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseSession","Connexion_CloseSession: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseSession","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseSession","Connexion_CloseSession: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseSession","Connexion_CloseSession: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_CloseSessionResponse) {
	    	debug_iii(COM_ERR,"BaseSession","Connexion_CloseSession: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseSession","Connexion_CloseSession: answer is CloseSession") ;
	    *pRResponse = (CloseSessionResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_CloseSessionRequest_Clear(&request) ;
		return status ;
	}





} ;    /* class BaseClient */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASESESSION_H_ */
