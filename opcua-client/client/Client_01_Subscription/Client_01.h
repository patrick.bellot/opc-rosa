
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_01_H_
#define CLIENT_01_H_

#include "lib/OpcUa.h"

#if (WITH_SUBSCRIPTION == 1)

#include "lib/Stacks/All.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"
#include "lib/NotificationsAndEvents/OpcUa_IPCS_DataChangeFilter.h"
#include "lib/Utils/OpcUa_Sequence.h"
#include "lib/NotificationsAndEvents/All.h"
#include "lib/CommonParametersTypes/All.h"
#include "lib/StandardDataTypes/All.h"

#include "../OpcUa_BaseClient.h"

namespace opcua {

class Client_01
	: public BaseClient
{
public:

	void run()
	{
		int nbExt =  10 ; // Number of total loops from CreateSession to CloseSession
		int nbInt =  50 ; // Number of internal loops
		int nbPub =  2000 ; // Number of publish inside internal loops

		SOPC_StatusCode status = STATUS_OK ;

		// EXTERNAL LOOP
		while (--nbExt >= 0) {

			// CREATE SESSION
			debug(MAIN_LIFE_DBG,"Client_01","Create Session begin") ;
			status = createSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_01","Cannot Create Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_01","Create Session succeeds") ;

			// ACTIVATE SESSION
			debug(MAIN_LIFE_DBG,"Client_01","Activate Session begin") ;
			status = activateSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_01","Cannot Activate Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_01","Activate Session succeeds") ;

			IntegerId * subscriptionId            = NULL ;
			Duration  * revisedPublishingInterval = NULL ;
			Counter   * revisedLifetimeCount      = NULL ;
			Counter   * revisedMaxKeepAliveCount  = NULL ;

			// CREATE SUBSCRIPTION
			debug(MAIN_LIFE_DBG,"Client_01","CreateSubscription begin") ;
			status = doCreateSubscription(
					&subscriptionId,
					&revisedPublishingInterval,
					&revisedLifetimeCount,
					&revisedMaxKeepAliveCount) ;

			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_01","Cannot CreateSubscription: status=0x%08x",status) ;
				break  ;
			}

			debug(MAIN_LIFE_DBG,"Client_01","CreateSubscription succeeds") ;

			// INTERNAL LOOP
			int _nbInt = nbInt ;
			while (--_nbInt >= 0) {

				IntegerId * monitoredItemId0         = NULL ;
				Duration  * revisedSamplingInterval0 = NULL ;
				Counter   * revisedQueueSize0        = NULL ;

				IntegerId * monitoredItemId1         = NULL ;
				Duration  * revisedSamplingInterval1 = NULL ;
				Counter   * revisedQueueSize1        = NULL ;

				// CREATE MONITORED ITEMS (0)
				debug(MAIN_LIFE_DBG,"Client_01","CreateMonitoredItems (0) begin") ;
				status = doCreateMonitoredItems(
						subscriptionId,
						"XMouse",
						&monitoredItemId0,
						&revisedSamplingInterval0,
						&revisedQueueSize0) ;

				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_01","Cannot CreateMonitoredItems(0): status=0x%08x",status) ;
					break  ;
				}

				debug(MAIN_LIFE_DBG,"Client_01","CreateMonitoredItems (0) succeeds") ;

				// CREATE MONITORED ITEMS (1)
				debug(MAIN_LIFE_DBG,"Client_01","CreateMonitoredItems (1) begin") ;
				status = doCreateMonitoredItems(
						subscriptionId,
						"YMouse",
						&monitoredItemId1,
						&revisedSamplingInterval1,
						&revisedQueueSize1) ;

				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_01","Cannot CreateMonitoredItems (1): status=0x%08x",status) ;
					break  ;
				}

				debug(MAIN_LIFE_DBG,"Client_01","CreateMonitoredItems (1) succeeds") ;

				// BEGINNING PUBLISH SEQUENCE
				Counter * sequenceNumber = Counter::zero ;
				sequenceNumber->take() ;

				// INTERNAL PUBLISH LOOP
				int _nbPub = nbPub ;
				while (--_nbPub >= 0) {

					// PUBLISH
					debug(MAIN_LIFE_DBG,"Client_01","Publish (1) begin") ;
					status = doPublish(
							subscriptionId,
							&sequenceNumber) ;
					if(status != STATUS_OK) {
						debug_i(COM_ERR,"Client_01","Cannot Publish (1): status=0x%08x",status) ;
						break  ;
					}
					debug(MAIN_LIFE_DBG,"Client_01","Publish (1) succeeds") ;
				}

				// END PUBLISH SEQUENCE
				if (sequenceNumber != NULL)
					sequenceNumber->release() ;

				// DELETE MONITORED ITEMS
				debug(MAIN_LIFE_DBG,"Client_01","DeleteMonitoredItems begin") ;
				status = doDeleteMonitoredItems(
						subscriptionId,
						monitoredItemId0,
						monitoredItemId1) ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_01","Cannot DeleteMonitoredItems: status=0x%08x",status) ;
					break  ;
				}

				debug(MAIN_LIFE_DBG,"Client_01","DeleteMonitoredItems succeeds") ;

				// DELETE CREATE MONITORED ITEMS (0) DATA
				monitoredItemId0         ->release() ;
				revisedSamplingInterval0 ->release() ;
				revisedQueueSize0        ->release() ;

				// DELETE CREATE MONITORED ITEMS (1) DATA
				monitoredItemId1         ->release() ;
				revisedSamplingInterval1 ->release() ;
				revisedQueueSize1        ->release() ;
			} // nbInt

			// DELETE CREATE SUBSCRIPTION DATA
			subscriptionId            ->release() ;
			revisedPublishingInterval ->release() ;
			revisedLifetimeCount      ->release() ;
			revisedMaxKeepAliveCount  ->release() ;

			// CLOSE SESSION
			debug(MAIN_LIFE_DBG,"Client_01","Close Session begin") ;
			status = closeSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_01","Cannot Close Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_01","Close Session succeeds") ;
		} // nbExt

		debug(MAIN_LIFE_DBG,"Client_01","Ending thread") ;
	} // run()

public:

	Client_01(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx,String * endpointUrl)
		: BaseClient(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~Client_01()
	{}

private:

	SOPC_StatusCode createSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (Certificates::computeRandNonce(&clientNonce)) {
			debug(COM_ERR, "Client_01", "computeRandNonce(clientNonce) operation failed");
			if (clientNonce != NULL)
				clientNonce->checkRefCount() ;
			return 0x00000001 ;
		}

		clientNonce->take() ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSessionRequest * createSessionRequest =
		// OPC UA, Part 4, 5.5.2.2, table 11, p. 26		=
				new CreateSessionRequest(
						// OPC_UA, Part 4, 7.26, table 157, p. 138
						new RequestHeader(
								SessionAuthenticationToken::nullNodeId,
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
								),
						// OPC_UA, Part 4, 7.1, p. 106
						new ApplicationDescription(
								endpointUrl,
								new String("opc-rosa"),
								new LocalizedText(new String("Server opc-rosa"),LocaleId::empty),
								ApplicationType::client_1,
								String::empty,
								String::empty,
								new TableString  (0)),
						String::empty,
						endpointUrl,
						new String("Session Client_01"),
						clientNonce,
						Certificates::selfCertificate,
						Duration::maximum,
						UInt32::maximum);

		CreateSessionResponse * createSessionResponse = NULL ;

		status = Connexion_CreateSession(createSessionRequest,&createSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot Create Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_01","Create Session: processing parameters from response");

		sessionId              = createSessionResponse->getSessionId();
		authenticationTokenNum = createSessionResponse->getAuthenticationToken()->get() ;
	    serverNonce            = createSessionResponse->getServerNonce() ;

		#if (SECURITY != UANONE)
		if (createSessionResponse->getServerCertificate() == NULL ||
			createSessionResponse->getServerSignature  () == NULL ||
			Certificates::selfCertificate                 == NULL ||
			clientNonce 								  == NULL) {

			debug (COM_ERR,"Client_01","Create Session: channel parameter has not been found") ;
			status = 0x00000001 ;
			goto error ;
		}
#endif

		session = new Session(authenticationTokenNum, sessionId);
		session->setLastCreateOrActivateSessionNonce(serverNonce) ;

#if (SECURITY != UANONE)
		session->setDistantNonce(serverNonce) ;
		session->setLocalNonce(clientNonce) ;
		session->setDistantCertificate(createSessionResponse->getServerCertificate()) ;
#endif

		clientChannel->pushNewSession(authenticationTokenNum, session);

	    createSessionRequest->checkRefCount() ;
	    createSessionResponse->checkRefCount() ;

		clientNonce->release() ;
		clientNonce = NULL ;

		return STATUS_OK ;

error:
		clientNonce->release() ;
		clientNonce = NULL ;

		createSessionRequest->checkRefCount() ;

		if (createSessionResponse != NULL)
			createSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

private:

	SOPC_StatusCode activateSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Nonce  * serverNonce = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_01", "activateSession(): clientChannel null");
			return 0x00000001 ;
		}

		Session * session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_01", "activateSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000001 ;
		}

#if (SECURITY == UANONE)
		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
				// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						new UInt32(5000) // TimeoutHint
				),
				SignatureData::fakeSignatureData,
				new TableSignedSoftwareCertificate  (0),
				tableLocaleId,
				new AnonymousIdentityToken(new String("Anonymous")),
				SignatureData::fakeSignatureData);
#endif

#if (SECURITY != UANONE)
		Nonce * distantNonce = session->getDistantNonce() ;
		if (distantNonce == NULL) {
			debug(COM_ERR, "Client_01", "activateSession(): distantNonce == NULL");
			return 0x00000003 ;
		}

		ApplicationInstanceCertificate * distantCertificate = session->getDistantCertificate() ;
		if (distantCertificate == NULL) {
			debug(COM_ERR, "Client_01", "activateSession(): distantCertificate == NULL");
			return 0x00000003 ;
		}

		SignatureData * clientSignatureData = NULL ;
		if ((status = Certificates::computeSignatureData(&clientSignatureData, session->getDistantCertificate(), session->getDistantNonce()))) {
			debug_i(COM_ERR, "Client_01", "activateSession(): Certificates::computeSignatureData(..) returns %d",status);
			return status ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							new UInt32(5000) // TimeoutHint
					),
					clientSignatureData, // clientSignatureData
					new TableSignedSoftwareCertificate  (0),
					tableLocaleId,
					new AnonymousIdentityToken(new String("Anonymous")),
					SignatureData::fakeSignatureData); // fakeSignature
#endif

		ActivateSessionResponse * activateSessionResponse = NULL ;

		status = Connexion_ActivateSession(activateSessionRequest,&activateSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot Activate Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_01","Activate Session: processing parameters from response");

		serverNonce = activateSessionResponse->getServerNonce();

		session->setLastCreateOrActivateSessionNonce(serverNonce);

	    activateSessionRequest  ->checkRefCount() ;
	    activateSessionResponse ->checkRefCount() ;

		return STATUS_OK ;

error:

		activateSessionRequest->checkRefCount() ;

		if (activateSessionResponse != NULL)
			activateSessionResponse->checkRefCount() ;

		return status ;
	}

private:

	SOPC_StatusCode closeSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_01", "closeSession(): clientChannel null");
			return 0x00000001 ;
		}

		session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_01", "closeSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000001 ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		// OPC-UA, Part3, 5.6.2, p. 26
		CloseSessionRequest * closeSessionRequest = new CloseSessionRequest(
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
				),
				Boolean::booleanFalse);

		CloseSessionResponse * closeSessionResponse = NULL ;

		status = Connexion_CloseSession(closeSessionRequest,&closeSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot Close Session: status=0x%08x",status) ;
			goto error ;
		}

	    clientChannel->deleteSession(authenticationTokenNum) ;

error:

		closeSessionRequest->checkRefCount() ;

		if (closeSessionResponse != NULL)
			closeSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

		return status ;
	}

private:

	SOPC_StatusCode doCreateSubscription(
			IntegerId ** pSubscriptionId,
			Duration  ** pRevisedPublishingInterval,
			Counter   ** pRevisedLifetimeCount,
			Counter   ** pRevisedMaxKeepAliveCount)
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateSubscription") ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSubscriptionRequest  * createSubscriptionRequest
			// OPC UA, Part 4, 5.13.2.2, table 83, p. 81
			= new CreateSubscriptionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
					new Duration(1000), // PublishingInterval
					new Counter(1000),  // LifeTimeCounter
					new Counter(5),     // MaxKeepAliveCount
					new Counter(0),     // MaxNotificationsPerPublish
					new Boolean(true),  // PublishingEnabled,
					Byte::zero) ;       // Priority

		CreateSubscriptionResponse * createSubscriptionResponse = NULL ;

		status = Connexion_CreateSubscription(createSubscriptionRequest,&createSubscriptionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot CreateSubscription: status=0x%08x",status) ;
			goto error ;
		}

	    debug(MAIN_LIFE_DBG,"Client_01","Begin exploiting CreateSubscription result") ;

	    (* pSubscriptionId            = createSubscriptionResponse->getSubscriptionId())            -> take() ;
		(* pRevisedPublishingInterval = createSubscriptionResponse->getRevisedPublishingInterval()) -> take() ;
		(* pRevisedLifetimeCount      = createSubscriptionResponse->getRevisedLifetimeCount())      -> take() ;
		(* pRevisedMaxKeepAliveCount  = createSubscriptionResponse->getRevisedMaxKeepAliveCount())  -> take() ;

	    createSubscriptionRequest  ->checkRefCount() ;
	    createSubscriptionResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateSubscription OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateSubscription NOT OK") ;

		createSubscriptionRequest->checkRefCount() ;
		if (createSubscriptionResponse != NULL)
			createSubscriptionResponse->checkRefCount() ;

		return status ;
	}

private:

	SOPC_StatusCode doCreateMonitoredItems(
			IntegerId   * subscriptionId,
			const char  * variableName,
			IntegerId  ** pMonitoredItemId,
			Duration   ** pRevisedSamplingInterval,
			Counter    ** pRevisedQueueSize)
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateMonitoredItems") ;

		uint32_t sequence = requestSequence->allocate(1) ;

		ReadValueId * readValueId = new ReadValueId(
				new NodeId(UInt16::zero,new String(variableName)),
				new IntegerId(AttributeId_Value),
				NumericRange::null,
				QualifiedName::defaultBinary) ;

		MonitoringParameters * requestedParameters = new MonitoringParameters(
				new IntegerId(sequence<<8 | num),
				new Duration(200.0),
				DataChangeFilter::nullFilter,
				new Counter(64),
				Boolean::booleanTrue
				) ;

		TableMonitoredItemCreateRequest * itemsToCreate = new TableMonitoredItemCreateRequest(1) ;

		itemsToCreate->set(0,new MonitoredItemCreateRequest(
				readValueId,
				MonitoringMode::reporting_2,
				requestedParameters)) ;

		CreateMonitoredItemsRequest  * createMonitoredItemsRequest
			// OPC UA, Part 4, 5.12.2.2, table 64, p. 67
			= new CreateMonitoredItemsRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
					subscriptionId,
					TimestampsToReturn::server_1,
					itemsToCreate
					) ;

		CreateMonitoredItemsResponse * createMonitoredItemsResponse = NULL ;

		status = Connexion_CreateMonitoredItems(createMonitoredItemsRequest,&createMonitoredItemsResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot CreateMonitoredItems: status=0x%08x",status) ;
			goto error ;
		}

		debug(MAIN_LIFE_DBG,"Client_01","Begin exploiting CreateMonitoredItems result") ;

		{
			TableMonitoredItemCreateResult   * results = createMonitoredItemsResponse->getResults() ;

			MonitoredItemCreateResult * result = results->get(0) ;

			(* pMonitoredItemId         = result->getMonitoredItemId())         -> take() ;
			(* pRevisedSamplingInterval = result->getRevisedSamplingInterval()) -> take() ;
			(* pRevisedQueueSize        = result->getRevisedQueueSize())        -> take() ;
		}

		createMonitoredItemsRequest  ->checkRefCount() ;
		createMonitoredItemsResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateMonitoredItems OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_01","Testing CreateMonitoredItems NOT OK") ;

		createMonitoredItemsRequest->checkRefCount() ;
		if (createMonitoredItemsResponse != NULL)
			createMonitoredItemsResponse->checkRefCount() ;

		return status ;
	}

private:

	SOPC_StatusCode doDeleteMonitoredItems(
			IntegerId  * subscriptionId,
			IntegerId  * monitoredItemId0,
			IntegerId  * monitoredItemId1)
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing DeleteMonitoredItems") ;

		TableIntegerId   * monitoredItemIds = new TableIntegerId  (2) ;
		monitoredItemIds->set(0,monitoredItemId0) ;
		monitoredItemIds->set(1,monitoredItemId1) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		DeleteMonitoredItemsRequest  * deleteMonitoredItemsRequest
			// OPC UA, Part 4, 5.13.8.2, table 98, p. 89
			= new DeleteMonitoredItemsRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
					subscriptionId,
					monitoredItemIds
					) ;

		DeleteMonitoredItemsResponse * deleteMonitoredItemsResponse = NULL ;

		status = Connexion_DeleteMonitoredItems(deleteMonitoredItemsRequest,&deleteMonitoredItemsResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot DeleteMonitoredItems: status=0x%08x",status) ;
			goto error ;
		}

		debug(MAIN_LIFE_DBG,"Client_01","Begin exploiting DeleteMonitoredItems result") ;
		{
			TableStatusCode   * results = deleteMonitoredItemsResponse->getResults() ;
			int32_t             len     = results->getLength() ;

			for (int i = 0 ; i < len ; i++) {
				debug_ii(MAIN_LIFE_DBG,"DeleteMonitoredItems","   Result %d = 0x%08x",i,results->get(i)->get()) ;
			}
		}

		deleteMonitoredItemsRequest  ->checkRefCount() ;
		deleteMonitoredItemsResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing DeleteMonitoredItems OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_01","Testing DeleteMonitoredItems NOT OK") ;

		deleteMonitoredItemsRequest->checkRefCount() ;
		if (deleteMonitoredItemsResponse != NULL)
			deleteMonitoredItemsResponse->checkRefCount() ;

		return status ;
	}

private:

	SOPC_StatusCode doPublish(
			IntegerId  * subscriptionId,
			Counter   ** sequenceNumber)
	{
		SOPC_StatusCode status = STATUS_OK ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing Publish") ;

		SubscriptionAcknowledgement *  subsciptionAcknowledgement
			= new SubscriptionAcknowledgement(subscriptionId,*sequenceNumber) ;

		TableSubscriptionAcknowledgement   * subsciptionAcknowledgements = new TableSubscriptionAcknowledgement  (1) ;
		subsciptionAcknowledgements->set(0,subsciptionAcknowledgement) ;

		uint32_t sequence = requestSequence->allocate(1) ;

		PublishRequest  * publishRequest
			// OPC UA, Part 4, 5.13.5.2, table 90, p. 86
			= new PublishRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							UInt32::zero // TimeoutHint
					),
					subsciptionAcknowledgements
					) ;

		PublishResponse * publishResponse = NULL ;

		status = Connexion_Publish(publishRequest,&publishResponse) ;

		if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_01","Cannot Publish: status=0x%08x",status) ;
			goto error ;
		}

		debug(MAIN_LIFE_DBG,"Client_01","Begin exploiting Publish result") ;
		{
			IntegerId           * subscriptionIdReturn     = publishResponse->getSubscriptionId() ;
			TableCounter        * availableSequenceNumbers = publishResponse->getAvailableSequenceNumbers() ;
			Boolean             * moreNotifications        = publishResponse->getMoreNotifications() ;
			NotificationMessage * notificationMessage      = publishResponse->getNotificationMessage() ;
			TableStatusCode     * results                  = publishResponse->getResults() ;

			int resultsLen = results->getLength() ;
			for (int i = 0 ; i < resultsLen ; i++) {
				if (results->get(i)->get() != _Good){
					debug_i(COM_ERR,"Client_01","Publish result number %d NOT OK",i) ;
				} else {
					debug_i(MAIN_LIFE_DBG,"Client_01","Publish result number %d OK",i) ;
				}
			}

			if (subscriptionId->notEquals(subscriptionIdReturn)) {
				debug(COM_ERR,"Client02","Publish : returned subscriptionId different from sent subscriptionId") ;
				status = 0x00000001 ;
				goto error ;
			}

			if (availableSequenceNumbers->getLength() != 0) {
				debug(COM_ERR,"Client02","Publish : availableSequenceNumbers->getLength() != 0") ;
				status = 0x00000002 ;
				goto error ;
			}

			if (moreNotifications->get()) {
				debug(COM_ERR,"Client02","Publish : moreNotifications is TRUE (it must not)") ;
				status = 0x00000002 ;
				goto error ;
			}

			(*sequenceNumber)->release() ;
			*sequenceNumber = notificationMessage->getSequenceNumber() ;
			(*sequenceNumber)->take() ;

			debug_i(MAIN_LIFE_DBG,"Client_01","Publish: received notificationMessage with sequenceNumber = %u",(*sequenceNumber)->get()) ;
			debug_l(MAIN_LIFE_DBG,"",         "         and publish time = %lld",(long long)(notificationMessage->getPublishTime()->get())) ;

			TableNotificationData   * notificationDataTable = notificationMessage->getNotificationData() ;
			int notificationDataTableLen = notificationDataTable->getLength() ;

			debug_i(MAIN_LIFE_DBG,"Client_01","      Beginning of %d notificationData",notificationDataTableLen) ;

			for (int nbNotificationData = 0 ; nbNotificationData < notificationDataTableLen ; nbNotificationData++) {

				debug_i(MAIN_LIFE_DBG,"Client_01","         - NotificationData number %d",nbNotificationData) ;

				NotificationData * notificationData = notificationDataTable->get(nbNotificationData) ;

				switch (notificationData->getTypeId()) {
				case OpcUaId_DataChangeNotification :
				{
					debug(MAIN_LIFE_DBG,"Client_01","            . Nodeid = OpcUaId_DataChangeNotification") ;
					DataChangeNotification * dataChangeNotification = static_cast<DataChangeNotification *>(notificationData) ;

					TableMonitoredItemNotification   * monitoredItems = dataChangeNotification->getMonitoredItems() ;
					int monitoredIemsLen = monitoredItems->getLength() ;
					debug_i(MAIN_LIFE_DBG,"Client_01","            . Beginning of %d MonitoredItems",monitoredIemsLen) ;

					for (int nbMonitoredItem = 0 ; nbMonitoredItem < monitoredIemsLen ; nbMonitoredItem++) {
						MonitoredItemNotification * monitoredItem = monitoredItems->get(nbMonitoredItem) ;
						debug_i(MAIN_LIFE_DBG,"Client_01","               - MonitoredItems number %d",nbMonitoredItem) ;

						debug_i(MAIN_LIFE_DBG,"Client_01","                  . clientHandle = %d",monitoredItem->getClientHandle()->get()) ;

						DataValue * dataValue = monitoredItem->getValue() ;

						StatusCode * statusCode = dataValue->getStatusCode() ;
						if (statusCode == NULL) {
							debug(MAIN_LIFE_DBG,"Client_01","                  . statusCode = NULL") ;
						} else {
							debug_i(MAIN_LIFE_DBG,"Client_01","                  . statusCode = 0x%08x",statusCode->get()) ;
						}

						UtcTime * sourceTimeStamp = dataValue->getSourceTimeStamp() ;
						if (sourceTimeStamp == NULL) {
							debug(MAIN_LIFE_DBG,"Client_01","                  . sourceTimeStamp = NULL") ;
						} else {
							debug_l(MAIN_LIFE_DBG,"Client_01","                  . sourceTimeStamp = %lld",(long long int)(sourceTimeStamp->get())) ;
						}

						UtcTime * serverTimeStamp = dataValue->getServerTimeStamp() ;
						if (serverTimeStamp == NULL) {
							debug(MAIN_LIFE_DBG,"Client_01","                  . serverTimeStamp = NULL") ;
						} else {
							debug_l(MAIN_LIFE_DBG,"Client_01","                  . serverTimeStamp = %lld",(long long int)(serverTimeStamp->get())) ;
						}
						Variant * variant = dataValue->getVariantAttr() ;
						if (variant == NULL) {
							debug(MAIN_LIFE_DBG,"Client_01","                  . variant = NULL") ;
						} else {
							if (variant->getArrayLength() == -1) {
								switch (variant->getBuiltinTypeId()) {
								case Builtin_Int32 :
									debug_i(MAIN_LIFE_DBG,"Client_01","                  . variant = %d (Int32)",(dynamic_cast<Int32 *>(variant->getValue()))->get()) ;
									break ;
								case Builtin_UInt32 :
									debug_i(MAIN_LIFE_DBG,"Client_01","                  . variant = %u (UInt32)",(dynamic_cast<UInt32 *>(variant->getValue()))->get()) ;
									break ;
								case Builtin_Double :
									debug_i(MAIN_LIFE_DBG,"Client_01","                  . variant = %f (Double)",(dynamic_cast<Double *>(variant->getValue()))->get()) ;
									break ;
								case Builtin_Float :
									debug_i(MAIN_LIFE_DBG,"Client_01","                  . variant = %f (Float)",(dynamic_cast<Float *>(variant->getValue()))->get()) ;
									break ;
								default:
									debug_i(MAIN_LIFE_DBG,"Client_01","                  . variant = uncovered typeId: %u",(uint32_t)(variant->getBuiltinTypeId())) ;
									break ;
								}
							} else {
								debug_ii(MAIN_LIFE_DBG,"Client_01","                  . variant = arrayLength: %d, typeId: %u",variant->getArrayLength(),(uint32_t)(variant->getBuiltinTypeId())) ;
							}
						}
					}

					debug(MAIN_LIFE_DBG,"Client_01","            . End of MonitoredItems") ;
				} ;
				break ;
				case OpcUaId_EventNotificationList :
				{
					debug(MAIN_LIFE_DBG,"Client_01","            . Nodeid = OpcUaId_EventNotificationList") ;
					//EventNotificationList * eventNotificationList = static_cast<EventNotificationList *>(notificationData) ;
				}
					break ;
				case OpcUaId_StatusChangeNotification :
				{
					debug(MAIN_LIFE_DBG,"Client_01","            . Nodeid = OpcUaId_StatusChangeNotification") ;
					//StatusChangeNotification * statusChangeNotification = static_cast<StatusChangeNotification *>(notificationData) ;

				} ;
				break ;
				default: {
					debug_i(MAIN_LIFE_DBG,"Client_01","            . Nodeid = %u",notificationData->getTypeId()) ;
				}
				break ;
				}
			}

			debug(MAIN_LIFE_DBG,"Client_01","      End of notificationData") ;

		}

		publishRequest  ->checkRefCount() ;
		publishResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_01","Testing Publish OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_01","Testing Publish NOT OK") ;

		publishRequest->checkRefCount() ;
		if (publishResponse != NULL)
			publishResponse->checkRefCount() ;

		return status ;
	}


};     /* class */
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* CLIENT01_H_ */
