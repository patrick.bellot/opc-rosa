
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef STARTCLIENTS_01_H_
#define STARTCLIENTS_01_H_

#include "lib/Stacks/All.h"
#include "Client_01.h"
#include "../OpcUa_BaseClient.h"

#include <sopc_toolkit_config.h>

extern "C" {

	extern opcua::BaseClient ** the_clients ;
	extern int                  the_nbClients ;

}

#if (WITH_SUBSCRIPTION == 1)

namespace opcua {

class StartClients_01
{
public:

	static SOPC_StatusCode startClients(
			String                    * endpointUrl,
			SOPC_SecureChannel_Config * pScConfig
			)
	{
		int nbClients = 3 ; // Number of client threads
		int nbTry    = 10 ; // Number of total loops from OpenSecureChannel to CloseSecureChannel

		uint32_t           channel_config_idx = 0 ;
		Client_01       ** clients = NULL ;

		SOPC_StatusCode    status  = STATUS_OK ;

		// CREATING CHANNEL CONFIGURATION
		channel_config_idx = SOPC_ToolkitClient_AddSecureChannelConfig(pScConfig);
		if (channel_config_idx == 0) {
			debug(COM_ERR,"StartClientss_02","Cannot add secure channel config") ;
			goto error ;
		} else {
			debug_i(IPCS_DBG,"StartClients_02","channel_config_idx=%d",channel_config_idx) ;
		}
		SOPC_Toolkit_Configured() ;
		debug(MAIN_LIFE_DBG,"StartClients_02","Open Secure Channels config succeeds") ;


		// EXTERNAL LOOP
		while (--nbTry >= 0) {

		    // CREATING CLIENTS
			debug(MAIN_LIFE_DBG,"StartClients_02","Building clients") ;
			clients = new Client_01 *[nbClients] ;

			for (int i = 0 ; i < nbClients ; i++)
				clients[i] = new Client_01(i,pScConfig,channel_config_idx,endpointUrl) ;

			the_clients  = (BaseClient **)clients ;
			the_nbClients = nbClients ;


			// OPEN SECURE CHANNEL
			debug(MAIN_LIFE_DBG,"StartClients_01","Open Secure Channel begin") ;
			status = clients[0]->openSecureChannel() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"StartClients_01","Cannot open Secure Channel: status=0x%08x",status) ;
				goto error ;
			}
			debug(MAIN_LIFE_DBG,"StartClients_01","Open Secure Channel succeeds") ;


			// STARTING CLIENTS
			debug(MAIN_LIFE_DBG,"StartClients_01","Starting clients") ;
			for (int i = 0 ; i < nbClients ; i++)
				clients[i]->start() ;
			debug(MAIN_LIFE_DBG,"StartClients_01","Starting clients succeeds") ;


			// WAITING FOR CLIENTS
			debug(MAIN_LIFE_DBG,"StartClients_01","Waiting for clients") ;
			for (int i = 0 ; i < nbClients ; i++)
				clients[i]->join() ;
			debug(MAIN_LIFE_DBG,"StartClients_01","Waiting for clients succeeds") ;


			// CLOSE SECURE CHANNEL
			debug(MAIN_LIFE_DBG,"StartClients_01","Close Secure Channel begin") ;
			status = clients[0]->closeSecureChannel() ;
			if(status != STATUS_OK) {
				debug_ii(COM_ERR,"StartClients_01","Cannot close Secure Channel: status=0x%08x, loop=%d",status,nbTry) ;
				goto error ;
			}
			debug(MAIN_LIFE_DBG,"StartClients_01","Close Secure Channel succeeds") ;


			// DELETING CLIENTS
			debug(MAIN_LIFE_DBG,"StartClients_01","Deleting clients") ;
			for (int i = 0 ; i < nbClients ; i++)
				delete clients[i] ;
			delete[] clients ;
			clients = NULL ;

			debug_i(MAIN_LIFE_DBG,"StartClients_01","End of Loop (%d)",nbTry) ;

		} // while (--nbTry >= 0)

		SOPC_Toolkit_Clear();

		debug_i(MAIN_LIFE_DBG,"StartClients_01","Stopping with status=0x%08x",STATUS_OK) ;

		return STATUS_OK ;

error:
		debug(MAIN_LIFE_DBG,"StartClients_01","Releasing memory with error") ;

		if (clients != NULL) {
			for (int i = 0 ; i < nbClients ; i++)
				delete clients[i] ;
			delete[] clients ;
		}

		SOPC_Toolkit_Clear();

		debug_i(MAIN_LIFE_DBG,"StartClients_01","Stopping with error, status=0x%08x",status) ;
		return status;
	}


} ;    /* class StartClients01 */
}      /* namespace opcua */
#endif /* WITH_SUBSCRIPTION == 1 */
#endif /* STARTCLIENTS_01_H_ */
