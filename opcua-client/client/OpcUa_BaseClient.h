
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASECLIENT_H_
#define CLIENT_OPCUA_BASECLIENT_H_

#include "lib/OpcUa.h"
#include "lib/Utils/OpcUa_SyncQueue.h"
#include "lib/s2opc/S2OPC_ClientServices.h"

#include "OpcUa_BaseMethod.h"

namespace opcua {

class BaseClient
	: public BaseMethod
{
public:

	BaseClient(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseMethod(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~BaseClient() {}

} ;    /* class BaseClient */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASECLIENT_H_ */
