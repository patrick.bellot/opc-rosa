
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_02_H_
#define CLIENT_02_H_

#include "lib/OpcUa.h"

#if (WITH_CALL == 1)

#include "lib/Stacks/All.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

#include "../OpcUa_BaseClient.h"

namespace opcua {

class Client_02
	: public BaseClient
{
public:

	void run()
	{
		int nbExt =  3 ; // Number of total loops from CreateSession to CloseSession
		int nbInt = 10 ; // Number of internal loops (after ActivateSession and before CloseSession)

		SOPC_StatusCode status = STATUS_OK ;

		// EXTERNAL LOOP
		while (--nbExt >= 0) {

			// CREATE SESSION
			debug(MAIN_LIFE_DBG,"Client_02","Create Session begin") ;
			status = createSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_02","Cannot Create Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_02","Create Session succeeds") ;

			// ACTIVATE SESSION
			debug(MAIN_LIFE_DBG,"Client_02","Activate Session begin") ;
			status = activateSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_02","Cannot Activate Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_02","Activate Session succeeds") ;

			// INTERNAL LOOP
			int _nbInt = nbInt ;
			while (--_nbInt >= 0) {

				// CALL
				debug(MAIN_LIFE_DBG,"Client_02","Call begin") ;
				status = doCall() ;
				if(status != STATUS_OK) {
					debug_i(COM_ERR,"Client_02","Cannot Call: status=0x%08x",status) ;
					break  ;
				}
				debug(MAIN_LIFE_DBG,"Client_02","Call succeeds") ;

			} // nbInt

			// CLOSE SESSION
			debug(MAIN_LIFE_DBG,"Client_02","Close Session begin") ;
			status = closeSession() ;
			if(status != STATUS_OK) {
				debug_i(COM_ERR,"Client_02","Cannot Close Session: status=0x%08x",status) ;
				return ;
			}
			debug(MAIN_LIFE_DBG,"Client_02","Close Session succeeds") ;
		} // nbExt

		debug(MAIN_LIFE_DBG,"Client_02","Ending thread") ;
	} // run()

public:

	Client_02(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx,String * endpointUrl)
		: BaseClient(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~Client_02()
	{}


public:

	SOPC_StatusCode createSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (Certificates::computeRandNonce(&clientNonce)) {
			debug(COM_ERR, "Client_02", "computeRandNonce(clientNonce) operation failed");
			if (clientNonce != NULL)
				clientNonce->checkRefCount() ;
			return 0x00000001 ;
		}

		clientNonce->take() ;

		uint32_t sequence = requestSequence->allocate(1) ;

		CreateSessionRequest * createSessionRequest =
		// OPC UA, Part 4, 5.5.2.2, table 11, p. 26		=
				new CreateSessionRequest(
						// OPC_UA, Part 4, 7.26, table 157, p. 138
						new RequestHeader(
								SessionAuthenticationToken::nullNodeId,
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
								),
						// OPC_UA, Part 4, 7.1, p. 106
						new ApplicationDescription(
								new String("opc-tcp://192.168.1.150:10000"),
								new String("opc-rosa"),
								new LocalizedText(new String("Client_02"),LocaleId::en),
								ApplicationType::client_1,
								String::empty,
								String::empty,
								new TableString  (0)),
						String::empty,
						endpointUrl,
						new String("Session Client_02"),
						clientNonce,
						Certificates::selfCertificate,
						Duration::maximum,
						UInt32::maximum);

		CreateSessionResponse * createSessionResponse = NULL ;

		status = Connexion_CreateSession(createSessionRequest,&createSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_02","Cannot Create Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_02","Create Session: processing parameters from response");

		sessionId              = createSessionResponse->getSessionId();
		authenticationTokenNum = createSessionResponse->getAuthenticationToken()->get() ;
	    serverNonce            = createSessionResponse->getServerNonce() ;

#if (SECURITY != UANONE)
		if (createSessionResponse->getServerCertificate() == NULL ||
			createSessionResponse->getServerSignature  () == NULL ||
			Certificates::selfCertificate                 == NULL ||
			clientNonce 								  == NULL) {

			debug (COM_ERR,"Client_02","Create Session: channel parameter has not been found") ;
			status = 0x00000002 ;
			goto error ;
		}
#endif

		session = new Session(authenticationTokenNum, sessionId);
		session->setLastCreateOrActivateSessionNonce(serverNonce) ;

#if (SECURITY != UANONE)
		session->setDistantNonce(serverNonce) ;
		session->setLocalNonce(clientNonce) ;
		session->setDistantCertificate(createSessionResponse->getServerCertificate()) ;
#endif

		clientChannel->pushNewSession(authenticationTokenNum, session);

	    createSessionRequest->checkRefCount() ;
	    createSessionResponse->checkRefCount() ;

		clientNonce->release() ;
		clientNonce = NULL ;

		return STATUS_OK ;

error:
		clientNonce->release() ;
		clientNonce = NULL ;

		createSessionRequest->checkRefCount() ;

		if (createSessionResponse != NULL)
			createSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

	    return status ;
	}

public:

	SOPC_StatusCode activateSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Nonce  * serverNonce = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_02", "activateSession(): clientChannel null");
			return 0x00000001 ;
		}

		Session * session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_02", "activateSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

#if (SECURITY == UANONE)
		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
				// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						new UInt32(5000) // TimeoutHint
				),
				SignatureData::fakeSignatureData,
				new TableSignedSoftwareCertificate  (0),
				tableLocaleId,
				new AnonymousIdentityToken(new String("Anonymous")),
				SignatureData::fakeSignatureData);
#endif

#if (SECURITY != UANONE)
		Nonce * distantNonce = session->getDistantNonce() ;
		if (distantNonce == NULL) {
			debug(COM_ERR, "Client_02", "activateSession(): distantNonce == NULL");
			return 0x00000003 ;
		}

		ApplicationInstanceCertificate * distantCertificate = session->getDistantCertificate() ;
		if (distantCertificate == NULL) {
			debug(COM_ERR, "Client_02", "activateSession(): distantCertificate == NULL");
			return 0x00000003 ;
		}

		SignatureData * clientSignatureData = NULL ;
		if ((status = Certificates::computeSignatureData(&clientSignatureData, session->getDistantCertificate(), session->getDistantNonce()))) {
			debug_i(COM_ERR, "Client_02", "activateSession(): Certificates::computeSignatureData(..) returns %d",status);
			return status ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		TableLocaleId   * tableLocaleId = new TableLocaleId  (1) ;
		tableLocaleId->set(0,LocaleId::en) ;

		ActivateSessionRequest * activateSessionRequest
			// OPC UA, Part 4, 5.6.3.2, table 13, p. 29
			= new ActivateSessionRequest(
					// OPC_UA, Part 4, 7.26, table 157, p. 138
					new RequestHeader(
							new SessionAuthenticationToken(authenticationTokenNum),
							UtcTime::now(),
							new IntegerId(sequence<<8 | num),
							UInt32::zero, // ReturnDiagnostics
							String::empty, // auditEntryId
							new UInt32(5000) // TimeoutHint
					),
					clientSignatureData, // clientSignatureData
					new TableSignedSoftwareCertificate  (0),
					tableLocaleId,
					new AnonymousIdentityToken(new String("Anonymous")),
					SignatureData::fakeSignatureData); // fakeSignature
#endif

		ActivateSessionResponse * activateSessionResponse = NULL ;

		status = Connexion_ActivateSession(activateSessionRequest,&activateSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_02","Cannot Activate Session: status=0x%08x",status) ;
			goto error ;
		}

		debug(CLIENT_DBG, "Client_02","Activate Session: processing parameters from response");

		serverNonce = activateSessionResponse->getServerNonce();

		session->setLastCreateOrActivateSessionNonce(serverNonce);

	    activateSessionRequest  ->checkRefCount() ;
	    activateSessionResponse ->checkRefCount() ;

		return STATUS_OK ;

error:

		activateSessionRequest->checkRefCount() ;

		if (activateSessionResponse != NULL)
			activateSessionResponse->checkRefCount() ;

		return status ;
	}

public:

	SOPC_StatusCode closeSession()
	{
		SOPC_StatusCode status = STATUS_OK ;

		Session * session = NULL ;

		if (clientChannel == NULL) {
			debug(COM_ERR, "Client_02", "closeSession(): clientChannel null");
			return 0x00000001 ;
		}

		session = clientChannel->getSession(authenticationTokenNum) ;

		if (session == NULL) {
			debug_i(COM_ERR, "Client_02", "closeSession(): bad session id (bad authenticationToken = %u)",authenticationTokenNum);
			return 0x00000002 ;
		}

		uint32_t sequence = requestSequence->allocate(1) ;

		// OPC-UA, Part3, 5.6.2, p. 26
		CloseSessionRequest * closeSessionRequest = new CloseSessionRequest(
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
								UtcTime::now(),
								new IntegerId(sequence<<8 | num),
								UInt32::zero, // ReturnDiagnostics
								String::empty, // auditEntryId
								UInt32::zero // TimeoutHint
				),
				Boolean::booleanFalse);

		CloseSessionResponse * closeSessionResponse = NULL ;

		status = Connexion_CloseSession(closeSessionRequest,&closeSessionResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_02","Cannot Close Session: status=0x%08x",status) ;
			goto error ;
		}

	    clientChannel->deleteSession(authenticationTokenNum) ;

error:

		closeSessionRequest->checkRefCount() ;

		if (closeSessionResponse != NULL)
			closeSessionResponse->checkRefCount() ;

		sessionId              = NULL ;
		authenticationTokenNum = 0 ;
	    serverNonce            = NULL ;

		return status ;
	}

public:

	SOPC_StatusCode doCall()
	{
		SOPC_StatusCode status = STATUS_OK ;

		TableCallMethodResult   * results = NULL ;
		int32_t                   length  = 0 ;

		debug(MAIN_LIFE_DBG,"Client_02","Testing Call") ;

		NodeId * ownerNode  = new NodeId(UInt16::zero,new String("AddObject")) ;
		NodeId * methodNode = new NodeId(UInt16::zero,new String("AddMethod")) ;

		TableVariant   * inputArguments = new TableVariant  (6);
		inputArguments->set(0, new Variant(Builtin_Int32, new Int32(5)));
		inputArguments->set(1, new Variant(Builtin_Int32, new Int32(7)));
		inputArguments->set(2, new Variant(Builtin_Int32, new Int32(2)));
		inputArguments->set(3, new Variant(Builtin_Int32, new Int32(1)));
		inputArguments->set(4, new Variant(Builtin_Int32, new Int32(6)));
		inputArguments->set(5, new Variant(Builtin_Int32, new Int32(9)));

		CallMethodRequest * methodToCall = new CallMethodRequest(ownerNode,methodNode, inputArguments);

		TableCallMethodRequest   * methodsToCall = new TableCallMethodRequest  (1);
		methodsToCall->set(0, methodToCall);

		uint32_t sequence = requestSequence->allocate(1) ;

		CallRequest * callRequest
		// OPC UA, Part 4, 5.11.2.2, p. 60
		= new CallRequest(
		// OPC_UA, Part 4, 7.26, table 157, p. 138
				new RequestHeader(
						new SessionAuthenticationToken(authenticationTokenNum),
						UtcTime::now(),
						new IntegerId(sequence<<8 | num),
						UInt32::zero, // ReturnDiagnostics
						String::empty, // auditEntryId
						UInt32::zero), // TimeoutHint
						methodsToCall);

		CallResponse * callResponse = NULL ;

		status = Connexion_Call(callRequest,&callResponse) ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"Client_02","Cannot Call: status=0x%08x",status) ;
			goto error ;
		}

		results = callResponse->getResults();
		length  = results->getLength();

		debug_i(MAIN_LIFE_DBG,"Client_02","       Call: number of results = %d",length) ;

		for (int i = 0; i < length; i++) {

			CallMethodResult * result = results->get(i);

			StatusCode * statusCode = result->getStatusCode();
			debug_ii(MAIN_LIFE_DBG,"Client_02","            Results number %d, status=0x%08x",i,statusCode->get()) ;

			TableVariant   * outputArguments = result->getOutputArguments();
			int32_t len = outputArguments->getLength();

			for (int j = 0; j < len; j++) {

				Variant * outputArgument = outputArguments->get(j);
				BaseDataType * val = outputArgument->getValue();
				int32_t  l   = val->getLength();
				uint32_t bit = outputArgument->getBuiltinTypeId();

				if (l == -1 && bit == Builtin_Int32) {
					debug_ii(MAIN_LIFE_DBG,"Client_02","                 Call Result %d: %d",j,((Int32 *) val)->get()) ;
				} else {
					debug_i(MAIN_LIFE_DBG,"Client_02","                 Call Result %d: not an Int32",j) ;
				}
			}
		}

		callRequest  ->checkRefCount() ;
	    callResponse ->checkRefCount() ;

		debug(MAIN_LIFE_DBG,"Client_02","Call Good OK") ;

		return STATUS_OK ;

error:

		debug(MAIN_LIFE_DBG,"Client_02","Register Good NOT OK") ;

		callRequest->checkRefCount() ;

		if (callResponse != NULL)
			callResponse->checkRefCount() ;

		return status ;
	}


};     /* class */
}      /* namespace opcua */
#endif /* WITH_CALL == 1 */
#endif /* CLIENT02_H_ */
