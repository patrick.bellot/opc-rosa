
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASEMETHOD_H_
#define CLIENT_OPCUA_BASEMETHOD_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseMonitoredItem.h"

namespace opcua {

class BaseMethod
	: public BaseMonitoredItem
{
public:

	BaseMethod(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseMonitoredItem(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~BaseMethod() {}

#if (WITH_CALL ==1)

protected:

	SOPC_StatusCode Connexion_Call(
			CallRequest   * rrequest,
			CallResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseMethod","Connexion_Call begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_CallRequest request ;
	    OpcUa_CallRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseMethod","Connexion_Call failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseMethod","Connexion_Call: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_CallRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseMethod","Connexion_Call: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseMethod","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseMethod","Connexion_Call: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseMethod","Connexion_Call: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_CallResponse) {
	    	debug_iii(COM_ERR,"BaseMethod","Connexion_Call: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseMethod","Connexion_Call: answer is Call") ;
	    *pRResponse = (CallResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_CallRequest_Clear(&request) ;
		return status ;
	}

#endif // WITH_CALL == 1

} ;    /* class BaseMethod */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASEMETHOD_H_ */
