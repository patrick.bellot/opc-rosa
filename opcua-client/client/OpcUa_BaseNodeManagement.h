
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASENODEMANAGEMENT_H_
#define CLIENT_OPCUA_BASENODEMANAGEMENT_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseQuery.h"

namespace opcua {

class BaseNodeManagement
	: public BaseQuery
{
public:

	BaseNodeManagement(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseQuery(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~BaseNodeManagement() {}



#if (WITH_NODEMNGT ==1)

protected:

	SOPC_StatusCode Connexion_AddNodes(
			AddNodesRequest   * rrequest,
			AddNodesResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseNodeManagement","Connexion_AddNodes begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_AddNodesRequest request ;
	    OpcUa_AddNodesRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseNodeManagement","Connexion_AddNodes failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseNodeManagement","Connexion_AddNodes: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_AddNodesRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseNodeManagement","Connexion_AddNodes: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseNodeManagement","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseNodeManagement","Connexion_AddNodes: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseNodeManagement","Connexion_AddNodes: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_AddNodesResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_AddNodes: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseNodeManagement","Connexion_AddNodes: answer is AddNodes") ;
	    *pRResponse = (AddNodesResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_AddNodesRequest_Clear(&request) ;
		return status ;
	}

protected:

	SOPC_StatusCode Connexion_AddReferences(
			AddReferencesRequest   * rrequest,
			AddReferencesResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseNodeManagement","Connexion_AddReferences begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_AddReferencesRequest request ;
	    OpcUa_AddReferencesRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseNodeManagement","Connexion_AddReferences failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseNodeManagement","Connexion_AddReferences: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_AddReferencesRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseNodeManagement","Connexion_AddReferences: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseNodeManagement","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseNodeManagement","Connexion_AddReferences: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseNodeManagement","Connexion_AddReferences: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_AddReferencesResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_AddReferences: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseNodeManagement","Connexion_AddReferences: answer is AddReferences") ;
	    *pRResponse = (AddReferencesResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_AddReferencesRequest_Clear(&request) ;
		return status ;
	}

#endif // WITH_NODEMNGT == 1

} ;    /* class BaseNodeManagement */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASENODEMANAGEMENT_H_ */
