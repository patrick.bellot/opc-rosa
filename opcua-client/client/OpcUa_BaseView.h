
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASEVIEW_H_
#define CLIENT_OPCUA_BASEVIEW_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseError.h"
#include "lib/s2opc/S2OPC_ClientServices.h"

namespace opcua {

class BaseView
	: public BaseError
{
public:

	BaseView(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseError(num, pScConfig, channel_config_idx, endpointUrl)
	{}

	virtual ~BaseView() {}

#if (WITH_BROWSE == 1)

protected: // INTERNAL BROWSE

	SOPC_StatusCode Connexion_Browse(
			BrowseRequest   * rrequest,
			BrowseResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseView","Connexion_Browse begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_BrowseRequest request ;
	    OpcUa_BrowseRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseView","Connexion_Browse failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseView","Connexion_Browse: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_BrowseRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseView","Connexion_Browse: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseView","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseView","Connexion_Browse: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseView","Connexion_Browse: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_BrowseResponse) {
	    	debug_iii(COM_ERR,"BaseView","Connexion_Browse: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseView","Connexion_Browse: answer is Browse") ;
	    *pRResponse = (BrowseResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_BrowseRequest_Clear(&request) ;
		return status ;
	}


#endif // BROWSE == 1



#if (WITH_REGISTER_UNREGISTER_NODES == 1)

protected: // INTERNAL REGISTERNODES

	SOPC_StatusCode Connexion_RegisterNodes(
			RegisterNodesRequest   * rrequest,
			RegisterNodesResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseView","Connexion_RegisterNodes begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_RegisterNodesRequest request ;
	    OpcUa_RegisterNodesRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseView","Connexion_RegisterNodes failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseView","Connexion_RegisterNodes: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_RegisterNodesRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseView","Connexion_RegisterNodes: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseView","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseView","Connexion_RegisterNodes: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseView","Connexion_RegisterNodes: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_RegisterNodesResponse) {
	    	debug_iii(COM_ERR,"BaseView","Connexion_RegisterNodes: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseView","Connexion_RegisterNodes: answer is RegisterNodes") ;
	    *pRResponse = (RegisterNodesResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_RegisterNodesRequest_Clear(&request) ;
		return status ;
	}

protected:

	SOPC_StatusCode Connexion_UnregisterNodes(
			UnregisterNodesRequest   * rrequest,
			UnregisterNodesResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseView","Connexion_UnregisterNodes begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_UnregisterNodesRequest request ;
	    OpcUa_UnregisterNodesRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseView","Connexion_UnregisterNodes failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseView","Connexion_UnregisterNodes: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_UnregisterNodesRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseView","Connexion_UnregisterNodes: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseView","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseView","Connexion_UnregisterNodes: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseView","Connexion_UnregisterNodes: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_UnregisterNodesResponse) {
	    	debug_iii(COM_ERR,"BaseView","Connexion_UnregisterNodes: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseView","Connexion_UnregisterNodes: answer is UnregisterNodes") ;
	    *pRResponse = (UnregisterNodesResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_UnregisterNodesRequest_Clear(&request) ;
		return status ;
}

#endif // WITH_REGISTER_UNREGISTER_NODES == 1


#if (WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS ==1)

protected:

	SOPC_StatusCode Connexion_TranslateBrowsePathsToNodeIds(
			TranslateBrowsePathsToNodeIdsRequest   * rrequest,
			TranslateBrowsePathsToNodeIdsResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseView","Connexion_TranslateBrowsePathsToNodeIds begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_TranslateBrowsePathsToNodeIdsRequest request ;
	    OpcUa_TranslateBrowsePathsToNodeIdsRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseView","Connexion_TranslateBrowsePathsToNodeIds failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_TranslateBrowsePathsToNodeIdsRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseView","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_TranslateBrowsePathsToNodeIdsResponse) {
	    	debug_iii(COM_ERR,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseView","Connexion_TranslateBrowsePathsToNodeIds: answer is TranslateBrowsePathsToNodeIds") ;
	    *pRResponse = (TranslateBrowsePathsToNodeIdsResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_TranslateBrowsePathsToNodeIdsRequest_Clear(&request) ;
		return status ;
}

#endif // WITH_TRANSLATE_BROWSEPATHS_TO_NODEIDS == 1

} ;    /* class BaseView */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASEVIEW_H_ */


