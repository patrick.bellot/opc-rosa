
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASEMONITOREDITEM_H_
#define CLIENT_OPCUA_BASEMONITOREDITEM_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseNodeManagement.h"

namespace opcua {

class BaseMonitoredItem
	: public BaseNodeManagement
{
public:

	BaseMonitoredItem(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseNodeManagement(num, pScConfig, channel_config_idx,endpointUrl)
	{}

	virtual ~BaseMonitoredItem() {}



#if (WITH_SUBSCRIPTION == 1)

protected:

	SOPC_StatusCode Connexion_CreateMonitoredItems(
			CreateMonitoredItemsRequest   * rrequest,
			CreateMonitoredItemsResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseMonitoredItem","Connexion_CreateMonitoredItems begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_CreateMonitoredItemsRequest request ;
	    OpcUa_CreateMonitoredItemsRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseMonitoredItem","Connexion_CreateMonitoredItems failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseMonitoredItem","Connexion_CreateMonitoredItems: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_CreateMonitoredItemsRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_CreateMonitoredItems: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseMonitoredItem","Connexion_CreateMonitoredItems: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseMonitoredItem","Connexion_CreateMonitoredItems: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_CreateMonitoredItemsResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_CreateMonitoredItems: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_CreateMonitoredItems: answer is CreateMonitoredItems") ;
	    *pRResponse = (CreateMonitoredItemsResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_CreateMonitoredItemsRequest_Clear(&request) ;
		return status ;
	}

protected:

	SOPC_StatusCode Connexion_DeleteMonitoredItems(
			DeleteMonitoredItemsRequest   * rrequest,
			DeleteMonitoredItemsResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseMonitoredItem","Connexion_DeleteMonitoredItems begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_DeleteMonitoredItemsRequest request ;
	    OpcUa_DeleteMonitoredItemsRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseMonitoredItem","Connexion_DeleteMonitoredItems failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseMonitoredItem","Connexion_DeleteMonitoredItems: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_DeleteMonitoredItemsRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_DeleteMonitoredItems: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseMonitoredItem","Connexion_DeleteMonitoredItems: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseMonitoredItem","Connexion_DeleteMonitoredItems: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_DeleteMonitoredItemsResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_DeleteMonitoredItems: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_DeleteMonitoredItems: answer is DeleteMonitoredItems") ;
	    *pRResponse = (DeleteMonitoredItemsResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_DeleteMonitoredItemsRequest_Clear(&request) ;
		return status ;
	}

protected:

	SOPC_StatusCode Connexion_ModifyMonitoredItems(
			ModifyMonitoredItemsRequest   * rrequest,
			ModifyMonitoredItemsResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseMonitoredItem","Connexion_ModifyMonitoredItems begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_ModifyMonitoredItemsRequest request ;
	    OpcUa_ModifyMonitoredItemsRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseMonitoredItem","Connexion_ModifyMonitoredItems failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseMonitoredItem","Connexion_ModifyMonitoredItems: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_ModifyMonitoredItemsRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_ModifyMonitoredItems: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseMonitoredItem","Connexion_ModifyMonitoredItems: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseMonitoredItem","Connexion_ModifyMonitoredItems: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_ModifyMonitoredItemsResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_ModifyMonitoredItems: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_ModifyMonitoredItems: answer is ModifyMonitoredItems") ;
	    *pRResponse = (ModifyMonitoredItemsResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_ModifyMonitoredItemsRequest_Clear(&request) ;
		return status ;
	}

protected:

	SOPC_StatusCode Connexion_SetMonitoringMode(
			SetMonitoringModeRequest   * rrequest,
			SetMonitoringModeResponse ** pRResponse
			)
	{
		debug(MAIN_LIFE_DBG,"BaseMonitoredItem","Connexion_SetMonitoringMode begin") ;

		SOPC_StatusCode status = STATUS_OK ;

		OpcUa_RequestHeader requestHeader ;
	    OpcUa_RequestHeader_Initialize(&requestHeader);

		OpcUa_SetMonitoringModeRequest request ;
	    OpcUa_SetMonitoringModeRequest_Initialize(&request);

		rrequest->fromCpptoC(&status,requestHeader,request) ;

	    int loopCpt = 0;
	    BaseDataType * obj = NULL ;

	    if(status != STATUS_OK) {
			debug_i(COM_ERR,"BaseMonitoredItem","Connexion_SetMonitoringMode failed (case 1): status=0x%08x",status) ;
		    goto out ;
		}

	    debug_ii(IPCS_DBG,"BaseMonitoredItem","Connexion_SetMonitoringMode: sending request with scId=%d ad requestId=%d...",channel_config_idx,num) ;

	    ClientService_SendRequest(
	    	    channel_config_idx,
	    		num,
				&OpcUa_SetMonitoringModeRequest_EncodeableType,
	    	    &requestHeader,
	    	    &request) ;

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_SetMonitoringMode: waiting for answer...") ;

	    while ((obj = responsesQueue->tryPop()) == NULL && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","After waiting for answer...") ;

	    if (obj==NULL || loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseMonitoredItem","Connexion_SetMonitoringMode: service failed with timeout, client=%d",num) ;
	    	status = SOPC_STATUS_TIMEOUT;
	        goto out ;
	    }

	    if (obj->getTypeId() == OpcUaId_ServiceFault) {
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	debug_ii(COM_ERR,"BaseMonitoredItem","Connexion_SetMonitoringMode: service failed with rc=0x%08x for client=%d",status,num) ;
	    	goto out ;
	    }

	    if (obj->getTypeId() != OpcUaId_SetMonitoringModeResponse) {
	    	debug_iii(COM_ERR,"BaseQuery","Connexion_SetMonitoringMode: bad Response answer with typeId=%d, rc=0x%08x for client=%d",obj->getTypeId(),status,num) ;
	    	ServiceFault * serviceFault = (ServiceFault *)obj ;
	    	status = serviceFault->getResponseHeader()->getServiceResult()->get() ;
	    	delete obj ;
	    	goto out ;
	    }

	    debug(IPCS_DBG,"BaseMonitoredItem","Connexion_SetMonitoringMode: answer is ModifyMonitoredItems") ;
	    *pRResponse = (SetMonitoringModeResponse *)obj ;

	out:
		OpcUa_RequestHeader_Clear(&requestHeader) ;
		OpcUa_ModifyMonitoredItemsRequest_Clear(&request) ;
		return status ;
	}

#endif // WITH_SUBSCRPTION==1

} ;    /* class BaseMonitoredItem */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASEMONITOREDITEM_H_ */
