
/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_OPCUA_BASESECURECHANNEL_H_
#define CLIENT_OPCUA_BASESECURECHANNEL_H_

#include "lib/OpcUa.h"
#include "OpcUa_BaseSession.h"

extern "C" {
		extern int endpointOpened ;
}

namespace opcua {

class BaseSecureChannel
	: public BaseSession
{
public:

	BaseSecureChannel(int num, SOPC_SecureChannel_Config * pScConfig, uint32_t channel_config_idx, String * endpointUrl)
		: BaseSession(num, pScConfig, channel_config_idx, endpointUrl)
	{}

	virtual ~BaseSecureChannel() {}

public: // OPEN SECURE CHANNEL

	SOPC_StatusCode openSecureChannel()
	{
		debug_i(COM_DBG,"BaseSecureChannel","openSecureChannel with channel_config_idx=%d",channel_config_idx) ;
		SOPC_SecureChannels_EnqueueEvent(SC_CONNECT, channel_config_idx, NULL, 0);

	    int loopCpt = 0;
	    while (endpointOpened == 0 && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    if (loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseSecureChannel","openSecureChannel failed with timeout, client=%d",num) ;
	    	return SOPC_STATUS_TIMEOUT;
	    }

		debug_i(COM_DBG,"BaseSecureChannel","openSecureChannel succeeds with channel_config_idx=%d",channel_config_idx) ;
		return STATUS_OK ;
	}

public: // CLOSE SECURECHANNEL

	SOPC_StatusCode closeSecureChannel()
	{
		SOPC_SecureChannels_EnqueueEvent(SC_DISCONNECT, channel_config_idx, NULL, 0);

	    int loopCpt = 0;
	    while (endpointOpened != 0 && loopCpt * sleepTimeout <= loopTimeout) {
	        loopCpt++;
	        SOPC_Sleep(sleepTimeout);
	    }

	    if (loopCpt * sleepTimeout > loopTimeout) {
	    	debug_i(COM_ERR,"BaseSecureChannel","closeSecureChannel failed with timeout, client=%d",num) ;
	    	return SOPC_STATUS_TIMEOUT;
	    }

		debug_i(COM_DBG,"BaseSecureChannel","closeSecureChannel succeeds with channel_config_idx=%d",channel_config_idx) ;
		return STATUS_OK ;
	}

} ;    /* class BaseSecureChannel */
}      /* namespace opcua */
#endif /* CLIENT_OPCUA_BASESECURECHANNEL_H_ */
