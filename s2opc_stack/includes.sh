#!/bin/bash

dir=$1

if [ -z "$dir" ] ; then
	echo 'Usage: ./includes.sh <dir>'
	echo 'Where is <dir> ?'
	exit 1 ;
fi

echo 'dir = ' $dir
echo -n 'continue (y/n) ? It must be done only one time.'

read r

case $r in 
y | Y)
	;;
n | N)
	echo 'Bye !' ; 
	exit 0 ;;
*) 
	echo 'I cannot understand ! Bye !' ; 
	exit 1 ;;
esac

rm -f include_linux/*.h
rm -f include_windows/*.h

cp -i $dir/csrc/api_toolkit/*.h include_linux/
cp -i $dir/csrc/api_toolkit/*.h include_windows/

cp -i $dir/csrc/configuration/*.h include_linux/
cp -i $dir/csrc/configuration/*.h include_windows/

cp -i $dir/csrc/crypto/*.h include_linux/
cp -i $dir/csrc/crypto/*.h include_windows/

cp -i $dir/csrc/crypto/mbedtls/*.h include_linux/
cp -i $dir/csrc/crypto/mbedtls/*.h include_windows/

cp -i $dir/csrc/helpers/*.h include_linux/
cp -i $dir/csrc/helpers/*.h include_windows/

cp -i $dir/csrc/helpers_platform_dep/*.h include_linux/
cp -i $dir/csrc/helpers_platform_dep/*.h include_windows/

cp -i $dir/csrc/helpers_platform_dep/linux/*.h include_linux/
cp -i $dir/csrc/helpers_platform_dep/windows/*.h include_windows/

cp -i $dir/csrc/opcua_types/*.h include_linux/
cp -i $dir/csrc/opcua_types/*.h include_windows/

cp -i $dir/csrc/secure_channels/*.h include_linux/
cp -i $dir/csrc/secure_channels/*.h include_windows/

cp -i $dir/csrc/services/*.h include_linux/
cp -i $dir/csrc/services/*.h include_windows/

cp -i $dir/csrc/services/b2c/*.h include_linux/
cp -i $dir/csrc/services/b2c/*.h include_windows/

cp -i $dir/csrc/services/bgenc/*.h include_linux/
cp -i $dir/csrc/services/bgenc/*.h include_windows/

cp -i $dir/csrc/sockets/*.h include_linux/
cp -i $dir/csrc/sockets/*.h include_windows/

cp -i $dir/install_linux/include/sopc_toolkit_build_info.h include_linux/
cp -i $dir/install_linux/include/sopc_toolkit_build_info.h include_windows/

echo 'Done !'

