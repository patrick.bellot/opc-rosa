/*
 *  Copyright (C) 2018 Systerel and others.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** \file
 *
 * \brief Defines the common declarations for the cryptographic objects.
 *
 * Avoids the circular dependencies.
 */

#ifndef SOPC_CRYPTO_DECL_H_
#define SOPC_CRYPTO_DECL_H_

typedef struct SOPC_CryptoProvider SOPC_CryptoProvider;
typedef struct SOPC_CryptoProfile SOPC_CryptoProfile;
typedef struct SOPC_CryptolibContext SOPC_CryptolibContext;
typedef struct SOPC_AsymmetricKey SOPC_AsymmetricKey;
typedef struct SOPC_Certificate SOPC_Certificate;
typedef struct SOPC_PKIProvider SOPC_PKIProvider;

#endif /* SOPC_CRYPTO_DECL_H_ */
