/*
 * This file is part of OPC-ROSA.
 *
 * OPC-ROSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OPC-ROSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OPC-ROSA.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lib/OpcUa.h"
#include "lib/StandardDataTypes/All.h"
#include "lib/CommonParametersTypes/All.h"
#include "lib/Utils/OpcUa_IPCS_Table.h"

extern "C" {
namespace opcua {

int32_t method(NodeId                 * objectId,
               NodeId                 * methodId,
               TableVariant           * inputArguments,
               StatusCode            ** statusCode,
               TableStatusCode       ** inputArgumentResults,
               TableDiagnosticInfo   ** inputArgumentDiagnosticInfos,
               TableVariant          ** outputArguments,
               DiagnosticInfo        ** diagnosticInfo)
{
	int length = inputArguments->getLength() ;

	if (length != 2) {
		*statusCode      = StatusCode::Bad_TypeMismatch ;
		return 0 ;
	}

	*statusCode      = StatusCode::Good ;

	*inputArgumentResults         = new TableStatusCode  (length) ;
	*inputArgumentDiagnosticInfos = new TableDiagnosticInfo  (0) ;

	for (int32_t i = 0 ; i < length ; i++) {

		Variant      * arg = inputArguments->get(i) ;
		BaseDataType * val = arg->getValue() ;
		int32_t        len = val->getLength() ;
		uint8_t        bit = arg->getBuiltinTypeId() ;

		if (len == -1 && bit == Builtin_Int32) {
			(*inputArgumentResults)->set(i,StatusCode::Good) ;
		} else {
			(*inputArgumentResults)->set(i,StatusCode::Bad_TypeMismatch) ;
		}
	}

	*outputArguments = new TableVariant  (2) ;

	for (int i = 0 ; i < 2 ; i++)
		(*outputArguments)->set(i,new Variant(Builtin_Int32,new Int32(i))) ;

	*diagnosticInfo  = DiagnosticInfo::fakeDiagnosticInfo ;

	return (int32_t)0 ;
}


}
}
